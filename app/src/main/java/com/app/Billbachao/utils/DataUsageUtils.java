package com.app.Billbachao.utils;

/**
 * Created by mihir on 06-09-2015.
 */
public class DataUsageUtils {

    final static int CONVERTER_GB_MB = (int) Math.pow(2, 10);

    public final static int CONVERTER_MB_BYTES = (int) Math.pow(2, 20);

    final static int[] USAGE_VALUES = new int[]{300, 500, CONVERTER_GB_MB, 2 * CONVERTER_GB_MB, 3 * CONVERTER_GB_MB, 5 * CONVERTER_GB_MB};

    public final static String[] USAGE_TEXT = new String[]{"300 MB", "500 MB", "1 GB", "2 GB", "3 GB", "5 GB"};

    // Number of days after install till which user will be given choice of changing data
    public static final int DAYS_COUNT_DATA_BAR = 7;

    public static final int DATA_LIMIT_PREPAID_MB = (int) (2.2 * Math.pow(2, 10));

    public static final int DATA_LIMIT_POSTPAID_MB = (int) (3.2 * Math.pow(2, 10));

    public static String getDataUsageText(long dataUsageMb) {
        if (dataUsageMb >= CONVERTER_GB_MB) {
            return String.format("%.2f", (float) dataUsageMb / CONVERTER_GB_MB) + " GB";
        } else {
            return String.format("%d", dataUsageMb) + " MB";
        }
    }

    public static int findBestMatch(long dataUsageMB) {
        int match = 0;
        long diff = Integer.MAX_VALUE;
        for (int index = 0; index < USAGE_VALUES.length; index++) {
            if (Math.abs(USAGE_VALUES[index] - dataUsageMB) < diff) {
                diff = Math.abs(USAGE_VALUES[index] - dataUsageMB);
                match = index;
            }
        }
        return match;
    }

    public static long getDataUsageValue(int index) {
        return USAGE_VALUES[index];
    }
}
