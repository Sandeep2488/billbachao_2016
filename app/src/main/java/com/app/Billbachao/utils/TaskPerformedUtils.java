package com.app.Billbachao.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by BB-001 on 5/9/2016.
 */
public class TaskPerformedUtils {


    static final String PREFERENCE_FILE = "TaskPreferences";

    public static final int TASK_PUSH_RECHARGE_SMS_DATA = 1;

    public static final String PUSH_RECHARGE_SMS = "push_recharge_sms";

    static final String TIME_SUFFIX = "_time", PENDING = "_pending";

    public static String getPendingPreference(int taskType) {
        return getPrefKey(taskType) + PENDING;
    }

    public static String getTimePreference(int taskType) {
        return getPrefKey(taskType) + TIME_SUFFIX;
    }

    public static String getPrefKey(int taskType) {
        switch (taskType) {
            case TASK_PUSH_RECHARGE_SMS_DATA:
                return PUSH_RECHARGE_SMS;
        }
        return "";
    }

    public static void taskPerformed(Context context, int taskType) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCE_FILE, Context.MODE_PRIVATE);
        preferences.edit().putLong(getTimePreference(taskType), System.currentTimeMillis()).putBoolean(getPendingPreference(taskType), true).apply();
    }

    public static void updateTaskPending(Context context, int taskType) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCE_FILE, Context.MODE_PRIVATE);
        preferences.edit().putBoolean(getPendingPreference(taskType), false).apply();
    }

    public static boolean isTaskPending(Context context, int taskType) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCE_FILE, Context.MODE_PRIVATE);
        return !preferences.getBoolean(getPendingPreference(taskType), false);
    }

    public static long getLastPerformedTime(Context context, int taskType) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCE_FILE, Context.MODE_PRIVATE);
        return preferences.getLong(getTimePreference(taskType), 0);
    }
}
