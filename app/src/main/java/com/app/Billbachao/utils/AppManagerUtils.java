package com.app.Billbachao.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.TrafficStats;

import com.app.Billbachao.model.PackageDataInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by mihir on 24-08-2015.
 */
public class AppManagerUtils {

    public static final int APPS_DATA_LIST_COUNT = 4;

    public static PackageInfo getPackageInfo(Context context, String packageName) {
        PackageManager pm = context.getPackageManager();
        PackageInfo info = null;
        try {
            info = pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return info;
    }

    public static Drawable getIcon(Context context, String packageName) {
        PackageManager packageManager = context.getPackageManager();

        try {
            Drawable drawable = packageManager.getApplicationIcon(packageName);
            return drawable;
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    // Default call for data usage packages list
    public static List<PackageDataInfo> getPackageWiseDataUsage(Context context) {
        return getPackageWiseDataUsage(context, APPS_DATA_LIST_COUNT);
    }

    // Call for data usage packages list, with varying count
    public static List<PackageDataInfo> getPackageWiseDataUsage(Context context, int maxApps) {
        List<PackageDataInfo> dataInfoList = new ArrayList<PackageDataInfo>();
        if (context == null) {
            return dataInfoList;
        }
        PackageManager pm = context.getPackageManager();
        String currentPackage = context.getPackageName();
        for (ApplicationInfo info : pm.getInstalledApplications(0)) {

            String name = info.loadLabel(pm).toString();
            if (info.packageName.equalsIgnoreCase(currentPackage)) {
                continue;
            }
            int uid = info.uid;
            long data = TrafficStats.getUidRxBytes(uid) + TrafficStats.getUidTxBytes(uid);
            if (data == 0) {
                continue;
            }
            String packageName = info.packageName;
            Drawable drawable = info.loadIcon(pm);
            PackageDataInfo dataInfo = new PackageDataInfo(uid, name, packageName, drawable, data);
            dataInfoList.add(dataInfo);
        }

        Collections.sort(dataInfoList, new Comparator<PackageDataInfo>() {
            @Override
            public int compare(PackageDataInfo lhs, PackageDataInfo rhs) {
                return Double.compare(rhs.getData(), lhs.getData());
            }
        });

        if(dataInfoList.size() > maxApps) {
            dataInfoList = dataInfoList.subList(0, maxApps);
        }

        return dataInfoList;
    }
}
