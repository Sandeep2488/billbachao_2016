package com.app.Billbachao.utils;

import android.content.Context;
import android.telephony.TelephonyManager;

import com.app.Billbachao.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by mihir on 17-08-2015.
 */
public class OperatorUtils {


    private static final ArrayList<OperatorMap> sOperatorMap = new ArrayList<>(),
            sPostpaidOperatorMap = new ArrayList<>();

    public static String[] sOperatorNamesSorted, sOperatorIdSorted, sPostpaidOperatorNamesSorted,
            sPostpaidOperatorIdSorted;

    // List added for future use.
    public static String[] sOperatorId, sOperatorNames;

    public static final String OPERATOR_VODAFONE = "1", OPERATOR_AIRTEL = "2", OPERATOR_AIRCEL =
            "3", OPERATOR_IDEA = "4", OPERATOR_RELIANCE = "5", OPERATOR_BSNL = "6",
            OPERATOR_TELENOR = "8", OPERATOR_TATA = "7", OPERATOR_VIDEOCON = "10";

    private static String[] sOperatorNamesPostpaid, sOperatorPostpaidId;


    public static void init(Context context) {
        formPrepaidOperatorMapping(context);
        formPostpaidOperatorMapping(context);
    }

    public static String getOperatorDetails(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context
                .TELEPHONY_SERVICE);
        return telephonyManager.getNetworkOperatorName();
    }

    public static String getOperatorNameFromId(String id) {
        int index = Arrays.asList(sOperatorId).indexOf(id);
        if (index == -1) {
            return PreferenceUtils.NA;
        }
        return sOperatorNames[index];
    }

    public static String getOperatorName(Context context) {
        String id = PreferenceUtils.getOperatorId(context);
        return getOperatorNameFromId(id);
    }

    public static int getOperatorIndex(String id) {
        return Arrays.asList(sOperatorId).indexOf(id);
    }

    public static int getOperatorIndexForSorted(String id) {
        return Arrays.asList(sOperatorIdSorted).indexOf(id);
    }

    public static String getOperatorIdFromName(String name) {
        // TODO added try catch only to confirm information obtained from server through
        // crashlytics logging. Should be removed in next release
        try {
            int index = Arrays.asList(sOperatorNames).indexOf(name);
            return sOperatorId[index];
        } catch (ArrayIndexOutOfBoundsException ex) {
            ex.printStackTrace();
        }
        return PreferenceUtils.NA_ID;
    }

    public static boolean isOperatorMatchFound(String name) {
        return (Arrays.asList(sOperatorNames).indexOf(name) >= 0);
    }

    public static boolean isOperatorIdMatchFound(String id) {
        return (Arrays.asList(sOperatorId).indexOf(id) >= 0);
    }

    public static boolean isOperatorIdPostpaidMatchFound(String id) {
        return (Arrays.asList(sOperatorPostpaidId).indexOf(id) >= 0);
    }

    public static String getOperatorIdFromSortedNames(String name) {
        int index = Arrays.asList(sOperatorNamesSorted).indexOf(name);
        return sOperatorMap.get(index).getId();
    }

    public static String getOperatorIdFromSortedPosition(int position) {
        return sOperatorMap.get(position).getId();
    }

    public static String getPostpaidOperatorIdFromSortedPosition(int position) {
        return sPostpaidOperatorMap.get(position).getId();
    }

    public static int getPostpaidOperatorIndexForSorted(String id) {
        return Arrays.asList(sPostpaidOperatorIdSorted).indexOf(id);
    }

    private static void formPrepaidOperatorMapping(Context context) {
        sOperatorNames = context.getResources().getStringArray(R.array.prepaid_operators);
        sOperatorId = context.getResources().getStringArray(R.array.prepaid_operators_id);

        String tempArrays[] = Arrays.copyOf(sOperatorNames, sOperatorNames.length);
        Arrays.sort(tempArrays);
        sOperatorNamesSorted = tempArrays;

        for (int index = 0; index < Math.min(sOperatorNames.length, sOperatorId.length); index++) {
            OperatorMap map = new OperatorMap(sOperatorId[index], sOperatorNames[index]);
            sOperatorMap.add(map);
        }

        Collections.sort(sOperatorMap, new Comparator<OperatorMap>() {
            @Override
            public int compare(OperatorMap lhs, OperatorMap rhs) {
                return lhs.getName().compareTo(rhs.getName());
            }
        });

        sOperatorIdSorted = new String[sOperatorId.length];
        int index = 0;
        for (OperatorMap map : sOperatorMap) {
            sOperatorIdSorted[index++] = map.getId();
        }
    }

    private static void formPostpaidOperatorMapping(Context context) {
        sOperatorNamesPostpaid = context.getResources().getStringArray(R.array.postpaid_operators);
        sOperatorPostpaidId = context.getResources().getStringArray(R.array.postpaid_operators_id);

        String tempArrays[] = Arrays.copyOf(sOperatorNamesPostpaid, sOperatorNamesPostpaid.length);
        Arrays.sort(tempArrays);
        sPostpaidOperatorNamesSorted = tempArrays;

        for (int index = 0; index < Math.min(sOperatorNamesPostpaid.length, sOperatorPostpaidId
                .length); index++) {
            OperatorMap map = new OperatorMap(sOperatorPostpaidId[index],
                    sOperatorNamesPostpaid[index]);
            sPostpaidOperatorMap.add(map);
        }

        Collections.sort(sPostpaidOperatorMap, new Comparator<OperatorMap>() {
            @Override
            public int compare(OperatorMap lhs, OperatorMap rhs) {
                return lhs.getName().compareTo(rhs.getName());
            }
        });

        sPostpaidOperatorIdSorted = new String[sOperatorPostpaidId.length];
        int index = 0;
        for (OperatorMap map : sPostpaidOperatorMap) {
            sPostpaidOperatorIdSorted[index++] = map.getId();
        }
    }

    static class OperatorMap {
        String id;
        String name;

        public OperatorMap(String id, String name) {
            this.id = id;
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }
}
