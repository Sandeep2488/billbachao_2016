package com.app.Billbachao.utils;

/**
 * Created by BB-001 on 3/29/2016.
 */
public class TextUtils {
    public static final String NA = "NA";

    public static String getFilteredValue(String value) {
        if (value == null || value.equalsIgnoreCase("null") || value.equals(""))
            return NA;
        else
            return value;
    }
}
