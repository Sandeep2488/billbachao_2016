package com.app.Billbachao.utils;

import android.content.Context;

import com.app.Billbachao.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.Locale;

/**
 * Created by mihir on 23-09-2015.
 * Utils class for common functions like date and time parsing
 */
public class DateUtils {

    static final String FORMAT_HH_MM = "HH:mm", FORMAT_HH = "HH";

    public static final String FORMAT_DATE_ONLY = "yyyyMMdd", DATE_FORMAT = "dd MMM | hh:mm a", FORMAT_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd hh:mm:ss", FORMAT_YYYY_MM_DD = "yyyy-MM-dd", FORMAT_DD_MMM = "MMM dd";

    private static StringBuilder sFormatBuilder = new StringBuilder();

    private static Formatter sFormatter = new Formatter(sFormatBuilder, Locale.getDefault());

    private static final Object[] sTimeArgs = new Object[5];

    public static Date parseDateFromHourMinutes(String dateString) {
        return parseDateFormat(dateString, FORMAT_HH_MM);
    }

    public static Date parseDateFromHour(String dateString) {
        return parseDateFormat(dateString, FORMAT_HH);
    }

    public static Date parseDateFormat(String dateString, String format) {
        try {
            return new SimpleDateFormat(format, Locale.ENGLISH).parse(dateString);
        } catch (ParseException e) {
            return new Date(0);
        }
    }

    static String extractTimeStringFromDate(long timeInMillis) {
        Date fullDate = new Date(timeInMillis);
        return new SimpleDateFormat(FORMAT_HH_MM, Locale.ENGLISH).format(fullDate);
    }

    public static String convertTimeToDate(long timeInMillis, String dateFormat) {
        Date fullDate = new Date(timeInMillis);
        return new SimpleDateFormat(dateFormat, Locale.ENGLISH).format(fullDate);
    }

    // Function for Usagehelper Night calls/sms
    public static Date extractTimeFromDate(long timeInMillis) {
        return parseDateFromHourMinutes(extractTimeStringFromDate(timeInMillis));
    }

    // Get date in format of "yyyyMMddHH24mm".

    public static String convertIntoDesiredDateFormat(long milliSeconds) {
        DateFormat formatter = new SimpleDateFormat("yyyyMMddHH24mm");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static String getDurationString(Context context, long seconds) {
        String durationFormat = context.getString(seconds < 3600 ? R.string.duration_format_short
                : R.string.duration_format_long);
        sFormatBuilder.setLength(0);

        final Object[] timeArgs = sTimeArgs;
        timeArgs[0] = seconds / 3600;
        timeArgs[1] = seconds / 60;
        timeArgs[2] = (seconds / 60) % 60;
        timeArgs[3] = seconds;
        timeArgs[4] = seconds % 60;

        return sFormatter.format(durationFormat, timeArgs).toString();

    }

    public static String getDayString(String dateString, String formatString) {
        Date date = parseDateFormat(dateString, formatString);

        int diff = (int) (Math.max(System.currentTimeMillis() - date.getTime(), 0) / 86400000);

        switch (diff) {
            case 0:
                return "TODAY";
            case 1:
                return "YESTERDAY";
            default:
                return "" + diff + " days ago";
        }
    }

    public static long getBeforeDays(int days) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.add(Calendar.DATE, -days);
        return cal.getTimeInMillis();
    }

    public static String convertDateFormat(String dateString, String fromFormat, String toFormat) {
        SimpleDateFormat format = new SimpleDateFormat(fromFormat);
        try {
            Date date = format.parse(dateString);
            return new SimpleDateFormat(toFormat).format(date).toUpperCase();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static long getMillisFromDate(String dateString, String format) {
        return parseDateFormat(dateString, format).getTime();
    }

    public static int getDaysBetween(long timeMillis) {
        return (int) ((System.currentTimeMillis() - timeMillis) / 86400000);

    }

    public static boolean isToday(long timeMillis) {
        return DateUtils.getBeforeDays(0) < timeMillis;
    }

    public static String customDateFormat(String desireFormat) {
        return new SimpleDateFormat(desireFormat, Locale.ENGLISH).format(new Date());
    }
}