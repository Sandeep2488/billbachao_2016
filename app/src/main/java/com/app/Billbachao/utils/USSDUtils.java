package com.app.Billbachao.utils;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.telephony.SmsManager;
import android.widget.Toast;

import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by mihir on 26-08-2015.
 */
public class USSDUtils {

    final static String TAG = USSDUtils.class.getSimpleName();

    public final static int CHECK_BALANCE = 1, DEACTIVATE_VAS = 2, DND = 3, CHECK_BILL = 4,
            CALL_CENTER = 5, DATA_SETTINGS = 6, GET_3G = 7, DATA_BALANCE = 8;

    // KEY part 1 for various requests
    final static String BALANCE = "CHKBAL", BILL = "CHKBILL", GET_DATA = "GET_DATA", CALL_CC =
            "CALL_CC", ACT_DND = "ACT_DND", DEACT_VAS = "DEACT_VAS", SETTINGS_3G = "GET_3G",
            DATA_BALANCE_KEY = "DATA_BALANCE";

    public static void callNumber(Context context, String number) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) !=
                PackageManager.PERMISSION_GRANTED) {
            ((BaseActivity) context).showSnack(R.string.no_call_permissions);
            return;
        }
        try {
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + Uri.encode
                    (number)));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void sendMessage(Context context, String number, String msg) {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(number, null, msg, null, null);
        Toast.makeText(context, R.string.sms_request_sent, Toast.LENGTH_SHORT).show();
    }

    public static String getMainKey(int requestType) {
        switch (requestType) {
            case CHECK_BALANCE:
                return BALANCE;
            case DEACTIVATE_VAS:
                return DEACT_VAS;
            case CALL_CENTER:
                return CALL_CC;
            case DATA_SETTINGS:
                return GET_DATA;
            case DND:
                return ACT_DND;
            case CHECK_BILL:
                return BILL;
            case GET_3G:
                return SETTINGS_3G;
            case DATA_BALANCE:
                return DATA_BALANCE_KEY;
            // Prepaid
            case LAST_RECHARGE:
                return LAST_RECHARGED;
            case MY_PLAN:
                return PLAN;
            // Postpaid
            case BILL_SUMMARY:
                return BILL_SUMMARY_KEY;
            case ITEMISED_BILL:
                return ITEMISED_BILL_KEY;
            case UNBILLED_AMOUNT:
                return UNBILLED_AMOUNT_KEY;
            case LAST_PAYMENT:
                return LAST_PAYMENT_KEY;
            case LOSS_OF_SIM:
                return LOSS_OF_SIM_KEY;
        }
        return null;
    }

    // Different Modes for requests
    static final String SMS = "SMS", USSD = "USSD", DIAL = "DIAL";

    static final int TYPE_SMS = 1, TYPE_DIAL = 2, TYPE_USSD = 0, TYPE_INVALID = -1;
    static final int[] TYPE_LIST = new int[]{TYPE_SMS, TYPE_USSD, TYPE_DIAL};

    public static String getModeKey(int mode) {
        switch (mode) {
            case TYPE_DIAL:
                return DIAL;
            case TYPE_SMS:
                return SMS;
            case TYPE_USSD:
                return USSD;
        }
        return null;
    }

    // Additional key substrings for processing USSD, DIAL, SMS;
    public static final String M_NO_TO = "mNoTo", SMS_BODY = "Message";

    public static void entertainRequest(Context context, int type) {
        String typeKey = getMainKey(type);

        int mode = getRequestMode(context, type);
        if (mode == TYPE_INVALID) {
            return;
        }

        SharedPreferences preferences = context.getSharedPreferences(USSD_HELP_PREFERENCES,
                Context.MODE_PRIVATE);
        String key = typeKey + getModeKey(mode);
        String number = preferences.getString(key + M_NO_TO, "");
        ILog.d(TAG, "Number preference " + number);
        switch (mode) {
            case TYPE_USSD:
            case TYPE_DIAL:
                callNumber(context, number);
                break;
            case TYPE_SMS:
                String msg = preferences.getString(key + SMS_BODY, "");
                sendMessage(context, number, msg);
        }
    }

    public static boolean isSupported(Context context, int type) {
        return (getRequestMode(context, type) != TYPE_INVALID);
    }

    public static int getRequestMode(Context context, int type) {
        String typeKey = getMainKey(type);

        SharedPreferences preferences = context.getSharedPreferences(USSD_HELP_PREFERENCES,
                Context.MODE_PRIVATE);

        String key = "";
        for (int requestMode : TYPE_LIST) {
            key = typeKey + getModeKey(requestMode);
            if (preferences.contains(key) && preferences.getString(key, "Y").equalsIgnoreCase
                    ("Y")) {
                return requestMode;
            }
        }
        return TYPE_INVALID;
    }

    public static void requestMnp(Context context) {
        String number = "1900";
        String message = "PORT " + PreferenceUtils.getMobileNumber(context);
        sendMessage(context, number, message);
    }


    // Storage part
    public static void storeSelfHelpDetails(Context context, String dataString) {
        final String USSD_DETAILS = "ussdDtls", TYPE = "fType", MSG_BODY = "mBody", MODE = "mode";

        try {
            JSONObject dataObject = new JSONObject(dataString);
            JSONArray selfHelpArray = dataObject.getJSONArray(USSD_DETAILS);

            SharedPreferences preferences = context.getSharedPreferences(USSD_HELP_PREFERENCES,
                    Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();

            editor.clear();

            for (int index = 0; index < selfHelpArray.length(); index++) {
                JSONObject selfHelpObject = selfHelpArray.getJSONObject(index);
                try {
                    String typeString = selfHelpObject.getString(TYPE);
                    String modeString = selfHelpObject.getString(MODE);
                    String key = typeString + modeString;

                    editor.putString(key, "Y");
                    editor.putString(key + M_NO_TO, selfHelpObject.getString(M_NO_TO));
                    editor.putString(key + SMS_BODY, selfHelpObject.getString(MSG_BODY));

                } catch (JSONException ex) {
                    ex.printStackTrace();
                    continue;
                }
            }
            editor.apply();
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }


    // Extra preferences for Prepaid
    public final static int LAST_RECHARGE = 21, MY_PLAN = 22;

    final static String LAST_RECHARGED = "LAST_RECHARGE", PLAN = "MY_PLAN";

    // Extra preferences for Postpaid
    public final static int BILL_SUMMARY = 31, ITEMISED_BILL = 32, UNBILLED_AMOUNT = 33,
            LAST_PAYMENT = 34, LOSS_OF_SIM = 35;

    final static String BILL_SUMMARY_KEY = "BILL_SUMMARY", ITEMISED_BILL_KEY = "ITEMISED_BILL",
            UNBILLED_AMOUNT_KEY = "UNBILLED_AMOUNT", LAST_PAYMENT_KEY = "LAST_PAYMENT",
            LOSS_OF_SIM_KEY = "LOSS_OF_SIM";


    // Shift USSD list to separate file, so that it can be cleared on editprofile

    private static final String USSD_HELP_PREFERENCES = "UssdHelpPreferences";

    public static void shiftPreferences(Context context) {
        final int[] LIST_ORIGINAL_USSDs = new int[]{CHECK_BALANCE, DEACTIVATE_VAS, DND,
                CHECK_BILL, CALL_CENTER, DATA_SETTINGS, GET_3G};

        final String preferenceShifted = "prefsShifted";

        SharedPreferences originalPreferences = context.getSharedPreferences(PreferenceUtils
                .ACTIVITY_PREFERENCE, Context.MODE_PRIVATE);

        if (originalPreferences.getBoolean(preferenceShifted, false)) {
            return;
        }

        SharedPreferences newPreferences = context.getSharedPreferences(USSD_HELP_PREFERENCES,
                Context
                .MODE_PRIVATE);
        SharedPreferences.Editor newPrefEditor = newPreferences.edit(), originalPrefEditor =
                originalPreferences.edit();

        ArrayList<String> prefList = new ArrayList<String>();

        for (int requestType : LIST_ORIGINAL_USSDs) {
            String mainKey = getMainKey(requestType);

            int mode = TYPE_INVALID;
            String key = "";
            for (int requestMode : TYPE_LIST) {
                key = mainKey + getModeKey(requestMode);
                if (originalPreferences.contains(key) && originalPreferences.getString(key, "Y")
                        .equalsIgnoreCase
                                ("Y")) {
                    mode = requestMode;
                    break;
                }
            }

            if (mode == TYPE_INVALID) {
                continue;
            }

            prefList.add(key);
            prefList.add(key + M_NO_TO);
            prefList.add(key + SMS_BODY);
        }

        for (String prefString : prefList) {
            if (originalPreferences.contains(prefString)) {
                String value = originalPreferences.getString(prefString, "");
                newPrefEditor.putString(prefString, value);
                originalPrefEditor.remove(prefString);
            }
        }

        // shift done - update flag
        originalPrefEditor.putBoolean(preferenceShifted, true);
    }

    public static void setPlanAlert(Context context, boolean value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(PreferenceUtils
                .PLAN_ALERTS, value).apply();
    }
}
