package com.app.Billbachao.utils;

/**
 * File should be used for enabling/dsiabling various features to control the flow of app
 * Created by mihir on 02-12-2015.
 */
public class Features {

    public static final boolean FLAG_FEEDBACK_REDIRECT_MAIL = false;

    public static final boolean RECEIVE_BILL_DUE_MSG = false;
}
