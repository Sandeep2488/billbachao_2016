package com.app.Billbachao.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Caching mechanism for various API results
 * Created by mihir on 11-01-2016.
 */
public class ApiDataCacheUtils {

    // API_TYPES
    public static final int BEST_PLAN_API = 1, BEST_NETWORK_API = 2, APP_RECOMMEND_API = 3,
            CARDS_API = 4;

    // Preference keys
    public static final String PREF_BEST_PLAN = "best_plan", PREF_BEST_NETWORK = "best_network",
            PREF_APP_RECOMMEND = "app_recommend", CARDS_PREF = "cards_pref";

    public static final String PREFERENCES = "ApiPreferences";

    public static void storeApi(Context context, int apiType, String apiData) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES, Context.MODE_MULTI_PROCESS);
        preferences.edit().putString(getPrefForApi(apiType), apiData).putLong(getTimePrefForApi
                (apiType), System.currentTimeMillis()).apply();
    }

    public static long getTime(Context context, int type) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES, Context.MODE_MULTI_PROCESS);
        return preferences.getLong(getTimePrefForApi(type), 0);
    }

    public static boolean isApiCached(Context context, int apiType) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES, Context.MODE_MULTI_PROCESS);
        return preferences.contains(getPrefForApi(apiType));
    }

    public static String getApi(Context context, int apiType) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES, Context.MODE_MULTI_PROCESS);
        return preferences.getString(getPrefForApi(apiType), "NA");
    }

    public static void removeApi(Context context, int apiType) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES, Context.MODE_MULTI_PROCESS);
        preferences.edit().remove(getPrefForApi(apiType)).remove(getTimePrefForApi(apiType))
                .commit();
    }

    private static String getPrefForApi(int apiType) {
        switch (apiType) {
            case BEST_PLAN_API:
                return PREF_BEST_PLAN;
            case BEST_NETWORK_API:
                return PREF_BEST_NETWORK;
            case APP_RECOMMEND_API:
                return PREF_APP_RECOMMEND;
            case CARDS_API:
                return CARDS_PREF;
        }
        return "NA";
    }

    private static String getTimePrefForApi(int apiType) {
        return getPrefForApi(apiType) + "_time";
    }
}
