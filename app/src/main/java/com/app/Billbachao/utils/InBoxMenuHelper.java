package com.app.Billbachao.utils;

import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.notifications.gcm.db.NotificationDataSource;

/**
 * CommonMenuHelper for DashBoardActivity
 * Created by nitesh on 26-04-2016.
 */
public class InBoxMenuHelper {

    Activity mActivity;

    public InBoxMenuHelper(Activity activity) {
        mActivity = activity;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        mActivity.getMenuInflater().inflate(R.menu.inbox_menu, menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        View view = menu.findItem(R.id.menu_inbox_notification).getActionView();
        TextView inBoxItems = (TextView) view.findViewById(R.id.menu_inbox_count);

        int inBoxCount = new NotificationDataSource(mActivity).readUnReadCount();

        inBoxItems.setVisibility(inBoxCount > 0 ? View.VISIBLE : View.GONE);
        inBoxItems.setText(String.valueOf(inBoxCount));
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchNotificationInBox();
            }
        });

        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_inbox_notification:
                launchNotificationInBox();
                return true;
        }
        return false;
    }

    private void launchNotificationInBox() {
        UiUtils.launchNotificationInBox(mActivity);
    }
}
