package com.app.Billbachao.utils;

import android.content.Context;
import android.widget.TextView;

import com.app.Billbachao.database.MobileOperatorDBHelper;
import com.app.Billbachao.model.OperatorCircleInfo;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by mihir on 21-08-2015.
 */
public class NumberUtils {

    public static final int INDIAN_COUNTRY_CODE = 91;

    public static final int UNKNOWN = 0, YES = 1, NO = 2;

    public static String parseNumber(String number) {
        number = number.replaceAll("[- ]", "").trim();
        if (number.length() < 10 || isNumberInternational(number))
            return null;
        number = number.substring(number.length() - 10, number.length());
        if (number.startsWith("7") || number.startsWith("8") || number.startsWith("9")) {
            return number;
        }
        return null;
    }

    public static boolean isValidNumber(String number) {
        return isValidNumber(number, false);
    }

    public static boolean isValidNumber(String number, boolean incomplete) {
        if (number.length() > 10 || number.length() <= 0)
            return false;
        if (!incomplete && number.length() < 10)
            return false;
        return number.startsWith("7") || number.startsWith("8") || number.startsWith("9");
    }

    /**
     * Check if number is international
     */
    public static boolean isNumberInternational(String number) {
        PhoneNumberUtil numberUtil = PhoneNumberUtil.getInstance();
        try {
            Phonenumber.PhoneNumber indianNumberProto = numberUtil.parse(number, "IN");
            int countyCode = indianNumberProto.getCountryCode();
            return countyCode != INDIAN_COUNTRY_CODE;
        } catch (NumberParseException ex) {
            ex.printStackTrace();
        }
        return true;
    }

    public static int checkIfMobileNumber(String number) {
        String MOBILE_NO_REGEX = "^[789]\\d{9}$";
        String LANDLINE_NO_REGEX = "^[0-9]\\d{2,4}\\d{6,8}$";

        if (number == null)
            return NO;

        Pattern mobPattern = Pattern.compile(MOBILE_NO_REGEX);
        Matcher mobMatcher = mobPattern.matcher(number);

        Pattern linePattern = Pattern.compile(LANDLINE_NO_REGEX);
        Matcher lineMatcher = linePattern.matcher(number);

        if (mobMatcher.find())
            return YES;
        else if (lineMatcher.find())
            return NO;

        return UNKNOWN;
    }

    /**
     * Removes country code or preceding 0's and spaces
     *
     * @param number
     * @return
     */
    public static String processNumber(String number) {
        if (number != null) {
            if (number.charAt(0) == '0')
                number = number.replaceFirst("0", "");
            else
                number = removeCountryCode(number);

            number = number.trim();
        }
        return number;
    }

    public static String removeCountryCode(String number) {
        if (number != null) {
            if (number.contains("+91"))
                number = number.replaceFirst("\\+91", "").trim();
        }
        return number;
    }

    public static boolean isNumber(String text) {
        try {
            Integer.parseInt(text);
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Get operator circle info from number
     * @param context
     * @param number
     * @return
     */
    public static OperatorCircleInfo getOperatorCircleInfo(Context context, String number) {
        return MobileOperatorDBHelper.getInstance(context).getOperatorCircleInfo(number);
    }

    /**
     * Get text from a textview, parse it in int and check if valid number
     * Used for amount text in recharge screen
     * @param amountText
     * @return Amount integer value
     */
    public static int getAmountEntered(TextView amountText) {
        if (amountText != null) {
            String text = amountText.getText().toString();
            if (!text.isEmpty()) {
                return Integer.parseInt(amountText.getText().toString());
            }
        }
        return -1;
    }
}
