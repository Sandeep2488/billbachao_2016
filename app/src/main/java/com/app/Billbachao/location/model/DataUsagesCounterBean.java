package com.app.Billbachao.location.model;


public class DataUsagesCounterBean {
    private long txBytes;
    private long dataInDay;
    private String isRecharged;
    private String recordedDate;
    private double totalDataMBUsedInMonth;
    private long totalDaysDataUsed;

    public long getTxBytes() {
        return txBytes;
    }

    public void setTxBytes(long txBytes) {
        this.txBytes = txBytes;
    }

    public long getDataInDay() {
        return dataInDay;
    }

    public void setDataInDay(long dataInDay) {
        this.dataInDay = dataInDay;
    }

    public String getIsRecharged() {
        return isRecharged;
    }

    public void setIsRecharged(String isRecharged) {
        this.isRecharged = isRecharged;
    }

    public String getRecordedDate() {
        return recordedDate;
    }

    public void setRecordedDate(String recordedDate) {
        this.recordedDate = recordedDate;
    }

    public long getTotalDaysDataUsed() {
        return totalDaysDataUsed;
    }

    public void setTotalDaysDataUsed(long totalDaysDataUsed) {
        this.totalDaysDataUsed = totalDaysDataUsed;
    }

    public double getTotalDataMBUsedInMonth() {
        return totalDataMBUsedInMonth;
    }

    public void setTotalDataMBUsedInMonth(double totalDataMBUsedInMonth) {
        this.totalDataMBUsedInMonth = totalDataMBUsedInMonth;
    }


}
