package com.app.Billbachao.location.utils;

import android.content.Context;

import com.app.Billbachao.apis.ApiUtils;
import com.app.Billbachao.location.model.CallLogBean;
import com.app.Billbachao.location.model.NetworkBean;
import com.google.myjson.Gson;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by rajkumar on 4/26/2016.
 */
public class NetworkCallLogUtils extends ApiUtils {

    public static final String URL = BASE_URL + "bbdetails";

    public static final String NETWORK_DATA = "jsonNetwork", CALL_DATA = "jsonStats", MOBILE = "mobile_number";


    public static Map<String, String> getNwCallLogHeader(Context context, ArrayList<NetworkBean> networkBeans, ArrayList<CallLogBean> callLogBeans) {
        Gson gson = new Gson();
        Map<String, String> headerMap = new LinkedHashMap<>();
        headerMap.put(MOBILE, "9224788824");
        headerMap.put(NETWORK_DATA, gson.toJson(networkBeans));
        headerMap.put(CALL_DATA, gson.toJson(callLogBeans));
        headerMap.put(REQUEST_DATE, getDate());

        return getParamsWithCheckSum(context, headerMap);
    }

}
