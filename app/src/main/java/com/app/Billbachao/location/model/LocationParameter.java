package com.app.Billbachao.location.model;

/**
 * Created by rajkumar on 4/23/2016.
 */
public class LocationParameter {

    private static double sLatitude;
    private static double sLongitude;

    public double getsLongitude() {
        return sLongitude;
    }

    public void setsLongitude(double sLongitude) {
        LocationParameter.sLongitude = sLongitude;
    }

    public double getsLatitude() {
        return sLatitude;
    }

    public void setsLatitude(double sLatitude) {
        LocationParameter.sLatitude = sLatitude;
    }

}
