package com.app.Billbachao.location.service;


import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import com.app.Billbachao.common.permissions.PermissionWrapper;
import com.app.Billbachao.location.listener.CellPhoneStateListener;
import com.app.Billbachao.utils.ILog;


/**
 * Created by rajkumar on 4/23/2016.
 */
public class CellLocationService extends Service {

    public static final String TAG = CellLocationService.class.getSimpleName();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ILog.d(TAG, "location service started");
        initListener();
        return Service.START_STICKY;
    }

    private void initListener() {
        if (!PermissionWrapper.hasLocationPermissions(this)) return;
        TelephonyManager telManager = (TelephonyManager) getSystemService(Context
                .TELEPHONY_SERVICE);
        CellPhoneStateListener myPhoneStateListener = CellPhoneStateListener.getInstance(this);
        telManager.listen(myPhoneStateListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS
                | PhoneStateListener.LISTEN_CELL_LOCATION
                | PhoneStateListener.LISTEN_CALL_STATE);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
