package com.app.Billbachao.location.model;

import com.google.myjson.annotations.SerializedName;

public class NetworkBean {

    @SerializedName("O")
    private String operatorName;
    @SerializedName("N")
    private String networkType;
    @SerializedName("S")
    private int signalStrength;
    @SerializedName("R")
    private String roamingStatus;
    private String circle;
    @SerializedName("time")
    private String time;
    @SerializedName("mobileno")
    private String mobileno;
    @SerializedName("L")
    private int LAC;
    @SerializedName("M")
    private int MCC;
    @SerializedName("C")
    private int cellID;
    private String locationName;
    @SerializedName("P")
    private String networkProtocol;
    @SerializedName("B")
    private float battery;
    @SerializedName("LT")
    private double latitude = 0;
    @SerializedName("LG")
    private double longtitude = 0;
    private String prevNetworkState = null;

    // added by Yogesh
    private String simSerialNo = "0";

    @SerializedName("internetType")
    private String internetType;

    public String getInternetType() {
        return internetType;
    }

    public void setInternetType(String internetType) {
        this.internetType = internetType;
    }

    public String getSimSerialNo() {
        return simSerialNo;
    }

    public void setSimSerialNo(String simSerialNo) {
        this.simSerialNo = simSerialNo;
    }

    public String getPrevNetworkState() {
        return prevNetworkState;
    }

    public void setPrevNetworkState(String prevNetworkState) {
        this.prevNetworkState = prevNetworkState;
    }

    public String getCircle() {
        return circle;
    }

    public void setCircle(String circle) {
        this.circle = circle;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getNetworkType() {
        return networkType;
    }

    public void setNetworkType(String networkType) {
        this.networkType = networkType;
    }

    public int getSignalStrength() {
        return signalStrength;
    }

    public void setSignalStrength(int signalStrength) {
        this.signalStrength = signalStrength;
    }

    public String getRoamingStatus() {
        return roamingStatus;
    }

    public void setRoamingStatus(String roamingStatus) {
        this.roamingStatus = roamingStatus;
    }

    public int getLAC() {
        return LAC;
    }

    public void setLAC(int lAC) {
        LAC = lAC;
    }

    public int getMCC() {
        return MCC;
    }

    public void setMCC(int mCC) {
        MCC = mCC;
    }

    public int getCellID() {
        return cellID;
    }

    public void setCellID(int cellID) {
        this.cellID = cellID;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getNetworkProtocol() {
        return networkProtocol;
    }

    public void setNetworkProtocol(String networkProtocol) {
        this.networkProtocol = networkProtocol;
    }

    public float getBattery() {
        return battery;
    }

    public void setBattery(float battery) {
        this.battery = battery;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double d) {
        this.latitude = d;
    }

    public double getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(double longtitude) {
        this.longtitude = longtitude;
    }

    public boolean isInvalid() {
        return latitude == 0 || longtitude == 0;
    }
}
