package com.app.Billbachao.location.api;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.location.utils.MobDataUsageUtils;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.volley.request.NetworkStringRequest;
import com.app.Billbachao.volley.utils.VolleySingleTon;

/**
 * This class is responsible to push mobile data traffic stats.
 * <p>
 * Created by rajkumar on 4/26/2016.
 */
public class MobDataUsagePushAPI {

    public static final String TAG = MobDataUsagePushAPI.class.getSimpleName();

    public static void pushMobDataUsage(Context context) {

        NetworkStringRequest stringRequest = new NetworkStringRequest(Request.Method.POST, MobDataUsageUtils.URL,
                MobDataUsageUtils.getMobDataUsageHeader(context), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                ILog.d(TAG, "Mob data traffic push resp: " + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ILog.d(TAG, error.toString());
            }
        });

        VolleySingleTon.getInstance(context).addToRequestQueue(stringRequest, TAG);
    }
}
