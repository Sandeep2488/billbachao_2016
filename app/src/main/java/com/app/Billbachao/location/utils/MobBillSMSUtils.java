package com.app.Billbachao.location.utils;

import android.content.Context;


import com.app.Billbachao.apis.ApiUtils;
import com.app.Billbachao.utils.PreferenceUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by rajkumar on 4/27/2016.
 */
public class MobBillSMSUtils extends ApiUtils {

    public static final String URL = BASE_URL + "SaveSMSServlet";

    public static final String SMS_BODY = "smsBody";

    public static Map<String, String> getMobBillSMSHeader(Context context, String smsDetails) {
        Map<String, String> header = new LinkedHashMap<>();
        header.put(MOBILE_NUMBER, PreferenceUtils.getMobileNumber(context));
        header.put(REQUEST_DATE, getDate());
        header.put(SMS_BODY, smsDetails);
        return getParamsWithCheckSum(context, header);
    }
}
