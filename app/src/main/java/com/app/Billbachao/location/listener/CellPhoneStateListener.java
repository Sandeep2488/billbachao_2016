package com.app.Billbachao.location.listener;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;

import com.app.Billbachao.location.helper.DataUsageHelper;
import com.app.Billbachao.location.helper.NetworkCallLogHelper;
import com.app.Billbachao.location.model.LocationParameter;
import com.app.Billbachao.location.model.NetworkBean;
import com.app.Billbachao.location.utils.NetworkLocationUtils;
import com.app.Billbachao.location.utils.PhoneStateChangeUtils;
import com.app.Billbachao.notifications.inapp.SimpleNotification;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.utils.PreferenceUtils;


/**
 * This class is responsible for updating phone's state i.e. call, location, signal strength.
 * <p/>
 * Created by rajkumar on 4/23/2016.
 */
public class CellPhoneStateListener extends PhoneStateListener {

    public static final String TAG = CellPhoneStateListener.class.getSimpleName();

    Context mContext;

    int mCurrentCallState = -1;

    private static int sCallState = -10;

    private static CellPhoneStateListener sStateChangeListener;

    public boolean mZeroSignalFlag = false;

    LocationParameter parameter;

    long mStartTime, mEndTime;

    private CellPhoneStateListener(Context context) {
        mContext = context;
        parameter = new LocationParameter();
    }

    /**
     * This method creates global singleton object for this class.
     */
    public static CellPhoneStateListener getInstance(Context context) {
        if (sStateChangeListener == null) {
            synchronized (CellPhoneStateListener.class) {
                if (sStateChangeListener == null)
                    sStateChangeListener = new CellPhoneStateListener(context.getApplicationContext());
            }
        }
        return sStateChangeListener;
    }

    /**
     * This callback invoked when device call state changes.
     *
     * @param state          call state
     * @param incomingNumber incoming call phone number. If application does not have
     *                       READ_PHONE_STATE permission, an empty string will be passed as an argument.
     */
    @Override
    public void onCallStateChanged(int state, String incomingNumber) {
        switch (state) {
            case TelephonyManager.CALL_STATE_IDLE:
                if (mCurrentCallState == 1 || mCurrentCallState == 2) {
                    sCallState = TelephonyManager.CALL_STATE_IDLE;
                    mCurrentCallState = TelephonyManager.CALL_STATE_IDLE;
                }
                break;
            case TelephonyManager.CALL_STATE_RINGING:
                mCurrentCallState = TelephonyManager.CALL_STATE_RINGING;
                break;
            case TelephonyManager.CALL_STATE_OFFHOOK:
                mCurrentCallState = TelephonyManager.CALL_STATE_OFFHOOK;
                break;

        }
    }

    /**
     * Callback invoked when device cell location changes and updates location parameter.
     *
     * @param location CellLocation object holding latitude and longitude of location.
     */
    @Override
    public void onCellLocationChanged(CellLocation location) {
        if (mContext == null) {
            return;
        }
        String phoneNumber = PreferenceUtils.getMobileNumber(mContext);
        if (!TextUtils.isEmpty(phoneNumber) && !phoneNumber.equalsIgnoreCase(PreferenceUtils.NA)
                && !PhoneStateChangeUtils.isAirplaneModeOn(mContext)) {

            CellLocationListener.getInstance(mContext, parameter).updateLocation();

            TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context
                    .TELEPHONY_SERVICE);
            if (telephonyManager != null) {
                CellLocation cellLocation = telephonyManager.getCellLocation();
                if (cellLocation != null && cellLocation instanceof GsmCellLocation) {
                    NetworkBean networkbean = NetworkLocationUtils.getNetworkBean(mContext, telephonyManager,
                            (GsmCellLocation)cellLocation, parameter);
                    NetworkCallLogHelper.insertNetworkDetails(mContext, networkbean);
                    networkbean = null;
                    if (telephonyManager.isNetworkRoaming()) {
                        if (PhoneStateChangeUtils.canShowRoaming(mContext)) {
                            new SimpleNotification(mContext,
                                    "Roaming Alert!", "Hi there, We realize you " +
                                    "are travelling. Click here to save cost " +
                                    "while roaming").show();
                            PhoneStateChangeUtils.updateRoamingNotified(mContext);
                        }
                    }
                } else {
                    NetworkLocationUtils.insertNetworkBeanDtls(mContext, phoneNumber, parameter.getsLatitude(),
                            parameter.getsLongitude());
                }
                DataUsageHelper.setDataUsageCounter(mContext, "N", false);
                NetworkLocationUtils.pushNetworkPoint(mContext);
            } else {
                NetworkLocationUtils.insertNetworkBeanDtls(mContext, phoneNumber, parameter.getsLatitude(),
                        parameter.getsLongitude());
            }
        }
    }

    // Used as key in preference to store start time of call drop when signal strength is low.
    public static final String START_TIME = "mStartTime";

    /**
     * Callback invoked when network signal strengths changes and counts call drops on low signal strength.
     */
    @Override
    public void onSignalStrengthsChanged(SignalStrength signalStrengthInput) {
        if (mContext == null) {
            return;
        }
        String phoneNumber = PreferenceUtils.getMobileNumber(mContext);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        int signalStrength = signalStrengthInput.getGsmSignalStrength();
        PhoneStateChangeUtils.StoreSignalStrength(mContext, signalStrength);
        if (!PhoneStateChangeUtils.isLowerOSVersion() && !PhoneStateChangeUtils.isAirplaneModeOn(mContext)) { // for lower version no need to capture calldrop and zerosignal

            long date = System.currentTimeMillis();
            if ((signalStrength == 0 || signalStrength == 99) && mZeroSignalFlag == false) {
                mZeroSignalFlag = true;
                mStartTime = date;
                preferences.edit().putLong(START_TIME, mStartTime).apply();
                ILog.d(TAG, "Start zero signal date:" + mStartTime);
            } else {
                if (mZeroSignalFlag == true && parameter != null) {
                    mZeroSignalFlag = false;
                    mEndTime = date;
                    mStartTime = preferences.getLong(START_TIME, 0);
                    ILog.d(TAG, " zero signal date: mStartTime =" + mStartTime + " , mEndTime =" + mEndTime);
                    NetworkCallLogHelper.insertDropCalls(mContext, 0, NetworkLocationUtils.getTimeDiffInSecond(mStartTime, mEndTime));
                    NetworkLocationUtils.insertNetworkBeanDtls(mContext, phoneNumber, parameter.getsLatitude(), parameter.getsLongitude());
                    ILog.d(TAG, "Stop zero signal date:" + date);
                }
            }
            // if SS is < 5 and call disconnected so its a call drop.
            if (sCallState == 0) {
                ILog.d(TAG, "Call Drop disconnected");
                if (signalStrength == 99 || signalStrength <= 5) {
                    ILog.d(TAG, "Call Drop Count Calculated ss less than 5: " + signalStrength);
                    NetworkCallLogHelper.insertDropCalls(mContext, 1L, 0);
                    sCallState = -10;
                }
            }
        }
    }

}
