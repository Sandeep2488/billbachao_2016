package com.app.Billbachao.location.model;

import com.google.myjson.annotations.SerializedName;

public class CallLogBean {
    @SerializedName("mobileno")
    public String mobileno;

    @SerializedName("TO")
    public String time;
    @SerializedName("zerosignalduration")
    public long zerosignalduration;

    @SerializedName("TMDU")
    public long total_mobile_data_usage;
    @SerializedName("droppedcallscount")
    public int droppedcallscount;

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public long getZerosignalduration() {
        return zerosignalduration;
    }

    public void setZerosignalduration(long zerosignalduration) {
        this.zerosignalduration = zerosignalduration;
    }

    public long getTotal_mobile_data_usage() {
        return total_mobile_data_usage;
    }

    public void setTotal_mobile_data_usage(long total_mobile_data_usage) {
        this.total_mobile_data_usage = total_mobile_data_usage;
    }

    public int getDroppedcallscount() {
        return droppedcallscount;
    }

    public void setDroppedcallscount(int droppedcallscount) {
        this.droppedcallscount = droppedcallscount;
    }

}
