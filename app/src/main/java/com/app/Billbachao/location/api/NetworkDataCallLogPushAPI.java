package com.app.Billbachao.location.api;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.location.helper.NetworkCallLogHelper;
import com.app.Billbachao.location.model.CallLogBean;
import com.app.Billbachao.location.model.NetworkBean;
import com.app.Billbachao.location.utils.NetworkCallLogUtils;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.volley.request.NetworkJSONRequest;
import com.app.Billbachao.volley.utils.VolleySingleTon;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * This class is responsible to push network and call logs details
 * <p>
 * Created by rajkumar on 4/26/2016.
 */
public class NetworkDataCallLogPushAPI {

    public static final String TAG = NetworkDataCallLogPushAPI.class.getSimpleName();

    private static final String NETWORK_DATA_STATUS = "jsonNetworkstatus",

    CALL_STATS_STATUS = "jsonStatsstatus", SUCCESSS = "Success";

    /**
     * This method push network and call log details and on success response from server deletes stored old data in DB.
     *
     * @param context      caller's context
     * @param networkBeans list of NetworkBean objects holding network data
     * @param callLogBeans list of CallLogBean objects holding call log data
     */
    public static void pushNetworkCallLogData(final Context context, ArrayList<NetworkBean> networkBeans,
                                              ArrayList<CallLogBean> callLogBeans) {


        NetworkJSONRequest request = new NetworkJSONRequest(Request.Method.POST, NetworkCallLogUtils.URL,
                NetworkCallLogUtils.getNwCallLogHeader(context, networkBeans, callLogBeans),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            if (response.getString(NETWORK_DATA_STATUS).equalsIgnoreCase(SUCCESSS)) {
                                NetworkCallLogHelper.removeData(context, NetworkCallLogHelper.CALL_DATA);
                            }
                            if (response.getString(CALL_STATS_STATUS).equalsIgnoreCase(SUCCESSS)) {
                                NetworkCallLogHelper.removeData(context, NetworkCallLogHelper.NETWORK_DATA);
                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }

                        ILog.d(TAG, response.toString());

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ILog.d(TAG, error.toString());
            }
        });

        VolleySingleTon.getInstance(context).addToRequestQueue(request, TAG);

    }
}
