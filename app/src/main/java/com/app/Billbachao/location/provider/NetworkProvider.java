package com.app.Billbachao.location.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

import com.app.Billbachao.utils.ILog;


/**
 * Created by BB-001 on 4/16/2016.
 */
public class NetworkProvider extends ContentProvider {

    public static final String TAG = NetworkProvider.class.getSimpleName();

    private Context mContext;

    static String DATABASE_NAME = "networkLogs.db";

    // TODO Should be done as 1 in release. 2 is set to avoid clearing data for testing
    static int DATA_BASE_VERSION = 1;

    private NetworkSQLiteHelper mHelper;

    public static final String AUTHORITY = "com.app.Billbachao.data.networkprovider";

    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    public static final int NETWORK_DATA = 1, STATISTICS_DATA = 2;

    private static final UriMatcher URI_MATCHER;

    static {
        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        URI_MATCHER.addURI(AUTHORITY, NetworkColoumns.TABLE_NAME, NETWORK_DATA);
        URI_MATCHER.addURI(AUTHORITY, StatisticsColoumns.TABLE_NAME, STATISTICS_DATA);
    }

    public SQLiteDatabase mDatabase;

    @Override
    public boolean onCreate() {
        mContext = getContext();
        mHelper = new NetworkSQLiteHelper(mContext, DATABASE_NAME, null,
                DATA_BASE_VERSION);
        try {
            mDatabase = mHelper.getWritableDatabase();
        } catch (Exception ex) {
            mDatabase = mHelper.getReadableDatabase();
        }
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor cursor = null;
        switch (URI_MATCHER.match(uri)) {
            case NETWORK_DATA:
                cursor = mDatabase.query(NetworkColoumns.TABLE_NAME, projection,
                        selection, selectionArgs, null, null, sortOrder);
                break;
            case STATISTICS_DATA:
                cursor = mDatabase.query(StatisticsColoumns.TABLE_NAME, projection,
                        selection, selectionArgs, null, null, sortOrder);
                break;
            default:
                break;
        }
        if (cursor != null) {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long rowId = -1;
        Uri insertUri = null;
        switch (URI_MATCHER.match(uri)) {
            case NETWORK_DATA:
                rowId = mDatabase.insert(NetworkColoumns.TABLE_NAME, null, values);
                insertUri = Uri.withAppendedPath(NetworkColoumns.CONTENT_URI,
                        Long.toString(rowId));
                break;
            case STATISTICS_DATA:
                rowId = mDatabase.insert(StatisticsColoumns.TABLE_NAME, null, values);
                insertUri = Uri.withAppendedPath(StatisticsColoumns.CONTENT_URI,
                        Long.toString(rowId));
                break;
        }
        if (rowId != -1) {
            ILog.d(TAG, "Inserted rowId: " + rowId);
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return insertUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int numRows = 0;
        switch (URI_MATCHER.match(uri)) {
            case NETWORK_DATA:
                numRows = mDatabase.delete(NetworkColoumns.TABLE_NAME, selection,
                        selectionArgs);
                break;
            case STATISTICS_DATA:
                numRows = mDatabase.delete(StatisticsColoumns.TABLE_NAME, selection,
                        selectionArgs);
                break;
        }
        return numRows;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int numRows = 0;
        switch (URI_MATCHER.match(uri)) {
            case NETWORK_DATA:
                numRows = mDatabase.update(NetworkColoumns.TABLE_NAME, values,
                        selection, selectionArgs);
                break;
            case STATISTICS_DATA:
                numRows = mDatabase.update(StatisticsColoumns.TABLE_NAME, values,
                        selection, selectionArgs);
                break;
        }
        ILog.d(TAG, "Updated rows: " + numRows);
        return numRows;
    }

    public static class NetworkSQLiteHelper extends SQLiteOpenHelper {

        static NetworkSQLiteHelper sInstance;

        public static NetworkSQLiteHelper getInstance(Context context) {
            if (sInstance == null) {
                sInstance = new NetworkSQLiteHelper(context.getApplicationContext());
            }
            return sInstance;
        }

        public NetworkSQLiteHelper(Context context) {
            super(context, DATABASE_NAME, null,
                    DATA_BASE_VERSION);
        }

        public NetworkSQLiteHelper(Context context, String name,
                                   SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            createNetworkDetailsTable(db);
            createStatisticsDetailsTable(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }

        void createNetworkDetailsTable(SQLiteDatabase db) {
            db.execSQL(NetworkColoumns.CREATE_TABLE);
        }

        void createStatisticsDetailsTable(SQLiteDatabase db) {
            db.execSQL(StatisticsColoumns.CREATE_TABLE);
        }

    }

    public static class NetworkColoumns {

        public static String TABLE_NAME = "NetworkDetails";

        public static final Uri CONTENT_URI = Uri.withAppendedPath(
                NetworkProvider.CONTENT_URI, TABLE_NAME);


        public static final String ID = "id", OPERATOR_NAME = "operatorname", NETWORK_TYPE = "networktype", SIGNAL_STRENGTH = "signalstrength", ROAMING_STATUS = "roamingstatus",
                MOBILE_NO = "mobileno", TIME = "time", CIRCLE = "circle", MCC = "mcc", LAC = "lac",
                LAT = "lat", LANG = "lang", PROTOCOL = "protocol", CELL_ID = "cellid", BATTERY_STATUS = "battery_status", INTERNET_TYPE = "internettype";

        public static final String CREATE_TABLE = "create table " + TABLE_NAME + " ( "
                + ID + " integer primary key autoincrement" + ", "
                + OPERATOR_NAME + " text" + ", "
                + NETWORK_TYPE + " text" + ", "
                + SIGNAL_STRENGTH + " integer" + ", "
                + ROAMING_STATUS + " text" + ", "
                + MOBILE_NO + " text" + ", "
                + TIME + " text" + ", "
                + CIRCLE + " text" + ", "
                + MCC + " integer" + ", "
                + LAC + " integer" + ", "
                + LAT + " integer" + ", "
                + LANG + " integer" + ", "
                + PROTOCOL + " text" + ", "
                + CELL_ID + " text" + ", "
                + BATTERY_STATUS + " text" + ", "
                + INTERNET_TYPE + " text" + " ) ";
    }

    public static class StatisticsColoumns {

        public static String TABLE_NAME = "StatisticsDetails";

        public static final Uri CONTENT_URI = Uri.withAppendedPath(
                NetworkProvider.CONTENT_URI, TABLE_NAME);

        public static final String MOBILE_NO = "mobileno", TIME = "time", DROPPEDCALLS = "droppedcalls", ZEROSIGNAL = "zerosignal";

        public static final String CREATE_TABLE = " create table " + TABLE_NAME + " ( "
                + MOBILE_NO + " text" + ", "
                + TIME + " text" + ", "
                + DROPPEDCALLS + " text" + ", "
                + ZEROSIGNAL + " text"
                + " ) ";
    }
}
