package com.app.Billbachao.tutorial.utils;

import com.app.Billbachao.R;

/**
 * Created by mihir on 04-11-2015.
 */
public class WalkthroughUtils {

    public static final int WELCOME = 1, PLANS = 2, NETWORK = 3, USAGE = 5, SELF_CARE = 6,
            RECHARGE = 4;

    public static final int PAGES[] = new int[]{WELCOME, PLANS, NETWORK, RECHARGE, USAGE,
            SELF_CARE};

    public static int getTutorialIconId(int page) {
        switch (page) {
            case PLANS:
                return R.drawable.tut_img_plans;
            case NETWORK:
                return R.drawable.tut_img_network;
            case USAGE:
                return R.drawable.tut_img_recharge;
            case SELF_CARE:
                return R.drawable.tut_img_usage;
            case RECHARGE:
                return R.drawable.tut_img_self_help;
            case WELCOME:
            default:
                return R.drawable.tut_img_intro;
        }
    }

    public static int getTitleId(int page) {
        switch (page) {
            case PLANS:
                return R.string.tutorial_plans_title;
            case NETWORK:
                return R.string.tutorial_network_title;
            case USAGE:
                return R.string.tutorial_usage_title;
            case SELF_CARE:
                return R.string.tutorial_self_care_title;
            case RECHARGE:
                return R.string.tutorial_recharge_title;
            case WELCOME:
            default:
                return R.string.tutorial_welcome_title;
        }
    }

    public static int getDescriptionList(int page) {
        switch (page) {
            case PLANS:
                return R.string.plans_description;
            case NETWORK:
                return R.string.network_description;
            case USAGE:
                return R.string.usage_description;
            case SELF_CARE:
                return R.string.self_care_description;
            case RECHARGE:
                return R.string.recharge_description;
            case WELCOME:
            default:
                return R.string.welcome_description;
        }
    }

    public static final String TUTORIAL_TYPE = "tutorial";

    // Initial pager walkthrough
    public static final int INITIAL = 0x111;

    public static String getPreferenceKey(int type) {
        String key = "";

        switch (type) {
            case INITIAL:
                key = "initial";
                break;
        }
        return TUTORIAL_TYPE + key;
    }

}
