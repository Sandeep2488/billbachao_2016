package com.app.Billbachao.tutorial;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.tutorial.utils.WalkthroughUtils;

/**
 * Created by mihir on 04-11-2015.
 */
public class WalkthroughFragment extends Fragment {

    public static final String PAGE = "page";

    int mPage;

    public static WalkthroughFragment getInstance(int page) {
        WalkthroughFragment fragment = new WalkthroughFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(PAGE, page);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(PAGE, WalkthroughUtils.WELCOME);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_walkthrough, container, false);
        initView(view);
        return view;
    }

    void initView(View view) {
        ImageView iconView = (ImageView) view.findViewById(R.id.icon);
        iconView.setImageResource(WalkthroughUtils.getTutorialIconId(mPage));

        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(WalkthroughUtils.getTitleId(mPage));

        ((TextView) view.findViewById(R.id.description)).setText(WalkthroughUtils.getDescriptionList
                (mPage));
    }
}
