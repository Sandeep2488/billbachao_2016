package com.app.Billbachao.tutorial;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.app.Billbachao.tutorial.utils.WalkthroughUtils;

/**
 * Created by mihir on 12-09-2015.
 */
public class TutorialHelper {

    public static boolean isTutorialShown(Context context, int type) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String key = WalkthroughUtils.getPreferenceKey(type);
        return preferences.getBoolean(key, false);
    }

    public static void setTutorialShown(Context context, int type) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String key = WalkthroughUtils.getPreferenceKey(type);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, true);
        editor.apply();
    }
}
