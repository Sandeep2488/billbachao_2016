package com.app.Billbachao.recharge.plans.contents;

import android.content.Intent;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.apis.BrowsePlansApi;
import com.app.Billbachao.externalsdk.google.AppIndexBehavior;
import com.app.Billbachao.launch.DeepLinkHelper;
import com.app.Billbachao.recharge.cart.CartPlanHelper;
import com.app.Billbachao.recharge.cart.provider.CartPlansProvider;
import com.app.Billbachao.recharge.plans.contents.fragments.BasePlanFragment;
import com.app.Billbachao.recharge.plans.contents.fragments.PlanListFragment;
import com.app.Billbachao.recharge.plans.contents.fragments.RecentFragment;
import com.app.Billbachao.recharge.plans.model.PlanCartDBItem;
import com.app.Billbachao.recharge.plans.model.PlanItem;
import com.app.Billbachao.recharge.plans.model.PlanValidationItem;
import com.app.Billbachao.recharge.plans.utils.PlanUtils;
import com.app.Billbachao.utils.CircleUtils;
import com.app.Billbachao.utils.ConstantUtils;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.utils.OperatorUtils;
import com.app.Billbachao.utils.PreferenceUtils;
import com.app.Billbachao.volley.utils.VolleySingleTon;

import java.util.List;

/**
 * Created by mihir on 25-04-2016.
 */
public class BrowsePlansActivity extends BaseActivity implements RecentFragment.OnRecentDataUpdater, BasePlanFragment
        .PlanSelectorListener, AppIndexBehavior {

    public static final String TAG = BrowsePlansActivity.class.getSimpleName();

    ViewPager mPager;

    String mOperatorId, mCircleId, mAmount, mPreferredPlan, mMobileNumber;

    boolean mLaunchForSelf, mPickedFromNetwork, mExternalLaunch, mRecentMoved;

    int mPreferredPlanIndex = -1;

    ContentObserver mCartObserver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_plans);
        loadDetails();
        initView();
        mCartObserver = new CartContentObserver(new Handler(getMainLooper()));
        getContentResolver().registerContentObserver(CartPlansProvider.PlanDetails.CONTENT_URI,
                true, mCartObserver);
    }

    @Override
    protected void onDestroy() {
        getContentResolver().unregisterContentObserver(mCartObserver);
        VolleySingleTon.getInstance(this).cancelPendingRequests(TAG);
        super.onDestroy();
    }

    void loadDetails() {
        String planCategories = PreferenceUtils.getPlanList(this);
        if (!planCategories.isEmpty()) {
            PlanUtils.updateCategoriesFromNetwork(planCategories);
        }
        mPickedFromNetwork = PlanUtils.isPickedFromNetwork();
        Intent intent = getIntent();
        mLaunchForSelf = intent.getBooleanExtra(ConstantUtils.LAUNCH_SELF_PLANS, false);
        mExternalLaunch = intent.getBooleanExtra(ConstantUtils.LAUNCH_FROM_EXTERNAL, false);
        mPreferredPlan = PlanUtils.getAbstractPlanName(this, intent.getIntExtra(ConstantUtils
                .PREFERRED_RECHARGE_TYPE, 0));
        mPreferredPlanIndex = PlanUtils.getPlanIndex(this, mPreferredPlan, mPickedFromNetwork);

        if (mExternalLaunch) {
            int preferredPlan = intent.getIntExtra(ConstantUtils.PREFERRED_RECHARGE_TYPE, 0);
            // TODO events
        }

        if (mExternalLaunch && mPreferredPlanIndex > -1) {
            mRecentMoved = true;
        }

        mMobileNumber = PreferenceUtils.getMobileNumber(this);
        if (!mLaunchForSelf) {
            mMobileNumber = intent.getStringExtra(PreferenceUtils.MOBILE_NUMBER);
            mCircleId = intent.getStringExtra(PreferenceUtils.CIRCLE);
            mOperatorId = intent.getStringExtra(PreferenceUtils.OPERATOR);
            mAmount = intent.getStringExtra(PreferenceUtils.AMOUNT);
        } else {
            mCircleId = PreferenceUtils.getCircleId(this);
            mOperatorId = PreferenceUtils.getOperatorId(this);
            mAmount = BrowsePlansApi.NA;
        }

        if (!OperatorUtils.isOperatorIdMatchFound(mOperatorId) || !CircleUtils
                .isCircleIdMatchFound(mCircleId)) {
            Toast.makeText(this, R.string.circle_operator_incorrect, Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle(getString(R.string.plans_for_operator, OperatorUtils.getOperatorNameFromId
                (mOperatorId)));
        toolbar.setSubtitle(CircleUtils.getCircleCompleteNameFromId(mCircleId));

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new PlansPagerAdapter(getSupportFragmentManager()));
        // +1 offset is for recent plans
        mPager.setCurrentItem(mPreferredPlanIndex + 1, true);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mPager);
    }

    @Override
    public void onPlanSelected(PlanItem plan, boolean isSelectedFromRecent) {
        if (mLaunchForSelf) {
            // For self number, add plan to cart and dont finish

            if (isSelectedFromRecent) {
                addPlanToCartIfNotPresent(plan);
            } else {
                togglePlanInCart(plan);
            }
        } else {
            Intent intent = new Intent();
            intent.putExtra(PlanItem.PLAN_ITEM, plan);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private void updatePlanList() {
        List<Fragment> activeFragments = getSupportFragmentManager().getFragments();
        for (Fragment fragment : activeFragments) {
            if (fragment instanceof PlanListFragment) {
                ((PlanListFragment) fragment).updateData();
            }
        }
    }

    // Strictly called for self plans only, not through quick recharge
    void togglePlanInCart(PlanItem item) {
        if (CartPlanHelper.isPlanInCart(this, item.getId(), mMobileNumber)) {
            removePlanFromCart(item);
        } else {
            addPlanToCart(item);
        }
    }

    void addPlanToCartIfNotPresent(PlanItem item) {
        if (CartPlanHelper.isPlanInCart(this, item.getId(), mMobileNumber)) {
            showSnack(R.string.present_in_cart);
        } else {
            addPlanToCart(item);
        }
    }

    void addPlanToCart(PlanItem item) {
        // TODO events
        PlanCartDBItem detailedItem = new PlanCartDBItem(item, mMobileNumber,
                mOperatorId, mCircleId, true, PlanValidationItem.BROWSE_PLAN);
        if (CartPlanHelper.addPlanToCart(this, detailedItem)) {
            showSnack(R.string.added_to_cart);
        }
    }

    void removePlanFromCart(PlanItem item) {
        if (CartPlanHelper.deletePlan(this, String.valueOf(item.getId()), mMobileNumber) > 0) {
            showSnack(R.string.removed_from_cart);
        }
    }

    @Override
    public void onNoPlans() {
        if (mRecentMoved) return;
        // Current index of Recent items is zero
        int RECENT_PLAN_INDEX = 0;
        if (mPager.getCurrentItem() == RECENT_PLAN_INDEX) {
            mPager.setCurrentItem(RECENT_PLAN_INDEX + 1, true);
        }
        mRecentMoved = true;
    }

    @Override
    public String getAppIndexTitle() {
        return Keywords.BROWSE_PLANS;
    }

    @Override
    public String getAppIndexAppendedUrl() {
        return DeepLinkHelper.BROWSE_PLANS;
    }

    public class PlansPagerAdapter extends FragmentStatePagerAdapter {

        public PlansPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return RecentFragment.getInstance(mCircleId, mOperatorId, mLaunchForSelf);
            } else {
                // Position - 1 -> for Recent plans at 0
                return PlanListFragment.getInstance(PlanUtils.getPlanTypeText(position - 1,
                        mPickedFromNetwork), mCircleId,
                        mOperatorId, mAmount, mLaunchForSelf);
            }
        }

        @Override
        public int getCount() {
            // + 1 for RecentPlans
            return PlanUtils.getCount(mPickedFromNetwork) + 1;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            // Position - 1 -> for Recent plans at 0
            if (position == 0) {
                return "Recent";
            }
            return PlanUtils.getTitle(BrowsePlansActivity.this, position - 1, mPickedFromNetwork);
        }
    }

    class CartContentObserver extends ContentObserver {

        long mLastUpdate;

        final static long THRESHOLD_MS = 100;

        /**
         * Creates a content observer.
         *
         * @param handler The handler to run {@link #onChange} on, or null if none.
         */
        public CartContentObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange) {
            checkAndUpdate();
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            checkAndUpdate();
        }

        void checkAndUpdate() {
            long currentTime = System.currentTimeMillis();
            ILog.d(TAG, "checkAndUpdate");
            if (currentTime - mLastUpdate > THRESHOLD_MS) {
                ILog.d(TAG, "checkAndUpdate applied");
                updatePlanList();
                mLastUpdate = currentTime;
            }
        }
    }

}
