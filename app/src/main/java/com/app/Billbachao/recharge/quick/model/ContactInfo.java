package com.app.Billbachao.recharge.quick.model;

/**
 * Created by mihir on 05-05-2016.
 */
public class ContactInfo {

    String number;

    String name;

    public ContactInfo(String number, String name) {
        this.number = number;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    /*@Override
    public String toString() {
        return number;
    }*/
}
