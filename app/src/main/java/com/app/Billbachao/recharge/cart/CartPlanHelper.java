package com.app.Billbachao.recharge.cart;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.app.Billbachao.recharge.cart.provider.CartPlansProvider;
import com.app.Billbachao.recharge.plans.model.PlanCartDBItem;
import com.app.Billbachao.utils.PreferenceUtils;

import java.util.ArrayList;

/**
 * Created by mihir on 03-05-2016.
 */
public class CartPlanHelper {

    /**
     * Common helper method converting plans cursor to list.
     *
     * @param cursor Cursor retrieved from DB
     * @return List of plans, extracted from cursor
     */
    private static ArrayList<PlanCartDBItem> plansCursorToList(Cursor cursor) {
        ArrayList<PlanCartDBItem> planItems = new ArrayList<>();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int nameIndex = cursor.getColumnIndex(CartPlansProvider.PlanDetails.PLAN_NAME);
                int descriptionIndex = cursor.getColumnIndex(CartPlansProvider.PlanDetails
                        .PLAN_DESCRIPTION);
                int idX = cursor.getColumnIndex(CartPlansProvider.PlanDetails.ID);
                int costIndex = cursor.getColumnIndex(CartPlansProvider.PlanDetails.PLAN_COST);
                int validityIndex = cursor.getColumnIndex(CartPlansProvider.PlanDetails.PLAN_VALIDITY);
                int countIndex = cursor.getColumnIndex(CartPlansProvider.PlanDetails.PLAN_COUNT);
                int dataIndex = cursor.getColumnIndex(CartPlansProvider.PlanDetails.PLAN_MB);
                int circleIndex = cursor.getColumnIndex(CartPlansProvider.PlanDetails.CIRCLE_ID);
                int operatorIndex = cursor.getColumnIndex(CartPlansProvider.PlanDetails.OPERATOR_ID);
                int mobileIndex = cursor.getColumnIndex(CartPlansProvider.PlanDetails.MOBILE_NO);
                int addedIndex = cursor.getColumnIndex(CartPlansProvider.PlanDetails.ADDED_FROM);
                int connectionIndex = cursor.getColumnIndex(CartPlansProvider.PlanDetails
                        .CONNECTION_TYPE);

                do {
                    String name = cursor.getString(nameIndex);
                    String description = cursor.getString(descriptionIndex);
                    String id = cursor.getString(idX);
                    int cost = cursor.getInt(costIndex);
                    String validity = cursor.getString(validityIndex);
                    int planCount = cursor.getInt(countIndex);
                    String dataMb = cursor.getString(dataIndex);
                    int circle = cursor.getInt(circleIndex);
                    int operator = cursor.getInt(operatorIndex);
                    String mobileNumber = cursor.getString(mobileIndex);
                    int addedFrom = cursor.getInt(addedIndex);
                    boolean isPrepaid = cursor.getInt(connectionIndex) == CartPlansProvider.PlanDetails.PREPAID;

                    PlanCartDBItem item = new PlanCartDBItem(Integer.parseInt(id), name,
                            description, cost, validity, isPrepaid, planCount, mobileNumber, String.valueOf(operator),
                            String.valueOf(circle), addedFrom);
                    item.setDataMb(dataMb);
                    planItems.add(item);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return planItems;
    }

    /**
     * Checks for duplicate plans, on basis of mobile and planId
     *
     * @param context
     * @param planId
     * @param mobileNumber
     * @return
     */
    public static boolean isPlanInCart(Context context, int planId, String mobileNumber) {
        Cursor cursor = context.getContentResolver().query(CartPlansProvider.PlanDetails.CONTENT_URI, null,
                CartPlansProvider.PlanDetails.PLAN_ID + "=? AND " + CartPlansProvider.PlanDetails.MOBILE_NO + "=?",
                new String[]{String.valueOf(planId), mobileNumber}, null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                return true;
            }
            cursor.close();
        }
        return false;
    }

    /**
     * Delete plan on basis of planId and number
     *
     * @param context
     * @param planId
     * @param mobileNumber
     * @return rows effected
     */
    public static int deletePlan(Context context, String planId, String mobileNumber) {
        return context.getContentResolver().delete(CartPlansProvider.PlanDetails.CONTENT_URI,
                CartPlansProvider.PlanDetails.PLAN_ID + " =? AND " + CartPlansProvider.PlanDetails.MOBILE_NO + " =? ",
                new String[]{planId, mobileNumber});
    }

    /**
     * Deletes plan on basis of planId
     *
     * @param context
     * @param primaryId
     * @return rows effected
     */
    public static int deletePlanByPrimaryId(Context context, String primaryId) {
        return context.getContentResolver().delete(CartPlansProvider.PlanDetails.CONTENT_URI,
                CartPlansProvider.PlanDetails.ID+ " =? ",
                new String[]{primaryId});
    }

    /**
     * Deletes all plans in cart
     *
     * @param context
     * @return rows effected/deleted
     */
    public static int deleteAllPlans(Context context) {
        return context.getContentResolver().delete(CartPlansProvider.PlanDetails.CONTENT_URI, null, null);
    }

    /**
     * Add plans to cart, final object for adding plan.
     * Ideally its preferred to convert other objects into PlanCartDBItem, and then add it using
     * this function
     *
     * @param context
     * @param item
     * @return
     */
    public static boolean addPlanToCart(Context context, PlanCartDBItem item) {
        ContentValues values = getValuesFromPlanItem(item);
        Uri uri = context.getContentResolver().insert(CartPlansProvider.PlanDetails.CONTENT_URI, values);
        return uri != null;
    }

    /**
     * Bulk insert of plans for efficiency
     *
     * @param context
     * @param itemList
     * @return
     */
    public static int addBulkPlansToCart(Context context, ArrayList<PlanCartDBItem> itemList) {
        if (itemList.isEmpty()) return 0;
        ContentValues[] valuesList = new ContentValues[itemList.size()];
        int index = 0;
        for (PlanCartDBItem item : itemList) {
            valuesList[index++] = getValuesFromPlanItem(item);
        }
        return context.getContentResolver().bulkInsert(CartPlansProvider.PlanDetails.CONTENT_URI,
                valuesList);
    }

    /**
     * Converts @PlanCartDBItem into DB contentValues
     * Can be reused for insert, bulkinsert
     *
     * @param item
     * @return
     */
    private static ContentValues getValuesFromPlanItem(PlanCartDBItem item) {
        ContentValues values = new ContentValues();
        values.put(CartPlansProvider.PlanDetails.PLAN_ID, item.getId());
        values.put(CartPlansProvider.PlanDetails.PLAN_NAME, item.getTitle());
        values.put(CartPlansProvider.PlanDetails.PLAN_COST, item.getValue());
        values.put(CartPlansProvider.PlanDetails.PLAN_DESCRIPTION, item.getDescription());
        values.put(CartPlansProvider.PlanDetails.PLAN_VALIDITY, item.getValidity());
        values.put(CartPlansProvider.PlanDetails.PLAN_COUNT, item.getPlanCount());
        values.put(CartPlansProvider.PlanDetails.PLAN_MB, item.getDataMb());
        values.put(CartPlansProvider.PlanDetails.MOBILE_NO, item.getMobileNo());
        values.put(CartPlansProvider.PlanDetails.CIRCLE_ID, item.getCircleId());
        values.put(CartPlansProvider.PlanDetails.OPERATOR_ID, item.getOperatorId());
        values.put(CartPlansProvider.PlanDetails.ADDED_FROM, item.getAddedFrom());
        values.put(CartPlansProvider.PlanDetails.CONNECTION_TYPE, item.getType().equalsIgnoreCase(PreferenceUtils
                .PREPAID) ? CartPlansProvider.PlanDetails.PREPAID : CartPlansProvider.PlanDetails.POSTPAID);
        return values;
    }

    /**
     * Get count of items in cart
     *
     * @param context
     * @return
     */
    public static int getItemCount(Context context) {
        int count = 0;
        Cursor cursor = context.getContentResolver().query(CartPlansProvider.PlanDetails.CONTENT_URI, null, null, null, null);
        if (cursor != null) {
            count = cursor.getCount();
            cursor.close();
        }
        return count;
    }

    /**
     * Retrieve all plans present in cart
     *
     * @return List of plans grouped by mobile number
     */
    public static ArrayList<PlanCartDBItem> getPlansList(Context context) {
        Cursor cursor = context.getContentResolver().query(CartPlansProvider.PlanDetails
                .CONTENT_URI, null, null, null, CartPlansProvider.PlanDetails.MOBILE_NO);
        return plansCursorToList(cursor);
    }
}
