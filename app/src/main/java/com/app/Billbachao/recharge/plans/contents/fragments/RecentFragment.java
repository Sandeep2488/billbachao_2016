package com.app.Billbachao.recharge.plans.contents.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.apis.TransactionHistoryApi;
import com.app.Billbachao.recharge.plans.model.PlanItem;
import com.app.Billbachao.recharge.plans.model.TransactionHistoryGsonResponse;
import com.app.Billbachao.recharge.plans.model.TransactionItem;
import com.app.Billbachao.recharge.quick.PlanValidatorTask;
import com.app.Billbachao.utils.CircleUtils;
import com.app.Billbachao.utils.ConstantUtils;
import com.app.Billbachao.utils.OperatorUtils;
import com.app.Billbachao.utils.PreferenceUtils;
import com.app.Billbachao.volley.request.NetworkGsonRequest;
import com.app.Billbachao.volley.utils.NetworkErrorHelper;
import com.app.Billbachao.volley.utils.VolleySingleTon;

import java.util.Iterator;
import java.util.List;

/**
 * Created by mihir on 25-04-2016.
 */
public class RecentFragment extends BasePlanFragment<TransactionItem> {

    public static final String TAG = RecentFragment.class.getSimpleName();

    OnRecentDataUpdater mDataUpdater;

    public static RecentFragment getInstance(String circleId, String operatorId, boolean
            launchForSelf) {
        RecentFragment fragment = new RecentFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(ConstantUtils.LAUNCH_SELF_PLANS, launchForSelf);
        bundle.putString(PreferenceUtils.CIRCLE, circleId);
        bundle.putString(PreferenceUtils.OPERATOR, operatorId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mDataUpdater = (OnRecentDataUpdater) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mDataUpdater = null;
    }

    @Override
    protected RecyclerView.Adapter getAdapter() {
        return new RecentPlansAdapter(getActivity(), mPlansList);
    }

    @Override
    void fetchPlanList() {
        NetworkGsonRequest<TransactionHistoryGsonResponse> planFetchRequest = new NetworkGsonRequest
                <>(Request.Method.POST, TransactionHistoryApi.URL, TransactionHistoryGsonResponse
                .class,
                TransactionHistoryApi.getHistoryParams(getActivity()), new
                Response.Listener<TransactionHistoryGsonResponse>() {
                    @Override
                    public void onResponse(TransactionHistoryGsonResponse response) {
                        mPlansList = response.getTransactionItems();
                        filterPlansForCurrent();
                        if (mPlansList != null && mPlansList.isEmpty()) {
                            if (mDataUpdater != null) mDataUpdater.onNoPlans();
                        }
                        loadData();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                setEmptyText(NetworkErrorHelper.getErrorStatus(error).getErrorMessage());
            }
        }, true);
        VolleySingleTon.getInstance(getActivity()).addToRequestQueue(planFetchRequest, TAG);
    }

    @Override
    public void onDestroy() {
        VolleySingleTon.getInstance(getActivity()).cancelPendingRequests(TAG);
        super.onDestroy();
    }

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = mRecyclerView.getChildAdapterPosition(v);
            TransactionItem item = mPlansList.get(position);
            validateQuickRechargePlan(item.getPlanAmount());
        }
    };

    void filterPlansForCurrent() {
        if (mPlansList == null || mPlansList.isEmpty()) return;
        String network = OperatorUtils.getOperatorNameFromId(mOperatorId);
        String circle = CircleUtils.getCircleCompleteNameFromId(mCircleId);
        Iterator<TransactionItem> iterator = mPlansList.iterator();
        while (iterator.hasNext()) {
            TransactionItem item = iterator.next();
            if (!item.isHavingNetworkCircle(network, circle)) iterator.remove();
        }
    }

    void validateQuickRechargePlan(String amount) {
        PlanValidatorTask planValidatorTask = new PlanValidatorTask(getActivity(),
                mPlanResultListener);
        planValidatorTask.trigger(mCircleId, mOperatorId, amount, TAG);
    }

    PlanValidatorTask.PlanResultListener mPlanResultListener = new PlanValidatorTask.PlanResultListener() {
        @Override
        public void onPlanObtained(PlanItem planItem) {
            planItem.setType(getString(R.string.recent));
            mPlanListener.onPlanSelected(planItem, true);
        }

        @Override
        public void onInvalidPlanDetected() {
            ((BaseActivity) getActivity()).showSnack(R.string.invalid_plan);
        }

        @Override
        public void onError(String error) {
            setEmptyText(error);
        }
    };

    public interface OnRecentDataUpdater {
        void onNoPlans();
    }

    public class RecentPlansAdapter extends RecyclerView.Adapter<RecentPlansAdapter
            .CustomViewHolder> {

        Context mContext;

        List<TransactionItem> mItems;

        public RecentPlansAdapter(Context context, List<TransactionItem> items) {
            mItems = items;
            mContext = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .plan_item, null);
            CustomViewHolder holder = new CustomViewHolder(view);
            view.setOnClickListener(mClickListener);
            return holder;
        }

        @Override
        public void onBindViewHolder(CustomViewHolder holder, int position) {
            TransactionItem transactionItem = mItems.get(position);
            holder.value.setText(getString(R.string.inr_n, String.valueOf(Math.round
                    (Float.parseFloat(transactionItem.getPlanAmount())))));
            holder.main.setText(transactionItem.getPlanName());
            holder.sub.setText(getString(R.string.recharged_on_n, transactionItem
                    .getTransactionDate()));
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        public class CustomViewHolder extends RecyclerView.ViewHolder {
            protected TextView value, main, sub;
            protected ImageView planSelector;

            public CustomViewHolder(View view) {
                super(view);
                value = (TextView) view.findViewById(R.id.plan_value_text);
                main = (TextView) view.findViewById(R.id.plan_main_text);
                sub = (TextView) view.findViewById(R.id.plan_sub_text);
                planSelector = (ImageView) view.findViewById(R.id.plan_circle);
            }
        }
    }
}
