package com.app.Billbachao.recharge.plans.model;

import com.app.Billbachao.payment.cashback.model.CashbackInfo;
import com.google.myjson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by mihir on 25-04-2016.
 */
public class TransactionHistoryGsonResponse extends CashbackInfo {

    @SerializedName("transactionHistory")
    ArrayList<TransactionItem> transactionItems;

    @SerializedName("productTransactionHistory")
    ArrayList<BaseTransactionItem> productTransactionItems;

    public ArrayList<TransactionItem> getTransactionItems() {
        return transactionItems;
    }

    public ArrayList<BaseTransactionItem> getProductTransactionItems() {
        return productTransactionItems;
    }
}
