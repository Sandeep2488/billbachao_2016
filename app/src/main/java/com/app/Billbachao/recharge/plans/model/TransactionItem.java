package com.app.Billbachao.recharge.plans.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.myjson.annotations.SerializedName;

/**
 * Created by mihir on 19-08-2015.
 */
public class TransactionItem extends BaseTransactionItem implements Parcelable {

    @SerializedName("circle")
    String circleName;

    @SerializedName("operator")
    String operatorName;

    @SerializedName("planName")
    String planName;

    @SerializedName("planAmount")
    String planAmount;

    int couponCount;

    @SerializedName("cashbackAmount")
    int cashBackOfferedAmt;

    public TransactionItem(String id, String mobile, String circleName, String operatorName,
                           String planName, String planAmount, String transactionDate, String
                                   successStatus, int couponCount, int cashBackOfferedAmt) {
        this.id = id;
        this.mobile = mobile;
        this.circleName = circleName;
        this.operatorName = operatorName;
        this.planName = planName;
        this.planAmount = planAmount;
        this.transactionDate = transactionDate;
        this.successStatus = successStatus;
        this.couponCount = couponCount;
        this.cashBackOfferedAmt = cashBackOfferedAmt;

    }

    TransactionItem() {
    }

    protected TransactionItem(Parcel in) {
        mobile = in.readString();
        id = in.readString();
        transactionDate = in.readString();
        successStatus = in.readString();
        circleName = in.readString();
        operatorName = in.readString();
        planName = in.readString();
        planAmount = in.readString();
        couponCount = in.readInt();
        cashBackOfferedAmt = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mobile);
        dest.writeString(id);
        dest.writeString(transactionDate);
        dest.writeString(successStatus);
        dest.writeString(circleName);
        dest.writeString(operatorName);
        dest.writeString(planName);
        dest.writeString(planAmount);
        dest.writeInt(couponCount);
        dest.writeInt(cashBackOfferedAmt);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TransactionItem> CREATOR = new Creator<TransactionItem>() {
        @Override
        public TransactionItem createFromParcel(Parcel in) {
            return new TransactionItem(in);
        }

        @Override
        public TransactionItem[] newArray(int size) {
            return new TransactionItem[size];
        }
    };

    public String getCircleName() {
        return circleName;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public String getPlanName() {
        return planName;
    }

    public String getPlanAmount() {
        return planAmount;
    }

    public int getCouponCount() {
        return couponCount;
    }

    public void setCouponCount(int couponCount) {
        this.couponCount = couponCount;
    }

    public int getCashBackOfferedAmt() {
        return cashBackOfferedAmt;
    }

    public void setCashBackOfferedAmt(int cashBackOfferedAmt) {
        this.cashBackOfferedAmt = cashBackOfferedAmt;
    }

    public boolean isHavingNetworkCircle(String network, String circle) {
        return network.equalsIgnoreCase(operatorName) && circle.equalsIgnoreCase(circleName);
    }
}
