package com.app.Billbachao.recharge.cart;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.payment.helper.PaymentIntermediaryHelper;
import com.app.Billbachao.recharge.cart.contents.adapters.CartCursorAdapter;
import com.app.Billbachao.recharge.cart.provider.CartPlansProvider;
import com.app.Billbachao.recharge.plans.model.PlanCartDBItem;
import com.app.Billbachao.recharge.plans.model.PlanItem;
import com.app.Billbachao.recharge.plans.model.PlanValidationItem;
import com.app.Billbachao.utils.ConstantUtils;
import com.app.Billbachao.utils.UiUtils;

/**
 * Created by mihir on 03-05-2016.
 */
public class CartActivity extends BaseActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    ListView mListView;

    Button mPaymentButton;

    CartCursorAdapter mAdapter;

    String mPendingPlanMobile, mPendingOperatorId, mPendingCircleId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cart_activity);
        initView();
        loadData();
    }

    void initView() {
        mListView = (ListView) findViewById(R.id.list);
        mPaymentButton = (Button) findViewById(R.id.goto_payment);
        View emptyView = findViewById(R.id.empty_view);
        emptyView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UiUtils.launchSelfBrowsePlans(CartActivity.this);
            }
        });
        mListView.setEmptyView(emptyView);
    }

    void loadData() {
        mAdapter = new CartCursorAdapter(this, null, mPlanAdder);
        final View footerView = View.inflate(this, R.layout.add_number_cart_footer, null);
        footerView.findViewById(R.id.add_more_number).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UiUtils.launchRecharge(CartActivity.this);
            }
        });
        mListView.addFooterView(footerView, null, false);
        mListView.setAdapter(mAdapter);
        getSupportLoaderManager().initLoader(0, null, this);
        mPaymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PaymentIntermediaryHelper.launchFromCart(CartActivity.this);
            }
        });
    }

    @Override
    protected boolean isCartItemSupported() {
        return false;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this, CartPlansProvider.PlanDetails
                .CONTENT_URI, null, null, null, CartPlansProvider.PlanDetails.MOBILE_NO);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.updateCursorIndices(data);
        mAdapter.changeCursor(data);
        mPaymentButton.setVisibility(data != null && data.getCount() > 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.changeCursor(null);
    }

    CartCursorAdapter.PlanAddRequester mPlanAdder = new CartCursorAdapter.PlanAddRequester() {
        @Override
        public void onPlanRequested(String mobile, String circleId, String operatorId) {
            mPendingPlanMobile = mobile;
            mPendingCircleId = circleId;
            mPendingOperatorId = operatorId;
            UiUtils.launchBrowsePlans(CartActivity.this, circleId, operatorId);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ConstantUtils.PLAN_PICKER:
                if (resultCode == RESULT_OK) {
                    PlanItem item = data.getParcelableExtra(PlanItem.PLAN_ITEM);
                    PlanCartDBItem cartDBItem = new PlanCartDBItem(item, mPendingPlanMobile,
                            mPendingOperatorId, mPendingCircleId, true, PlanValidationItem
                            .QUICK_RECHARGE);
                    CartPlanHelper.addPlanToCart(this, cartDBItem);
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
