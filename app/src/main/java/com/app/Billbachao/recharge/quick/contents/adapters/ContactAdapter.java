package com.app.Billbachao.recharge.quick.contents.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.recharge.quick.model.ContactInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mihir on 05-05-2016.
 */
public class ContactAdapter extends ArrayAdapter<ContactInfo> {

    public static final String TAG = ContactAdapter.class.getSimpleName();

    List<ContactInfo> contactInfoList;

    int mResourceId;

    List<ContactInfo> suggestions = new ArrayList<>();

    public ContactAdapter(Context context, int resource, List<ContactInfo> objects) {
        super(context, resource, objects);
        contactInfoList = new ArrayList<>(objects.size());
        contactInfoList.addAll(objects);
        mResourceId = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(getContext(), mResourceId, null);
            convertView.setTag(new ViewHolder(convertView));
        }
        ((ViewHolder) convertView.getTag()).populate(getItem(position));
        return convertView;
    }


    @Override
    public Filter getFilter() {
        return contactFilter;
    }

    class ViewHolder {

        TextView numberText, nameText;

        ViewHolder(View itemView) {
            numberText = (TextView) itemView.findViewById(R.id.number);
            nameText = (TextView) itemView.findViewById(R.id.name);
        }

        void populate(ContactInfo info) {
            numberText.setText(info.getNumber());
            nameText.setText(info.getName());
        }
    }

    Filter contactFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((ContactInfo) resultValue).getNumber();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (ContactInfo contactInfo : contactInfoList) {
                    if (contactInfo.getNumber().startsWith(constraint.toString())) {
                        suggestions.add(contactInfo);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results == null) return;
            ArrayList<ContactInfo> filteredList = (ArrayList<ContactInfo>) results.values;
            if (filteredList == null) return;
            if (results.count > 0) {
                clear();
                addAll(filteredList);
                notifyDataSetChanged();
            }
        }
    };
}
