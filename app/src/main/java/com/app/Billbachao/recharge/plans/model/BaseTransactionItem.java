package com.app.Billbachao.recharge.plans.model;

import com.google.myjson.annotations.SerializedName;

/**
 * Created by mihir on 11-03-2016.
 */
public class BaseTransactionItem {

    @SerializedName("mobile")
    String mobile;

    @SerializedName("bbpgorderid")
    String id;

    @SerializedName("txnDate")
    String transactionDate;

    @SerializedName("txnStatus")
    String successStatus;

    public String getId() {
        return id;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public String getSuccessStatus() {
        return successStatus;
    }

    public String getMobile() {
        return mobile;
    }

}
