package com.app.Billbachao.recharge.quick;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.apis.QuickRechargeApi;
import com.app.Billbachao.model.QuickRechargePlanItem;
import com.app.Billbachao.recharge.plans.model.PlanItem;
import com.app.Billbachao.volley.request.NetworkGsonRequest;
import com.app.Billbachao.volley.utils.NetworkErrorHelper;
import com.app.Billbachao.volley.utils.VolleySingleTon;

/**
 * Created by mihir on 28-04-2016.
 */
public class PlanValidatorTask {

    Context mContext;

    PlanResultListener mListener;

    public PlanValidatorTask(Context context) {
        this(context, null);
    }

    public PlanValidatorTask(Context context, PlanResultListener listener) {
        mContext = context;
        mListener = listener;
    }

    public void trigger(String circleId, String operatorId, String amount, String tag) {
        NetworkGsonRequest<QuickRechargePlanItem[]> validatePlanRequest = new NetworkGsonRequest
                <>(Request.Method.POST, QuickRechargeApi.URL, QuickRechargePlanItem[].class,
                QuickRechargeApi.getParams(mContext, circleId, operatorId, amount),
                new
                        Response.Listener<QuickRechargePlanItem[]>() {
                            @Override
                            public void onResponse(QuickRechargePlanItem[] response) {
                                if (response.length > 0 && response[0].isValidPlan()) {
                                    if (mListener != null) mListener.onPlanObtained(response[0]);
                                } else {
                                    if (mListener != null) mListener.onInvalidPlanDetected();
                                }
                            }
                        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (mListener != null) mListener.onError(NetworkErrorHelper.getErrorStatus(error)
                        .getErrorMessage());
            }
        });
        VolleySingleTon.getInstance(mContext).addToRequestQueue(validatePlanRequest, tag);
    }

    public interface PlanResultListener {
        void onPlanObtained(PlanItem planItem);

        void onInvalidPlanDetected();

        void onError(String error);
    }
}
