package com.app.Billbachao.recharge.utils;

/**
 * Created by mihir on 04-05-2016.
 * Add constants and util functionality for recharge
 */
public class RechargeUtils {

    public static final int MINIMUM_BILL_PAY_AMOUNT = 50;

    public static boolean isValidAmount(boolean isPrepaid, int amount) {
        return ((amount > 0 && isPrepaid) || (!isPrepaid && amount > MINIMUM_BILL_PAY_AMOUNT));
    }
}
