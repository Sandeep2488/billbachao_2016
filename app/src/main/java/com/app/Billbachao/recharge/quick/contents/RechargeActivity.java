package com.app.Billbachao.recharge.quick.contents;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.apis.ApiUtils;
import com.app.Billbachao.common.permissions.PermissionWrapper;
import com.app.Billbachao.externalsdk.AppsFlyerUtils;
import com.app.Billbachao.externalsdk.google.AppIndexBehavior;
import com.app.Billbachao.launch.DeepLinkHelper;
import com.app.Billbachao.model.OperatorCircleInfo;
import com.app.Billbachao.payment.helper.PaymentIntermediaryHelper;
import com.app.Billbachao.recharge.cart.CartPlanHelper;
import com.app.Billbachao.recharge.plans.model.PlanCartDBItem;
import com.app.Billbachao.recharge.plans.model.PlanItem;
import com.app.Billbachao.recharge.plans.model.PlanValidationItem;
import com.app.Billbachao.recharge.quick.PlanValidatorTask;
import com.app.Billbachao.recharge.quick.RechargeHelper;
import com.app.Billbachao.recharge.quick.contents.adapters.ContactAdapter;
import com.app.Billbachao.recharge.quick.model.ContactInfo;
import com.app.Billbachao.recharge.utils.RechargeUtils;
import com.app.Billbachao.utils.CircleUtils;
import com.app.Billbachao.utils.ConstantUtils;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.utils.NumberUtils;
import com.app.Billbachao.utils.OperatorUtils;
import com.app.Billbachao.utils.PreferenceUtils;
import com.app.Billbachao.utils.UiUtils;

import java.util.ArrayList;

/**
 * Created by mihir on 04-05-2016.
 */
public class RechargeActivity extends BaseActivity implements AppIndexBehavior {

    public static final String TAG = RechargeActivity.class.getSimpleName();

    EditText mAmountEditText;

    AutoCompleteTextView mMobileText;

    Spinner mOperatorSpinner, mCircleSpinner;

    RadioGroup mPrepaidPostpaidGroup;

    boolean mIsPrepaid = true, mIsRechargePending = false, mIsPendingInstantMode = false;

    ArrayAdapter<String> mPrepaidOperatorAdapter, mPostpaidOperatorAdapter;

    String mCircleId, mOperatorId, mNumber;

    ViewGroup mPlansGroup;

    ArrayList<PlanItem> mPlansList;

    Button mAddToCart, mRechargeNow;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recharge_activity);
        initView();
        loadData();
        mMobileText.post(new Runnable() {
            @Override
            public void run() {
                getContactData();
            }
        });
        AppsFlyerUtils.trackEvent(this, AppsFlyerUtils.QUICK_RECHARGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ConstantUtils.PLAN_PICKER:
                if (resultCode == RESULT_OK) {
                    PlanItem item = data.getParcelableExtra(PlanItem.PLAN_ITEM);
                    if (!mPlansList.contains(item)) {
                        addPlanToGroup(item);
                    } else {
                        showSnack(R.string.duplicate_plan);
                    }
                }
                break;
            case ConstantUtils.CONTACT_PICKER:
                if (resultCode == RESULT_OK) {
                    ContactInfo info = RechargeHelper.getContactInfo(this, data.getData());
                    if (info != null) mMobileText.setText(info.getNumber());
                }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Init UI
     */
    void initView() {
        mMobileText = (AutoCompleteTextView) findViewById(R.id.mobile_number);
        mAddToCart = (Button) findViewById(R.id.add_to_cart);
        mRechargeNow = (Button) findViewById(R.id.recharge_now);
        mPrepaidPostpaidGroup = (RadioGroup) findViewById(R.id.prepaid_postpaid_group);
        mOperatorSpinner = (Spinner) findViewById(R.id.operator);
        mCircleSpinner = (Spinner) findViewById(R.id.circle);
        mPlansGroup = (ViewGroup) findViewById(R.id.plans_group);
        mAddToCart.setOnClickListener(mClickListener);
        mRechargeNow.setOnClickListener(mClickListener);
        findViewById(R.id.select_contact).setOnClickListener(mClickListener);
    }

    /**
     * Load data
     */
    private void loadData() {
        mPrepaidPostpaidGroup.setOnCheckedChangeListener(mCheckChangedListener);
        mPlansList = new ArrayList<>();
        loadOperatorData();
        loadCircleData();
        mIsPrepaid = PreferenceUtils.isPrepaid(this);
        mPrepaidPostpaidGroup.check(mIsPrepaid ? R.id.prepaid : R.id.postpaid);
        mMobileText.addTextChangedListener(mTextWatcher);
        mMobileText.setText(PreferenceUtils.getMobileNumber(this));
        reloadPlanGroup();
    }

    /**
     * Load and init operator data
     */
    private void loadOperatorData() {
        mPrepaidOperatorAdapter = new ArrayAdapter<>(this, android.R
                .layout.simple_spinner_dropdown_item, OperatorUtils.sOperatorNamesSorted);
        mPostpaidOperatorAdapter = new ArrayAdapter<>(this, android.R
                .layout.simple_spinner_dropdown_item, OperatorUtils.sPostpaidOperatorNamesSorted);
        updateModeChanged();
        mOperatorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedOperator = mIsPrepaid ? OperatorUtils
                        .getOperatorIdFromSortedPosition(position) : OperatorUtils
                        .getPostpaidOperatorIdFromSortedPosition(position);
                if (!selectedOperator.equalsIgnoreCase(mOperatorId)) {
                    invalidatePlans();
                }
                mOperatorId = selectedOperator;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /**
     * Load and init circle data
     */
    private void loadCircleData() {
        ArrayAdapter<String> circleAdapter = new ArrayAdapter<String>(this, android.R
                .layout.simple_spinner_dropdown_item, CircleUtils.sCircleNames_shortCut);
        mCircleSpinner.setAdapter(circleAdapter);
        mCircleSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!CircleUtils.sCircleId[position].equalsIgnoreCase(mCircleId)) {
                    invalidatePlans();
                }
                mCircleId = CircleUtils.sCircleId[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /**
     * Prepaid/postpaid changed
     */
    private void updateModeChanged() {
        String tempOperator = mOperatorId;
        mOperatorSpinner.setAdapter(mIsPrepaid ? mPrepaidOperatorAdapter : mPostpaidOperatorAdapter);
        selectOperatorId(tempOperator);
        mRechargeNow.setText(mIsPrepaid ? R.string.recharge_now : R.string.pay_now);
        invalidatePlans();
    }

    /**
     * Clear plans
     */
    private void invalidatePlans() {
        mPlansList.clear();
        reloadPlanGroup();
    }

    /**
     * Add a selected plan item to group
     *
     * @param item PlanItem to be added
     */
    private void addPlanToGroup(PlanItem item) {
        mPlansList.add(item);
        reloadPlanGroup();
    }

    /**
     * Reload plan list group
     */
    private void reloadPlanGroup() {
        mPlansGroup.removeAllViews();
        for (PlanItem planItem : mPlansList) {
            View planRow = View.inflate(this, R.layout.recharge_plan_row, null);
            mPlansGroup.addView(planRow);
            fillPlan(planRow, planItem);
        }
        if (checkNumberDetails(false)) {
            addCurrentGroup();
        }
    }

    /**
     * Fill planItem UI, with entered details
     *
     * @param planGroup
     * @param planItem
     */
    void fillPlan(View planGroup, final PlanItem planItem) {
        EditText amountEnter = (EditText) planGroup.findViewById(R.id.enter_amount);
        amountEnter.setVisibility(View.GONE);

        View planDetails = planGroup.findViewById(R.id.amount_entered_view);
        planDetails.setVisibility(View.VISIBLE);

        ((TextView) planGroup.findViewById(R.id.amount_value)).setText(String.valueOf(planItem
                .getValue()));
        ((TextView) planGroup.findViewById(R.id.plan_type)).setText(planItem.getType());
        Button browsePlans = (Button) planGroup.findViewById(R.id.browse_plans);
        browsePlans.setText(R.string.remove);
        browsePlans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPlansList.remove(planItem);
                reloadPlanGroup();
            }
        });
    }

    /**
     * Add one extra row for filling in the plan amount/selection
     */
    private void addCurrentGroup() {
        View planGroup = View.inflate(this, R.layout.recharge_plan_row, null);
        mPlansGroup.addView(planGroup);
        TextView browsePlans = (TextView) planGroup.findViewById(R.id.browse_plans);
        mAmountEditText = (EditText) planGroup.findViewById(R.id.enter_amount);
        mAmountEditText.requestFocus();
        if (mIsPrepaid) {
            browsePlans.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String amount = mAmountEditText.getText().toString();
                    UiUtils.launchBrowsePlans(RechargeActivity.this, mCircleId, mOperatorId, amount
                            .isEmpty() ? ApiUtils.NA : amount);
                }
            });
            if (mPlansGroup.getChildCount() > 1) mAmountEditText.setHint(R.string.add_more);
        } else {
            browsePlans.setVisibility(View.INVISIBLE);
            mAmountEditText.setHint(R.string.enter_bill_amount);
        }
    }

    /**
     * Prepaid postpaid check change listener
     */
    RadioGroup.OnCheckedChangeListener mCheckChangedListener = new RadioGroup
            .OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (group.getId() == R.id.prepaid_postpaid_group) {
                mIsPrepaid = checkedId == R.id.prepaid;
                updateModeChanged();
            }
        }
    };

    TextWatcher mTextWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.length() == 10) {
                String number = NumberUtils.parseNumber(s.toString());
                if (number != null) {
                    mNumber = number;
                    updateCircleOperator(number);
                }
            }
        }
    };

    /**
     * Update circle, operator on number entry
     *
     * @param number
     */
    private void updateCircleOperator(String number) {
        String circleId, operatorId;

        // Handle special case for default number, get circle, operator from user's choice during
        // registration
        if (PreferenceUtils.getMobileNumber(this).equalsIgnoreCase(number)) {
            circleId = PreferenceUtils.getCircleId(this);
            operatorId = PreferenceUtils.getOperatorId(this);
        } else {
            OperatorCircleInfo info = NumberUtils.getOperatorCircleInfo(this, number);
            circleId = String.valueOf(info.getCircleId());
            operatorId = String.valueOf(info.getOperatorId());
        }

        if (CircleUtils.isCircleIdMatchFound(circleId)) {
            mCircleSpinner.setSelection(CircleUtils.getCircleIndex(circleId));
        }

        selectOperatorId(operatorId);
    }

    /**
     * Trying to select valid operator based on prepaid/postpaid
     *
     * @param operatorId
     */
    void selectOperatorId(String operatorId) {
        if ((mIsPrepaid && OperatorUtils.isOperatorIdMatchFound(operatorId)) || OperatorUtils
                .isOperatorIdPostpaidMatchFound(operatorId)) {
            mOperatorSpinner.setSelection(mIsPrepaid ? OperatorUtils.getOperatorIndexForSorted
                    (operatorId) : OperatorUtils.getPostpaidOperatorIndexForSorted(operatorId));
        }
    }

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.recharge_now:
                    processPlans(true);
                    break;
                case R.id.add_to_cart:
                    processPlans(false);
                    break;
                case R.id.select_contact:
                    Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                    intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                    startActivityForResult(intent, ConstantUtils.CONTACT_PICKER);
                    break;
            }
        }
    };

    /**
     * Checks if number entry, plan etc are valid
     *
     * @return
     */
    private boolean checkValidation() {
        if (!checkNumberDetails(true)) return false;
        if (mPlansList.isEmpty()) {
            int amount = NumberUtils.getAmountEntered(mAmountEditText);
            if (!RechargeUtils.isValidAmount(mIsPrepaid, amount)) {
                if (mIsPrepaid)
                    showSnack(R.string.no_plans);
                else
                    showSnack(getString(R.string.minimum_bill_amount_n, RechargeUtils
                            .MINIMUM_BILL_PAY_AMOUNT));
                return false;
            }
        }
        return true;
    }

    /**
     * Check if number details are valid
     *
     * @param showMessage Whether to show error message to user
     * @return
     */
    private boolean checkNumberDetails(boolean showMessage) {
        if (NumberUtils.parseNumber(mMobileText.getText().toString()) == null) {
            if (showMessage) showSnack(R.string.wrong_mobile_number_validation_msg);
            return false;
        }
        return true;
    }

    /**
     * Redirects to addtoCart and rechargeNow, depending on the mode
     *
     * @param isInstant
     */
    private void processPlans(boolean isInstant) {
        ILog.d(TAG, "processPlans isInstant : " + isInstant);
        if (!checkValidation()) return;
        ILog.d(TAG, "processPlans valid !!");
        mIsRechargePending = false;
        if (mPlansList.isEmpty()) {
            if (mIsPrepaid) mIsRechargePending = true;
            else addPostpaidPlan();
        }
        if (!mIsRechargePending) {
            ILog.d(TAG, "processPlans non pending");
            if (isInstant) {
                rechargeNow();
            } else {
                addToCart();
            }
        } else {
            ILog.d(TAG, "processPlans pending");
            // Quick recharge
            mIsPendingInstantMode = isInstant;
            quickValidateAndRecharge();
        }
    }

    /**
     * Postpaid bill payment
     */
    private void addPostpaidPlan() {
        ILog.d(TAG, "addPostpaidPlan");
        PlanItem item = PlanItem.getPostpaidPlanInstance(mAmountEditText.getText().toString());
        mPlansList.add(item);
    }

    /**
     * Quick recharge
     */
    private void quickValidateAndRecharge() {
        PlanValidatorTask planValidatorTask = new PlanValidatorTask(this, resultListener);
        showProgressDialog(getString(R.string.validating_recharge));
        planValidatorTask.trigger(mCircleId, mOperatorId, mAmountEditText.getText().toString(),
                TAG);
    }

    /**
     * Add plans list to cart
     */
    private void addToCart() {
        ILog.d(TAG, "addToCart");
        ArrayList<PlanCartDBItem> cartDBItems = new ArrayList<>();
        for (PlanItem planItem : mPlansList) {
            PlanCartDBItem cartDBItem = new PlanCartDBItem(planItem, mNumber,
                    mOperatorId, mCircleId, mIsPrepaid, PlanValidationItem.QUICK_RECHARGE);
            cartDBItems.add(cartDBItem);
        }
        int addedCount = CartPlanHelper.addBulkPlansToCart(this, cartDBItems);
        String toast = addedCount == mPlansList.size() ? getString(R.string.n_items_added_cart,
                addedCount) : getString(R.string.n_items_added_cart_k_items_already_present,
                addedCount, mPlansList.size() - addedCount);
        showSnack(toast);
        invalidatePlans();
    }

    /**
     * Recharge instantly
     */
    private void rechargeNow() {
        ILog.d(TAG, "rechargeNow");
        PaymentIntermediaryHelper.launchFromRecharge(this, mPlansList, mNumber, mOperatorId,
                mCircleId, mIsPrepaid);
    }

    PlanValidatorTask.PlanResultListener resultListener = new PlanValidatorTask.PlanResultListener() {
        @Override
        public void onPlanObtained(PlanItem planItem) {
            ILog.d(TAG, "onPlanObtained " + mIsPendingInstantMode);
            dismissProgressDialog();
            mPlansList.clear();
            mPlansList.add(planItem);
            if (mIsPendingInstantMode) rechargeNow();
            else addToCart();
        }

        @Override
        public void onInvalidPlanDetected() {
            dismissProgressDialog();
            showSnack(R.string.invalid_plan);
        }

        @Override
        public void onError(String error) {
            dismissProgressDialog();
            showSnack(error);
        }
    };

    /**
     * Fetch contacts list for auto complete
     */
    private void getContactData() {
        if (!PermissionWrapper.hasContactsPermissions(this)) return;
        new Thread(new Runnable() {
            public void run() {
                final ArrayList<ContactInfo> contactInfoArrayList = RechargeHelper.getContactsData
                        (RechargeActivity.this);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mMobileText.setAdapter(new ContactAdapter(RechargeActivity.this, R.layout
                                .contact_info_item_row, contactInfoArrayList));
                    }
                });
            }
        }).start();
    }

    @Override
    public String getAppIndexTitle() {
        return mIsPrepaid ? Keywords.RECHARGE : Keywords.BILL_PAYMEMNT;
    }

    @Override
    public String getAppIndexAppendedUrl() {
        return DeepLinkHelper.RECHARGE + (mIsPrepaid ? "/quick" : "/bill");
    }
}
