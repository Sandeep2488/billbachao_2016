package com.app.Billbachao.recharge.plans.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.myjson.annotations.SerializedName;

/**
 * Created by mihir on 02-05-2016.
 * Contains required fields for validation of plans, similar to planitem and detailed item
 * Always use this class for final payment.
 */
public class PlanValidationItem implements Parcelable {

    public static final int BEST_PLAN = 1, WEB = 2, QUICK_RECHARGE = 7, REPEAT = 3, BROWSE_PLAN =
            4, FAVOURITE_PLAN = 5, GROUP_RECHARGE = 6, RECO_CARD = 8;

    final static String PLAN_ID = "p_id", PLAN_NAME = "p_name", PLAN_COST = "p_cost", PLAN_COUNT =
            "p_count", PLAN_VALIDITY = "p_validity", PLAN_TYPE
            = "p_type", PLAN_MOB_NO = "p_mobNo", PLAN_OP_ID = "p_opId", PLAN_CIRCLE_ID =
            "p_cId", PLAN_DESCRIPTION = "p_desc", PLAN_ADDED_FROM = "addedFrom";

    public static final String PREPAID = "PREPAID", POSTPAID = "POSTPAID";

    @SerializedName(PLAN_ID)
    int id = -1;

    @SerializedName(PLAN_NAME)
    String title;

    @SerializedName(PLAN_DESCRIPTION)
    String description;

    @SerializedName(PLAN_COST)
    int value;

    @SerializedName(PLAN_VALIDITY)
    String validity;

    // Dont confuse this with plan category, this is for prepaid/postpaid
    @SerializedName(PLAN_TYPE)
    private String type;

    @SerializedName(PLAN_COUNT)
    int planCount = 1;

    @SerializedName(PLAN_MOB_NO)
    String mobileNo;

    @SerializedName(PLAN_OP_ID)
    String operatorId;

    @SerializedName(PLAN_CIRCLE_ID)
    String circleId;

    @SerializedName(PLAN_ADDED_FROM)
    int addedFrom;

    public PlanValidationItem(int id, String title, String description, int value, String
            validity, boolean isPrepaid, int planCount, String mobileNo, String operatorId, String
                                      circleId, int addedFrom) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.value = value;
        this.validity = validity;
        this.type = isPrepaid ? PREPAID : POSTPAID;
        this.planCount = planCount;
        this.mobileNo = mobileNo;
        this.operatorId = operatorId;
        this.circleId = circleId;
        this.addedFrom = addedFrom;
    }

    protected PlanValidationItem(Parcel in) {
        id = in.readInt();
        title = in.readString();
        description = in.readString();
        value = in.readInt();
        validity = in.readString();
        type = in.readString();
        planCount = in.readInt();
        mobileNo = in.readString();
        operatorId = in.readString();
        circleId = in.readString();
        addedFrom = in.readInt();
    }

    public PlanValidationItem(PlanItem planItem, String mobile, String operatorId, String
            circleId, boolean isPrepaid, int addedFrom) {
        id = planItem.getId();
        title = planItem.getTitle();
        description = planItem.getDescription();
        value = planItem.getValue();
        validity = planItem.getValidity();
        type = isPrepaid ? PREPAID : POSTPAID;
        planCount = planItem.getPlanCount();
        mobileNo = mobile;
        this.operatorId = operatorId;
        this.circleId = circleId;
        this.addedFrom = addedFrom;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeInt(value);
        dest.writeString(validity);
        dest.writeString(type);
        dest.writeInt(planCount);
        dest.writeString(mobileNo);
        dest.writeString(operatorId);
        dest.writeString(circleId);
        dest.writeInt(addedFrom);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PlanValidationItem> CREATOR = new Creator<PlanValidationItem>() {
        @Override
        public PlanValidationItem createFromParcel(Parcel in) {
            return new PlanValidationItem(in);
        }

        @Override
        public PlanValidationItem[] newArray(int size) {
            return new PlanValidationItem[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getValue() {
        return value;
    }

    public String getValidity() {
        return validity;
    }

    public String getType() {
        return type;
    }

    public int getPlanCount() {
        return planCount;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public String getCircleId() {
        return circleId;
    }

    public int getAddedFrom() {
        return addedFrom;
    }
}
