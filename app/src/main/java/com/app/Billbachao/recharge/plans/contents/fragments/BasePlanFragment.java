package com.app.Billbachao.recharge.plans.contents.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.recharge.plans.model.PlanItem;
import com.app.Billbachao.utils.ConstantUtils;
import com.app.Billbachao.utils.PreferenceUtils;

import java.util.ArrayList;

/**
 * Created by mihir on 31-08-2015.
 */
public abstract class BasePlanFragment<T extends Parcelable> extends Fragment {

    public static final String DATA = "data";

    RecyclerView mRecyclerView;

    String mOperatorId, mCircleId, mAmount, mMobileNumber;

    TextView mEmptyView;

    ProgressBar mProgressBar;

    boolean mLaunchForSelf = false;

    PlanSelectorListener mPlanListener;

    protected ArrayList<T> mPlansList;

    protected RecyclerView.Adapter mPlansAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            mCircleId = arguments.getString(PreferenceUtils.CIRCLE);
            mOperatorId = arguments.getString(PreferenceUtils.OPERATOR);
            mAmount = arguments.getString(PreferenceUtils.AMOUNT);
            mLaunchForSelf = arguments.getBoolean(ConstantUtils.LAUNCH_SELF_PLANS);
        }
        if (mLaunchForSelf) {
            mMobileNumber = PreferenceUtils.getMobileNumber(getActivity());
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View containerView = inflater.inflate(R.layout.plans_list_fragment, container, false);
        initView(containerView);
        return containerView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(DATA)) {
                mPlansList = savedInstanceState.getParcelableArrayList(DATA);
                loadData();
                return;
            }
        }
        // Volley fetch
        fetchPlanList();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mPlanListener = (PlanSelectorListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mPlanListener = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mPlansList != null) {
            outState.putParcelableArrayList(DATA, mPlansList);
        }
    }

    void initView(View containerView) {
        mRecyclerView = (RecyclerView) containerView.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mEmptyView = (TextView) containerView.findViewById(android.R.id.empty);
        mProgressBar = (ProgressBar) containerView.findViewById(R.id.progress);
    }

    void loadData() {
        mPlansAdapter = getAdapter();
        mPlansAdapter.notifyDataSetChanged();
        mRecyclerView.setAdapter(mPlansAdapter);
        updateListVisibility();
    }

    protected abstract RecyclerView.Adapter getAdapter();


    void setEmptyText(String emptyText) {
        mEmptyView.setText(emptyText);
        updateListVisibility();
    }

    void updateListVisibility() {
        boolean isValidData = mPlansList != null && mPlansList.size() > 0;
        mProgressBar.setVisibility(View.GONE);
        mEmptyView.setVisibility(isValidData ? View.GONE : View.VISIBLE);
        mRecyclerView.setVisibility(isValidData ? View.VISIBLE : View.GONE);
    }

    abstract void fetchPlanList();

    public interface PlanSelectorListener {
        void onPlanSelected(PlanItem plan, boolean isSelectedFromRecent);
    }
}
