package com.app.Billbachao.recharge.plans.model;

import android.os.Parcel;

/**
 * Created by mihir on 03-05-2016.
 * Used as a model to store Plan db info
 */
public class PlanCartDBItem extends PlanValidationItem {

    transient String dataMb;

    public PlanCartDBItem(int id, String title, String description, int value, String validity, boolean isPrepaid, int planCount, String mobileNo, String operatorId, String circleId, int addedFrom) {
        super(id, title, description, value, validity, isPrepaid, planCount, mobileNo, operatorId, circleId, addedFrom);
    }

    protected PlanCartDBItem(Parcel in) {
        super(in);
        dataMb = in.readString();
    }

    public PlanCartDBItem(PlanItem item, String mMobileNumber, String mOperatorId, String mCircleId, boolean isPrepaid, int addedFrom) {
        super(item, mMobileNumber, mOperatorId, mCircleId, isPrepaid, addedFrom);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(dataMb);
    }

    public static final Creator<PlanCartDBItem> CREATOR = new Creator<PlanCartDBItem>() {
        @Override
        public PlanCartDBItem createFromParcel(Parcel in) {
            return new PlanCartDBItem(in);
        }

        @Override
        public PlanCartDBItem[] newArray(int size) {
            return new PlanCartDBItem[size];
        }
    };

    public void setDataMb(String dataMb) {
        this.dataMb = dataMb;
    }

    public String getDataMb() {
        return dataMb;
    }
}
