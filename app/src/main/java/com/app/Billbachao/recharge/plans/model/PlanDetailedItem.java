package com.app.Billbachao.recharge.plans.model;

import android.os.Parcel;

import com.google.myjson.JsonElement;
import com.google.myjson.JsonObject;
import com.google.myjson.JsonSerializationContext;
import com.google.myjson.JsonSerializer;

import java.lang.reflect.Type;

/**
 * Created by mihir on 20-08-2015.
 */
public class PlanDetailedItem extends PlanItem {

    String mobile;
    int circleId;
    int operatorId;
    int addedFrom;
    boolean isPrepaid = true;

    PlanDetailedItem(Parcel source) {
        super(source);
        mobile = source.readString();
        circleId = source.readInt();
        operatorId = source.readInt();
        isPrepaid = source.readByte() != 0;
        addedFrom = source.readInt();

    }

    public PlanDetailedItem(String mobile, int circleId, int operatorId, boolean isPrepaid, PlanItem
            planItem, int addedFrom) {
        super(planItem);
        this.mobile = mobile;
        this.circleId = circleId;
        this.operatorId = operatorId;
        this.addedFrom = addedFrom;
        this.isPrepaid = isPrepaid;
    }

    public int getOperatorId() {
        return operatorId;
    }

    public int getCircleId() {
        return circleId;
    }

    public String getMobile() {
        return mobile;
    }

    public int getAddedFrom() {
        return addedFrom;
    }

    public void setAddedFrom(int addedFrom) {
        this.addedFrom = addedFrom;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(mobile);
        dest.writeInt(circleId);
        dest.writeInt(operatorId);
        dest.writeByte((byte) (isPrepaid ? 1 : 0));
        dest.writeInt(addedFrom);
    }

    public static final Creator<PlanDetailedItem> CREATOR = new Creator<PlanDetailedItem>() {

        @Override
        public PlanDetailedItem createFromParcel(Parcel source) {
            return new PlanDetailedItem(source);
        }

        @Override
        public PlanDetailedItem[] newArray(int size) {
            return new PlanDetailedItem[size];
        }

    };

    static class PlanDetailedItemSerializer implements JsonSerializer<PlanDetailedItem> {

        final static String PLAN_ID = "p_id", PLAN_NAME = "p_name", PLAN_COST = "p_cost", PLAN_COUNT =
                "p_count", PLAN_VALIDITY = "p_validity", PLAN_TYPE
                = "p_type", PLAN_MOB_NO = "p_mobNo", PLAN_OP_ID = "p_opId", PLAN_CIRCLE_ID =
                "p_cId", PLAN_DESCRIPTION = "p_desc", PLAN_ADDED_FROM = "addedFrom", PREPAID = "PREPAID", POSTPAID = "POSTPAID";

        @Override
        public JsonElement serialize(PlanDetailedItem planDetailedItem, Type type, JsonSerializationContext jsonSerializationContext) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty(PLAN_ID, planDetailedItem.getId());
            jsonObject.addProperty(PLAN_NAME, planDetailedItem.getTitle());
            jsonObject.addProperty(PLAN_COST, String.valueOf(planDetailedItem.getValue()));
            jsonObject.addProperty(PLAN_COUNT, String.valueOf(planDetailedItem.getPlanCount()));
            jsonObject.addProperty(PLAN_VALIDITY, planDetailedItem.getValidity());
            jsonObject.addProperty(PLAN_TYPE, planDetailedItem.isPrepaid ? PREPAID : POSTPAID);
            jsonObject.addProperty(PLAN_MOB_NO, planDetailedItem.getMobile());
            jsonObject.addProperty(PLAN_OP_ID, String.valueOf(planDetailedItem.getOperatorId()));
            jsonObject.addProperty(PLAN_CIRCLE_ID, String.valueOf(planDetailedItem.getCircleId()));
            jsonObject.addProperty(PLAN_DESCRIPTION, planDetailedItem.getDescription());
            jsonObject.addProperty(PLAN_ADDED_FROM, planDetailedItem.getAddedFrom());
            return null;
        }
    }
}
