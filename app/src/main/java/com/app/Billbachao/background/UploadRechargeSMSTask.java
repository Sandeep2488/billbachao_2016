package com.app.Billbachao.background;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.apis.ApiUtils;
import com.app.Billbachao.apis.UssdDetailsApi;
import com.app.Billbachao.model.RechargeSMSBean;
import com.app.Billbachao.utils.TaskPerformedUtils;
import com.app.Billbachao.volley.request.NetworkJSONRequest;
import com.app.Billbachao.volley.utils.VolleySingleTon;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by BB-001 on 5/9/2016.
 */
public class UploadRechargeSMSTask extends BaseActivity {

    Context mcontext;
    String mode;
    static String RECHARGE_SMS_UPLOADED = "recharge_sms_uploaded", CALL_LEG_MODE = "Recharge_SMS";
    static final String CALL_SMS_TO = "call_sms_to", ACTIVITY = "activity", CALL_LEG = "call_leg", TOTAL_DURATION = "total_duration", TOTAL_COST = "total_cost", DATA_USED = "data_used", DATA_LEFT = "data_left", PACK_EXP = "pack_exp", BAL_LEFT = "bal_left", CAPTURE_DATE = "capture_date", USSD_TEXT = "ussd_text";
    public static final String TAG = UploadRechargeSMSTask.class.getSimpleName();

    public UploadRechargeSMSTask(Context ctx, String mode) {
        this.mcontext = ctx;
        this.mode = mode;
    }

    public void uploadA2PSMS() {
        SmsReader smsReader = new SmsReader();

        List<RechargeSMSBean> smsBeanList = smsReader.fetchSMS(mcontext);

        JSONArray sms_details = new JSONArray();
        try {

            if (smsBeanList.size() > 0) {
                for (int i = 0; i < smsBeanList.size(); i++) {
                    RechargeSMSBean sms_bean = smsBeanList.get(i);
                    JSONObject jsonOBJ = new JSONObject();

                    jsonOBJ.put(CALL_SMS_TO, sms_bean.getNumber());
                    jsonOBJ.put(ACTIVITY, sms_bean.getActivity());
                    jsonOBJ.put(CALL_LEG, CALL_LEG_MODE);
                    jsonOBJ.put(TOTAL_DURATION, sms_bean.getTotal_duration());
                    jsonOBJ.put(TOTAL_COST, sms_bean.getTotal_cost());
                    jsonOBJ.put(DATA_USED, sms_bean.getData_used());
                    jsonOBJ.put(DATA_LEFT, sms_bean.getData_left());
                    jsonOBJ.put(PACK_EXP, sms_bean.getPack_exp());
                    jsonOBJ.put(BAL_LEFT, sms_bean.getBal_left());
                    jsonOBJ.put(CAPTURE_DATE, sms_bean.getCapture_date());
                    jsonOBJ.put(USSD_TEXT, sms_bean.getBody());

                    sms_details.put(jsonOBJ);
                    jsonOBJ = null;
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (smsBeanList.size() > 0) {
            final NetworkJSONRequest jsonRequest = new
                    NetworkJSONRequest(Request.Method.POST, UssdDetailsApi.URL, UssdDetailsApi.getParams(mcontext, sms_details.toString()), new Response.Listener<JSONObject>() {


                @Override
                public void onResponse(JSONObject jsonObject) {
                    if (ApiUtils.isValidResponse(jsonObject.toString())) {
                        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mcontext);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean(RECHARGE_SMS_UPLOADED, true).apply();
                        TaskPerformedUtils.taskPerformed(mcontext, TaskPerformedUtils.TASK_PUSH_RECHARGE_SMS_DATA);
                    } else {
                        TaskPerformedUtils.updateTaskPending(mcontext, TaskPerformedUtils.TASK_PUSH_RECHARGE_SMS_DATA);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    TaskPerformedUtils.updateTaskPending(mcontext, TaskPerformedUtils.TASK_PUSH_RECHARGE_SMS_DATA);
                }
            });
            VolleySingleTon.getInstance(mcontext).addToRequestQueue(jsonRequest, TAG);

        }
    }
}
