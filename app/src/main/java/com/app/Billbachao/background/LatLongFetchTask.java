package com.app.Billbachao.background;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.model.LocationFetchResponse;
import com.app.Billbachao.registration.contents.background.VerificationSenderTask;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.utils.LocationUtils;
import com.app.Billbachao.volley.request.NetworkGsonRequest;
import com.app.Billbachao.volley.utils.VolleySingleTon;

/**
 * Created by BB-001 on 3/11/2016.
 */
public class LatLongFetchTask {


    Context context;
    String ipAddress = null;
    String URL = "http://www.geoplugin.net/json.gp?ip=";

    public LatLongFetchTask(Context context, String ipAddress) {
        this.context = context;
        this.ipAddress = ipAddress;
    }

    public void getUserLocation() {
        NetworkGsonRequest<LocationFetchResponse> locSender = new
                NetworkGsonRequest<>(Request.Method.GET, URL, LocationFetchResponse.class, null, new Response.Listener<LocationFetchResponse>() {
            @Override
            public void onResponse(LocationFetchResponse locationFetchResponse) {
                if (locationFetchResponse != null) {

                    String geoplugin_status = locationFetchResponse.getStatus();
                    if (geoplugin_status.equalsIgnoreCase("200")) {
                        String geoplugin_city = locationFetchResponse.getCity();
                        String geoplugin_latitude = locationFetchResponse.getLatitude();
                        String geoplugin_longitude = locationFetchResponse.getLongitude();
                        ILog.d("User Location:", geoplugin_latitude + geoplugin_longitude + geoplugin_city);
                        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString(LocationUtils.LATITUDE, geoplugin_latitude);
                        editor.putString(LocationUtils.LONGITUDE, geoplugin_longitude);
                        editor.putString(LocationUtils.USER_REG_CITY, geoplugin_city);
                        editor.apply();
                    }
                }

                new VerificationSenderTask(context).trigger();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new VerificationSenderTask(context).trigger();
            }
        }, false);
        VolleySingleTon.getInstance(context).addToRequestQueue(locSender);

    }
}

