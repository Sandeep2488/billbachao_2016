package com.app.Billbachao.background;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.app.Billbachao.BuildConfig;
import com.app.Billbachao.common.ConnectionManager;
import com.app.Billbachao.registration.contents.background.VerificationSenderTask;
import com.app.Billbachao.usagelog.LogHelper;
import com.app.Billbachao.usagelog.notifications.UsageNotificationReceiver;
import com.app.Billbachao.utils.PreferenceUtils;
import com.app.Billbachao.utils.TaskPerformedUtils;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by mihir on 12/5/2016.
 */
public class TaskScheduler {

    public static void verifyUser(Context context) {
        if (PreferenceUtils.isSMSReceived(context) && !PreferenceUtils.isMobVerified(context)) {
            new VerificationSenderTask(context).trigger();
        }
    }

    public static void performWifiTasks(Context context) {
        if (PreferenceUtils.isRegistered(context)) {
            if (PreferenceUtils.isPrepaid(context)) {
                long lastPerformedTime = TaskPerformedUtils.getLastPerformedTime(context, TaskPerformedUtils.TASK_PUSH_RECHARGE_SMS_DATA);
                long nextTrigger = AlarmManager.INTERVAL_DAY * 7 + lastPerformedTime;
                if (nextTrigger <= System.currentTimeMillis()) {
                    if (ConnectionManager.isConnected(context)) {
                       new UploadRechargeSMSTask(context, PreferenceUtils.SCHEDULER).uploadA2PSMS();
                    }
                }
            }
        }
    }

    public static void setUsageNotificationAlarm(Context context) {

        Intent usageNotifier = new Intent(context, UsageNotificationReceiver.class);
        PendingIntent recurringUsageNotifier = PendingIntent.getBroadcast(context,
                0, usageNotifier, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        if (PreferenceUtils.isRegistered(context) &&
                LogHelper.isDailyUsageActive(context)) {
            Calendar updateTime = Calendar.getInstance();
            updateTime.setTimeZone(TimeZone.getDefault());
            updateTime.set(Calendar.HOUR_OF_DAY, 22);
            updateTime.set(Calendar.MINUTE, 00);
            if (updateTime.getTimeInMillis() < System.currentTimeMillis()) {
                updateTime.add(Calendar.DATE, 1);
            }

            alarms.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                    updateTime.getTimeInMillis(),
                    AlarmManager.INTERVAL_DAY / (BuildConfig.DEBUG ? 3 : 1),
                    recurringUsageNotifier);
        } else {
            alarms.cancel(recurringUsageNotifier);
        }
    }

}
