package com.app.Billbachao.background;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.model.IpFetchResponse;
import com.app.Billbachao.registration.contents.background.VerificationSenderTask;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.volley.request.NetworkGsonRequest;
import com.app.Billbachao.volley.utils.VolleySingleTon;

/**
 * Created by BB-001 on 3/18/2016.
 */
public class IpAddressFetchTask {
    private String ipAddress = "";
    Context context;
    public static final String URL = "https://api.ipify.org?format=json";


    public IpAddressFetchTask(Context context) {
        this.context = context;
    }

    public void fetchIp() {
        NetworkGsonRequest<IpFetchResponse> ipSender = new
                NetworkGsonRequest<>(Request.Method.GET, URL, IpFetchResponse.class, null, new Response
                .Listener<IpFetchResponse>() {
            @Override
            public void onResponse(IpFetchResponse ipFetchResponse) {
                if (ipFetchResponse != null) {
                    ipAddress = ipFetchResponse.getIpAddress();
                    ILog.d("Ipaddress", ipAddress);
                    if (ipAddress != null) {
                        new LatLongFetchTask(context, ipAddress).getUserLocation();
                    }

                }else {
                    new VerificationSenderTask(context).trigger();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new VerificationSenderTask(context).trigger();
            }
        });
        VolleySingleTon.getInstance(context).addToRequestQueue(ipSender);

    }
}

