package com.app.Billbachao.usagelog.library.parser.base.aircel;

/**
 * Created by mihir on 23-11-2015.
 */
public interface AircelConstants {

    String VOICE_COST = "VOICE COST", CALL_COST = "CALL COST", DURATION = "DURATION", SMS_COST =
            "SMS COST", SESSION = "SESSION", VOLUME = "VOLUME", DATA = "DATA", COST = "COST";

}
