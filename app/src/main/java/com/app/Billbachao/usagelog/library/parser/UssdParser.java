package com.app.Billbachao.usagelog.library.parser;

import android.content.Context;
import android.os.Handler;

import com.app.Billbachao.usagelog.LogHelper;
import com.app.Billbachao.usagelog.helper.UsageAlertsHelper;
import com.app.Billbachao.usagelog.library.parser.base.BaseParser;
import com.app.Billbachao.usagelog.library.parser.base.ParserFactory;
import com.app.Billbachao.usagelog.library.parser.model.CostInfo;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by mihir on 30-10-2015.
 */
public class UssdParser {

    static final int USSD_LENGTH = 10;

    Context mContext;

    public void process(Context context, String result) {
        if (!basicPass(result)) {
            return;
        }

        if (result != null || result.length() > USSD_LENGTH) {
            result = parseExtraString(result);
        }

        mContext = context;
        BaseParser parser = ParserFactory.getParser(context);
        if (parser != null) {
            if (parser.parse(result.toUpperCase())) {
                CostInfo costInfo = parser.getCostInfo();
                storeInfo(costInfo);
            }
        }
    }

    static boolean basicPass(String result) {

        if (result == null || result.length() < USSD_LENGTH) {
            return false;
        }

        return true;
    }

    void storeInfo(CostInfo info) {
        if (info != null) {

            updateBalanceInfo(info);

            switch (info.type) {
                case CostInfo.USSD:
                    storeUssdInfo(info);
                    break;
                case CostInfo.CALL:
                    storeCallInfo(info);
                    break;
                case CostInfo.DATA:
                    storeDataInfo(info);
                    break;
                case CostInfo.SMS:
                    storeSmsInfo(info);
                    break;
                case CostInfo.DATA_CHECK:
                    updateDataBalance(info);
                    break;
            }
        }
    }

    void storeUssdInfo(CostInfo info) {

    }

    void storeSmsInfo(final CostInfo info) {
        if (info.cost < 0)
            return;

        // Unfortunately, had to add delay because sms logs are not updating immediately
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                LogHelper.saveLastSmsLog(mContext);
                LogHelper.storeSmsCostInfo(mContext, info);
            }
        }, 2000);
    }

    void storeCallInfo(final CostInfo info) {
        if (info.cost < 0)
            return;

        // Unfortunately, had to add delay because call logs are not updating immediately
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                LogHelper.saveLastCallLog(mContext);
                LogHelper.storeCallCostInfo(mContext, info);
            }
        }, 2000);
    }

    void storeDataInfo(CostInfo info) {
        LogHelper.storeDayWiseDataUsage(mContext, true);
        LogHelper.addToDayWiseDataCostOrConsumption(mContext, info.cost, info.dataUsed);
        LogHelper.storeAppWiseDataUsage(mContext);
        updateDataBalance(info);
    }

    void updateBalanceInfo(CostInfo info) {
        LogHelper.storeBalance(mContext, LogHelper.TYPE_CALL, info.mainBalance);
        UsageAlertsHelper.checkMainBalanceThreshold(mContext, (int) info.mainBalance);
    }

    void updateDataBalance(CostInfo info) {
        LogHelper.storeBalance(mContext, LogHelper.TYPE_DATA, info.dataLeft);
        if (info.dataLeft >= 0) {
            UsageAlertsHelper.checkDataThreshold(mContext, (int) info.dataLeft);
        }
    }


    String parseExtraString(String data) {

        int index = 0;
        int length;
        String sentence;

        while (data.contains(",") || data.contains(";") || data.contains(".")) {

            boolean flag = false;
            if (data.contains(";")) {
                index = data.lastIndexOf(";");
            } else if (data.contains(",") && data.contains(".")) {
                if (data.lastIndexOf(",") < data.lastIndexOf(".")) {
                    index = data.lastIndexOf(".");
                } else {
                    index = data.lastIndexOf(",");
                }
            } else if (data.contains(",")) {
                index = data.lastIndexOf(",");
            } else if (data.contains(".")) {
                index = data.lastIndexOf(".");
            }

            length = data.length();
            sentence = data.substring(index, length);
            Pattern p = Pattern.compile("\\d*\\.\\d+");
            Matcher m = p.matcher(sentence);
            if (m.find()) {
                flag = true;
            } else {
                data = data.substring(0, index);
            }
            if (flag) {
                break;
            }
        }
        return data;
    }

}
