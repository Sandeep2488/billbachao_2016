package com.app.Billbachao.usagelog.notifications;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.app.Billbachao.R;
import com.app.Billbachao.dashboard.DashboardActivity;
import com.app.Billbachao.notifications.inapp.SimpleNotification;
import com.app.Billbachao.usagelog.contents.UsageLogActivity;

/**
 * Created by mihir on 19-11-2015.
 */
public class UsageNotification extends SimpleNotification {

    String mUsageText;
    int notificationType;
    public UsageNotification(Context context, String usageText, int notificationType) {
        super(context, context.getString(R.string.usage_notification_title), usageText);
        mUsageText = usageText;
        this.notificationType = notificationType;
    }

    @Override
    protected NotificationCompat.Builder getNotificationBuilder() {
        Intent intent;
        if (notificationType == UsageNotificationReceiver.PREPAID_USSD_ON) {
            intent = new Intent(mContext, UsageLogActivity.class);
            intent.putExtra(UsageLogActivity.LAUNCH_FROM_NOTIFICATION, true);
        } else if (notificationType == UsageNotificationReceiver.PREPAID_USSD_OFF) {
            intent = new Intent(mContext, DashboardActivity.class);
            intent.setAction("android.intent.action.MAIN");
            intent.putExtra(UsageLogActivity.LAUNCH_FROM_NOTIFICATION, "true");
        } else {
            intent = new Intent(mContext, DashboardActivity.class);
        }
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        return super.getNotificationBuilder().setContentIntent(pendingIntent);
    }

    @Override
    public void show() {
        super.show();
        // TODO EventCt
    }
}
