package com.app.Billbachao.usagelog.library.parser.base.reliance;

/**
 * Created by mihir on 03-11-2015.
 */
public interface RelianceConstants {

    String CALL= "CALL", LAST = "LAST", DATA = "DATA", SMS = "SMS", MAIN = "MAIN";

    String VAL = "VAL";
}
