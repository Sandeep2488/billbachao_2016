package com.app.Billbachao.usagelog.library.parser.base.videocon;

/**
 * Created by mihir on 02-11-2015.
 */
public interface VideoconConstants {

    String COST = "COST", CALL = "CALL", SMS = "SMS", DATA = "DATA", SESSION= "SESSION", INTERNET = "INTERNET", CHARGE = "CHARGE";

    String DURATION = "DURATION", VOL = "VOL", USED = "USED", CONSUMED = "CONSUMED", GPRS = "GPRS", USAGE = "USAGE";
}
