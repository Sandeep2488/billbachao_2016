package com.app.Billbachao.usagelog.notifications;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.app.Billbachao.usagelog.AccessibilityHelper;
import com.app.Billbachao.utils.AppUtils;
import com.app.Billbachao.utils.PreferenceUtils;

import java.util.Arrays;

/**
 * Created by mihir on 19-11-2015.
 */
public class UsageNotificationReceiver extends WakefulBroadcastReceiver {

    AccessibilityHelper accessibilityHelper;

    public static int PREPAID_USSD_ON = 1, PREPAID_USSD_OFF = 2, POSTPAID = 3;

    String[] days = {"0", "2", "6"};

    @Override
    public void onReceive(Context context, Intent intent) {


        String tickerText = "";
        accessibilityHelper = new AccessibilityHelper(context);
        UsageNotification notification = null;

        //prepaid and accessability permission is given
        if (PreferenceUtils.isPrepaid(context)) {

            if (accessibilityHelper.isEnabledAndRunning()) {

                tickerText = new UsageNotificationGenerator(context).generateText();

                notification = new UsageNotification(context, tickerText, PREPAID_USSD_ON);

            } else {
                //prepaid and accessability service is not given, send user notification on 1st,3rd and 7th day.
                String dayFromInstallation = String.valueOf(AppUtils.getDaysSinceInstalled(context));
                if (isNotificationDay(days, dayFromInstallation)) {
                    tickerText = new PostPaidUsageNotificationGenerator(context).generateText();
                    notification = new UsageNotification(context, tickerText, PREPAID_USSD_OFF);
                }
            }
        }
        //postpaid user
        else {
            tickerText = new PostPaidUsageNotificationGenerator(context).generateText();
            notification = new UsageNotification(context, tickerText, POSTPAID);
        }

        if (tickerText.isEmpty())
            return;

        if (null != notification)
            notification.show();

        completeWakefulIntent(intent);
    }

    public boolean isNotificationDay(String[] arr, String targetValue) {
        return Arrays.asList(arr).contains(targetValue);
    }

}
