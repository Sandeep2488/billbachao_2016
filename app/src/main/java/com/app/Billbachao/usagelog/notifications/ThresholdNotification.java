package com.app.Billbachao.usagelog.notifications;

import android.content.Context;

import com.app.Billbachao.notifications.inapp.SimpleNotification;

/**
 * Created by mihir on 24-11-2015.
 */
public class ThresholdNotification extends SimpleNotification {

    public ThresholdNotification(Context context, String title) {
        super(context, title);
    }

    public ThresholdNotification(Context context, String title, String text) {
        super(context, title, text);
    }

    /*@Override
    protected NotificationCompat.Builder getNotificationBuilder() {
        Intent intent = new Intent(mContext, UsageLogActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return super.getNotificationBuilder().setContentIntent(pendingIntent);
    }*/
}
