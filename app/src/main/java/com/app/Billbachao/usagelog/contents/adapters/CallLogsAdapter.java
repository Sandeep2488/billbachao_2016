package com.app.Billbachao.usagelog.contents.adapters;

import android.content.Context;
import android.database.Cursor;
import android.provider.CallLog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.usagelog.provider.UsageLogsProvider;
import com.app.Billbachao.utils.DateUtils;

/**
 * Created by mihir on 21-10-2015.
 */
public class CallLogsAdapter extends BaseResourceCursorAdapter {

    int mNameIdx, mNumberIdx, mDurationIdx, mTimeDx, mCostIdx, mTypeIdx, mCallTypeIdx;

    boolean mShowLogsFromSystem;

    public CallLogsAdapter(Cursor cursor, Context context, boolean showLogsFromSystem) {
        super(cursor, context);
        mShowLogsFromSystem = showLogsFromSystem;
    }

    public void initIndices(Cursor cursor) {
        if (mShowLogsFromSystem) {
            mNameIdx = cursor.getColumnIndex(UsageLogsProvider.CallColumns.NAME);
            mNumberIdx = cursor.getColumnIndex(UsageLogsProvider.CallColumns.NUMBER);
            mDurationIdx = cursor.getColumnIndex(UsageLogsProvider.CallColumns.DURATION);
            mTimeDx = cursor.getColumnIndex(UsageLogsProvider.CallColumns.DATE);
            mCostIdx = cursor.getColumnIndex(UsageLogsProvider.CallColumns.COST);
            mTypeIdx = cursor.getColumnIndex(UsageLogsProvider.CallColumns.USAGE_TYPE);
            mCallTypeIdx = cursor.getColumnIndex(UsageLogsProvider.CallColumns.CALL_TYPE);
        } else {
            mNameIdx = cursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
            mNumberIdx = cursor.getColumnIndex(CallLog.Calls.NUMBER);
            mDurationIdx = cursor.getColumnIndex(CallLog.Calls.DURATION);
            mTimeDx = cursor.getColumnIndex(CallLog.Calls.DATE);
        }
    }

    @Override
    protected Cursor getChildrenCursor(Cursor groupCursor) {
        return null;
    }

    @Override
    protected View newGroupView(Context context, Cursor cursor, boolean isExpanded, ViewGroup
            parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.call_log_row,
                parent, false);

        ViewHolder holder = new ViewHolder();
        view.setTag(holder);
        holder.name = (TextView) view.findViewById(R.id.name);
        holder.number = (TextView) view.findViewById(R.id.number);
        holder.duration = (TextView) view.findViewById(R.id.duration);
        holder.cost = (TextView) view.findViewById(R.id.cost);
        holder.callRate = (TextView) view.findViewById(R.id.call_rate);
        holder.icon = (ImageView) view.findViewById(R.id.logs_icon_type);
        holder.callTypeIcon = (ImageView) view.findViewById(R.id.call_type);
        holder.date = (TextView) view.findViewById(R.id.date);
        return view;
    }

    @Override
    protected void bindGroupView(View view, Context context, Cursor cursor, boolean isExpanded) {
        ViewHolder holder = (ViewHolder) view.getTag();
        boolean isCall = true;
        if (mShowLogsFromSystem) {
            isCall = cursor.getInt(mTypeIdx) == UsageLogsProvider.CallColumns.TYPE_CALL;
        }

        String name = cursor.getString(mNameIdx);
        if (name != null && !name.isEmpty()) {
            holder.name.setText(name);
            holder.name.setVisibility(View.VISIBLE);
        } else {
            holder.name.setVisibility(View.GONE);
        }
        holder.number.setText(cursor.getString(mNumberIdx));
        holder.date.setText(DateUtils.convertTimeToDate(cursor.getLong(mTimeDx), DateUtils
                .DATE_FORMAT));

        if (mShowLogsFromSystem) {
            float cost = cursor.getFloat(mCostIdx);
            if (cost < 0) {
                holder.cost.setText(R.string.inr_invalid);
            } else {
                holder.cost.setText(context.getString(R.string.inr_n2, cost));
            }
        }
        if (isCall) {
            long durationSeconds = cursor.getLong(mDurationIdx);
            holder.duration.setText(DateUtils.getDurationString(context, durationSeconds));
            holder.callTypeIcon.setImageResource(cursor.getInt(mCallTypeIdx) == CallLog.Calls
                    .OUTGOING_TYPE ? R.drawable.ic_outgoing : R.drawable.ic_incoming);
        }
        holder.duration.setVisibility(isCall ? View.VISIBLE : View.GONE);
        holder.callTypeIcon.setVisibility(isCall ? View.VISIBLE : View.GONE);
        holder.icon.setImageResource(isCall ? R.drawable.ic_call : R.drawable.ic_message);
    }

    @Override
    protected View newChildView(Context context, Cursor cursor, boolean isLastChild, ViewGroup parent) {
        return null;
    }

    @Override
    protected void bindChildView(View view, Context context, Cursor cursor, boolean isLastChild) {

    }

    class ViewHolder {

        TextView name, number, duration;

        TextView circleOperator;

        TextView date, cost, callRate;

        ImageView icon, callTypeIcon;
    }
}
