package com.app.Billbachao.usagelog.library.parser.base;

/**
 * Created by mihir on 02-11-2015.
 */
public interface RegexConstants {

    String NUMBER = "\\d+", SINGLE_TIME_SPLIT = "\\d", NORMAL_TIME_SPLIT = "[0-6]\\d",
            TIME_MM_SS = NORMAL_TIME_SPLIT + ":" + NORMAL_TIME_SPLIT, TIME_HH_MM_SS = NORMAL_TIME_SPLIT + ":" + TIME_MM_SS;

    String BASE_INT = "\\d+", BASE_FLOAT = "\\d+\\.\\d+", BASE_STRING = "[A-Z]+";

    String RS = "RS", RS_PATTERN = "RS ?\\.? ?", RS_XX_YY = RS_PATTERN + BASE_FLOAT, XX_YY_INR = BASE_FLOAT + " ?INR";

    String DATE_DD_MM_YYYY = "\\d{2}-\\d{2}-\\d4";

    String DATA_PATTERN_AFTER = BASE_FLOAT + " ?[KM]B", DATA_PATTERN_BEFORE = "[KM]B ?:? ?" +
            BASE_FLOAT, DATA_PATTERN = "(" + DATA_PATTERN_AFTER + "|" + DATA_PATTERN_BEFORE + ")";

    // int data patterns
    String INT_DATA_PATTERN_AFTER = BASE_INT + " ?[KM]B", INT_DATA_PATTERN_BEFORE = "[KM]B ?: ?" +
            BASE_INT, INT_DATA_PATTERN = "(" + INT_DATA_PATTERN_AFTER + "|" + INT_DATA_PATTERN_BEFORE + ")";

    String IDEA_CHRG = "CH[A]?RG", BAL = "BAL", BALANCE = "BALANCE";
}
