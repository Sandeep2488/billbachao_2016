package com.app.Billbachao.usagelog.model;

import com.app.Billbachao.utils.DateUtils;
import com.google.myjson.annotations.SerializedName;

/**
 * Created by mihir on 18-11-2015.
 */
public class DataUsageDetailInfo {

    public static final String DATA_USAGE_MB = "dataUseInMB", SPEND = "spend", TIME = "time";

    @SerializedName(DATA_USAGE_MB)
    int dataUsageInMb;

    @SerializedName(SPEND)
    int costPaisa;

    transient long time;

    // No use of this variable, except for GSON serialization
    @SerializedName(TIME)
    String timeString;

    public DataUsageDetailInfo(int dataUsageInMb, int costInPaisa, long time) {
        this.dataUsageInMb = dataUsageInMb;
        this.costPaisa = costInPaisa;
        this.time = time;
        timeString = DateUtils.convertDateFormat(String.valueOf(time), DateUtils
                .FORMAT_DATE_ONLY, DateUtils.FORMAT_YYYY_MM_DD);
    }

    public int getDataUsageInMb() {
        return dataUsageInMb;
    }

    public int getCostPaisa() {
        return costPaisa;
    }

    public long getTime() {
        return time;
    }
}
