package com.app.Billbachao.usagelog.helper;

import android.content.Context;
import android.database.Cursor;

import com.app.Billbachao.usagelog.model.DataUsageDetailInfo;
import com.app.Billbachao.usagelog.model.DayCostInfo;
import com.app.Billbachao.usagelog.model.UsageDetailsJsonInfo;
import com.app.Billbachao.usagelog.provider.UsageLogsProvider;
import com.app.Billbachao.utils.DateUtils;

import java.util.ArrayList;

/**
 * Created by mihir on 19-11-2015.
 */
public class DataUsageHelper {

    // Data logs
    public static void getDataLogsInfo(Context context, long time, UsageDetailsJsonInfo usageInfo) {
        String currentDate = DateUtils.convertTimeToDate(time, DateUtils.FORMAT_DATE_ONLY);
        Cursor cursor = context.getContentResolver().query(UsageLogsProvider.DayWiseDataColumns
                .CONTENT_URI, null, UsageLogsProvider.DayWiseDataColumns.DATE + " >= ?", new String[]{currentDate}, null);

        ArrayList<DataUsageDetailInfo> infoList = new ArrayList<>();

        if (cursor != null) {

            int totalCost = 0;

            if (cursor.moveToFirst()) {
                int dateIndex = cursor.getColumnIndex(UsageLogsProvider.DayWiseDataColumns.DATE);
                int ussdDataUsageIndex = cursor.getColumnIndex(UsageLogsProvider.DayWiseDataColumns.USSD_USAGE_MOBILE);
                int costIndex = cursor.getColumnIndex(UsageLogsProvider.DayWiseDataColumns.COST);
                int usageMobileIndex = cursor.getColumnIndex(UsageLogsProvider.DayWiseDataColumns.USAGE_MOBILE);

                do {
                    long date = cursor.getLong(dateIndex);
                    float cost = cursor.getFloat(costIndex);
                    float dataUsage = cursor.getFloat(ussdDataUsageIndex);
                    if (dataUsage == 0) {
                        dataUsage = cursor.getFloat(usageMobileIndex);
                    }

                    int costPaisa = (int) (cost * 100);

                    DataUsageDetailInfo info = new DataUsageDetailInfo((int) (dataUsage),
                            costPaisa, date);
                    infoList.add(info);
                } while(cursor.moveToNext());
            }
            cursor.close();

            // Convert to JSON arrays
            usageInfo.dataUsages = infoList;
            usageInfo.addCost(totalCost);
        }
    }

    public static DayCostInfo getDayUsageDetails(Context context) {
        DayCostInfo dayCostInfo = new DayCostInfo();
        String currentDate = DateUtils.convertTimeToDate(DateUtils.getBeforeDays(0), DateUtils
                .FORMAT_DATE_ONLY);
        Cursor cursor = context.getContentResolver().query(UsageLogsProvider.DayWiseDataColumns
                .CONTENT_URI, null, UsageLogsProvider.DayWiseDataColumns.DATE + " =?", new
                String[]{currentDate}, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                dayCostInfo.dataMb = cursor.getFloat(cursor.getColumnIndex(UsageLogsProvider
                        .DayWiseDataColumns.USSD_USAGE_MOBILE));
                dayCostInfo.cost = cursor.getFloat(cursor.getColumnIndex(UsageLogsProvider
                        .DayWiseDataColumns.COST));
            }
            cursor.close();
        }
        return dayCostInfo;
    }
}
