package com.app.Billbachao.usagelog.contents.utils;

import android.net.Uri;
import android.provider.CallLog;

import com.app.Billbachao.usagelog.provider.UsageLogsProvider;

/**
 * Created by mihir on 21-10-2015.
 */
public class ListUtils {

    public abstract static class QueryArgs {

        protected QueryArgs() {

            projection = null;

            selection = null;

            selectionArgs = null;

            sortOrder = null;
        }

        public Uri uri;

        public String[] projection;

        public String selection;

        public String[] selectionArgs;

        public String sortOrder;
    }

    public static class CallLogsArgs extends QueryArgs {
        public CallLogsArgs() {
            super();

            uri = CallLog.Calls.CONTENT_URI;

            selection = CallLog.Calls.TYPE + "=?" + " AND " + CallLog.Calls.DURATION + "> ?";

            selectionArgs = new String[]{String.valueOf(CallLog.Calls
                    .OUTGOING_TYPE), "0"};

            sortOrder = CallLog.Calls.DEFAULT_SORT_ORDER;
        }
    }

    public static class AppCallLogsArgs extends QueryArgs {
        public AppCallLogsArgs() {
            super();

            uri = UsageLogsProvider.CallColumns.CONTENT_URI;

            sortOrder = UsageLogsProvider.CallColumns.DEFAULT_SORT_ORDER;

        }
    }

    public static class DataLogsArgs extends QueryArgs {
        public DataLogsArgs() {
            super();

            uri = UsageLogsProvider.DataColumns.CONTENT_URI;

            projection = null;

            selection = null;

            selectionArgs = null;

            sortOrder = UsageLogsProvider.DataColumns.DEFAULT_SORT_ORDER;
        }
    }

    public static class RawDataLogsArgs extends QueryArgs {
        public RawDataLogsArgs() {
            super();

            uri = UsageLogsProvider.RawDataColumns.CONTENT_URI;
        }
    }

    public static class DayWiseDataArgs extends QueryArgs {
        public DayWiseDataArgs() {
            super();

            uri = UsageLogsProvider.DayWiseDataColumns.CONTENT_URI;

            sortOrder = UsageLogsProvider.DayWiseDataColumns.DEFAULT_SORT_ORDER;
        }
    }
}
