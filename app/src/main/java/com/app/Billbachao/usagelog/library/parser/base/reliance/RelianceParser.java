package com.app.Billbachao.usagelog.library.parser.base.reliance;

import com.app.Billbachao.usagelog.library.parser.base.BaseParser;
import com.app.Billbachao.usagelog.library.parser.model.CostInfo;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by mihir on 03-11-2015.
 */
public class RelianceParser extends BaseParser implements RelianceConstants {

    @Override
    public boolean parse(String data) {

        if (data.contains(LAST) && data.contains(CALL)) {
            return parseCallUsage(data);
        } else if (data.contains(DATA)) {
            return parseDataUsage(data);
        } else if (data.contains(SMS)) {
            return parseSmsUsage(data);
        } else if (data.contains(MAIN) && (data.contains(BALANCE) || data.contains(BAL))) {
            return parseUssdData(data);
        }
        return false;
    }

    @Override
    protected boolean parseDataUsage(String data) {
        // TODO Format not clear, need to get correct one

        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.DATA;

        boolean isDataPack = true;

        if(data.contains(MAIN)) {
            Pattern amountPattern = Pattern.compile(RS_XX_YY);
            Matcher amountMatcher = amountPattern.matcher(data);
            if (amountMatcher.find()) {
                isDataPack = false;
                mCostInfo.mainBalance = extractAmountFromRupees(amountMatcher.group());
            }
        }

        float amount[] = new float[2];

        Pattern floatDigitPattern = Pattern.compile(BASE_FLOAT);
        Matcher floatDigitMatcher = floatDigitPattern.matcher(data);

        boolean dataFound = false;
        for (int i = 0; i < amount.length; i++) {
            if (floatDigitMatcher.find()) {
                dataFound = true;
                try {
                    amount[i] = Float.parseFloat(floatDigitMatcher.group());
                } catch (NumberFormatException ex) {
                    ex.printStackTrace();
                }
            }
        }

        mCostInfo.dataUsed = amount[0] / MB_KB_RATIO;
        if (isDataPack) {
            mCostInfo.dataLeft = amount[1] / MB_KB_RATIO;
        }

        return dataFound;
    }

    @Override
    protected boolean parseCallUsage(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.CALL;

        if (findDuration(data)) {
            return findCostAndBalance(data);
        }

        return false;
    }

    @Override
    protected boolean parseSmsUsage(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.SMS;

        return findCostAndBalance(data);
    }

    @Override
    protected boolean parseUssdData(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.USSD;

        float dataBalance = extractDataMbKb(data, 1)[0];
        if (dataBalance >= 0) {
            mCostInfo.type = CostInfo.DATA_CHECK;
            mCostInfo.dataLeft = dataBalance;
            return true;
        } else {
            float mainBalance = extractINRGroups(data, 1)[0];
            if (mainBalance >= 0) {
                mCostInfo.mainBalance = mainBalance;
                return true;
            }
        }

        return false;
    }

    boolean findDuration(String data) {
        Pattern durationPatternHMS = Pattern.compile(TIME_HH_MM_SS);
        Matcher durationMatcherHMS = durationPatternHMS.matcher(data);

        if (durationMatcherHMS.find()) {
            String durationString = durationMatcherHMS.group();

            String durationTokens[] = durationString.split(":");
            int duration = 0;
            for (int i = 0; i < durationTokens.length; i++) {
                int durationToken = Integer.parseInt(durationTokens[i]);
                duration = duration * 60 + durationToken;
            }
            mCostInfo.duration = duration;
            return true;
        }
        return false;
    }

    boolean findCostAndBalance(String data) {
        float amount[] = new float[2];

        Pattern floatDigitPattern = Pattern.compile(BASE_FLOAT);
        Matcher floatDigitMatcher = floatDigitPattern.matcher(data);

        boolean dataFound = false;
        for (int i = 0; i < amount.length; i++) {
            if (floatDigitMatcher.find()) {
                dataFound = true;
                try {
                    amount[i] = Float.parseFloat(floatDigitMatcher.group());
                } catch (NumberFormatException ex) {
                    ex.printStackTrace();
                }
            }
        }

        mCostInfo.cost = amount[0];
        mCostInfo.mainBalance = amount[1];
        return dataFound;
    }
}
