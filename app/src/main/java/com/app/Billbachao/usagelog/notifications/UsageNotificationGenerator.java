package com.app.Billbachao.usagelog.notifications;

import android.content.Context;

import com.app.Billbachao.R;
import com.app.Billbachao.usagelog.helper.CallSmsUsageHelper;
import com.app.Billbachao.usagelog.helper.DataUsageHelper;
import com.app.Billbachao.usagelog.model.DayCostInfo;
import com.app.Billbachao.usagelog.provider.UsageLogsProvider;

/**
 * Created by mihir on 20-11-2015.
 */
public class UsageNotificationGenerator {

    Context mContext;

    public UsageNotificationGenerator(Context context) {
        mContext = context;
    }

    public String generateText() {

        StringBuilder notificationTextBuilder = new StringBuilder();
        notificationTextBuilder.append(parseCallInfo(CallSmsUsageHelper.getTodayCallSmsDetails
                (mContext, UsageLogsProvider.CallColumns
                        .TYPE_CALL)));
        notificationTextBuilder.append(parseSmsInfo(CallSmsUsageHelper.getTodayCallSmsDetails
                (mContext,
                UsageLogsProvider.CallColumns.TYPE_SMS)));
        notificationTextBuilder.append(parseDataInfo(DataUsageHelper.getDayUsageDetails(mContext)));

        return notificationTextBuilder.toString();
    }

    String parseCallInfo(DayCostInfo callInfo) {
        StringBuilder builder = new StringBuilder();
        if (callInfo.duration != 0) {
            builder.append(mContext.getString(R.string.call_usage_text, (int)Math.ceil(callInfo
                    .duration / 60), callInfo.cost));
            builder.append(" ");
        }
        return builder.toString();
    }

    String parseSmsInfo(DayCostInfo smsInfo) {
        StringBuilder builder = new StringBuilder();
        if (smsInfo.count != 0) {
            builder.append(mContext.getString(R.string.sms_cost_text, smsInfo.count, smsInfo.cost));
            builder.append(" ");
        }
        return builder.toString();
    }

    String parseDataInfo(DayCostInfo dataInfo) {
        StringBuilder builder = new StringBuilder();
        if (dataInfo.dataMb != 0) {
            if (dataInfo.cost != 0) {
                builder.append(mContext.getString(R.string.data_usage_cost_text, (int)Math.ceil(dataInfo.dataMb),
                        dataInfo.cost));
            } else {
                builder.append(mContext.getString(R.string.data_usage_no_cost_text, (int)Math.ceil(dataInfo.dataMb)));
            }
            builder.append(" ");
        }
        return builder.toString();
    }
}
