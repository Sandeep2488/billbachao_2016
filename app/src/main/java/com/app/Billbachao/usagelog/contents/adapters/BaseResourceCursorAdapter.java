package com.app.Billbachao.usagelog.contents.adapters;

import android.content.Context;
import android.database.Cursor;
import android.widget.CursorTreeAdapter;

/**
 * Created by mihir on 27-10-2015.
 */
public abstract class BaseResourceCursorAdapter extends CursorTreeAdapter {

    public BaseResourceCursorAdapter(Cursor cursor, Context context) {
        super(cursor, context);
    }

    public abstract void initIndices(Cursor cursor);
}
