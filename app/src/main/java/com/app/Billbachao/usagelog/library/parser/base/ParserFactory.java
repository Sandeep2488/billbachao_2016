package com.app.Billbachao.usagelog.library.parser.base;

import android.content.Context;

import com.app.Billbachao.usagelog.library.parser.base.aircel.AircelParser;
import com.app.Billbachao.usagelog.library.parser.base.airtel.AirtelParser;
import com.app.Billbachao.usagelog.library.parser.base.bsnl.BsnlParser;
import com.app.Billbachao.usagelog.library.parser.base.idea.IdeaParser;
import com.app.Billbachao.usagelog.library.parser.base.reliance.RelianceParser;
import com.app.Billbachao.usagelog.library.parser.base.tata.TataParser;
import com.app.Billbachao.usagelog.library.parser.base.uninor.TelenorParser;
import com.app.Billbachao.usagelog.library.parser.base.videocon.VideoconParser;
import com.app.Billbachao.usagelog.library.parser.base.vodafone.VodafoneParser;
import com.app.Billbachao.utils.OperatorUtils;
import com.app.Billbachao.utils.PreferenceUtils;

/**
 * Created by mihir on 02-11-2015.
 */
public class ParserFactory {

    public static BaseParser getParser(Context context) {
        switch (PreferenceUtils.getOperatorId(context)) {
            case OperatorUtils.OPERATOR_AIRTEL :
                return new AirtelParser();
            case OperatorUtils.OPERATOR_RELIANCE:
                return new RelianceParser();
            case OperatorUtils.OPERATOR_VODAFONE:
                return new VodafoneParser();
            case OperatorUtils.OPERATOR_IDEA:
                return new IdeaParser();
            case OperatorUtils.OPERATOR_TELENOR:
                return new TelenorParser();
            case OperatorUtils.OPERATOR_AIRCEL:
                return new AircelParser();
            case OperatorUtils.OPERATOR_BSNL:
                return new BsnlParser();
            case OperatorUtils.OPERATOR_TATA:
                return new TataParser();
            case OperatorUtils.OPERATOR_VIDEOCON:
                return new VideoconParser();
            default:
                return null;
        }
    }
}
