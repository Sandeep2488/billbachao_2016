package com.app.Billbachao.usagelog.library.parser.base;

import com.app.Billbachao.usagelog.library.parser.model.CostInfo;
import com.app.Billbachao.utils.DateUtils;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by mihir on 30-10-2015.
 */
public abstract class BaseParser implements RegexConstants {

    protected static final int MB_KB_RATIO = 1024;

    protected static final float EPSILON = 0.01f;

    protected CostInfo mCostInfo;

    public abstract boolean parse(String data);

    protected abstract boolean parseDataUsage(String data);

    protected abstract boolean parseCallUsage(String data);

    protected abstract boolean parseSmsUsage(String data);

    protected boolean parseUssdData(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.USSD;

        int balIndex = data.indexOf(BAL);
        if (balIndex > -1) {
            float balance = extractFloats(data.substring(balIndex), 1)[0];
            float dataBalance = extractDataMbKb(data.substring(balIndex), 1)[0];

            if (balance >= 0) {
                if (Math.abs(dataBalance - balance) < EPSILON) {
                    mCostInfo.type = CostInfo.DATA_CHECK;
                    mCostInfo.dataLeft = dataBalance;
                } else {
                    mCostInfo.mainBalance = balance;
                }
                return true;
            }
        }
        return false;
    };

    public CostInfo getCostInfo() {
        return mCostInfo;
    }

    protected float extractAmountFromRupees(String data) {
        Pattern floatDigitPattern = Pattern.compile(BASE_FLOAT);
        Matcher floatDigitMatcher = floatDigitPattern.matcher(data);
        float amount = -1;

        if (floatDigitMatcher.find()) {
            try {
                amount = Float.parseFloat(floatDigitMatcher.group());
            } catch (NumberFormatException ex) {
                ex.printStackTrace();
            }
        }
        return amount;
    }

    // Returns date in milliseconds
    protected long extractDate(String data, String regexFormat) {
        Pattern datePattern = Pattern.compile(regexFormat);
        Matcher dateMatcher = datePattern.matcher(data);

        if (dateMatcher.find()) {
            dateMatcher.group();
            return DateUtils.parseDateFormat(data, getDateFormat(regexFormat)).getTime();
        }
        return -1;
    }

    String getDateFormat(String regex) {
        switch (regex) {
            case DATE_DD_MM_YYYY:
                return "dd-MM-yyyy";
            default:
                return "";
        }
    }

    protected float[] extractINRGroups(String data, int count) {
        return extractNumericData(data, count, XX_YY_INR);
    }

    protected float[] extractDataMbKb(String data, int count) {
        return extractNumericData(data, count, DATA_PATTERN);
    }

    // Extract particular numeric patterns, appended by units like MB/KB or INR etc.
    protected float[] extractNumericData(String data, int count, String patternString) {
        return extractNumericData(data, count, patternString, false);
    }

    protected float[] extractNumericData(String data, int count, String patternString, boolean isInt) {
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(data);
        float amount[] = new float[count];
        Arrays.fill(amount, -1);

        for (int i = 0; i < count; i++) {
            if (matcher.find()) {
                String group = matcher.group();

                Pattern floatDigitPattern = Pattern.compile(isInt ? BASE_INT : BASE_FLOAT);
                Matcher floatDigitMatcher = floatDigitPattern.matcher(group);

                if (floatDigitMatcher.find()) {
                    try {
                        amount[i] = Float.parseFloat(floatDigitMatcher.group());
                    } catch (NumberFormatException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
        return amount;
    }

    // Extract particular numeric patterns, appended by units like MB/KB or INR etc.
    protected float[] extractFloats(String data, int count) {
        Pattern pattern = Pattern.compile(BASE_FLOAT);
        Matcher matcher = pattern.matcher(data);
        float amount[] = new float[count];
        Arrays.fill(amount, -1);

        for (int i = 0; i < count; i++) {
            if (matcher.find()) {
                try {
                    amount[i] = Float.parseFloat(matcher.group());
                } catch (NumberFormatException ex) {
                    ex.printStackTrace();
                }
            }

        }
        return amount;
    }

    protected float extractFloatFromEnd(String data) {
        Pattern pattern = Pattern.compile(BASE_FLOAT);
        Matcher matcher = pattern.matcher(data);

        float value = -1;

        String group = "";
        while (matcher.find()) {
            group = matcher.group();
        }

        if(!group.isEmpty()) {
            try {
                value = Float.parseFloat(group);
            } catch (NumberFormatException ex) {
                ex.printStackTrace();
            }
        }

        return value;
    }

}
