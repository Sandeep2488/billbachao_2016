package com.app.Billbachao.usagelog.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.app.Billbachao.utils.ILog;

/**
 * Created by mihir on 27-10-2015.
 */
public class UsageLogsProvider extends ContentProvider {

    private static final String TAG = UsageLogsProvider.class.getSimpleName();

    private Context mContext;

    static String DATABASE_NAME = "callLogs.db";

    static int DATA_BASE_VERSION = 1;

    private CallSQLiteHelper mHelper;

    public static final String AUTHORITY = "com.app.Billbachao.data.logsprovider";

    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    public static final int CALL_LOGS = 1, DATA_LOGS = 2, RAW_DATA_LOGS = 3, DAY_DATA_USAGE = 4;

    private static final UriMatcher URI_MATCHER;

    public SQLiteDatabase mDatabase;

    static {
        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        URI_MATCHER.addURI(AUTHORITY, CallColumns.TABLE_NAME, CALL_LOGS);
        URI_MATCHER.addURI(AUTHORITY, DataColumns.TABLE_NAME, DATA_LOGS);
        URI_MATCHER.addURI(AUTHORITY, RawDataColumns.TABLE_NAME, RAW_DATA_LOGS);
        URI_MATCHER.addURI(AUTHORITY, DayWiseDataColumns.TABLE_NAME, DAY_DATA_USAGE);
    }

    @Override
    public boolean onCreate() {
        mContext = getContext();
        mHelper = new CallSQLiteHelper(mContext, DATABASE_NAME, null,
                DATA_BASE_VERSION);
        try {
            mDatabase = mHelper.getWritableDatabase();
        } catch (Exception ex) {
            mDatabase = mHelper.getReadableDatabase();
        }
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {
        Cursor cursor = null;
        switch (URI_MATCHER.match(uri)) {
            case CALL_LOGS:
                cursor = mDatabase.query(CallColumns.TABLE_NAME, projection,
                        selection, selectionArgs, null, null, sortOrder);
                break;
            case DATA_LOGS:
                cursor = mDatabase.query(DataColumns.TABLE_NAME, projection,
                        selection, selectionArgs, null, null, sortOrder);
                break;
            case RAW_DATA_LOGS:
                cursor = mDatabase.rawQuery(RawDataColumns.RAW_QUERY, null);
                break;
            case DAY_DATA_USAGE:
                cursor = mDatabase.query(DayWiseDataColumns.TABLE_NAME, projection,
                        selection, selectionArgs, null, null, sortOrder);
                break;
            default:
                break;
        }
        if (cursor != null) {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long rowId = -1;
        Uri insertUri = null;
        switch (URI_MATCHER.match(uri)) {
            case CALL_LOGS:
                rowId = mDatabase.insert(CallColumns.TABLE_NAME, null, values);
                insertUri = Uri.withAppendedPath(CallColumns.CONTENT_URI,
                        Long.toString(rowId));
                break;
            case DATA_LOGS:
                rowId = mDatabase.insert(DataColumns.TABLE_NAME, null, values);
                insertUri = Uri.withAppendedPath(DataColumns.CONTENT_URI,
                        Long.toString(rowId));
                break;
            case DAY_DATA_USAGE:
                rowId = mDatabase.insert(DayWiseDataColumns.TABLE_NAME, null, values);
                insertUri = Uri.withAppendedPath(DayWiseDataColumns.CONTENT_URI,
                        Long.toString(rowId));
                break;
        }
        if (rowId != -1) {
            ILog.d(TAG, "Inserted rowId: " + rowId);
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return insertUri;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        int count = 0;
        switch (URI_MATCHER.match(uri)) {
            case DATA_LOGS:
                for (ContentValues value : values) {
                    if (mDatabase.insert(DataColumns.TABLE_NAME, null, value) != -1) {
                        count++;
                    }
                }
                break;
            case CALL_LOGS:
                for (ContentValues value : values) {
                    if (mDatabase.insert(CallColumns.TABLE_NAME, null, value) != -1) {
                        count++;
                    }
                }
                break;
            default:
                break;
        }
        ILog.d(TAG, "Inserted rows: " + count);
        if (count > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return (count > 0 ? count : super.bulkInsert(uri, values));
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int numRows = 0;
        switch (URI_MATCHER.match(uri)) {
            case CALL_LOGS:
                numRows = mDatabase.delete(CallColumns.TABLE_NAME, selection,
                        selectionArgs);
                break;
            case DATA_LOGS:
                numRows = mDatabase.delete(DataColumns.TABLE_NAME, selection,
                        selectionArgs);
                break;
            case DAY_DATA_USAGE:
                numRows = mDatabase.delete(DayWiseDataColumns.TABLE_NAME, selection,
                        selectionArgs);
                break;
        }
        ILog.d(TAG, "Deleted rows: " + numRows+ " Uri "+ uri);
        if (numRows != 0) {
            mContext.getContentResolver().notifyChange(uri, null);
        }
        return numRows;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int numRows = 0;
        switch (URI_MATCHER.match(uri)) {
            case CALL_LOGS:
                numRows = mDatabase.update(CallColumns.TABLE_NAME, values,
                        selection, selectionArgs);
                break;
            case DATA_LOGS:
                numRows = mDatabase.update(DataColumns.TABLE_NAME, values,
                        selection, selectionArgs);
                break;
            case DAY_DATA_USAGE:
                numRows = mDatabase.update(DayWiseDataColumns.TABLE_NAME, values,
                        selection, selectionArgs);
                break;
        }
        ILog.d(TAG, "Updated rows: " + numRows);
        if (numRows != 0) {
            mContext.getContentResolver().notifyChange(uri, null);
        }
        return numRows;
    }

    public static class CallSQLiteHelper extends SQLiteOpenHelper {

        static CallSQLiteHelper sInstance;

        public static CallSQLiteHelper getInstance(Context context) {
            if (sInstance == null) {
                sInstance = new CallSQLiteHelper(context.getApplicationContext());
            }
            return sInstance;
        }

        public CallSQLiteHelper(Context context) {
            super(context, DATABASE_NAME, null,
                    DATA_BASE_VERSION);
        }

        public CallSQLiteHelper(Context context, String name,
                                SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            createCallLogsTable(db);
            createDataLogsTable(db);
            createDayWiseDataTable(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            if (newVersion == 2) {
                createDayWiseDataTable(db);
            }
            if (newVersion == 3 && oldVersion == 2) {
                db.execSQL("drop table if exists " + DayWiseDataColumns.TABLE_NAME);
                createDayWiseDataTable(db);
            }
        }

        void createCallLogsTable(SQLiteDatabase db) {
            db.execSQL(CallColumns.CREATE_TABLE);
        }

        void createDataLogsTable(SQLiteDatabase db) {
            db.execSQL(DataColumns.CREATE_TABLE);
        }

        void createDayWiseDataTable(SQLiteDatabase db) {
            db.execSQL(DayWiseDataColumns.CREATE_TABLE);
        }

    }

    public static class CallColumns {

        public static String TABLE_NAME = "CallLogs";

        public static final Uri CONTENT_URI = Uri.withAppendedPath(
                UsageLogsProvider.CONTENT_URI, TABLE_NAME);

        public static final String _ID = "_id", NAME = "name", NUMBER = "number", DURATION = "duration", DATE = "date";

        // Incoming, outgoing etc
        public static final String CALL_TYPE = "callType";

        // CALL or SMS
        public static final String USAGE_TYPE = "usageType";

        public static final int TYPE_CALL = 1, TYPE_SMS = 2;

        public static final String COST = "cost";

        public static final String[] DEFAULT_PROJECTION = new String[]{_ID, NUMBER, NAME,
                DURATION, DATE, COST, CALL_TYPE, USAGE_TYPE, NAME};

        public static final String CREATE_TABLE = "create table " + TABLE_NAME
                + " (" + _ID + " integer primary key autoincrement"
                + ", " + NAME + " text"
                + ", " + NUMBER + " text"
                + ", " + DATE + " long"
                + ", " + DURATION + " long"
                + ", " + COST + " float"
                + ", " + CALL_TYPE + " int"
                + ", " + USAGE_TYPE + " int"
                + ");";

        public static final String DEFAULT_SORT_ORDER = "date DESC";

        public static final String TOTAL_COST = "totalCost", TOTAL_DURATION = "totalDuration", TOTAL_COUNT = "totalCount";

        public static final String DAY_TOTAL_CALL_RAW_QUERY = "SELECT" +
                " sum(" + CallColumns.COST + ") as " + TOTAL_COST +
                ", sum(" + CallColumns.DURATION + ") as " + TOTAL_DURATION +
                " FROM " + CallColumns.TABLE_NAME +
                " WHERE " + CallColumns.DATE + " >? AND " + CallColumns.USAGE_TYPE + " =? AND "
                + CallColumns.COST + " >=0";

        public static final String DAY_TOTAL_SMS_RAW_QUERY = "SELECT" +
                " sum(" + CallColumns.COST + ") as " + TOTAL_COST +
                ", count(*) as " + TOTAL_COUNT +
                " FROM " + CallColumns.TABLE_NAME +
                " WHERE " + CallColumns.DATE + " >? AND " + CallColumns.USAGE_TYPE + " =?";
    }

    public static class DataColumns {

        public static String TABLE_NAME = "DataLogs";

        public static final Uri CONTENT_URI = Uri.withAppendedPath(
                UsageLogsProvider.CONTENT_URI, TABLE_NAME);

        public static final String _ID = "_id", PACKAGE_NAME = "package_name", NAME = "name", DATE = "date", BITMAP = "bitmap";

        // Wifi/Mobile field, as of now of no use
        public static final String DATA_TYPE = "dataType";

        public static final int TYPE_WIFI = 1, TYPE_MOBILE = 2, TYPE_NA = 3;

        public static final String USAGE_KB = "usage";

        // Sorted order for day
        public static final String DAY_ORDER = "dayOrder";

        public static final String[] DEFAULT_PROJECTION = new String[]{_ID, PACKAGE_NAME,
                DATE, DATA_TYPE, DAY_ORDER, USAGE_KB};

        public static final String CREATE_TABLE = "create table " + TABLE_NAME
                + " (" + _ID + " integer primary key autoincrement"
                + ", " + PACKAGE_NAME + " text"
                + ", " + NAME + " text"
                + ", " + BITMAP + " blob"
                + ", " + DATE + " text"
                + ", " + USAGE_KB + " float"
                + ", " + DATA_TYPE + " int"
                + ", " + DAY_ORDER + " int"
                + ");";

        public static final String DEFAULT_SORT_ORDER = "date DESC";
    }

    public static class RawDataColumns {

        public static String TABLE_NAME = "RawDataLogs";

        public static final Uri CONTENT_URI = Uri.withAppendedPath(
                UsageLogsProvider.CONTENT_URI, TABLE_NAME);

        public static final String DATE_COUNT = "date_count", TOTAL_USAGE = "sum_usage";

        public static final String RAW_QUERY = "SELECT "
                + DataColumns._ID +
                ", " + DataColumns.DATE +
                ", count(" + DataColumns.DATE + ") as " + DATE_COUNT +
                ", sum(" + DataColumns.USAGE_KB + ") as " + TOTAL_USAGE +
                "  FROM " + DataColumns.TABLE_NAME + " GROUP BY " + DataColumns.DATE
                + " ORDER BY " + DataColumns.DATE + " DESC";
    }

    public static class DayWiseDataColumns {

        public static String TABLE_NAME = "DayDataUsage";

        public static final Uri CONTENT_URI = Uri.withAppendedPath(
                UsageLogsProvider.CONTENT_URI, TABLE_NAME);

        public static final String _ID = "_id", DATE = "date", USAGE_WIFI = "usageWifi",
                USAGE_MOBILE = "usageMobile", COST = "cost", USSD_USAGE_MOBILE = "ussdMobile";

        // Wifi/Mobile field, as of now of no use
        public static final String DATA_TYPE = "dataType";

        public static final int TYPE_WIFI = 1, TYPE_MOBILE = 2, TYPE_NA = 3;

        public static final String[] DEFAULT_PROJECTION = new String[]{_ID, DATE, USAGE_MOBILE, USAGE_WIFI, COST, USSD_USAGE_MOBILE};

        public static final String CREATE_TABLE = "create table " + TABLE_NAME
                + " (" + _ID + " integer primary key autoincrement"
                + ", " + DATE + " text"
                + ", " + USAGE_WIFI + " float"
                + ", " + USAGE_MOBILE + " float"
                + ", " + COST + " float"
                + ", " + USSD_USAGE_MOBILE + " float"
                + ");";

        public static final String DEFAULT_SORT_ORDER = "date DESC";
    }

}
