package com.app.Billbachao.usagelog.widget;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

/**
 * Created by mihir on 05-04-2016.
 */
public class AppBitmapCache {

    public static final String TAG = AppBitmapCache.class.getSimpleName();

    private static LruCache<String, Bitmap> bitmapLruCache;

    public static void ensureCache() {
        if (bitmapLruCache == null) {
            final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

            final int cacheSize = maxMemory / 8;
            bitmapLruCache = new LruCache<String, Bitmap>(cacheSize) {
                @Override
                protected int sizeOf(String key, Bitmap bitmap) {
                    return (int) (getSizeInBytes(bitmap) / 1024);
                }
            };
        }
    }

    public static void addBitmapToMemoryCache(String packageName, int size, Bitmap bitmap) {
        String key = getKey(packageName, size);
        if (bitmapLruCache.get(key) == null) {
            bitmapLruCache.put(key, bitmap);
        }
    }

    public static long getSizeInBytes(Bitmap bitmap) {
        return bitmap.getByteCount();
    }

    public static Bitmap getBitmapFromMemCache(String packageName, int size) {
        return bitmapLruCache.get(getKey(packageName, size));
    }

    public static String getKey(String packageName, int size) {
        return packageName + "_" + size;
    }

    public static void clearCache() {
        if (bitmapLruCache != null)
            bitmapLruCache.evictAll();
    }
}
