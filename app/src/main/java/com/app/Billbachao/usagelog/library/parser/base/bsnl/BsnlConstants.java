package com.app.Billbachao.usagelog.library.parser.base.bsnl;

/**
 * Created by mihir on 25-11-2015.
 */
public interface BsnlConstants {

    String COST = "COST", CHARGE = "CHARGE";

    String MAIN = "MAIN";
}
