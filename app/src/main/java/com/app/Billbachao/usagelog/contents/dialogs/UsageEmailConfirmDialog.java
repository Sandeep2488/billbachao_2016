package com.app.Billbachao.usagelog.contents.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.app.Billbachao.R;
import com.app.Billbachao.utils.MailUtils;
import com.app.Billbachao.utils.PreferenceUtils;

/**
 * Created by mihir on 19-11-2015.
 */
public class UsageEmailConfirmDialog extends DialogFragment {

    OnEmailUsageChoiceListener mEmailUsageChoiceListener;

    public static final String TAG = UsageEmailConfirmDialog.class.getSimpleName();

    public static UsageEmailConfirmDialog getInstance() {
        UsageEmailConfirmDialog dialog = new UsageEmailConfirmDialog();
        Bundle bundle = new Bundle();
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mEmailUsageChoiceListener = (OnEmailUsageChoiceListener) activity;
    }

    @Override
    public void onDetach() {
        mEmailUsageChoiceListener = null;
        super.onDetach();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.mail_usage_details);
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context
                .LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.usage_email_dialog, null);

        final EditText emailText = (EditText) view.findViewById(R.id.email);
        emailText.setText(PreferenceUtils.getMailId(getActivity()));
        final Spinner daysChoice = (Spinner) view.findViewById(R.id.days_before);

        final int[] daysValues = getActivity().getResources().getIntArray(R.array
                .usage_mail_duration_values);

        builder.setView(view);
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String email = emailText.getText().toString();
                if (!MailUtils.isValidMail(email)) {
                    Toast.makeText(getActivity(), R.string.invalid_email, Toast
                            .LENGTH_LONG).show();
                    return;
                }
                if (mEmailUsageChoiceListener != null) {
                    mEmailUsageChoiceListener.onInfoEntered(email,
                            daysValues[daysChoice.getSelectedItemPosition()]);
                }
            }
        });

        return builder.create();
    }

    public interface OnEmailUsageChoiceListener {
        void onInfoEntered(String email, int days);
    }
}
