package com.app.Billbachao.usagelog.library.parser.base.airtel;

import com.app.Billbachao.usagelog.library.parser.base.BaseParser;
import com.app.Billbachao.usagelog.library.parser.model.CostInfo;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by mihir on 02-11-2015.
 */
public class AirtelParser extends BaseParser implements AirtelConstants {

    @Override
    public boolean parse(String data) {

        if (data.contains(CALL_COST) || data.contains(CALL__COST)) {
            return parseCallUsage(data);
        } else if (data.contains(DATA)) {
            return parseDataUsage(data);
        } else if (data.contains(SMS)) {
            return parseSmsUsage(data);
        } else if (data.contains(BALANCE) || data.contains(BAL)) {
            return parseUssdData(data);
        }
        return false;
    }

    @Override
    protected boolean parseDataUsage(String data) {
        Pattern digitPattern = Pattern.compile(BASE_FLOAT);
        Matcher digitMatcher = digitPattern.matcher(data);

        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.DATA;

        if (digitMatcher.find()) {
            try {
                mCostInfo.dataUsed = Float.parseFloat(digitMatcher.group());
            } catch (NumberFormatException ex) {
                ex.printStackTrace();
                return false;
            }

            // Decide if data pack available or not
            boolean dataPack = true;
            int end = digitMatcher.end();
            String copyData = data.substring(end);
            Pattern stringPattern = Pattern.compile(BASE_STRING);
            Matcher stringMatcher = stringPattern.matcher(copyData);
            while (stringMatcher.find()) {
                String subStr = stringMatcher.group();
                if (subStr.contains(COST)) {
                    dataPack = false;
                    break;
                }
            }

            if (dataPack) {
                if (digitMatcher.find()) {
                    try {
                        mCostInfo.dataLeft = Float.parseFloat(digitMatcher.group());
                        return true;
                    } catch (NumberFormatException ex) {
                        ex.printStackTrace();
                        return false;
                    }

                    // Match found, now try validity

                }
            } else {
                if (digitMatcher.find()) {
                    try {
                        mCostInfo.cost = Float.parseFloat(digitMatcher.group());
                    } catch (NumberFormatException ex) {
                        ex.printStackTrace();
                        return false;
                    }

                    // Main balance
                    if (digitMatcher.find()) {
                        try {
                            mCostInfo.mainBalance = Float.parseFloat(digitMatcher.group());
                            return true;
                        } catch (NumberFormatException ex) {
                            ex.printStackTrace();
                            return false;
                        }
                    }
                }
            }
        }

        return false;
    }

    @Override
    protected boolean parseCallUsage(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.CALL;

        if (findDuration(data)) {
            return findCostAndBalance(data);
        }

        return false;
    }

    @Override
    protected boolean parseSmsUsage(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.SMS;

        return findCostAndBalance(data);
    }

    @Override
    protected boolean parseUssdData(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.USSD;

        float balance = extractFloats(data, 1)[0];
        float dataBalance = extractDataMbKb(data, 1)[0];

        if (balance >= 0) {
            if (Math.abs(dataBalance - balance) < EPSILON) {
                mCostInfo.type = CostInfo.DATA_CHECK;
                mCostInfo.dataLeft = dataBalance;
            } else {
                mCostInfo.mainBalance = balance;
            }
            return true;
        }

        return false;
    }

    boolean findDuration(String data) {
        Pattern durationPatternHMS = Pattern.compile(TIME_HH_MM_SS);
        Matcher durationMatcherHMS = durationPatternHMS.matcher(data);

        if (durationMatcherHMS.find()) {
            String durationString = durationMatcherHMS.group();

            String durationTokens[] = durationString.split(":");
            int duration = 0;
            for (int i = 0; i < durationTokens.length; i++) {
                int durationToken = Integer.parseInt(durationTokens[i]);
                duration = duration * 60 + durationToken;
            }
            mCostInfo.duration = duration;
            return true;
        }
        return false;
    }

    boolean findCostAndBalance(String data) {
        float amount[] = new float[2];

        Pattern amountPattern = Pattern.compile(RS_PATTERN + BASE_FLOAT);
        Matcher amountMatcher = amountPattern.matcher(data);

        boolean dataFound = false;
        for (int i = 0; i < amount.length; i++) {
            if (amountMatcher.find()) {
                String group = amountMatcher.group();

                Pattern floatDigitPattern = Pattern.compile(BASE_FLOAT);
                Matcher floatDigitMatcher = floatDigitPattern.matcher(group);

                if (floatDigitMatcher.find()) {
                    dataFound = true;
                    try {
                        amount[i] = Float.parseFloat(floatDigitMatcher.group());
                    } catch (NumberFormatException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }

        mCostInfo.cost = amount[0];
        mCostInfo.mainBalance = amount[1];
        return dataFound;
    }
}
