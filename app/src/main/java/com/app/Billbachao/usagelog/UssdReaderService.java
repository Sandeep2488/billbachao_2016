package com.app.Billbachao.usagelog;

import android.accessibilityservice.AccessibilityService;
import android.view.accessibility.AccessibilityEvent;

import com.app.Billbachao.usagelog.library.parser.UssdParser;
import com.app.Billbachao.utils.ILog;

/**
 * Created by mihir on 29-03-2016.
 */
public class UssdReaderService extends AccessibilityService {

    String TAG = UssdReaderService.class.getSimpleName();

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        ILog.d(TAG, "onAccessibilityEvent " + event.getEventType() + " Package: " + event
                .getPackageName());
        ILog.d(TAG, "onAccessibilityEvent description " + event.getContentDescription());
        String result = event.getText().toString().toUpperCase();
        new UssdParser().process(this, result);
    }

    @Override
    public void onInterrupt() {
    }
}
