package com.app.Billbachao.usagelog.notifications;

import android.content.Context;

import com.app.Billbachao.R;
import com.app.Billbachao.usagelog.LogHelper;
import com.app.Billbachao.usagelog.model.DayCostInfo;

/**
 * Created by Tejaswi on 06-04-2016.
 */
public class PostPaidUsageNotificationGenerator {

    Context mContext;

    public PostPaidUsageNotificationGenerator(Context context) {
        mContext = context;
    }

    public String generateText() {

        StringBuilder notificationTextBuilder = new StringBuilder();

        notificationTextBuilder.append(parseCallInfo(LogHelper.getCallDurationPostPaid(mContext)));

        notificationTextBuilder.append(parseSmsInfo(LogHelper.getSMSCountPostPaid(mContext)));

        notificationTextBuilder.append(parseDataInfo(LogHelper.getMobileDataTrafficStatsPostPaid()));

        return notificationTextBuilder.toString();
    }

    /**
     * call postpaid parse
     */

    String parseCallInfo(DayCostInfo callInfo) {
        StringBuilder builder = new StringBuilder();
        if (callInfo.duration != 0) {
            builder.append(mContext.getString(R.string.call_usage_text_postpaid, (int) Math.ceil(callInfo
                    .duration / 60)));

        }
        return builder.toString();
    }

    String parseSmsInfo(DayCostInfo smsInfo) {
        StringBuilder builder = new StringBuilder();
        if (smsInfo.count != 0) {
            if (!isEmpty(parseCallInfo(LogHelper.getCallDurationPostPaid(mContext)))) {
                builder.append(", ");
            }
            builder.append(mContext.getString(R.string.sms_cost_text_postpaid, smsInfo.count));
        }
        return builder.toString();
    }

    String parseDataInfo(DayCostInfo dataInfo) {
        StringBuilder builder = new StringBuilder();
        if (dataInfo.dataMb != 0) {
            if (!isEmpty(parseSmsInfo(LogHelper.getSMSCountPostPaid(mContext)))) {
                builder.append(", ");
            } else if (!isEmpty(parseSmsInfo(LogHelper.getCallDurationPostPaid(mContext)))) {
                builder.append(", ");
            }
            builder.append(mContext.getString(R.string.data_usage_no_cost_text, (int) Math.ceil(dataInfo.dataMb)));

        }
        return builder.toString();
    }

    private boolean isEmpty(String str) {
        boolean ifStrEmpty = false;
        if (str.equals("") || str.equals(null)) {
            return true;
        }
        return ifStrEmpty;
    }

}
