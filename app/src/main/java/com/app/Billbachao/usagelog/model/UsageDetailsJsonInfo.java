package com.app.Billbachao.usagelog.model;

import com.google.myjson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by mihir on 18-11-2015.
 */
public class UsageDetailsJsonInfo {

    public static final String LOCAL_CALLS = "localCalls", STD_CALLS = "stdCalls", ISD_CALLS =
            "isdCalls", ROAMING_CALLS = "roamingCalls", SMS = "sms", DATA_USAGE = "dataUsage", TOTAL_COST = "totalCost";

    @SerializedName(LOCAL_CALLS)
    public ArrayList<CallSmsDetailInfo> localCalls;

    @SerializedName(STD_CALLS)
    public ArrayList<CallSmsDetailInfo> stdCalls;

    @SerializedName(ISD_CALLS)
    public ArrayList<CallSmsDetailInfo> isdCalls;

    @SerializedName(ROAMING_CALLS)
    public ArrayList<CallSmsDetailInfo> roamingCalls;

    @SerializedName(SMS)
    public ArrayList<CallSmsDetailInfo> sms;

    @SerializedName(DATA_USAGE)
    public ArrayList<DataUsageDetailInfo> dataUsages;

    transient public int totalCost = 0;

    public void addCost(int cost) {
        if (cost <= 0) return;
        totalCost += cost;
    }

    public int getTotalCost() {
        return totalCost;
    }
}
