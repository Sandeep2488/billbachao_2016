package com.app.Billbachao.usagelog.model;

import com.app.Billbachao.utils.DateUtils;
import com.google.myjson.annotations.SerializedName;

/**
 * Created by mihir on 18-11-2015.
 */
public class CallSmsDetailInfo {

    public static final String MOBILE = "mobile", DURATION = "duration", SPEND = "spend", TIME =
            "time";

    transient int type;

    @SerializedName(MOBILE)
    String number;

    transient long time;

    @SerializedName(TIME)
    // No use of this variable, except for GSON serialization
            String timeString;

    @SerializedName(DURATION)
    long duration;

    @SerializedName(SPEND)
    int costPaisa;

    public CallSmsDetailInfo(int type, String number, long time, long duration, int costPaisa) {
        this.type = type;
        this.number = number;
        this.time = time;
        this.duration = duration;
        this.costPaisa = costPaisa;
        timeString = DateUtils.convertTimeToDate(time, DateUtils.FORMAT_YYYY_MM_DD_HH_MM_SS);
    }

    public int getType() {
        return type;
    }

    public String getNumber() {
        return number;
    }

    public long getTime() {
        return time;
    }

    public long getDuration() {
        return duration;
    }

    public float getCostPaisa() {
        return costPaisa;
    }
}
