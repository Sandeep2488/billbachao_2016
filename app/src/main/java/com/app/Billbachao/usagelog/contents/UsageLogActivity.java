package com.app.Billbachao.usagelog.contents;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.apis.UsageDetailsEmailApi;
import com.app.Billbachao.externalsdk.AppsFlyerUtils;
import com.app.Billbachao.externalsdk.google.AppIndexBehavior;
import com.app.Billbachao.launch.DeepLinkHelper;
import com.app.Billbachao.model.BaseGsonResponse;
import com.app.Billbachao.usagelog.LogHelper;
import com.app.Billbachao.usagelog.contents.dialogs.UsageEmailConfirmDialog;
import com.app.Billbachao.usagelog.contents.fragments.BaseUsageFragment;
import com.app.Billbachao.usagelog.contents.fragments.DataUsageLogFragment;
import com.app.Billbachao.usagelog.contents.fragments.UsageLogListFragment;
import com.app.Billbachao.usagelog.contents.utils.LogUtils;
import com.app.Billbachao.usagelog.widget.UsageHeaderItem;
import com.app.Billbachao.utils.USSDUtils;
import com.app.Billbachao.volley.request.NetworkGsonRequest;
import com.app.Billbachao.volley.utils.NetworkErrorHelper;
import com.app.Billbachao.volley.utils.VolleySingleTon;

/**
 * Created by mihir on 19-10-2015.
 */
public class UsageLogActivity extends BaseActivity implements UsageEmailConfirmDialog
        .OnEmailUsageChoiceListener, BaseUsageFragment.OnDataUpdater, AppIndexBehavior {

    public static final String TAG = UsageLogActivity.class.getSimpleName();

    ViewPager mPager;

    SharedPreferences mLogPreferences;

    UsageHeaderItem mCallHeader, mDataHeader;

    View mEmailDetails;

    boolean mBasicDataPresent = true;

    public static final String LAUNCH_FROM_NOTIFICATION = "launch_notification";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.usage_log_activity);
        initView();
        mLogPreferences = getSharedPreferences(LogHelper.PREFERENCE_FILE, Context
                .MODE_PRIVATE);
        mLogPreferences.registerOnSharedPreferenceChangeListener(mPreferenceChangeListener);
        getBasicData();
        AppsFlyerUtils.trackEvent(this, AppsFlyerUtils.USAGE_SUMMARY);
    }

    void initView() {
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new CustomPagerAdapter(getSupportFragmentManager()));
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mPager);
        mPager.addOnPageChangeListener(mPageChangeListener);
        mCallHeader = new UsageHeaderItem(findViewById(R.id.call_usage), LogUtils.TYPE_CALL);
        mDataHeader = new UsageHeaderItem(findViewById(R.id.data_usage), LogUtils.TYPE_DATA);
        mEmailDetails = findViewById(R.id.mail_details);
        mEmailDetails.setOnClickListener(mClickListener);
    }

    void getBasicData() {
        if (!LogHelper.hasBalance(this, LogHelper.TYPE_CALL) || LogHelper.isInfoExpired(this,
                LogHelper.TYPE_CALL)) {
            USSDUtils.entertainRequest(this, USSDUtils.CHECK_BALANCE);
            mBasicDataPresent = false;
        } else if (!LogHelper.hasBalance(this, LogHelper.TYPE_DATA) || LogHelper.isInfoExpired(this,
                LogHelper.TYPE_DATA)) {
            USSDUtils.entertainRequest(this, USSDUtils.DATA_BALANCE);
        }
        if (LogHelper.isCallLogsEmpty(this)) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    LogHelper.populateInitialLogs(UsageLogActivity.this);
                }
            }).start();
        }
        mCallHeader.update();
        mDataHeader.update();
    }

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.data_usage:
                    mPager.setCurrentItem(1);
                    break;
                case R.id.call_usage:
                    mPager.setCurrentItem(0);
                    break;
                case R.id.mail_details:
                    UsageEmailConfirmDialog dialog = UsageEmailConfirmDialog.getInstance();
                    dialog.show(getSupportFragmentManager(), UsageEmailConfirmDialog.TAG);
                    break;
            }
        }
    };

    ViewPager.OnPageChangeListener mPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            // TODO events stuff
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    void sendMailData(String email, int days) {
        NetworkGsonRequest<BaseGsonResponse> networkGsonRequest = new NetworkGsonRequest<BaseGsonResponse>
                (Request.Method.POST, UsageDetailsEmailApi.URL, BaseGsonResponse.class, UsageDetailsEmailApi.getParams(this, days, email), new Response.Listener<BaseGsonResponse>() {
            @Override
            public void onResponse(BaseGsonResponse response) {
                if (response.isSuccess()) {
                    showSnack(R.string.usage_summary_mail_success);
                } else {
                    showSnack(response.getStatusDesc());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showSnack(NetworkErrorHelper.getErrorStatus(error).getErrorMessage());
            }
        });
        VolleySingleTon.getInstance(this).addToRequestQueue(networkGsonRequest, TAG);
    }

    @Override
    public void onInfoEntered(String email, int days) {
        sendMailData(email, days);
    }

    @Override
    public void onDataUpdated() {
        findViewById(R.id.mail_details).setVisibility(LogHelper.isLogsEmpty(this) ? View.GONE :
                View.VISIBLE);
    }

    @Override
    public String getAppIndexTitle() {
        return Keywords.USAGE_SUMMARY;
    }

    @Override
    public String getAppIndexAppendedUrl() {
        return DeepLinkHelper.USAGE_LOG;
    }

    public class CustomPagerAdapter extends FragmentPagerAdapter {

        public CustomPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return UsageLogListFragment.getInstance();
            } else if (position == 1) {
                return DataUsageLogFragment.getInstance();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getString(LogUtils.getTitleId(position == 0 ? LogUtils.TYPE_CALL : LogUtils.TYPE_DATA));
        }
    }

    SharedPreferences.OnSharedPreferenceChangeListener mPreferenceChangeListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            mCallHeader.update();
            mDataHeader.update();
            if (!mBasicDataPresent && (!LogHelper.hasBalance(UsageLogActivity.this, LogHelper
                    .TYPE_DATA) || LogHelper.isInfoExpired(UsageLogActivity.this, LogHelper
                    .TYPE_DATA))) {
                USSDUtils.entertainRequest(UsageLogActivity.this, USSDUtils.DATA_BALANCE);
                mBasicDataPresent = true;
            }
        }
    };

    @Override
    protected void onDestroy() {
        mLogPreferences.unregisterOnSharedPreferenceChangeListener(mPreferenceChangeListener);
        mPager.removeOnPageChangeListener(mPageChangeListener);
        VolleySingleTon.getInstance(this).cancelPendingRequests(TAG);
        super.onDestroy();
    }
}
