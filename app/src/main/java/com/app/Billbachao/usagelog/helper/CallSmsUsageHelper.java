package com.app.Billbachao.usagelog.helper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.CallLog;

import com.app.Billbachao.database.MobileOperatorDBHelper;
import com.app.Billbachao.database.STDNumberSeriesDBHelper;
import com.app.Billbachao.model.OperatorCircleInfo;
import com.app.Billbachao.usagelog.model.CallSmsDetailInfo;
import com.app.Billbachao.usagelog.model.DayCostInfo;
import com.app.Billbachao.usagelog.model.UsageDetailsJsonInfo;
import com.app.Billbachao.usagelog.provider.UsageLogsProvider;
import com.app.Billbachao.utils.DateUtils;
import com.app.Billbachao.utils.NumberUtils;
import com.app.Billbachao.utils.PreferenceUtils;

import java.util.ArrayList;

/**
 * Created by mihir on 19-11-2015.
 */
public class CallSmsUsageHelper {

    public static void getCallSmsLogsInfo(Context context, long time, UsageDetailsJsonInfo usageInfo) {
        Cursor cursor = context.getContentResolver().query(UsageLogsProvider.CallColumns
                .CONTENT_URI, null, UsageLogsProvider.CallColumns.DATE + " > ?", new
                String[]{String.valueOf(time)}, null);

        ArrayList<CallSmsDetailInfo> localCallList = new ArrayList<>(), stdCallList = new
                ArrayList<>(), isdCallList = new ArrayList<>(), roamingCallList = new ArrayList<>(), smsList = new ArrayList<>();

        if (cursor != null) {

            int totalCost = 0;

            if (cursor.moveToFirst()) {

                int homeCircle = Integer.parseInt(PreferenceUtils.getCircleId(context));
                MobileOperatorDBHelper mobileDbHelper = MobileOperatorDBHelper.getInstance
                        (context);
                STDNumberSeriesDBHelper stdDbHelper = STDNumberSeriesDBHelper.getInstance(context);

                int numberIndex = cursor.getColumnIndex(UsageLogsProvider.CallColumns.NUMBER);
                int dateIndex = cursor.getColumnIndex(UsageLogsProvider.CallColumns.DATE);
                int durationIndex = cursor.getColumnIndex(UsageLogsProvider.CallColumns.DURATION);
                int costIndex = cursor.getColumnIndex(UsageLogsProvider.CallColumns.COST);
                int callTypeIndex = cursor.getColumnIndex(UsageLogsProvider.CallColumns.CALL_TYPE);
                int usageTypeIndex = cursor.getColumnIndex(UsageLogsProvider.CallColumns
                        .USAGE_TYPE);

                do {
                    String number = cursor.getString(numberIndex);
                    float cost = Math.max(cursor.getFloat(costIndex), 0);
                    long date = cursor.getLong(dateIndex);
                    long duration = cursor.getLong(durationIndex);
                    int callType = cursor.getInt(callTypeIndex);
                    int usageType = cursor.getInt(usageTypeIndex);

                    int costPaisa = (int) (cost * 100);

                    CallSmsDetailInfo info = new CallSmsDetailInfo(usageType, NumberUtils
                            .removeCountryCode(number), date, duration, costPaisa);

                    totalCost += costPaisa;
                    if (usageType == UsageLogsProvider.CallColumns.TYPE_SMS) {
                        // SMS
                        smsList.add(info);
                    } else {
                        // Calls bifurcation

                        number = NumberUtils.processNumber(number);
                        if (callType == CallLog.Calls.INCOMING_TYPE) {
                            // Incoming calls, mostly roaming
                            roamingCallList.add(info);
                        } else {
                            // Outgoing calls bifurcation
                            if (NumberUtils.isNumberInternational(number)) {
                                // International calls
                                isdCallList.add(info);
                            } else {
                                int isMobileNumber = NumberUtils.checkIfMobileNumber(number);
                                OperatorCircleInfo circleInfo = (isMobileNumber == NumberUtils
                                        .YES) ? mobileDbHelper.getOperatorCircleInfo(number) : stdDbHelper
                                        .getOperatorCircleInfo(number);

                                if (circleInfo.getCircleId() == homeCircle) {
                                    // Local calls
                                    localCallList.add(info);
                                } else {
                                    // STD calls
                                    stdCallList.add(info);
                                }
                            }
                        }
                    }
                } while (cursor.moveToNext());
            }
            cursor.close();

            // Convert to JSON arrays
            usageInfo.localCalls = localCallList;
            usageInfo.stdCalls = stdCallList;
            usageInfo.isdCalls = isdCallList;
            usageInfo.roamingCalls = roamingCallList;
            usageInfo.sms = smsList;
            usageInfo.addCost(totalCost);
        }
    }

    public static DayCostInfo getTodayCallSmsDetails(Context context, int type) {
        DayCostInfo dayCostInfo = new DayCostInfo();
        SQLiteDatabase usageDB = UsageLogsProvider.CallSQLiteHelper.getInstance(context)
                .getReadableDatabase();
        long todayTime = DateUtils.getBeforeDays(0);
        String queryString = type == UsageLogsProvider.CallColumns.TYPE_CALL ? UsageLogsProvider
                .CallColumns.DAY_TOTAL_CALL_RAW_QUERY : UsageLogsProvider.CallColumns
                .DAY_TOTAL_SMS_RAW_QUERY;
        Cursor cursor = usageDB.rawQuery(queryString, new String[]{String.valueOf
                (todayTime), String.valueOf(type)});
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                dayCostInfo.cost = cursor.getFloat(cursor.getColumnIndex(UsageLogsProvider
                        .CallColumns.TOTAL_COST));
                if (type == UsageLogsProvider.CallColumns.TYPE_CALL) {
                    dayCostInfo.duration = cursor.getFloat(cursor.getColumnIndex
                            (UsageLogsProvider.CallColumns.TOTAL_DURATION));
                } else {
                    dayCostInfo.count = cursor.getInt(cursor.getColumnIndex(UsageLogsProvider
                            .CallColumns.TOTAL_COUNT));
                }
            }
            cursor.close();
        }
        return dayCostInfo;
    }

}

