package com.app.Billbachao.usagelog.library.parser.base.tata;

import com.app.Billbachao.usagelog.library.parser.base.BaseParser;
import com.app.Billbachao.usagelog.library.parser.model.CostInfo;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Unique pattern found in IDEA, to know the cost amount find string COST/CALL. FOr finding balance, find String BAL_LEFT
 *
 * Created by mihir on 02-11-2015.
 */
public class TataParser extends BaseParser implements TataConstants {

    @Override
    public boolean parse(String data) {
        if (data.contains(CALL) && data.contains(COST) && !data.contains(VOL)) {
            if(data.contains(DURATION)) {
                return parseCallUsage(data);
            } else {
                return parseSmsUsage(data);
            }
        } else if(data.contains(DATA) || data.contains(SESSION) || data.contains(INTERNET)) {
            return parseDataUsage(data);
        } else {
            return parseUssdData(data);
        }
    }

    @Override
    protected boolean parseDataUsage(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.DATA;

        Pattern mbkbPattern = Pattern.compile(DATA_PATTERN);
        Matcher mbkbMatcher = mbkbPattern.matcher(data);

        boolean isDataPack = false;

        if (mbkbMatcher.find()) {
            boolean isMb = (mbkbMatcher.group().contains("MB"));

            float volume[] = extractNumericData(data, 2, DATA_PATTERN);

            if (volume[1] > 0) {
                float dataUsed, dataLeft;
                int volumeIndex = Math.max(data.indexOf(CONSUMED), data.indexOf(USED));
                if (volumeIndex >= 0) {
                    dataUsed = extractNumericData(data.substring(volumeIndex), 1, DATA_PATTERN)[0];
                    dataLeft = Math.abs(volume[0] - dataUsed) < EPSILON ? volume[0] : volume[1];
                } else {
                    dataUsed = volume[0];
                    dataLeft = volume[1];
                }
                mCostInfo.dataUsed = dataUsed / (isMb ? 1 : MB_KB_RATIO);
                mCostInfo.dataLeft = dataLeft / (isMb ? 1 : MB_KB_RATIO);
                isDataPack = true;
            } else {
                mCostInfo.dataUsed = volume[0] / (isMb ? 1 : MB_KB_RATIO);
                isDataPack = false;
            }
        }

        if (data.contains(LEFT)) {
            mCostInfo.dataLeft = extractNumericData(data.substring(data.indexOf(LEFT)),
                    1, INT_DATA_PATTERN, true)[0];
        }

        return findCostAndBalance(data);
    }

    @Override
    protected boolean parseCallUsage(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.CALL;
        return findCostAndBalance(data);
    }

    @Override
    protected boolean parseSmsUsage(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.SMS;
        return findCostAndBalance(data);
    }

    @Override
    protected boolean parseUssdData(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.USSD;

        int index = data.indexOf(BAL);

        if (index >= 0) {
            mCostInfo.mainBalance = extractFloats(data.substring(index), 1)[0];
            return true;
        }
        return false;
    }

    boolean findCostAndBalance(String data) {

        boolean dataFound = false;

        int finalIndex = data.indexOf(COST);
        if (finalIndex < 0) {
            if (mCostInfo.type == CostInfo.DATA) {
                finalIndex = data.indexOf(CHARGE);
            }
        }

        if (finalIndex >= 0) {
            mCostInfo.cost= extractFloats(data.substring(finalIndex), 1)[0];
            dataFound = true;
        }

        Pattern leftPattern = Pattern.compile(BAL);
        Matcher leftMatcher = leftPattern.matcher(data);

        if (leftMatcher.find()) {
            int index = leftMatcher.end();
            if (index < data.length()) {
                mCostInfo.mainBalance = extractFloats(data.substring(index), 1)[0];
                dataFound = true;
            }
        }

        return dataFound;
    }
}

