package com.app.Billbachao.usagelog.contents.dialogs;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.usagelog.AccessibilityHelper;
import com.app.Billbachao.usagelog.model.AccessServiceInfo;

import java.util.List;

/**
 * Created by mihir on 22-03-2016.
 */
@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class UssdDialogFragment extends DialogFragment {

    public static final String TAG = UssdDialogFragment.class.getSimpleName();

    View mRootView;

    ListView mListView;

    Animation mAlphaAnimation;

    List<AccessServiceInfo> mServiceInfoList;

    public interface ActionListener {
        void onUssdSelected();
    }

    ActionListener mActionListener;

    TextView mTxtTitle;

    boolean isFromNotification = false ;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof ActionListener) {
            mActionListener = (ActionListener) activity;
        }
    }

    public static UssdDialogFragment newInstance(boolean isFromNotification) {
        Bundle args = new Bundle();
        UssdDialogFragment fragment = new UssdDialogFragment();
        args.putBoolean("IS_FROM_NOTIFICATION", isFromNotification);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDetach() {
        mActionListener = null;
        super.onDetach();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        mRootView = View.inflate(getActivity(), R.layout.ussd_dialog, null);
        builder.setView(mRootView);
        initView();

        if (!isFromNotification) {
            mTxtTitle.setText(getString(R.string.ussd_dialog_title));

            builder.setPositiveButton(R.string.continue_button, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (mActionListener != null)
                        mActionListener.onUssdSelected();
                }
            });
        } else {
            mTxtTitle.setText(getString(R.string.ussd_notification_dialog_title));

            builder.setPositiveButton(R.string.turn_ussd_on, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (mActionListener != null)
                        mActionListener.onUssdSelected();
                }
            });
            builder.setNegativeButton(R.string.turn_ussd_off, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }

        return builder.create();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isFromNotification = getArguments() != null ? getArguments().getBoolean("IS_FROM_NOTIFICATION") : false;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setCanceledOnTouchOutside(false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mAlphaAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim
                .ussd_list_selection_alpha);
        mServiceInfoList = AccessibilityHelper.loadData(getActivity());

        // Truncating extra services for showing current ours and a few others
        int installedSize = mServiceInfoList.size();
        final int MAX_LIST_SIZE = 3;
        if (installedSize > MAX_LIST_SIZE) {
            int oursIndex = -1;
            int index = 0;
            for (AccessServiceInfo accessServiceInfo : mServiceInfoList) {
                if (accessServiceInfo.isOurs()) {
                    oursIndex = index;
                    break;
                }
                index++;
            }
            if (oursIndex >= 0 && oursIndex < installedSize) {
                int startIndex = Math.max(0, oursIndex - (MAX_LIST_SIZE - 1));
                int endIndex = Math.min(startIndex + MAX_LIST_SIZE, installedSize);
                mServiceInfoList = mServiceInfoList.subList(startIndex, endIndex);
            }
        }

        View headerView = View.inflate(getActivity(), R.layout.ussd_list_header, null);
        mListView.addHeaderView(headerView, null, false);
        mListView.setAdapter(new AccessListAdapter(getActivity(), R.layout.two_list_item, mServiceInfoList));
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mServiceInfoList.get(position - mListView.getHeaderViewsCount()).isOurs()) {
                    if (mActionListener != null) {
                        mActionListener.onUssdSelected();
                        dismiss();
                    }
                }
            }
        });
        mRootView.findViewById(R.id.settings_title_copy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mActionListener != null) {
                    mActionListener.onUssdSelected();
                    dismiss();
                }
            }
        });
    }

    void initView() {
        mListView = (ListView) mRootView.findViewById(R.id.list);
        mTxtTitle = (TextView) mRootView.findViewById(R.id.dialog_title);
    }

    class AccessListAdapter extends ArrayAdapter<AccessServiceInfo> {

        Context mContext;

        int mResId;

        List<AccessServiceInfo> serviceList;

        public AccessListAdapter(Context context, int resource, List<AccessServiceInfo> objects) {
            super(context, resource, objects);
            serviceList = objects;
            mContext = context;
            mResId = resource;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(mContext, mResId, null);
                ViewHolder viewHolder = new ViewHolder(convertView);
                if (serviceList.get(position).isOurs()) {
                    viewHolder.dummyView.startAnimation(mAlphaAnimation);
                }
            }
            ViewHolder holder = (ViewHolder) convertView.getTag();
            holder.populate(serviceList.get(position));
            return convertView;
        }

        class ViewHolder {

            View dummyView;
            TextView name, state;

            ViewHolder(View view) {
                name = (TextView) view.findViewById(android.R.id.text1);
                state = (TextView) view.findViewById(android.R.id.text2);
                dummyView = view.findViewById(R.id.dummy_selector_view);
                view.setTag(this);
            }

            void populate(AccessServiceInfo accessServiceInfo) {
                name.setText(accessServiceInfo.getName());
                state.setText(accessServiceInfo.isEnabled() ? R.string.on : R.string.off);
                if (accessServiceInfo.isOurs()) {
                    dummyView.setVisibility(View.VISIBLE);
                } else
                    dummyView.setVisibility(View.GONE);
            }
        }
    }

}
