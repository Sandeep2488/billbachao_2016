package com.app.Billbachao.usagelog;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.widget.Toast;

import com.app.Billbachao.BuildConfig;
import com.app.Billbachao.R;
import com.app.Billbachao.usagelog.model.AccessServiceInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mihir on 29-10-2015.
 */
public class AccessibilityHelper {

    public static final String TAG = AccessibilityHelper.class.getSimpleName();

    Context mContext;

    public AccessibilityHelper(Context context) {
        mContext = context;
    }

    public boolean isEnabledAndRunning() {
        return isUssdServiceEnabled() && isRunning();
    }

    public boolean isUssdServiceEnabled() {
        String serviceName = BuildConfig.APPLICATION_ID + "/" + UssdReaderService.class
                .getCanonicalName();
        int accessibilityEnabled = Settings.Secure.getInt(mContext.getContentResolver(), Settings
                .Secure.ACCESSIBILITY_ENABLED, 0);

        // Accessibility is enabled
        if (accessibilityEnabled == 1) {
            String enabledServices = Settings.Secure.getString(mContext.getContentResolver(),
                    Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
            TextUtils.SimpleStringSplitter splitter = new TextUtils.SimpleStringSplitter(':');
            splitter.setString(enabledServices);
            while (splitter.hasNext()) {
                String accessibilityService = splitter.next();
                if (accessibilityService.equalsIgnoreCase(serviceName)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isRunning() {
        ActivityManager manager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        String currentName = UssdReaderService.class.getName();
        for (ActivityManager.RunningServiceInfo info : manager.getRunningServices(Integer
                .MAX_VALUE)) {
            if (info == null || info.service == null) {
                continue;
            }
            if (currentName.equalsIgnoreCase(info.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public boolean launchAccessibilitySettings() {
        PackageManager packageManager = mContext.getPackageManager();

        Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
        if (packageManager.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY) != null) {
            mContext.startActivity(intent);
            Toast.makeText(mContext, mContext.getString(R.string.ussd_start_message, mContext
                    .getString(R.string.ussd_reader_name)), Toast.LENGTH_LONG).show();
            return true;
        } else {
            Toast.makeText(mContext, mContext.getString(R.string
                    .ussd_start_message_goto_settings, mContext
                    .getString(R.string.ussd_reader_name)), Toast.LENGTH_LONG).show();
            return false;
        }
    }

    /**
     * General function for all services
     */
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public static ArrayList<AccessServiceInfo> loadData(Context context) {
        String bbServiceName = UssdReaderService.class.getName();

        AccessibilityManager accessibilityManager = (AccessibilityManager) context.getSystemService
                (Context.ACCESSIBILITY_SERVICE);
        List<AccessibilityServiceInfo> installedServices = accessibilityManager.getInstalledAccessibilityServiceList();
        List<AccessibilityServiceInfo> enabledServices = accessibilityManager
                .getEnabledAccessibilityServiceList(AccessibilityEvent.TYPES_ALL_MASK);

        PackageManager packageManager = context.getPackageManager();

        // List of enabled services
        ArrayList<String> enabledServicesList = new ArrayList<>();
        for (AccessibilityServiceInfo enabledServiceInfo : enabledServices) {
            if (enabledServiceInfo.getResolveInfo() == null) continue;
            String serviceName = enabledServiceInfo.getResolveInfo().loadLabel(packageManager).toString();
            enabledServicesList.add(serviceName);
        }

        // List of installed service with enabled info
        ArrayList<AccessServiceInfo> servicesList = new ArrayList<>();
        for (AccessibilityServiceInfo accessibilityServiceInfo : installedServices) {
            if (accessibilityServiceInfo.getResolveInfo() == null) continue;
            String serviceName = accessibilityServiceInfo.getResolveInfo().loadLabel(packageManager).toString();
            AccessServiceInfo info = new AccessServiceInfo(serviceName);
            int index = enabledServicesList.indexOf(serviceName);
            if (index >= 0) {
                info.setEnabled(true);
            }
            ServiceInfo serviceInfo = accessibilityServiceInfo.getResolveInfo().serviceInfo;
            if (serviceInfo != null && serviceInfo.name.equalsIgnoreCase(bbServiceName)) {
                info.setOurs(true);
            }
            servicesList.add(info);
        }
        return servicesList;
    }
}
