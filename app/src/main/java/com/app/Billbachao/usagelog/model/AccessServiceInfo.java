package com.app.Billbachao.usagelog.model;

/**
 * Created by mihir on 22-03-2016.
 */
public class AccessServiceInfo  {

    private String name;

    private boolean isOurs, isEnabled;

    public AccessServiceInfo(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean isOurs() {
        return isOurs;
    }

    public void setOurs(boolean isOurs) {
        this.isOurs = isOurs;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }
}
