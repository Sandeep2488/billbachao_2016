package com.app.Billbachao.usagelog.contents.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.usagelog.provider.UsageLogsProvider;
import com.app.Billbachao.usagelog.widget.AppBitmapCache;
import com.app.Billbachao.usagelog.widget.AppBitmapFactory;
import com.app.Billbachao.utils.DateUtils;

/**
 * Created by mihir on 28-10-2015.
 */
public class DataLogsAdapter extends BaseResourceCursorAdapter {

    Context mContext;

    int mDateIdx, mCostIdx, mUsageKbIdx, mWifiUsageKbIdx, mUssdMobileUsageIdx;

    static final int MAX_APPS_SHOWN = 3;

    int mLargeBitmapSize, mSmallBitmapSize;

    public DataLogsAdapter(Cursor cursor, Context context) {
        super(cursor, context);
        mContext = context;
        mLargeBitmapSize = context.getResources().getDimensionPixelSize(R.dimen
                .data_usage_app_icon_expanded_size);
        mSmallBitmapSize = context.getResources().getDimensionPixelSize(R.dimen
                .data_usage_app_icon_size);
    }

    public void initIndices(Cursor cursor) {
        mDateIdx = cursor.getColumnIndex(UsageLogsProvider.DayWiseDataColumns.DATE);
        mUsageKbIdx = cursor.getColumnIndex(UsageLogsProvider.DayWiseDataColumns.USAGE_MOBILE);
        mCostIdx = cursor.getColumnIndex(UsageLogsProvider.DayWiseDataColumns.COST);
        mUssdMobileUsageIdx = cursor.getColumnIndex(UsageLogsProvider.DayWiseDataColumns.USSD_USAGE_MOBILE);
        mWifiUsageKbIdx = cursor.getColumnIndex(UsageLogsProvider.DayWiseDataColumns.USAGE_WIFI);
    }

    @Override
    protected Cursor getChildrenCursor(Cursor groupCursor) {
        String date = groupCursor.getString(mDateIdx);
        return mContext.getContentResolver().query(UsageLogsProvider.DataColumns
                .CONTENT_URI, null, UsageLogsProvider.DataColumns.DATE + "=?", new
                String[]{date}, UsageLogsProvider.DataColumns.USAGE_KB + " DESC");
    }

    @Override
    protected View newGroupView(Context context, Cursor cursor, boolean isExpanded, ViewGroup
            parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.data_log_row,
                parent, false);

        ViewHolder holder = new ViewHolder();
        view.setTag(holder);
        holder.date = (TextView) view.findViewById(R.id.date);
        holder.totalUsage = (TextView) view.findViewById(R.id.usage);
        holder.appsGroup = (LinearLayout) view.findViewById(R.id.apps_group);
        holder.nMore = (TextView) view.findViewById(R.id.n_more);
        holder.cost = (TextView) view.findViewById(R.id.cost);
        holder.divider = view.findViewById(R.id.divider);
        return view;
    }

    @Override
    protected void bindGroupView(View view, Context context, Cursor cursor, boolean isExpanded) {
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.divider.setVisibility(isExpanded ? View.GONE : View.VISIBLE);

        String dateString = cursor.getString(mDateIdx);
        holder.date.setText(DateUtils.getDayString(dateString, DateUtils.FORMAT_DATE_ONLY));

        float mobileUsageUssd = cursor.getFloat(mUssdMobileUsageIdx);
        if (mobileUsageUssd == 0) {
            mobileUsageUssd = cursor.getFloat(mUsageKbIdx) / 1024;
        }
        holder.totalUsage.setText(context.getString(R.string.n2_mb, mobileUsageUssd));

        float cost = cursor.getFloat(mCostIdx);
        if (cost != 0) {
            holder.cost.setText(context.getString(R.string.inr_n2, cost));
            holder.cost.setVisibility(View.VISIBLE);
        } else {
            holder.cost.setVisibility(View.GONE);
        }
        bindAppsGroupView(holder, context, dateString);
    }

    @Override
    protected View newChildView(Context context, Cursor cursor, boolean isLastChild, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.data_log_child_row,
                parent, false);
        ChildViewHolder holder = new ChildViewHolder();
        view.setTag(holder);

        holder.percentShare = (TextView) view.findViewById(R.id.percent_share);
        holder.nameText = (TextView) view.findViewById(R.id.name);
        holder.icon = (ImageView) view.findViewById(R.id.icon);
        holder.divider = view.findViewById(R.id.divider);

        return view;
    }

    @Override
    protected void bindChildView(View view, Context context, Cursor cursor, boolean isLastChild) {
        ChildViewHolder holder = (ChildViewHolder) view.getTag();

        Cursor groupCursor = getCursor();

        float mobileUsage = groupCursor.getFloat(groupCursor.getColumnIndex(UsageLogsProvider
                .DayWiseDataColumns.USAGE_MOBILE));
        float wifiUsage = groupCursor.getFloat(groupCursor.getColumnIndex(UsageLogsProvider
                .DayWiseDataColumns.USAGE_WIFI));
        float totalUsage = wifiUsage + mobileUsage;

        holder.nameText.setText(cursor.getString(cursor.getColumnIndex(UsageLogsProvider
                .DataColumns.NAME)));

        // Caching mechanism for memory saving
        String packageName = cursor.getString
                (cursor.getColumnIndex(UsageLogsProvider.DataColumns.PACKAGE_NAME));
        Bitmap bitmap = AppBitmapCache.getBitmapFromMemCache(packageName, mLargeBitmapSize);
        if (bitmap == null) {
            bitmap = AppBitmapFactory.getBitmapFromBlob(cursor.getBlob
                            (cursor.getColumnIndex(UsageLogsProvider.DataColumns.BITMAP)),
                    mLargeBitmapSize);
            AppBitmapCache.addBitmapToMemoryCache(packageName, mLargeBitmapSize, bitmap);
        }

        holder.icon.setImageBitmap(bitmap);
        int percent = (int) (totalUsage == 0 ? 0 : cursor.getFloat(cursor.getColumnIndex
                (UsageLogsProvider.DataColumns.USAGE_KB)) * 100 / totalUsage);
        holder.percentShare.setText(mContext.getString(R.string.n_percent, String.valueOf(percent)));
        holder.divider.setVisibility(isLastChild ? View.VISIBLE : View.GONE);
    }

    void bindAppsGroupView(ViewHolder holder, Context context, String date) {
        Cursor appsCursor = context.getContentResolver().query(UsageLogsProvider.DataColumns
                .CONTENT_URI, null, UsageLogsProvider.DataColumns.DATE + "=?", new
                String[]{date}, UsageLogsProvider.DataColumns.USAGE_KB + " DESC");

        if (appsCursor.moveToFirst()) {
            int bitmapColumn = appsCursor.getColumnIndex(UsageLogsProvider.DataColumns.BITMAP);
            int packageColumn = appsCursor.getColumnIndex(UsageLogsProvider.DataColumns
                    .PACKAGE_NAME);
            int index = 0;
            holder.appsGroup.removeAllViews();
            do {
                View view = View.inflate(context, R.layout.data_app_image, null);
                ImageView imageView = (ImageView) view.findViewById(R.id.app_icon);

                // Caching mechanism
                String packageName = appsCursor.getString(packageColumn);
                Bitmap bitmap = AppBitmapCache.getBitmapFromMemCache(packageName, mSmallBitmapSize);
                if (bitmap == null) {
                    bitmap = AppBitmapFactory.getBitmapFromBlob(appsCursor.getBlob(bitmapColumn), mSmallBitmapSize);
                    AppBitmapCache.addBitmapToMemoryCache(packageName, mSmallBitmapSize, bitmap);
                }

                imageView.setImageBitmap(bitmap);
                holder.appsGroup.addView(view);
            } while (appsCursor.moveToNext() && ++index < MAX_APPS_SHOWN);
        }

        int count = appsCursor.getCount();
        if (count > MAX_APPS_SHOWN) {
            String additional = context.getString(R.string.add_n_more, count - MAX_APPS_SHOWN);
            holder.nMore.setVisibility(View.VISIBLE);
            holder.nMore.setText(additional);
        }
        appsCursor.close();

    }

    class ViewHolder {

        TextView date;

        TextView totalUsage;

        LinearLayout appsGroup;

        TextView nMore;

        TextView cost;

        View divider;
    }

    class ChildViewHolder {

        TextView percentShare;

        ImageView icon;

        TextView nameText;

        int totalUsage;

        View divider;
    }

}
