package com.app.Billbachao.usagelog.library.parser.base.tata;

/**
 * Created by mihir on 02-11-2015.
 */
public interface TataConstants {

    String COST = "COST", CALL = "CALL", DATA = "DATA", SESSION= "SESSION", INTERNET = "INTERNET", CHARGE = "CHARGE";

    String DURATION = "DURATION", VOL = "VOL", USED = "USED", CONSUMED = "CONSUMED", LEFT = "LEFT";
}
