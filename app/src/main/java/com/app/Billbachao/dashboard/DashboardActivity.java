package com.app.Billbachao.dashboard;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.accessibility.AccessibilityManagerCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import android.widget.TextView;

import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.BuildConfig;
import com.app.Billbachao.R;
import com.app.Billbachao.apis.CardsListApi;
import com.app.Billbachao.cards.contents.fragments.DashboardCardFragment;
import com.app.Billbachao.launch.DeepLinkHelper;
import com.app.Billbachao.notifications.gcm.model.NotificationModel;
import com.app.Billbachao.notifications.gcm.utils.InBoxNotificationHelper;
import com.app.Billbachao.notifications.gcm.utils.NotificationConstant;
import com.app.Billbachao.notifications.gcm.utils.NotificationHelper;
import com.app.Billbachao.shareandrate.utils.ShareUtils;
import com.app.Billbachao.usage.helper.UsageHelper;
import com.app.Billbachao.usagelog.AccessibilityHelper;
import com.app.Billbachao.usagelog.contents.UsageLogActivity;
import com.app.Billbachao.usagelog.contents.dialogs.UssdDialogFragment;
import com.app.Billbachao.utils.CircleUtils;
import com.app.Billbachao.utils.InBoxMenuHelper;
import com.app.Billbachao.utils.OperatorUtils;
import com.app.Billbachao.utils.PreferenceUtils;
import com.app.Billbachao.utils.UiUtils;

public class DashboardActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, UssdDialogFragment.ActionListener {

    View mDropDownView, mAdditionalInfoView;

    boolean mAdditionalInfoVisible, mUSSDActionPending;

    AccessibilityManager mAccessibilityManager;

    AccessibilityHelper mAccessibilityHelper;

    DashboardCardFragment mDashboardCardFragment;

    InBoxMenuHelper mInBoxMenuHelper;

    InBoxNotificationHelper mInBoxNotificationHelper;

    private NotificationHelper mNotificationHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        initNotification();
        initMenuHelper();

        checkToSendNotificationCount();

        initDeepLinker();
        initView();
        initUssdParams();
        if (getIntent().getStringExtra(UsageLogActivity.LAUNCH_FROM_NOTIFICATION) != null) {
            UiUtils.showUssdDialog(this, true);
        }
    }

    private void checkToSendNotificationCount() {
        Intent dashBoardIntent = getIntent();

        mNotificationHelper = new NotificationHelper(this);
        if (dashBoardIntent == null)
            return;

        Bundle bundle = dashBoardIntent.getExtras();

        if (bundle == null)
            return;

        NotificationModel mNotificationModel = bundle.getParcelable(NotificationConstant.KEY_NOTIFICATION_MODEL);
        mNotificationHelper.updateNotificationStatus(mNotificationModel);
    }

    /**
     * Check of any deepLinking.
     */
    private void initDeepLinker() {
        DeepLinkHelper.checkForExternalCalls(this, getIntent());
    }

    /**
     * Initialise menu helper
     */
    private void initMenuHelper() {
        mInBoxMenuHelper = new InBoxMenuHelper(this);
    }

    /**
     * Initialise the notification count observer
     */
    private void initNotification() {
        mInBoxNotificationHelper = new InBoxNotificationHelper(this);
        mInBoxNotificationHelper.registerInBoxObserver();
    }

    @Override
    protected boolean isHomeAsUpEnabled() {
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        // State wherein user has returned after turning on USSD
        if (mUSSDActionPending) {
            UiUtils.launchUsageLogOrSettings(this, false);
            mDashboardCardFragment.updateCardList();
            mUSSDActionPending = false;
        }
    }

    @Override
    protected void onDestroy() {
        AccessibilityManagerCompat.removeAccessibilityStateChangeListener(mAccessibilityManager,
                mAccessibilityStateChangeListener);
        UsageHelper.reset();
        //Unregister notification count observer.
        mInBoxNotificationHelper.unRegisterInBoxObserver();

        //Cancel any pending request for notification.
        new NotificationHelper(this).cancelNotificationStatus();

        super.onDestroy();
    }

    void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDashboardCardFragment = new DashboardCardFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment, mDashboardCardFragment).commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        initNavigationView();
    }

    void initNavigationView() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = View.inflate(this, R.layout.nav_header_dashboard, null);
        navigationView.addHeaderView(headerView);
        navigationView.setNavigationItemSelectedListener(this);

        ((TextView) headerView.findViewById(R.id.mobile_number)).setText(PreferenceUtils.getMobileNumber(this));
        ((TextView) headerView.findViewById(R.id.mail_id)).setText(PreferenceUtils.getMailId(this));
        ((TextView) headerView.findViewById(R.id.operator)).setText(OperatorUtils.getOperatorName(this));
        ((TextView) headerView.findViewById(R.id.circle)).setText(CircleUtils.getCircleName(this));

        if (BuildConfig.DEBUG)
            ((TextView) headerView.findViewById(R.id.day)).setText("DAY " + CardsListApi.getDay(this));
        else headerView.findViewById(R.id.day).setVisibility(View.GONE);
        mDropDownView = headerView.findViewById(R.id.drop_down);
        mDropDownView.setOnClickListener(mClickListener);
        headerView.findViewById(R.id.edit_profile).setOnClickListener(mClickListener);
        mAdditionalInfoView = headerView.findViewById(R.id.additional_info);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        switch (item.getItemId()) {
            case R.id.quick_recharge:
                UiUtils.launchRecharge(this);
                break;
            case R.id.app_recommendation:
                UiUtils.launchAppRecommendation(this);
                break;
            case R.id.self_help:
                UiUtils.launchSelfHelp(this);
                break;
            case R.id.nav_share:
                ShareUtils.shareApp(this);
                break;
            case R.id.feedback:
                showFeedbackDialog();
                break;
            case R.id.alerts:
                UiUtils.launchAlerts(this);
                break;
            case R.id.tutorial:
                UiUtils.launchWalkthrough(this);
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.drop_down:
                    if (mAdditionalInfoView.getVisibility() == View.VISIBLE) {
                        mAdditionalInfoView.animate().alpha(0);
                        mDropDownView.animate().rotation(0).setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                mAdditionalInfoView.setVisibility(View.GONE);
                                mAdditionalInfoView.setAlpha(1);
                                mAdditionalInfoVisible = false;
                            }
                        });
                    } else {
                        mDropDownView.animate().rotation(180).setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                mAdditionalInfoView.setVisibility(View.VISIBLE);
                                mAdditionalInfoVisible = true;
                            }
                        });
                    }
                    break;
                case R.id.edit_profile:
                    UiUtils.launchEditProfile(DashboardActivity.this);
                    break;
            }
        }
    };

    private void initUssdParams() {
        mAccessibilityManager = (AccessibilityManager) getSystemService(ACCESSIBILITY_SERVICE);
        AccessibilityManagerCompat.addAccessibilityStateChangeListener(mAccessibilityManager,
                mAccessibilityStateChangeListener);
        mAccessibilityHelper = new AccessibilityHelper(this);
    }

    @Override
    public void onUssdSelected() {
        AccessibilityHelper helper = new AccessibilityHelper(this);
        if (helper.launchAccessibilitySettings()) {
            mUSSDActionPending = true;
        }
    }

    AccessibilityManagerCompat.AccessibilityStateChangeListenerCompat
            mAccessibilityStateChangeListener = new AccessibilityManagerCompat
            .AccessibilityStateChangeListenerCompat() {

        @Override
        public void onAccessibilityStateChanged(boolean enabled) {
            if (enabled) {
                if (mUSSDActionPending) {
                    if (mAccessibilityHelper.isEnabledAndRunning()) {
                        // clear top of current activity
                        Intent intent = new Intent(DashboardActivity.this, DashboardActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent
                                .FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                }
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mInBoxMenuHelper.onCreateOptionsMenu(menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return mInBoxMenuHelper.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        mInBoxMenuHelper.onPrepareOptionsMenu(menu);
        return super.onPrepareOptionsMenu(menu);
    }

}
