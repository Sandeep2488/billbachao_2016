package com.app.Billbachao.externalsdk.google;

/**
 * Created by mihir on 04-04-2016.
 */
public interface AppIndexBehavior {

    String getAppIndexTitle();

    String getAppIndexAppendedUrl();

    class Keywords {
        public static final String RECHARGE = "Recharge", BILL_PAYMEMNT = "Bill payment",
                NETWORK = "Best Network", BROWSE_PLANS = "Recharge plans", USAGE_SUMMARY =
                "Prepaid bill";
    }
}
