package com.app.Billbachao.externalsdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.volley.request.NetworkStringRequest;
import com.app.Billbachao.volley.utils.VolleySingleTon;
import com.app.Billbachao.volley.utils.VolleyUtils;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by mihir on 11-05-2016.
 */
public class CustomInstallReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        initRemarketing(context);
    }

    private void initRemarketing(Context context) {
        AdvertisingIdClient.Info adInfo = null;
        try {
            adInfo = AdvertisingIdClient.getAdvertisingIdInfo(context.getApplicationContext());
        } catch (IOException | GooglePlayServicesNotAvailableException |
                GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        }
        if (adInfo == null) {
            return;
        }
        String BASE_REMARKETING_URL = "https://www.googleadservices.com/pagead/conversion/", ID =
                "937697244";
        StringBuilder builder = new StringBuilder(BASE_REMARKETING_URL);
        builder.append(ID + "/?");

        HashMap<String, String> params = new LinkedHashMap<>();
        params.put("bundleid", context.getPackageName());
        params.put("rdid", adInfo.getId());
        params.put("idtype", "advertisingid");
        params.put("lat", adInfo.isLimitAdTrackingEnabled() ? "1" : "0");
        params.put("remarketing_only", "1");
        builder.append(VolleyUtils.encodeParameters(params, "UTF-8"));

        NetworkStringRequest remarketinRequest = new NetworkStringRequest(Request.Method.GET,
                BASE_REMARKETING_URL, null, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                ILog.appendLog("Remarketing successful !!");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ILog.appendLog("Remarketing unsuccessful !!");
            }
        });
        ILog.appendLog("CustomReceiver is activated to campaign");
        VolleySingleTon.getInstance(context).addToRequestQueue(remarketinRequest);
    }
}
