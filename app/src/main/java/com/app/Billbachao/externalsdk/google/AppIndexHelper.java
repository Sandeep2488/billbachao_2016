package com.app.Billbachao.externalsdk.google;

import android.net.Uri;

import com.app.Billbachao.utils.ILog;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

/**
 * Created by mihir on 12-05-2016.
 */
public class AppIndexHelper {

    public static final String TAG = AppIndexHelper.class.getSimpleName();

    public static final Uri BASE_APP_URI = Uri.parse("android-app://com.app.Billbachao/billbachao/goto");

    public static void sendAppIndexEvent(GoogleApiClient client, final String title, final String url,
                                         boolean isStart) {
        final Uri APP_URI = BASE_APP_URI.buildUpon().appendEncodedPath(url).build();
        Action viewAction = Action.newAction(Action.TYPE_VIEW, title, APP_URI);

        if (isStart)
            client.connect();

        // Call the App Indexing API view method
        PendingResult<Status> result = isStart ? AppIndex.AppIndexApi.start(client, viewAction)
                : AppIndex.AppIndexApi.end(client, viewAction);

        result.setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                if (status.isSuccess()) {
                    ILog.d(TAG, "App Indexing API: event sent successfully.");
                } else {
                    ILog.e(TAG, "App Indexing API: There was an error recording the event."
                            + status.toString());
                }
            }
        });

        if (!isStart)
            client.disconnect();
    }
}
