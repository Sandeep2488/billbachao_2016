package com.app.Billbachao.externalsdk;

import android.content.Context;

import com.appsflyer.AppsFlyerLib;

/**
 * Created by mihir on 19-03-2016.
 */
public class AppsFlyerUtils {

    public static final String REGISTERED = "MenuActivity_GetStarted";
    public static final String MY_BEST_NETWORK = "Best network";
    public static final String QUICK_RECHARGE = "Recharge";
    public static final String SELF_HELP = "Self Help";
    public static final String PAYMENT_SUCCESS = "Payment Success";
    public static final String USAGE_SUMMARY = "Usage Summary";
    public static final String APP_OF_THE_DAY = "App of the day";
    public static final String ALERTS = "Alerts";

    public static void trackEvent(Context context, String event) {
        AppsFlyerLib.getInstance().trackEvent(context, event, null);
    }

}
