package com.app.Billbachao.selfhelp.contents.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;

import com.app.Billbachao.R;
import com.app.Billbachao.utils.OperatorUtils;

/**
 * Created by mihir on 22-09-2015.
 */
public class MNPPopupDialog extends DialogFragment {

    public static final String TAG = MNPPopupDialog.class.getSimpleName();

    public static final int REQUEST_MNP = 0x901;

    public static MNPPopupDialog getInstance(Fragment parentFragment) {
        MNPPopupDialog dialog = new MNPPopupDialog();
        dialog.setTargetFragment(parentFragment, REQUEST_MNP);
        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.mnp_popup_title);
        builder.setMessage(getString(R.string.mnp_message, OperatorUtils.getOperatorName
                (getActivity())));
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent();
                getTargetFragment().onActivityResult(REQUEST_MNP, Activity.RESULT_OK,
                        intent);
            }
        });

        return builder.create();
    }

}
