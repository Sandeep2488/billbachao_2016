package com.app.Billbachao.selfhelp.contents.fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.selfhelp.contents.dialogs.MNPPopupDialog;
import com.app.Billbachao.utils.OperatorUtils;
import com.app.Billbachao.utils.PreferenceUtils;
import com.app.Billbachao.utils.USSDUtils;

/**
 * Created by mihir on 12-08-2015.
 */
public class SettingsFragment extends Fragment implements View.OnClickListener {

    boolean mIsPrepaid, mIsNewAlert;
    String TAG = "Self Help";

    SwitchCompat mPlanAlerts;

    public static final int[] VIEWGROUP_IDS_COMMON = new int[]{R.id
            .dnd_container, R.id.deactivate_vas_container, R.id.get_3G_container, R.id
            .get_data_container, R.id.data_balance_container};

    public static final int[] TYPE_COMMON = new int[]{USSDUtils.DND, USSDUtils.DEACTIVATE_VAS,
            USSDUtils.GET_3G, USSDUtils.DATA_SETTINGS, USSDUtils.DATA_BALANCE};

    public static final int[] VIEWGROUP_IDS_PREPAID = new int[]{R.id.check_balance_container, R
            .id.last_recharge_container, R.id.my_plan_container};

    public static final int[] TYPE_PREPAID = new int[]{USSDUtils.CHECK_BALANCE, USSDUtils
            .LAST_RECHARGE, USSDUtils.MY_PLAN};

    public static final int[] VIEWGROUP_IDS_POSTPAID = new int[]{R.id.check_balance_container, R
            .id.bill_summary_container, R.id.itemised_bill_container, R.id
            .unbilled_amount_container, R.id.last_payment_container, R.id.sim_lost_container};

    public static final int[] TYPE_POSTPAID = new int[]{USSDUtils.CHECK_BILL, USSDUtils
            .BILL_SUMMARY, USSDUtils.ITEMISED_BILL, USSDUtils.UNBILLED_AMOUNT, USSDUtils
            .LAST_PAYMENT, USSDUtils.LOSS_OF_SIM};

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIsPrepaid = PreferenceUtils.isPrepaid(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        initView(view);
        return view;
    }

    void initView(View containerView) {
        ((TextView) containerView.findViewById(R.id.check_balance_text)).setText(mIsPrepaid ? R
                .string.my_balance : R.string.my_bill);

        mPlanAlerts = (SwitchCompat) containerView.findViewById(R.id.alert_new_plans);
        mIsNewAlert = PreferenceUtils.isNewAlertsOn(getActivity());
        mPlanAlerts.setChecked(mIsNewAlert);
        mPlanAlerts.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            }
        });

        ((TextView) containerView.findViewById(R.id.call_care_text)).setText(getString(R.string
                .call_care_n, OperatorUtils.getOperatorName(getActivity())));

        showHideContainers(containerView);
        addClickListeners(containerView);
    }

    void showHideContainers(View view) {
        Activity activity = getActivity();

        // Show/Hide containers
        for (int index = 0; index < VIEWGROUP_IDS_COMMON.length; index++) {
            int viewGroup = VIEWGROUP_IDS_COMMON[index];
            int type = TYPE_COMMON[index];
            view.findViewById(viewGroup).setVisibility(USSDUtils.isSupported(activity, type) ?
                    View.VISIBLE : View.GONE);
        }

        int[] VIEWGROUP_UNCOMMON = mIsPrepaid ? VIEWGROUP_IDS_PREPAID : VIEWGROUP_IDS_POSTPAID;
        int[] typeUncommon = mIsPrepaid ? TYPE_PREPAID : TYPE_POSTPAID;

        for (int index = 0; index < VIEWGROUP_UNCOMMON.length; index++) {
            int viewGroup = VIEWGROUP_UNCOMMON[index];
            int type = typeUncommon[index];
            view.findViewById(viewGroup).setVisibility(USSDUtils.isSupported(activity, type) ?
                    View.VISIBLE : View.GONE);
        }
    }

    void addClickListeners(View containerView) {
        containerView.findViewById(R.id.check_balance).setOnClickListener(this);
        containerView.findViewById(R.id.deactivate_vas).setOnClickListener(this);
        containerView.findViewById(R.id.get_data).setOnClickListener(this);
        containerView.findViewById(R.id.get_3G).setOnClickListener(this);
        containerView.findViewById(R.id.dnd).setOnClickListener(this);
        containerView.findViewById(R.id.call_care).setOnClickListener(this);
        containerView.findViewById(R.id.request_mnp).setOnClickListener(this);
        containerView.findViewById(R.id.data_balance).setOnClickListener(this);

        if (mIsPrepaid) {
            // Prepaid
            containerView.findViewById(R.id.last_recharge).setOnClickListener(this);
            containerView.findViewById(R.id.my_plan).setOnClickListener(this);
        } else {
            // Postpaid
            containerView.findViewById(R.id.bill_summary).setOnClickListener(this);
            containerView.findViewById(R.id.itemised_bill).setOnClickListener(this);
            containerView.findViewById(R.id.unbilled_amount).setOnClickListener(this);
            containerView.findViewById(R.id.last_payment).setOnClickListener(this);
            containerView.findViewById(R.id.lost_sim).setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.check_balance:
                checkBalance();
                break;
            case R.id.deactivate_vas:
                USSDUtils.entertainRequest(getActivity(), USSDUtils.DEACTIVATE_VAS);
                break;
            case R.id.get_3G:
                USSDUtils.entertainRequest(getActivity(), USSDUtils.GET_3G);
                break;
            case R.id.get_data:
                USSDUtils.entertainRequest(getActivity(), USSDUtils.DATA_SETTINGS);
                break;
            case R.id.dnd:
                USSDUtils.entertainRequest(getActivity(), USSDUtils.DND);
                break;
            case R.id.call_care:
                USSDUtils.entertainRequest(getActivity(), USSDUtils.CALL_CENTER);
                break;
            case R.id.request_mnp:
                MNPPopupDialog dialog = MNPPopupDialog.getInstance(this);
                dialog.show(getFragmentManager(), MNPPopupDialog.TAG);
                break;
            case R.id.data_balance:
                USSDUtils.entertainRequest(getActivity(), USSDUtils.DATA_BALANCE);
                break;
            // Prepaid
            case R.id.last_recharge:
                USSDUtils.entertainRequest(getActivity(), USSDUtils.LAST_RECHARGE);
                break;
            case R.id.my_plan:
                USSDUtils.entertainRequest(getActivity(), USSDUtils.MY_PLAN);
                break;
            // Postpaid
            case R.id.bill_summary:
                USSDUtils.entertainRequest(getActivity(), USSDUtils.BILL_SUMMARY);
                break;
            case R.id.itemised_bill:
                USSDUtils.entertainRequest(getActivity(), USSDUtils.ITEMISED_BILL);
                break;
            case R.id.unbilled_amount:
                USSDUtils.entertainRequest(getActivity(), USSDUtils.UNBILLED_AMOUNT);
                break;
            case R.id.last_payment:
                USSDUtils.entertainRequest(getActivity(), USSDUtils.LAST_PAYMENT);
                break;
            case R.id.lost_sim:
                USSDUtils.entertainRequest(getActivity(), USSDUtils.LOSS_OF_SIM);
                break;
        }
    }

    void checkBalance() {
        if (mIsPrepaid) {
            USSDUtils.entertainRequest(getActivity(), USSDUtils.CHECK_BALANCE);
        } else {
            USSDUtils.entertainRequest(getActivity(), USSDUtils.CHECK_BILL);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case MNPPopupDialog.REQUEST_MNP:
                if (resultCode == Activity.RESULT_OK) {
                    USSDUtils.requestMnp(getActivity());
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        if (mPlanAlerts.isChecked() != mIsNewAlert) {
            mIsNewAlert = mPlanAlerts.isChecked();
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences
                    (getActivity());
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(PreferenceUtils.PLAN_ALERTS, mIsNewAlert);
            editor.apply();
        }
        super.onDestroy();
    }
}
