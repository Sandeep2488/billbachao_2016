package com.app.Billbachao.widget.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by mihir on 05-11-2015.
 * Custom view pager designed that would listen to swipe on last page, and pass the callback
 */
public class DismissLastViewPager extends ViewPager {

    static final float THRESHOLD_DIFFERENCE = 20;

    float mStartDragX;

    OnSwipeOutListener mSwipeOutListener;

    boolean mSwipeOutDone = false;

    public DismissLastViewPager(Context context) {
        super(context);
    }

    public DismissLastViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setOnSwipeOutListener(OnSwipeOutListener listener) {
        mSwipeOutListener = listener;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        float x = ev.getX();
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mStartDragX = x;
                break;
            case MotionEvent.ACTION_MOVE:
            case MotionEvent.ACTION_UP:
                if (!mSwipeOutDone) {
                    if ((mStartDragX - x) >= THRESHOLD_DIFFERENCE && getCurrentItem() == getAdapter()
                            .getCount() - 1) {
                        if (mSwipeOutListener != null) {
                            mSwipeOutListener.onSwipeOutAtEnd();
                            mSwipeOutDone = true;
                        }
                    }
                }
                break;
        }
        return super.onTouchEvent(ev);
    }

    public interface OnSwipeOutListener {
        void onSwipeOutAtEnd();
    }
}
