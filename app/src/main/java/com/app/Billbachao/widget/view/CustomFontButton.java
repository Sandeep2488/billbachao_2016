package com.app.Billbachao.widget.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.Button;

import com.app.Billbachao.R;
import com.app.Billbachao.utils.TypeFaceHelper;

/**
 * CustomFontButton with custom fonts from assets.
 * Created by nitesh on 25-04-2016.
 */
public class CustomFontButton extends Button {

    public CustomFontButton(Context context) {
        super(context);
    }

    public CustomFontButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public CustomFontButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.CustomFontTextView);
        String customFont = a.getString(R.styleable.CustomFontButton_fontButton);

        if (customFont == null)
            customFont = ctx.getString(R.string.font_regular);

        setCustomFont(ctx, customFont);
        a.recycle();
    }

    private void setCustomFont(Context ctx, String asset) {
        setTypeface(TypeFaceHelper.getInstance(ctx).getStyleTypeFace(asset));
    }

}
