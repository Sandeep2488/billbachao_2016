package com.app.Billbachao.widget.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.utils.TypeFaceHelper;

/**
 * CustomFontTextView with custom fonts from assets.
 * Created by nitesh on 25-04-2016.
 */
public class CustomFontTextView extends TextView {

    public CustomFontTextView(Context context) {
        super(context);
    }

    public CustomFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public CustomFontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.CustomFontTextView);
        String customFont = a.getString(R.styleable.CustomFontTextView_fontTextView);

        if (customFont == null)
            customFont = ctx.getString(R.string.font_regular);

        setCustomFont(ctx, customFont);
        a.recycle();
    }

    private void setCustomFont(Context ctx, String asset) {
        setTypeface(TypeFaceHelper.getInstance(ctx).getStyleTypeFace(asset));
    }

}
