package com.app.Billbachao.launch;

import android.app.Activity;
import android.content.Context;
import android.util.Patterns;
import android.widget.Toast;

import com.app.Billbachao.R;
import com.app.Billbachao.shareandrate.utils.ShareUtils;
import com.app.Billbachao.utils.PreferenceUtils;
import com.app.Billbachao.utils.USSDUtils;
import com.app.Billbachao.utils.UiUtils;

import java.util.regex.Pattern;

/**
 * Created by mihir on 12-04-2016.
 */
public class ActionLinkHelper implements Links {

    public static void parseActionLink(Context context, String link) {
        Pattern webUrlPattern = Patterns.WEB_URL;
        if (webUrlPattern.matcher(link).matches()) {
            // Launch Web Url
        } else {
            performAppAction(context, link);
        }
    }

    public static void performAppAction(Context context, String link) {
        switch (link) {
            case USAGE_BIFURCATION:
                UiUtils.launchInternalUsage(context);
                return;
            case SHARE_APP:
                if (context instanceof Activity)
                    ShareUtils.shareApp((Activity) context);
                else
                    ShareUtils.shareApp(context);
                return;
            case USAGE_LOG:
                UiUtils.launchUsageLogOrSettings(context, true);
                return;
            case RECOMMEND_APP:
                UiUtils.launchAppRecommendation(context);
                return;
            case BROWSE_PLANS:
                if (PreferenceUtils.isPrepaid(context))
                    UiUtils.launchSelfBrowsePlans(context);
                else UiUtils.launchRecharge(context);
                return;
            case DND:
            case CHECK_BALANCE:
            case PLAN_ALERTS:
            case DATA_SETTINGS:
                performSelfHelpAction(context, link);
                break;
            default:
                break;
        }
    }

    public static void performSelfHelpAction(Context context, String link) {
        int action = -1;
        switch (link) {
            case DND:
                action = USSDUtils.DND;
                break;
            case CHECK_BALANCE:
                action = USSDUtils.CHECK_BALANCE;
                break;
            case PLAN_ALERTS:
                USSDUtils.setPlanAlert(context, true);
                Toast.makeText(context, R.string.plan_alerts_activated, Toast.LENGTH_SHORT).show();
                break;
            case DATA_SETTINGS:
                action = USSDUtils.DATA_SETTINGS;
                break;
        }
        if (action >= 0) USSDUtils.entertainRequest(context, action);
    }


}
