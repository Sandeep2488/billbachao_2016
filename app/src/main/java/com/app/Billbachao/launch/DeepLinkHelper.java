package com.app.Billbachao.launch;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import com.app.Billbachao.R;
import com.app.Billbachao.deals.utils.DealsConstant;
import com.app.Billbachao.shareandrate.utils.ShareUtils;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.utils.NumberUtils;
import com.app.Billbachao.utils.PreferenceUtils;
import com.app.Billbachao.utils.USSDUtils;
import com.app.Billbachao.utils.UiUtils;

import java.util.List;

/**
 * Created by mihir on 01-10-2015.
 */
public class DeepLinkHelper implements Links {

    public static final String TAG = DeepLinkHelper.class.getSimpleName();
    public static final String IS_DEEP_LINK = "isDeepLink";

    /**
     * Check for calls from deep links
     *
     * @param context
     * @param intent
     */
    public static void checkForExternalCalls(Context context, Intent intent) {
        if (!intent.getBooleanExtra(IS_DEEP_LINK, false)) {
            return;
        }

        Uri uri = intent.getData();
        if (uri == null || !uri.getHost().equalsIgnoreCase(context.getString(R.string.app_host))) {
            return;
        }

        String prefix = uri.getPathSegments().get(0);
        ILog.d(TAG, "Deep link Prefix is " + prefix);
        handleDeepLink(context, prefix, uri, intent);
    }

    public static void handleDeepLink(Context context, String prefix, Intent intent) {
        handleDeepLink(context, prefix, null, intent);
    }


    /**
     * Parse link from deep link and take action
     *
     * @param context
     * @param link
     */
    public static void handleDeepLink(Context context, String link, Uri uri, Intent intent) {

        switch (link) {
            case CART:
                UiUtils.launchCart(context);
                return;
            case SAVED_PLANS:
                return;
            case SELF_HELP:
                UiUtils.launchSelfHelp(context);
                return;
            case RECHARGE:
                return;
            case NETWORK:
                UiUtils.launchNetworkScreen(context);
                return;
            case BROWSE_PLANS:
                if (PreferenceUtils.isPrepaid(context))
                    UiUtils.launchSelfBrowsePlans(context);
                else UiUtils.launchRecharge(context);
                return;
            case TEST_SPEED:
                return;
            case RATE_OPERATOR:
                return;
            case MY_ACCOUNT:
                return;
            case CHECK_BALANCE:
                USSDUtils.entertainRequest(context, PreferenceUtils.isPrepaid(context) ?
                        USSDUtils.CHECK_BALANCE : USSDUtils.CHECK_BILL);
                return;
            case PLAY_STORE:
                ShareUtils.launchStore(context);
                return;
            case USAGE_LOG:
                UiUtils.launchUsageLogOrSettings(context, true);
                return;
            case SET_ALERTS:
                UiUtils.launchAlerts(context);
                return;
            case RECOMMEND_APP:
                UiUtils.launchAppRecommendation(context);
                return;
            case WEBSITE:
                ShareUtils.launchWebsite(context);
                return;
            case RELIANCE_TV_DEALS:
                handleDeepLinkForDeals(context, uri);
                return;
            case DASHBOARD:
                return;
            default:
                break;
        }
    }

    /**
     * Clevertap deeplinks, takes only String values. So, typecast and pass
     *
     * @param launcherIntent
     * @param dashBoardIntent
     * @return
     */
    public static Intent passDeepLinkIntentData(Intent launcherIntent, Intent dashBoardIntent) {
        if (launcherIntent.hasExtra(Links.PREFERRED_RECHARGE_TYPE)) {
            String value = launcherIntent.getStringExtra(Links.PREFERRED_RECHARGE_TYPE);
            if (NumberUtils.isNumber(value)) {
                dashBoardIntent.putExtra(Links.PREFERRED_RECHARGE_TYPE, Integer.parseInt(value));
            }
        }
        return dashBoardIntent;
    }

    /**
     * Handle the deepLink for Reliance Deals.
     *
     * @param context
     * @param uri
     */
    private static void handleDeepLinkForDeals(Context context, Uri uri) {

        if (uri == null)
            return;

        List<String> path = uri.getPathSegments();

        if (path == null || path.size() == 0)
            return;

        if (path.size() == 1) {
            //TODO add events
            //EventUtils.sendEvent(EventUtils.NOTIFICATION, Constants.LYF_LIST_DEEP_LINK,Constants.PROPERTY_VALUE);
            UiUtils.launchDealsScreen(context, DealsConstant.KEY_FROM_TV_DEEP_LINKS);
            return;
        }

        String productId = path.get(1);

        if (TextUtils.isEmpty(productId))
            return;


        //TODO add events
        //EventUtils.sendEvent(EventUtils.NOTIFICATION, Constants.LYF_DETAIL_DEEP_LINK, Constants.PROPERTY_VALUE);

        Bundle bundle = new Bundle();
        bundle.putString(DealsConstant.KEY_FROM, DealsConstant.KEY_FROM_TV_DEEP_LINKS);
        bundle.putString(DealsConstant.KEY_DEALS_PRODUCT_ID, productId);
        UiUtils.launchDealsDetailsActivity(context, bundle);
    }
}
