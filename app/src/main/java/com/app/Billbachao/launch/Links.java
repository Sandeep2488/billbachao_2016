package com.app.Billbachao.launch;

/**
 * Created by mihir on 12-04-2016.
 */
public interface Links {

    String USAGE_BIFURCATION = "usage_bifurcation", NETWORK = "network", BEST_PLANS =
            "best_plans", RECHARGE =
            "recharge", SAVED_PLANS = "saved_plans", CART = "cart", BROWSE_PLANS =
            "browse_plans", SELF_HELP = "self_help", DASHBOARD = "dashboard", DEALS_POPUP =
            "deals_popup", MY_ACCOUNT = "my_account", RATE_OPERATOR = "rate_operator", TEST_SPEED
            = "speed_test", CHECK_BALANCE = "check_balance", PLAY_STORE = "play_store",
            HANDSETS_FOR_ME = "handsets_for_me", SET_ALERTS = "set_alerts", USAGE_LOG = "usage_log",
            RECOMMEND_APP = "recommend_app", WEBSITE = "website", RELIANCE_TV_DEALS = "tv_deals",
            PREFERRED_RECHARGE_TYPE = "recharge_type_preferred",
            GAME = "game", SHARE_APP = "share", MNP = "mnp", DND = "dnd", PLAN_ALERTS = "plan_alert", DATA_SETTINGS = "data_settings";
}
