package com.app.Billbachao.model;

import com.google.myjson.annotations.SerializedName;

/**
 * Created by BB-001 on 5/11/2016.
 */
public class LocationFetchResponse {

    @SerializedName("geoplugin_status")
    public String status;

    @SerializedName("geoplugin_city")
    public String city;

    @SerializedName("geoplugin_latitude")
    public String latitude;

    @SerializedName("geoplugin_longitude")
    public String longitude;

    public String getStatus() {
        return status;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getCity() {
        return city;
    }

    public String getLongitude() {
        return longitude;
    }
}

