package com.app.Billbachao.model;

import android.os.Parcel;

import com.app.Billbachao.recharge.plans.model.PlanItem;
import com.google.myjson.annotations.SerializedName;

/**
 * Created by mihir on 27-04-2016.
 */
public class QuickRechargePlanItem extends PlanItem {

    @SerializedName("msg")
    String msg;

    public QuickRechargePlanItem(int id, String title, String description, String validity,
                                 int value, String type, String blocked, String online, String
                                         dataMb, String talkTime, int planCount) {
        super(id, title, description, validity, value, type, blocked, online, dataMb, talkTime,
                planCount);
    }

    protected QuickRechargePlanItem(PlanItem item) {
        super(item);
    }

    public static final Creator<QuickRechargePlanItem> CREATOR = new Creator<QuickRechargePlanItem>() {

        @Override
        public QuickRechargePlanItem createFromParcel(Parcel source) {
            return new QuickRechargePlanItem(source);
        }

        @Override
        public QuickRechargePlanItem[] newArray(int size) {
            return new QuickRechargePlanItem[size];
        }

    };

    public QuickRechargePlanItem(Parcel source) {
        super(source);
    }

    public boolean isValidPlan() {
        return msg == null;
    }

}
