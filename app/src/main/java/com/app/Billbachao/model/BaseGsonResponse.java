package com.app.Billbachao.model;

import com.app.Billbachao.volley.utils.NetworkErrorHelper;
import com.google.myjson.annotations.SerializedName;

/**
 * Base GSON class which will have params like status, statusDesc, error
 * Created by mihir on 21-04-2016.
 */
public class BaseGsonResponse {

    public static final String SUCCESS = "success";

    @SerializedName("status")
    public String status;

    @SerializedName("statusDesc")
    public String statusDesc;

    public boolean isSuccess() {
        return status != null && status.equalsIgnoreCase(SUCCESS);
    }

    public String getStatusDesc() {
        return statusDesc != null ? statusDesc : NetworkErrorHelper.MSG_UNKNOWN;
    }
}
