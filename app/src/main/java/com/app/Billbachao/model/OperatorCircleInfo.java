package com.app.Billbachao.model;

/**
 * Created by mihir on 21-08-2015.
 */
public class OperatorCircleInfo {

    private int operatorId, circleId;

    public OperatorCircleInfo(int operatorId, int circleId) {
        this.operatorId = operatorId;
        this.circleId = circleId;
    }

    public int getOperatorId() {
        return operatorId;
    }

    public int getCircleId() {
        return circleId;
    }

    @Override
    public String toString() {
        return "Operator : " + operatorId + " Circle : " + circleId;
    }
}
