package com.app.Billbachao.model;

/**
 * Model class for checking response status, error, success etc.
 * Created by mihir on 11-03-2016.
 */
public class NetworkResponseInfo {

    int status;

    String error;

    String message;

    public boolean isNetworkError() {
        return status != 200;
    }

    public boolean isInputError() {
        return error != null && !error.isEmpty();
    }

}
