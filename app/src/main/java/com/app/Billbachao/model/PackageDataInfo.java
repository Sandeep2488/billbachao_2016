package com.app.Billbachao.model;

import android.graphics.drawable.Drawable;

/**
 * Created by mihir on 26-08-2015.
 */
public class PackageDataInfo {

    int uid;
    String name;
    String packageName;
    Drawable drawable;
    long data;

    public PackageDataInfo(int uid, String name, String packageName, Drawable drawable, long data) {
        this.uid = uid;
        this.name = name;
        this.packageName = packageName;
        this.drawable = drawable;
        this.data = data;
    }

    public int getUid() {
        return uid;
    }

    public String getName() {
        return name;
    }

    public String getPackageName() {
        return packageName;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public long getData() {
        return data;
    }

    @Override
    public String toString() {
        return super.toString() + " Name : "+name + " Data : "+ data + " Package : "+packageName;
    }
}
