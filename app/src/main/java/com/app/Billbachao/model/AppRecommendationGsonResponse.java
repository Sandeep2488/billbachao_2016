package com.app.Billbachao.model;

import com.app.Billbachao.recommendations.app.model.RecommendedAppDetails;
import com.google.myjson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by mihir on 21-04-2016.
 */
public class AppRecommendationGsonResponse extends BaseGsonResponse {

    @SerializedName("appForDay")
    public ArrayList<RecommendedAppDetails> appList;
}
