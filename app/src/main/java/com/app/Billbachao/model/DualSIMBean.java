package com.app.Billbachao.model;

/**
 * Created by sushil on 8/26/2015.
 */
public class DualSIMBean {
    private Boolean sim_Status;
    private Boolean isSim_Roaming;
    private String sim_IMEI="NA";
    private String sim_IMSI="NA";
    private String sim_OP_Code="NA";
    private String sim_OP_Name="NA";
    private String sim_Network_Type="NA";
     public String OSVersion="NA";

    public String getSim_OP_Name() {
        return sim_OP_Name;
    }

    public void setSim_OP_Name(String sim_OP_Name) {
        this.sim_OP_Name = sim_OP_Name;
    }

    public String getSim_Network_Type() {
        return sim_Network_Type;
    }

    public void setSim_Network_Type(String sim_Network_Type) {
        this.sim_Network_Type = sim_Network_Type;
    }

    public String getSim_OP_Code() {
        return sim_OP_Code;
    }

    public void setSim_OP_Code(String sim_OP_Code) {
        this.sim_OP_Code = sim_OP_Code;
    }

    public String getSim_IMSI() {
        return sim_IMSI;
    }

    public void setSim_IMSI(String sim_IMSI) {
        this.sim_IMSI = sim_IMSI;
    }

    public String getSim_IMEI() {
        return sim_IMEI;
    }

    public void setSim_IMEI(String sim_IMEI) {
        this.sim_IMEI = sim_IMEI;
    }

    public Boolean getSim_Status() {
        return sim_Status;
    }

    public void setSim_Status(Boolean sim_Status) {
        this.sim_Status = sim_Status;
    }

    public Boolean getIsSim_Roaming() {
        return isSim_Roaming;
    }

    public void setIsSim_Roaming(Boolean isSim_Roaming) {
        this.isSim_Roaming = isSim_Roaming;
    }

    public String getOSVersion() {
        return OSVersion;
    }

    public void setOSVersion(String OSVersion) {
        this.OSVersion = OSVersion;
    }
}
