package com.app.Billbachao.model;

import com.google.myjson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by mihir on 27-04-2016.
 */
public class OperatorWisePlanInfo {

    @SerializedName("plans")
    public ArrayList<PlanInfo> planList;

    @SerializedName("TSaveTT")
    public String savingsPercent;

    /*@SerializedName("mSave")
    String mSave;

    @SerializedName("spend")
    String spend;
*/
    public static class PlanInfo {

        @SerializedName("planId")
        public int planId;

        @SerializedName("pName")
        public String name;

        @SerializedName("pCnt")
        public int count;

        @SerializedName("pDescr")
        public String description;

        @SerializedName("pCost")
        public int cost;

        @SerializedName("pType")
        public String type;

        @SerializedName("savePercentage")
        public String savePercentage;

        @SerializedName("isOnline")
        public int isOnline;

        @SerializedName("isBlocked")
        public int isBlocked;

        @SerializedName("saveAmnt")
        public float saveAmnt;

        @SerializedName("rechargeOption")
        public String rechargeOption;

        @SerializedName("msg")
        public String msg;
    }
}
