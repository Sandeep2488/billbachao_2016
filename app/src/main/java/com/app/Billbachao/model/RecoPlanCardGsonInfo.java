package com.app.Billbachao.model;

import com.google.myjson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by mihir on 27-04-2016.
 */
public class RecoPlanCardGsonInfo {

    @SerializedName("plansDetails")
    ArrayList<OperatorWisePlanInfo> operatorWisePlanInfos;

    public ArrayList<OperatorWisePlanInfo> getOperatorWisePlanInfos() {
        return operatorWisePlanInfos;
    }
}
