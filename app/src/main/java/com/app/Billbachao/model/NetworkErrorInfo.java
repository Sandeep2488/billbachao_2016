package com.app.Billbachao.model;

/**
 * Model class for checking response status, error, success etc.
 * Created by mihir on 11-03-2016.
 */
public class NetworkErrorInfo {

    public int errorCode;

    public boolean isError() {
        return errorCode != 0;
    }

}
