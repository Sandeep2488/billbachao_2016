package com.app.Billbachao.registration.contents.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.View;

import com.app.Billbachao.apis.KeyGenerateApi;
import com.app.Billbachao.registration.contents.background.KeyGenerationTask;
import com.app.Billbachao.utils.MailUtils;
import com.app.Billbachao.utils.PreferenceUtils;
import com.app.Billbachao.utils.UiUtils;
import com.app.Billbachao.volley.utils.VolleySingleTon;

/**
 * Created by mihir on 10-03-2016.
 */
public class RegistrationFragment extends ProfileInfoBaseFragment implements KeyGenerationTask.KeyGenerationCallBack {

    public static final String TAG = RegistrationFragment.class.getSimpleName();

    public static final String NUMBER = "number";

    boolean mInitialEntry;

    public static RegistrationFragment getInstance(String mobile) {
        RegistrationFragment fragment = new RegistrationFragment();
        Bundle bundle = new Bundle();
        bundle.putString(NUMBER, mobile);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void initView() {
        super.initView();
        mheadingText.setVisibility(View.VISIBLE);
        mtermsConditionText.setVisibility(View.VISIBLE);
        mConfirmButton.requestFocus();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String mobile = null;
        if (getArguments() != null) {
            mobile = getArguments().getString(NUMBER, null);
        }
        if (mobile != null) {
            mNumberText.setText(mobile);
            showOtherDetails();
        } else {
            mInitialEntry = true;
        }
    }

    void showOtherDetails() {
        mInitialEntry = false;
        mMailIdText.setText(MailUtils.getGoogleAccountMail(getActivity()));
    }

    @Override
    protected void checkForInitialEntry() {
        if (mInitialEntry) {
            showOtherDetails();
        }
    }

    @Override
    protected void onConfirmClicked() {
        showProgressDialog(progressMsg);
        new KeyGenerationTask(getActivity(), mNumberText.getText().toString()).generateKey(this);
    }

    void getReadyForDashboard() {
        saveProfileInfo();
        UiUtils.launchDashboard(getActivity());
        getActivity().finish();
    }

    @Override
    public void onDestroy() {
        VolleySingleTon.getInstance(getActivity()).cancelPendingRequests(TAG);
        super.onDestroy();
    }

    @Override
    protected void onVerificationDone() {
        dismissProgressDialog();
        getReadyForDashboard();
    }

    @Override
    public void onSuccessKeyGeneration(String response) {
        if (KeyGenerateApi.parseResponse(getActivity(), response)) {
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();
            editor.putString(PreferenceUtils.USER_MODE, CREATE_USER).apply();
            // Register phone
            requestVerificationOnServer();
        } else {
            dismissProgressDialog();
        }
    }
}
