package com.app.Billbachao.registration.contents.background;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.apis.KeyGenerateApi;
import com.app.Billbachao.volley.request.NetworkStringRequest;
import com.app.Billbachao.volley.utils.VolleySingleTon;

/**
 * Created by BB-001 on 5/10/2016.
 */
public class KeyGenerationTask {

    Context context;
    String mobileNumber;

    public KeyGenerationTask(Context context,String mobileNumber){
        this.context=context;
        this.mobileNumber=mobileNumber;
    }

    public void generateKey(final KeyGenerationCallBack callBack){
    NetworkStringRequest stringRequest = new NetworkStringRequest(Request.Method.POST,
            KeyGenerateApi.URL,
            KeyGenerateApi.getParams(context, mobileNumber),
            new Response.Listener<String>() {
                @Override
                public void onResponse(String s) {
                    if (KeyGenerateApi.parseResponse(context, s)) {
                        callBack.onSuccessKeyGeneration(s);
                    }
                }
            }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError volleyError) {
        }
    });

    VolleySingleTon.getInstance(context).addToRequestQueue(stringRequest);
}

    public interface KeyGenerationCallBack{
        void onSuccessKeyGeneration(String response);
    }
}

