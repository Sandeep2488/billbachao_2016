package com.app.Billbachao.registration.contents.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.R;
import com.app.Billbachao.apis.ApiUtils;
import com.app.Billbachao.apis.CheckActiveMobileApi;
import com.app.Billbachao.apis.KeyGenerateApi;
import com.app.Billbachao.apis.VerificationApi;
import com.app.Billbachao.common.ConnectionManager;
import com.app.Billbachao.dashboard.DashboardActivity;
import com.app.Billbachao.registration.contents.background.KeyGenerationTask;
import com.app.Billbachao.utils.CircleUtils;
import com.app.Billbachao.utils.OperatorUtils;
import com.app.Billbachao.utils.PreferenceUtils;
import com.app.Billbachao.utils.USSDUtils;
import com.app.Billbachao.volley.request.NetworkArrayRequest;
import com.app.Billbachao.volley.request.NetworkJSONRequest;
import com.app.Billbachao.volley.utils.NetworkErrorHelper;
import com.app.Billbachao.volley.utils.VolleySingleTon;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mihir on 11-03-2016.
 */
public class EditProfileFragment extends ProfileInfoBaseFragment implements KeyGenerationTask.KeyGenerationCallBack {

    public static final String TAG = EditProfileFragment.class.getSimpleName();

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadData();
    }

    private void loadData() {
        Activity activity = getActivity();
        mNumberText.setText(PreferenceUtils.getMobileNumber(activity));
        mNumberText.setSelection(mNumberText.getText().length());
        mMailIdText.setText(PreferenceUtils.getMailId(activity));
        mOperatorId = PreferenceUtils.getOperatorId(activity);
        mCircleId = PreferenceUtils.getCircleId(activity);
        mCircleText.setText(CircleUtils.getCircleNameFromId(mCircleId));
        mOperatorText.setText(OperatorUtils.getOperatorNameFromId(mOperatorId));
        mConfirmButton.setText("Save");

        int connectionId = PreferenceUtils.isPrepaid(activity) ? R.id.prepaid : R.id.postpaid;
        mPrepaidPostpaidGroup.check(connectionId);

        int networkId = -1;
        String networkType = PreferenceUtils.getNetworkType(activity);
        for (int i = 0; i < networkTypes.length; i++) {
            if (networkType.equalsIgnoreCase(networkTypes[i])) {
                networkId = networkRadioIds[i];
                break;
            }
        }
        mDataTypeGroup.check(networkId);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onConfirmClicked() {
        if (isUserDataUpdated()) {
            showProgressDialog(progressMsg);
            if (!PreferenceUtils.getMobileNumber(getActivity()).equalsIgnoreCase(mNumberText.getText().toString())) {
                final NetworkArrayRequest arrayRequest = new NetworkArrayRequest(Request.Method.POST,
                        CheckActiveMobileApi.URL,
                        CheckActiveMobileApi.getParams(getActivity(), mNumberText.getText().toString()),
                        new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                try {
                                    if (response != null && response.getJSONObject(0).getString("status").equalsIgnoreCase("n")) {
                                        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();
                                        editor.putString(PreferenceUtils.USER_MODE, CREATE_USER).apply();
                                        generateKey();
                                    } else {
                                        dismissProgressDialog();
                                        DisplayDialog(getActivity());
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                    }
                }, false);
                VolleySingleTon.getInstance(getActivity()).addToRequestQueue(arrayRequest, TAG);

            } else {
                getUpdatedData();
                final NetworkJSONRequest jsonRequest = new
                        NetworkJSONRequest(Request.Method.POST, VerificationApi.URL, VerificationApi.getParams(getActivity(), mMobileNumber, mOperatorId, mCircleId, mConnectionType, mEmailId, PreferenceUtils.getUserMode(getActivity()), PreferenceUtils.isSMSReceived(getActivity()), getDevicedata(getActivity()).toString(), mNetworkType), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (ApiUtils.isValidResponse(response.toString())) {
                            USSDUtils.storeSelfHelpDetails(getActivity(), response.toString());
                            onVerificationDone();
                        } else {
                            try {
                                dismissProgressDialog();
                                showSnack(response.getString("statusDesc"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                dismissProgressDialog();
                                showSnack(NetworkErrorHelper.getErrorStatus(error).getErrorMessage());
                            }
                        }, true);
                VolleySingleTon.getInstance(getActivity()).addToRequestQueue(jsonRequest, TAG);
            }
        } else {
            showSnack(getString(R.string.no_changes_edit_msg));
        }
    }

    @Override
    protected void
    onVerificationDone() {
        saveProfileInfo();
        dismissProgressDialog();
        goBack(getActivity());
        Toast.makeText(getActivity(), R.string.profile_updated_msg, Toast.LENGTH_SHORT).show();
    }

    public void goBack(Context ctx) {
        Intent i = new Intent(ctx, DashboardActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        if (getActivity() != null) {
            getActivity().finish();
        }
    }

    protected void getUpdatedData() {
        mMobileNumber = mNumberText.getText().toString();
        mOperatorId = OperatorUtils.getOperatorIdFromName(mOperatorText.getText().toString());
        mCircleId = CircleUtils.getCircleIdFromName(mCircleText.getText().toString());
        mEmailId = mMailIdText.getText().toString();
        mConnectionType = mPrepaidPostpaidGroup.getCheckedRadioButtonId() == R.id.prepaid ? PreferenceUtils.PREPAID : PreferenceUtils.POSTPAID;
        mNetworkType = (String) (mDataTypeGroup.findViewById(mDataTypeGroup.getCheckedRadioButtonId())).getTag();
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();
        editor.putString(PreferenceUtils.USER_MODE, UPDATE_PROFILE).apply();
    }

    /**
     * Dialog popup for changing mobile number.
     */
    public void DisplayDialog(final Context ctx) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.mobileNum_exist);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (ctx != null) {
                    if (ConnectionManager.isConnected(ctx)) {
                        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();
                        editor.putString(PreferenceUtils.USER_MODE, UPDATE_PROFILE).apply();
                        generateKey();
                    } else {
                        showSnack(getString(R.string.internet_connection_down));
                    }
                }
            }
        });
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        builder.create();
        builder.show();
    }

    public void generateKey() {
        new KeyGenerationTask(getActivity(), mNumberText.getText().toString()).generateKey(this);
    }

    @Override
    public void onSuccessKeyGeneration(String response) {
        if (KeyGenerateApi.parseResponse(getActivity(), response)) {
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();
            editor.putBoolean(PreferenceUtils.SMSRECEIVED, false).apply();
            // Register phone
            requestVerificationOnServer();
        } else {
            dismissProgressDialog();
        }
    }
}
