package com.app.Billbachao.registration.contents.background;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Telephony;
import android.telephony.SmsMessage;

import com.app.Billbachao.location.api.MobBillSMSPushAPI;
import com.app.Billbachao.utils.Features;
import com.app.Billbachao.background.IpAddressFetchTask;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.utils.PreferenceUtils;

/**
 * Created by mihir on 23-04-2016.
 */
public class SmsReciever extends BroadcastReceiver {

    public static final String TAG = SmsReciever.class.getSimpleName();

    public static final String BB_CARE_ADDRESS = "BBCARE", BILL = "BILL", BACHAO = "BACHAO", DUE = "DUE";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equalsIgnoreCase(Telephony.Sms.Intents.SMS_RECEIVED_ACTION)) {
            Bundle bundle = intent.getExtras();
            SmsMessage[] msgs = null;
            if (bundle != null) {
                Object[] pdus = (Object[]) bundle.get("pdus");
                if (pdus != null) {
                    msgs = new SmsMessage[pdus.length];
                    StringBuilder smsBody = new StringBuilder();
                    for (int i = 0; i < msgs.length; i++) {
                        // Convert Object array
                        msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                        smsBody.append(msgs[i].getDisplayMessageBody());
                    }
                    String address = (msgs.length > 0) ? msgs[0].getOriginatingAddress() : "";
                    if (address != null && address.toUpperCase().contains(BB_CARE_ADDRESS)) {
                        parseVerifyMsg(context, smsBody);
                    }

                    if (Features.RECEIVE_BILL_DUE_MSG && PreferenceUtils.isMobVerified(context)) {
                        parseBillDueMsg(context, smsBody);
                    }
                }
            }
        }
    }

    void parseBillDueMsg(Context context, StringBuilder smsBody) {
        if (!PreferenceUtils.isPrepaid(context)) {
            String message = smsBody.toString().toUpperCase();
            if (message.contains(BILL) || message.contains(DUE)) {
                MobBillSMSPushAPI.pushMobBillSms(context, smsBody.toString());
            }
        }
    }

    void parseVerifyMsg(Context context, StringBuilder smsBody) {
        String message = smsBody.toString().toUpperCase();
        if (message.contains(BILL) && message.contains(BACHAO)) {
            if (PreferenceUtils.isRegistered(context)) {
                PreferenceManager.getDefaultSharedPreferences(context).edit()
                        .putBoolean(PreferenceUtils.SMSRECEIVED, true).commit();
                verifyUser(context);
            }
        }
    }

    void verifyUser(Context context) {
        ILog.d(TAG, "SMS received");
        new IpAddressFetchTask(context).fetchIp();
    }
}