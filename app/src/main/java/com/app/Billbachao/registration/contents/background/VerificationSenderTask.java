package com.app.Billbachao.registration.contents.background;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.apis.ApiUtils;
import com.app.Billbachao.apis.VerificationApi;
import com.app.Billbachao.notifications.gcm.service.RegistrationIntentService;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.utils.PreferenceUtils;
import com.app.Billbachao.utils.USSDUtils;
import com.app.Billbachao.volley.request.NetworkJSONRequest;
import com.app.Billbachao.volley.utils.VolleySingleTon;

import org.json.JSONObject;

/**
 * Created by mihir on 23-04-2016.
 */
public class VerificationSenderTask {

    Context mContext;

    public VerificationSenderTask(Context context) {
        mContext = context;
    }

    public void trigger() {
        NetworkJSONRequest verificationSender = new
                NetworkJSONRequest(Request.Method.POST, VerificationApi
                .URL, VerificationApi.getParamsForVerification
                (mContext), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (ApiUtils.isValidResponse(response.toString())) {
                    USSDUtils.storeSelfHelpDetails(mContext, response.toString());
                    SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
                    editor.putBoolean(PreferenceUtils.MOBVERIFIED, true).apply();
                    ILog.d("Registration", "|registration verification |");
                    PreferenceUtils.setMobVerified(mContext, true);
                    //register device to GCM
                    Intent intent = new Intent(mContext, RegistrationIntentService.class);
                    mContext.startService(intent);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }, true);
        VolleySingleTon.getInstance(mContext).addToRequestQueue(verificationSender);
    }
}
