package com.app.Billbachao;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.app.Billbachao.background.TaskScheduler;

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        TaskScheduler.setUsageNotificationAlarm(context);

    }
}
