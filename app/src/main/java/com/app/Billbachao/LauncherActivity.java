package com.app.Billbachao;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.apis.CardsListApi;
import com.app.Billbachao.cards.server.CardServerConverter;
import com.app.Billbachao.cards.server.CardServerObject;
import com.app.Billbachao.common.permissions.PermissionWrapper;
import com.app.Billbachao.dashboard.DashboardActivity;
import com.app.Billbachao.launch.DeepLinkHelper;
import com.app.Billbachao.notifications.gcm.model.NotificationModel;
import com.app.Billbachao.notifications.gcm.utils.NotificationConstant;
import com.app.Billbachao.registration.contents.activities.RegistrationActivity;
import com.app.Billbachao.tutorial.InitWalkthroughActivity;
import com.app.Billbachao.tutorial.TutorialHelper;
import com.app.Billbachao.tutorial.utils.WalkthroughUtils;
import com.app.Billbachao.utils.ApiDataCacheUtils;
import com.app.Billbachao.utils.DateUtils;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.utils.PreferenceUtils;
import com.app.Billbachao.utils.TelephonyUtils;
import com.app.Billbachao.volley.request.NetworkStringRequest;
import com.app.Billbachao.volley.utils.VolleySingleTon;
import com.appsflyer.AppsFlyerConversionListener;
import com.appsflyer.AppsFlyerLib;

import java.util.ArrayList;
import java.util.Map;

/**
 * Blank launcher activity to redirect to different screens.
 * Redirect all screens from here, for launch time performance.
 * <p/>
 * Created by mihir on 01-10-2015.
 */
public class LauncherActivity extends Activity {

    public static final String TAG = LauncherActivity.class.getSimpleName();

    public static final String APPS_FLYER_KEY = "gzCVZNxCcg89TSzkZU7qKa", GCM_ID = "1059088311387";

    ArrayList<CardServerObject> mCardsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.launcher_screen);
        if (!BuildConfig.DEBUG) {
            initAppsFlyer();
        }
        if (TutorialHelper.isTutorialShown(this, WalkthroughUtils.INITIAL) && PreferenceUtils
                .isRegistered(this)) {
            fetchCardList();
        } else {
            taskDone();
        }
    }

    void launchNextScreen() {
        Intent launchIntent;
        if (!TutorialHelper.isTutorialShown(this, WalkthroughUtils.INITIAL)) {
            launchIntent = new Intent(this, InitWalkthroughActivity.class);
            launchIntent.putExtra(InitWalkthroughActivity.LAUNCH_NEXT_SCREEN, true);
        } else if (PreferenceUtils.isRegistered(this)) {
            launchIntent = handleNextScreen();
        } else {
            launchIntent = new Intent(this, RegistrationActivity.class);
        }
        startActivity(launchIntent);
    }

    private Intent handleNextScreen() {

        Intent dashBoardIntent = new Intent(this, DashboardActivity.class);
        if (mCardsList != null)
            dashBoardIntent.putExtra(CardServerConverter.CARD_LIST, mCardsList);
        Intent launcherIntent = getIntent();

        if (launcherIntent != null) {
            if (launcherIntent.getAction().equalsIgnoreCase(Intent.ACTION_VIEW)) {
                if (launcherIntent.getScheme().equalsIgnoreCase(getString(R.string.app_scheme))) {
                    dashBoardIntent = getDeepLinkIntent(launcherIntent, dashBoardIntent);
                }
            }

            Bundle bundle = launcherIntent.getExtras();
            if (bundle != null) {
                NotificationModel notificationModel = bundle.getParcelable(NotificationConstant.KEY_NOTIFICATION_MODEL);
                if (notificationModel != null) {
                    dashBoardIntent.putExtra(NotificationConstant.KEY_NOTIFICATION_MODEL, notificationModel);
                }
            }

            dashBoardIntent.setData(launcherIntent.getData());
        }

        return dashBoardIntent;
    }

    private Intent getDeepLinkIntent(Intent launcherIntent, Intent dashBoardIntent) {
        dashBoardIntent.putExtra(DeepLinkHelper.IS_DEEP_LINK, true);
        dashBoardIntent = DeepLinkHelper.passDeepLinkIntentData(launcherIntent, dashBoardIntent);
        dashBoardIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        return dashBoardIntent;
    }

    void initAppsFlyer() {
        AppsFlyerLib appsFlyerLib = AppsFlyerLib.getInstance();
        appsFlyerLib.startTracking(getApplication(), APPS_FLYER_KEY);
        appsFlyerLib.setGCMProjectID(GCM_ID);
        if (PermissionWrapper.hasPermissions(this, Manifest.permission.READ_PHONE_STATE))
            appsFlyerLib.setCustomerUserId(TelephonyUtils.getIMEINumber(this));

        if (MyApplication.ct == null) return;
        appsFlyerLib.registerConversionListener(getApplicationContext(), new AppsFlyerConversionListener() {
            @Override
            public void onInstallConversionDataLoaded(Map<String, String> map) {
                String medium = map.get("media_source");
                String campaign = map.get("campaign");
                String source = map.get("agency");
                PreferenceUtils.saveInstallInfo(LauncherActivity.this, medium, campaign, source);
                MyApplication.ct.pushInstallReferrer(source, medium, campaign);
            }

            @Override
            public void onInstallConversionFailure(String s) {
            }

            @Override
            public void onAppOpenAttribution(Map<String, String> map) {
            }

            @Override
            public void onAttributionFailure(String s) {
            }
        });
    }

    void taskDone() {
        launchNextScreen();
        finish();
    }

    void fetchCardList() {

        if (DateUtils.isToday(ApiDataCacheUtils.getTime(this, ApiDataCacheUtils.CARDS_API))) {
            mCardsList = CardServerConverter.getCardsListFromJson(ApiDataCacheUtils.getApi(this,
                    ApiDataCacheUtils.CARDS_API));
            if (mCardsList != null) {
                taskDone();
                return;
            }
        }

        NetworkStringRequest cardsRequest = new NetworkStringRequest(Request.Method.POST, CardsListApi.URL,
                CardsListApi.getParams(this), new Response.Listener<String>() {
            @Override
            public void onResponse(String jsonObject) {
                mCardsList = CardServerConverter.getCardsListFromJson(jsonObject);
                ILog.d(TAG, "Initial card list " + mCardsList);
                if (mCardsList != null && !mCardsList.isEmpty()) {
                    ApiDataCacheUtils.storeApi(LauncherActivity.this, ApiDataCacheUtils
                            .CARDS_API, jsonObject);
                }
                ILog.d(TAG, "Initial card list " + mCardsList);
                taskDone();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                taskDone();
            }
        }, true);
        VolleySingleTon.getInstance(this).addToRequestQueue(cardsRequest);
    }
}
