package com.app.Billbachao.volley.utils;
/**
 * Get Network errorMessage and errorCode
 * Created by nitesh on 19-04-2016.
 */
public class NetworkErrorStatus {

    private String errorMessage = null;

    private int errorCode = 0;


    public String getErrorMessage() {
        return errorMessage;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorMessage(String errorMessage){
        this.errorMessage = errorMessage;
    }

    public void setErrorCode(int errorCode){
        this.errorCode = errorCode;
    }

}
