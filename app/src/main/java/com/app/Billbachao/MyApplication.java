package com.app.Billbachao;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;

import com.app.Billbachao.background.TaskScheduler;
import com.app.Billbachao.database.DatabaseCreater;
import com.app.Billbachao.database.MobileOperatorDBHelper;
import com.app.Billbachao.database.OperatorCircleNetworkMappingDBHelper;
import com.app.Billbachao.database.STDNumberSeriesDBHelper;
import com.app.Billbachao.location.service.CellLocationService;
import com.app.Billbachao.utils.ApiDataCacheUtils;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.utils.OperatorUtils;
import com.app.Billbachao.utils.PreferenceUtils;
import com.app.Billbachao.utils.ServiceUtils;
import com.clevertap.android.sdk.ActivityLifecycleCallback;
import com.clevertap.android.sdk.CleverTapAPI;
import com.clevertap.android.sdk.exceptions.CleverTapMetaDataNotFoundException;
import com.clevertap.android.sdk.exceptions.CleverTapPermissionsNotSatisfied;
import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import java.util.HashMap;

import io.fabric.sdk.android.Fabric;

/**
 * Created by mihir on 10-03-2016.
 */
public class MyApplication extends Application {

    private static final String PROPERTY_ID = "UA-55158402-2";
    private static final String TAG = MyApplication.class.getSimpleName();

    public static CleverTapAPI ct;

    public enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
        ECOMMERCE_TRACKER // Tracker used by all ecommerce transactions from a company.
    }

    private static GoogleAnalytics sAnalytics;

    private static Tracker sTracker;

    HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

    public MyApplication() {
        super();
    }

    public void onCreate() {
        ActivityLifecycleCallback.register(this);
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Fabric.with(this, new Crashlytics());
            initWizrocket();
        }
        initAnalytics();
        FacebookSdk.sdkInitialize(this);
        createDatabases();
        triggerScheduler();
        initialiseOperators();
        initPreferences();
        startCellLocationService();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private void createDatabases() {
        OperatorCircleNetworkMappingDBHelper operatorCircleNetworkMappingDBHelper = OperatorCircleNetworkMappingDBHelper.getInstance(this);
        new DatabaseCreater(this, operatorCircleNetworkMappingDBHelper, OperatorCircleNetworkMappingDBHelper.DB_NAME, OperatorCircleNetworkMappingDBHelper.DB_VERSION).createDataBase();
        operatorCircleNetworkMappingDBHelper.openDataBase();

        MobileOperatorDBHelper mobileOperatorDBHelper = MobileOperatorDBHelper.getInstance(this);
        new DatabaseCreater(this, mobileOperatorDBHelper, MobileOperatorDBHelper.DB_NAME, MobileOperatorDBHelper.DB_VERSION).createDataBase();
        mobileOperatorDBHelper.openDataBase();

        STDNumberSeriesDBHelper stdSeriesHelper = STDNumberSeriesDBHelper.getInstance(this);
        new DatabaseCreater(this, stdSeriesHelper, STDNumberSeriesDBHelper.DB_NAME, STDNumberSeriesDBHelper.DB_VERSION).createDataBase();
        stdSeriesHelper.openDataBase();
    }

    public synchronized Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            analytics.setDryRun(BuildConfig.DEBUG);
            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(R.xml.app_tracker)
                    : (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(PROPERTY_ID)
                    : analytics.newTracker(R.xml.ecommerce_tracker);
            mTrackers.put(trackerId, t);
        }
        return mTrackers.get(trackerId);
    }

    /**
     * Google Analytics
     */
    void initAnalytics() {
        sAnalytics = GoogleAnalytics.getInstance(this);
        if (BuildConfig.DEBUG) {
            sAnalytics.setDryRun(true);
        }
        sTracker = sAnalytics.newTracker(R.xml.global_tracker);

        sTracker.enableAutoActivityTracking(true);
    }

    // Initialise operators name and Id from arrays resource file
    void initialiseOperators() {
        OperatorUtils.init(this);
    }

    void initWizrocket() {

        try {
            ct = CleverTapAPI.getInstance(this);
            String registrationId = ct.getRegistrationId();

            //set registration Id for gcm calls.
            PreferenceUtils.setGcmRegistrationId(this, registrationId);
            ILog.d(TAG, "|RegistrationId|" + registrationId);
            ct.data.pushGcmRegistrationId(registrationId, true);
            ct.enablePersonalization();

        } catch (CleverTapMetaDataNotFoundException e) {
            e.printStackTrace();
        } catch (CleverTapPermissionsNotSatisfied e) {
            e.printStackTrace();
        }

        // TODO add event utils registerApkType.
        //EventUtils.registerApkType(this);

    }

    void startCellLocationService() {
        if (!ServiceUtils.isServiceRunning(this, CellLocationService.class)) {
            Intent intent = new Intent(this, CellLocationService.class);
            startService(intent);
        }
    }

    void triggerScheduler() {
        TaskScheduler.setUsageNotificationAlarm(this);
    }

    void initPreferences() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        preferences.registerOnSharedPreferenceChangeListener(mPreferenceChangeListener);
    }

    SharedPreferences.OnSharedPreferenceChangeListener mPreferenceChangeListener = new
            SharedPreferences.OnSharedPreferenceChangeListener() {
                @Override
                public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String
                        key) {
                    ILog.d(TAG, "onSharedPreferenceChanged " + key);
                    if (key.equals(PreferenceUtils.PREPAID_POSTPAID)) {
                        TaskScheduler.setUsageNotificationAlarm(MyApplication.this);
                        ApiDataCacheUtils.removeApi(MyApplication.this, ApiDataCacheUtils.CARDS_API);
                    }
                }
            };

}
