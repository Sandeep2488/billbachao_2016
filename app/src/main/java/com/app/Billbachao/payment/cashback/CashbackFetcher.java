package com.app.Billbachao.payment.cashback;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.apis.TransactionHistoryApi;
import com.app.Billbachao.payment.cashback.model.CashbackInfo;
import com.app.Billbachao.volley.request.NetworkGsonRequest;
import com.app.Billbachao.volley.utils.NetworkErrorHelper;
import com.app.Billbachao.volley.utils.VolleySingleTon;

/**
 * Created by mihir on 23-04-2016.
 */
public class CashbackFetcher {

    Context mContext;

    CashbackInfoListener mListener;

    public CashbackFetcher(Context context) {
        this(context, null);
    }

    public CashbackFetcher(Context context, CashbackInfoListener listener) {
        mContext = context;
        mListener = listener;
    }

    public void trigger(String tag) {
        NetworkGsonRequest<CashbackInfo> cashbackFetcher = new NetworkGsonRequest<>(Request
                .Method.POST, TransactionHistoryApi.URL, CashbackInfo.class,
                TransactionHistoryApi.getCashbackParams(mContext), new Response
                .Listener<CashbackInfo>() {
            @Override
            public void onResponse(CashbackInfo response) {
                if (mListener != null) {
                    if (response.isSuccess())
                        mListener.onCashbackUpdated(response);
                    else
                        mListener.onCashbackError(response.getStatusDesc());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (mListener != null) {
                    mListener.onCashbackError(NetworkErrorHelper.getErrorStatus(error)
                            .getErrorMessage());
                }
            }
        }, true);
        VolleySingleTon.getInstance(mContext).addToRequestQueue(cashbackFetcher, tag);
    }

    public interface CashbackInfoListener {
        void onCashbackUpdated(CashbackInfo info);

        void onCashbackError(String networkErrorMessage);
    }
}
