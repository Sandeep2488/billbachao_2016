package com.app.Billbachao.payment.background;

import android.content.Context;

import com.android.volley.Request;
import com.app.Billbachao.apis.PaymentAbortedApi;
import com.app.Billbachao.model.BaseGsonResponse;
import com.app.Billbachao.volley.request.NetworkGsonRequest;
import com.app.Billbachao.volley.utils.VolleySingleTon;

/**
 * Created by mihir on 11-05-2016.
 */
public class PaymentAbortUpdaterTask {

    Context mContext;

    public PaymentAbortUpdaterTask(Context context) {
        mContext = context;
    }

    public void trigger(String transactionId) {
        NetworkGsonRequest<BaseGsonResponse> abortSender = new NetworkGsonRequest<>(Request
                .Method.POST, PaymentAbortedApi.URL, BaseGsonResponse.class, PaymentAbortedApi
                .getParams(mContext, transactionId), null, null, true);
        VolleySingleTon.getInstance(mContext).addToRequestQueue(abortSender);
    }
}
