package com.app.Billbachao.payment.helper;

import android.content.Context;
import android.content.Intent;

import com.app.Billbachao.model.OperatorWisePlanInfo;
import com.app.Billbachao.payment.contents.activities.PaymentIntermediaryActivity;
import com.app.Billbachao.recharge.cart.CartPlanHelper;
import com.app.Billbachao.recharge.plans.model.PlanCartDBItem;
import com.app.Billbachao.recharge.plans.model.PlanItem;
import com.app.Billbachao.recharge.plans.model.PlanValidationItem;
import com.app.Billbachao.utils.PreferenceUtils;

import java.util.ArrayList;

/**
 * Created by mihir on 30-04-2016.
 * Payment intermediary screen redirect. This will support for recharge through any screen.
 */
public class PaymentIntermediaryHelper {

    /**
     * Launch payment from Reco card. So, inflate mobile details from user's registration values
     *
     * @param context
     * @param planInfo
     */
    public static void launchFromReco(Context context, OperatorWisePlanInfo.PlanInfo planInfo) {
        String mobile = PreferenceUtils.getMobileNumber(context);
        String operatorId = PreferenceUtils.getOperatorId(context);
        String circleId = PreferenceUtils.getCircleId(context);
        boolean isPrepaid = PreferenceUtils.isPrepaid(context);
        PlanValidationItem planValidationItem = new PlanValidationItem(planInfo.planId, planInfo.name, planInfo.description,
                planInfo.cost, PlanItem.DEFAULT_VALIDITY, isPrepaid, planInfo.count, mobile,
                operatorId, circleId, PlanValidationItem.RECO_CARD);
        ArrayList<PlanValidationItem> planValidationItems = new ArrayList<>();
        planValidationItems.add(planValidationItem);

        launchPaymentIntermediary(context, planValidationItems);
    }

    /**
     * Launch payment from Browse plans of user. So, inflate mobile details from user's
     * registration values
     *
     * @param context
     * @param planItem
     */
    public static void launchFromBrowsePlans(Context context, PlanItem planItem) {
        String mobile = PreferenceUtils.getMobileNumber(context);
        String operatorId = PreferenceUtils.getOperatorId(context);
        String circleId = PreferenceUtils.getCircleId(context);
        boolean isPrepaid = PreferenceUtils.isPrepaid(context);
        PlanValidationItem planValidationItem = new PlanValidationItem(planItem.getId(), planItem
                .getTitle(), planItem.getDescription(), planItem.getValue(), planItem.getValidity(),
                isPrepaid, planItem.getPlanCount(), mobile,
                operatorId, circleId, PlanValidationItem.BROWSE_PLAN);
        ArrayList<PlanValidationItem> planValidationItems = new ArrayList<>();
        planValidationItems.add(planValidationItem);

        launchPaymentIntermediary(context, planValidationItems);
    }


    public static void launchFromRecharge(Context context, ArrayList<PlanItem> planItems, String
            mobile, String operatorId, String circleId, boolean isPrepaid) {
        ArrayList<PlanValidationItem> planValidationItems = new ArrayList<>();
        for (PlanItem planItem : planItems) {
            PlanValidationItem planValidationItem = new PlanValidationItem(planItem.getId(), planItem
                    .getTitle(), planItem.getDescription(), planItem.getValue(), planItem.getValidity(),
                    isPrepaid, planItem.getPlanCount(), mobile, operatorId, circleId, PlanValidationItem.BROWSE_PLAN);
            planValidationItems.add(planValidationItem);
        }
        launchPaymentIntermediary(context, planValidationItems);
    }

    public static void launchFromCart(Context context) {
        ArrayList<? extends PlanValidationItem> baseList = CartPlanHelper.getPlansList(context);
        launchPaymentIntermediary(context, baseList);
    }

    public static void launchFromCart(Context context, ArrayList<PlanCartDBItem> planCartDBItems) {
        ArrayList<? extends PlanValidationItem> baseList = planCartDBItems;
        launchPaymentIntermediary(context, baseList);
    }

    /**
     * Launch payment intermediary, passing plans value.
     *
     * @param context
     * @param planValidationItems
     */
    private static void launchPaymentIntermediary(Context context, ArrayList<? extends PlanValidationItem>
            planValidationItems) {
        Intent intent = new Intent(context, PaymentIntermediaryActivity.class);
        intent.putExtra(PaymentIntermediaryActivity.PLANS, planValidationItems);
        context.startActivity(intent);
    }
}
