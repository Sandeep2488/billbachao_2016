package com.app.Billbachao.payment.contents.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.externalsdk.AppsFlyerUtils;
import com.app.Billbachao.payment.contents.adapters.PlanAdapter;
import com.app.Billbachao.payment.model.PaymentStatusInfo;
import com.app.Billbachao.recharge.plans.model.PlanValidationItem;
import com.app.Billbachao.utils.ConstantUtils;

import java.util.ArrayList;

/**
 * Created by mihir on 02-05-2016.
 */
public class PaymentStatusActivity extends BaseActivity {

    ArrayList<PlanValidationItem> mPlansList;

    PaymentStatusInfo mPaymentStatusInfo;

    RecyclerView recyclerView;

    TextView transactionText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_status);
        if (!getData()) {
            finish();
            return;
        }
        initView();
        loadData();
    }

    @Override
    protected boolean isCartItemSupported() {
        return false;
    }

    void initView() {
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        transactionText = (TextView) findViewById(R.id.transaction_id);
    }

    boolean getData() {
        mPlansList = getIntent().getParcelableArrayListExtra(ConstantUtils.PLANS);
        mPaymentStatusInfo = getIntent().getParcelableExtra(ConstantUtils.PAYMENT_STATUS);
        return mPlansList != null && !mPlansList.isEmpty() && mPaymentStatusInfo != null;
    }

    void loadData() {
        if (mPaymentStatusInfo.isPaymentSuccess())
            AppsFlyerUtils.trackEvent(this, AppsFlyerUtils.PAYMENT_SUCCESS);
        setTitle(mPaymentStatusInfo.isPaymentSuccess() ? R.string.payment_success : R.string.payment_failure);
        ((ImageView) findViewById(R.id.status_icon)).setImageResource(mPaymentStatusInfo
                .isPaymentSuccess() ? R.drawable.ic_check_circle_colored : R.drawable.ic_error_outline_colored);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,
                false));
        recyclerView.setAdapter(new PlanAdapter(this, mPlansList));
        transactionText.setText(getString(R.string.transaction_text, mPaymentStatusInfo
                .getPgOrderId()));
        notifyPaymentStatus();
    }

    // TODO broadcast successful payment and empty cart/recharge screen on this. Also add in
    // history DB
    private void notifyPaymentStatus() {

    }
}
