package com.app.Billbachao.payment.background;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.apis.ValidationRequestApi;
import com.app.Billbachao.payment.model.ValidationResult;
import com.app.Billbachao.recharge.plans.model.PlanValidationItem;
import com.app.Billbachao.volley.request.NetworkGsonRequest;
import com.app.Billbachao.volley.utils.NetworkErrorHelper;
import com.app.Billbachao.volley.utils.VolleySingleTon;

import java.util.ArrayList;

/**
 * Created by mihir on 02-05-2016.
 */
public class PaymentValidatorTask {

    Context mContext;

    PaymentValidatorListener mListener;

    public PaymentValidatorTask(Context context) {
        this(context, null);
    }

    public PaymentValidatorTask(Context context, PaymentValidatorListener listener) {
        mContext = context;
        mListener = listener;
    }

    public void trigger(String tag, final ArrayList<PlanValidationItem> planList, double transactionAmount,
                        double cashbackAmount, double paymentAmount) {
        NetworkGsonRequest<ValidationResult> cashbackFetcher = new NetworkGsonRequest<>(Request
                .Method.POST, ValidationRequestApi.URL, ValidationResult.class,
                ValidationRequestApi.getParams(mContext, planList, transactionAmount,
                        cashbackAmount, paymentAmount), new Response
                .Listener<ValidationResult>() {
            @Override
            public void onResponse(ValidationResult response) {
                if (mListener != null) {
                    if (response.isSuccess())
                        mListener.onValidated(planList, response);
                    else
                        mListener.onError(response.getStatusDesc());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (mListener != null) {
                    mListener.onError(NetworkErrorHelper.getErrorStatus(error)
                            .getErrorMessage());
                }
            }
        }, true);
        VolleySingleTon.getInstance(mContext).addToRequestQueue(cashbackFetcher, tag);
    }

    public interface PaymentValidatorListener {
        void onValidated(ArrayList<PlanValidationItem> planList, ValidationResult result);

        void onError(String networkErrorMessage);
    }

}
