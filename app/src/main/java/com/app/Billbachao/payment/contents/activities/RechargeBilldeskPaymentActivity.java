package com.app.Billbachao.payment.contents.activities;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.apis.PaymentStatusApi;
import com.app.Billbachao.payment.background.PaymentAbortUpdaterTask;
import com.app.Billbachao.payment.helper.PaymentHelper;
import com.app.Billbachao.payment.model.PaymentStatusInfo;
import com.app.Billbachao.payment.model.ValidationResult;
import com.app.Billbachao.payment.utils.PaymentUtils;
import com.app.Billbachao.recharge.plans.model.PlanValidationItem;
import com.app.Billbachao.utils.ConstantUtils;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.volley.request.NetworkGsonRequest;
import com.app.Billbachao.volley.utils.NetworkErrorHelper;
import com.app.Billbachao.volley.utils.VolleySingleTon;

import java.util.ArrayList;

/**
 * Replaced legacy class of PaymentWebView
 * Created by mihir on 02-05-2016.
 */
public class RechargeBilldeskPaymentActivity extends BaseActivity {

    public static final String TAG = RechargeBilldeskPaymentActivity.class.getSimpleName();

    ValidationResult mValidationResult;

    ArrayList<PlanValidationItem> mPlanItems;

    WebView mWebView;

    boolean mLoadingFinished = true;

    boolean mRedirect = false;

    private ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.billdesk_payment_activity);
        mValidationResult = getIntent().getParcelableExtra(ConstantUtils.VALIDATION_RESULT);
        mPlanItems = getIntent().getParcelableArrayListExtra(ConstantUtils.PLANS);
        if (mValidationResult == null || mPlanItems == null) {
            showToast(R.string.incomplete_data);
            finish();
            return;
        }
        initView();
        startWebView();
    }

    void initView() {
        mWebView = (WebView) findViewById(R.id.payment_web_view);
        progressBar = (ProgressBar) findViewById(R.id.progress);
    }

    void startWebView() {
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebViewClient(new PaymentWebViewClient());
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.setWebChromeClient(new WebChromeClient());
        String data = PaymentUtils.MSG + "=" + mValidationResult.getMsg();
        mWebView.postUrl(PaymentUtils.BB_PAYMENT_URL, data.getBytes());
    }

    @Override
    protected boolean isCartItemSupported() {
        return false;
    }

    private void showProgress() {
        if (progressBar != null && !(progressBar.getVisibility() == View.VISIBLE)) {
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    private void hideProgress() {
        if (progressBar != null && (progressBar.getVisibility() == View.VISIBLE)) {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        showCancellationPrompt();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                showCancellationPrompt();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void showCancellationPrompt() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this).setMessage(R.string.
                payment_cancel_confirm).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                setResult(RESULT_CANCELED);
                new PaymentAbortUpdaterTask(RechargeBilldeskPaymentActivity.this).trigger
                        (mValidationResult.getBbPgOrderId());
                finish();
            }
        }).setNegativeButton(R.string.no, null);
        builder.create().show();
    }

    void getPaymentStatus() {
        final String pgOrderId = mValidationResult.getBbPgOrderId();
        NetworkGsonRequest<PaymentStatusInfo> cashbackFetcher = new NetworkGsonRequest<>(Request
                .Method.POST, PaymentStatusApi.URL, PaymentStatusInfo.class,
                PaymentStatusApi.getParams(this, pgOrderId), new Response.Listener<PaymentStatusInfo>() {
            @Override
            public void onResponse(PaymentStatusInfo response) {
                hideProgress();
                if (response.isSuccess()) {
                    response.setPgOrderId(pgOrderId);
                    PaymentHelper.launchPaymentStatus(RechargeBilldeskPaymentActivity.this, response,
                            mPlanItems);
                    finish();
                } else
                    paymentStatusError(response.getStatusDesc());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgress();
                paymentStatusError(NetworkErrorHelper.getErrorStatus(error).getErrorMessage());
            }
        });

        showProgress();
        VolleySingleTon.getInstance(this).addToRequestQueue(cashbackFetcher, TAG);
    }

    void paymentStatusError(String error) {
        showToast(error);
        finish();
    }

    @Override
    protected void onDestroy() {
        VolleySingleTon.getInstance(this).cancelPendingRequests(TAG);
        super.onDestroy();
    }

    class PaymentWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            mLoadingFinished = false;
            showProgress();
            ILog.d(TAG, "onPageStarted " + url);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            ILog.d(TAG, "shouldOverrideUrlLoading URL " + url);
            if (!mLoadingFinished)
                mRedirect = true;

            mLoadingFinished = false;
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            ILog.d(TAG, "onPageFinished " + url);
            if (!mRedirect)
                mLoadingFinished = true;

            if (mLoadingFinished && !mRedirect) {
                hideProgress();
                if (url.contains(PaymentUtils.CONFIRM_PAYMENT_SERVLET)) {
                    mWebView.destroy();
                    getPaymentStatus();
                }
            } else {
                mRedirect = false;
            }
        }


        @SuppressWarnings("deprecation")
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            ILog.d(TAG, "onReceivedError " + description);
        }

        @TargetApi(android.os.Build.VERSION_CODES.M)
        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            ILog.d(TAG, "onReceivedError " + error.getDescription());
        }
    }
}
