package com.app.Billbachao.apis;

import android.content.Context;

import com.app.Billbachao.utils.PreferenceUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by mihir on 02-05-2016.
 */
public class PaymentStatusApi extends ApiUtils {

    public static final String URL = BASE_URL + "GetPaymentStatusServlet", BB_PG_ORDER_ID = 
            "bbPGOrderId";

    public static Map<String, String> getParams(Context context, String bbPgOrderId) {
        Map<String, String> map = new LinkedHashMap<>();
        map.put(MOBILE, PreferenceUtils.getMobileNumber(context));
        map.put(BB_PG_ORDER_ID, bbPgOrderId);
        map.put(REQUEST_TYPE, MOB);
        map.put(REQUEST_DATE, getDate());
        return getParamsWithCheckSum(context, map);
    }
}
