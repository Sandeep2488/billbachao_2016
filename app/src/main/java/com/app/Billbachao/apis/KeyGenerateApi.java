package com.app.Billbachao.apis;

import android.content.Context;

import com.app.Billbachao.common.CryptoUtility;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.utils.KeyUtils;
import com.app.Billbachao.utils.PreferenceUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by mihir on 11-03-2016.
 */
public class KeyGenerateApi extends ApiUtils {

    public static final String URL = BASE_URL + "GetTokenIdWS";

    private static final String KEY = "key";

    public static Map<String, String> getParams(Context context, String mobile) {
        Map<String, String> map = new LinkedHashMap<>();
        map.put(MOBILE, mobile);
        map.put(REQUEST_DATE, getDate());
        String checkSum = KeyGenerateApi.getCheckSum(context, map);
        map.put(CHECKSUM, checkSum);
        String urlString = mapToString(map);
        urlString = CryptoUtility.encrypt(urlString, KeyUtils.PWD_KEY);
        Map<String, String> keyMap = new LinkedHashMap<>();
        keyMap.put(MSG, urlString);
        return keyMap;
    }

    public static String getCheckSum(Context context, Map<String, String> map) {
        String md5String = generateMD5String(map, KeyUtils.PWD_KEY);
        ILog.d(TAG, "MD5String : " + md5String);
        return generateCheckSum(md5String);
    }

    public static String mapToString(Map<String, String> map) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            String name = entry.getKey();
            String value = entry.getValue();

            if (stringBuilder.length() > 0) {
                stringBuilder.append("&");
            }

            stringBuilder.append(name);
            stringBuilder.append("=");
            stringBuilder.append(value);
        }
        return stringBuilder.toString();
    }

    public static boolean parseResponse(Context context, String response) {
        if (isValidResponse(response)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                String key = jsonObject.getString(MSG);
                PreferenceUtils.saveKey(context, CryptoUtility.decrypt(key, KeyUtils.PWD_KEY));
            } catch (JSONException ex) {
                ex.printStackTrace();
                return false;
            }
            return true;
        }
        return false;
    }
}
