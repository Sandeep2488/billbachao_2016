package com.app.Billbachao.apis;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.app.Billbachao.BuildConfig;
import com.app.Billbachao.MyApplication;
import com.app.Billbachao.R;
import com.app.Billbachao.common.permissions.PermissionWrapper;
import com.app.Billbachao.dualsim.telephony.DualSimManager;
import com.app.Billbachao.registration.contents.fragments.ProfileInfoBaseFragment;
import com.app.Billbachao.utils.LocationUtils;
import com.app.Billbachao.utils.PreferenceUtils;
import com.app.Billbachao.utils.TextUtils;

import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by mihir on 23-04-2016.
 */
public class VerificationApi extends ApiUtils {

    public static final String URL = BASE_URL + "registration";

    static final String MODE = "mode", MOB_VERIFIED = "mobVerified", VERSION_NO = "versionNo",
            OS_TYPE = "osType", ANDROID = "ANDROID", DEVICE_DETAILS = "deviceDtls", DUAL_SIM =
            "isDualSim", CITY = "city", PROFILE_CODE = "profileCode", LATITUDE = "latitude",
            LONGITUDE
                    = "longitude";

    public static Map<String, String> getParamsForVerification(Context context) {
        // TODO check deviceDetails
        return getParams(context, PreferenceUtils.getMobileNumber(context), PreferenceUtils
                        .getOperatorId(context), PreferenceUtils.getCircleId(context),
                PreferenceUtils
                        .getCType(context), PreferenceUtils.getMailId(context), PreferenceUtils.getUserMode(context),
                PreferenceUtils.isSMSReceived(context), ProfileInfoBaseFragment.getDevicedata(context).toString(), PreferenceUtils.getNetworkType
                        (context));
    }

    public static Map<String, String> getParams(Context context, String mobile, String opId,
                                                String cId, String cType, String mail, String
                                                        mode, boolean isSMSReceived, String
                                                        deviceDetails, String networkType) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        Map<String, String> map = new LinkedHashMap<>();
        map.put(MOBILE, mobile);
        map.put(MODE, mode);
        map.put(OPERATOR_ID, opId);
        map.put(CIRCLE_ID, cId);
        map.put(TYPE, cType);
        map.put(MOB_VERIFIED, isSMSReceived ? "Y" : "N");
        map.put(VERSION_NO, BuildConfig.VERSION_NAME);
        map.put(EMAIL, mail);
        map.put(OS_TYPE, ANDROID);
        map.put(DEVICE_DETAILS, deviceDetails);
        String dualSim = PermissionWrapper.hasTelephonyPermissions(context) && new DualSimManager(context).isDualSIMSupported() ? "Y" : "N";
        map.put(DUAL_SIM, dualSim);
        map.put(NETWORK_TYPE, networkType);
        if (!(mode.equalsIgnoreCase(ProfileInfoBaseFragment.UPDATE_PROFILE) || mode.equalsIgnoreCase(ProfileInfoBaseFragment.UPDATE_USER))) {
            map.put(LATITUDE, preferences.getString(LocationUtils.LATITUDE, "0.0"));
            map.put(LONGITUDE, preferences.getString(LocationUtils.LONGITUDE, "0.0"));
        } else {
            map.put(LATITUDE, String.valueOf(preferences.getFloat(LocationUtils.LAST_LATITUDE,
                    0.0f)));
            map.put(LONGITUDE, String.valueOf(preferences.getFloat(LocationUtils.LAST_LONGITUDE,
                    0.0f)));
        }
        map.put(PROFILE_CODE, context.getString(R.string.apk_type));
        map.put(PreferenceUtils.MEDIUM, TextUtils.getFilteredValue(preferences.getString
                (PreferenceUtils
                        .MEDIUM, PreferenceUtils.NA)));
        map.put(PreferenceUtils.CAMPAIGN, TextUtils.getFilteredValue(preferences.getString
                (PreferenceUtils
                        .CAMPAIGN, PreferenceUtils.NA)));
        map.put(PreferenceUtils.SOURCE, TextUtils.getFilteredValue(preferences.getString
                (PreferenceUtils
                        .SOURCE, PreferenceUtils.NA)));
        map.put(GCM_ID, MyApplication.ct == null ? NA : TextUtils.getFilteredValue(MyApplication.ct
                .getRegistrationId()));
        if (!(mode.equalsIgnoreCase(ProfileInfoBaseFragment.UPDATE_PROFILE) || mode.equalsIgnoreCase(ProfileInfoBaseFragment.UPDATE_USER))) {
            map.put(CITY, preferences.getString(LocationUtils.USER_REG_CITY, NA));
        } else {
            String city = LocationUtils.getAddressFromLatLng(context, preferences.getFloat
                    (LocationUtils.LAST_LATITUDE, 0.0f), preferences.getFloat(LocationUtils
                    .LAST_LONGITUDE, 0.0f), Locale.getDefault());
            map.put(CITY, city != null ? city : NA);
        }
        map.put(REQUEST_DATE, getDate());
        return getParamsWithCheckSum(context, map);
    }
}
