package com.app.Billbachao.apis;

import android.content.Context;

import com.app.Billbachao.BuildConfig;
import com.app.Billbachao.cards.CardLogicEngine;
import com.app.Billbachao.utils.AppUtils;
import com.app.Billbachao.utils.PreferenceUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by mihir on 14-04-2016.
 */
public class CardsListApi extends ApiUtils {

    public static String URL = BASE_URL + "CardsServlet";

    static final String PROFILE_TYPE = "profileType", AGE_DAYS = "ageDays";

    public static Map<String, String> getParams(Context context) {
        Map<String, String> map = new LinkedHashMap<>();
        map.put(MOBILE, PreferenceUtils.getMobileNumber(context));
        map.put(PROFILE_TYPE, PreferenceUtils.getCType(context));
        map.put(AGE_DAYS, String.valueOf(getDay(context)));
        map.put(REQUEST_DATE, getDate());
        return getParamsWithCheckSum(context, map);
    }

    public static int getDay(Context context) {
        if (BuildConfig.DEBUG)
            return AppUtils.getDaysSinceInstalled(context) + 1;
        return CardLogicEngine.getAndUpdateCardDay(context);
    }
}
