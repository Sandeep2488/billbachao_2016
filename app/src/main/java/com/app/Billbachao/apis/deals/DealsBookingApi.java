package com.app.Billbachao.apis.deals;

import android.content.Context;

import com.app.Billbachao.apis.ApiUtils;
import com.app.Billbachao.utils.PreferenceUtils;
import com.app.Billbachao.volley.utils.VolleyUtils;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Reliance Booking Api
 * Created by nitesh on 24/02/2016.
 */
public class DealsBookingApi extends ApiUtils {

    public static final String BOOKING_URL = BASE_URL + "pay";
    private static final String TXN_AMOUNT = "txnAmount";
    private static final String PRODUCT_LIST = "productList";

    public static byte[] getBookingDeals(Context context, String pincode, JSONArray productListArray, String txnAmt) {
        return VolleyUtils.encodeRawBodyHashMapToByte(addCheckSum(context, pincode, productListArray, txnAmt), "UTF-8");
    }

    private static Map<String, String> addCheckSum(Context context, String pincode, JSONArray productListArray, String txnAmt) {

        HashMap<String, String> params = new LinkedHashMap<>();
        params.put(MOBILE, PreferenceUtils.getMobileNumber(context));
        params.put(EMAIL, PreferenceUtils.getMailId(context));

        params.put(PIN_CODE, pincode);
        params.put(PRODUCT_LIST, String.valueOf(productListArray));
        params.put(TXN_AMOUNT, txnAmt);

        params.put(REQUEST_FROM, APP);
        params.put(REQUEST_DATE, getDate());

        return getParamsWithCheckSum(context, params);
    }

}
