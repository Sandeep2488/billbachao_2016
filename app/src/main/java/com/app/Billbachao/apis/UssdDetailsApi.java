package com.app.Billbachao.apis;

import android.content.Context;

import com.app.Billbachao.utils.PreferenceUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by BB-001 on 5/9/2016.
 */
public class UssdDetailsApi extends ApiUtils {

    public static final String URL = BASE_URL + "PushUssdDetailsServlet";

    public static Map<String, String> getParams(Context context, String ussdDetails) {
        Map<String, String> params = new LinkedHashMap<>();
        params.put(MOBILE, PreferenceUtils.getMobileNumber(context));
        params.put(USSD_DETAILS, ussdDetails);
        params.put(CIRCLE_ID, PreferenceUtils.getCircleId(context));
        params.put(OPERATOR_ID, PreferenceUtils.getOperatorId(context));
        params.put(REQUEST_TYPE, MOB);
        params.put(REQUEST_DATE, getDate());

        return getParamsWithCheckSum(context, params);
    }
}