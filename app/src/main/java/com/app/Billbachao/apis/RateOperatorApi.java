package com.app.Billbachao.apis;

import android.content.Context;

import com.app.Billbachao.utils.PreferenceUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by mihir on 11-05-2016.
 */
public class RateOperatorApi extends ApiUtils {

    public static final String URL = BASE_URL + "RateOperatorServlet";

    static final String MOBILE_NUMBER_FULL = "mobile_number", LATITUDE = "lat", LONGITUDE = "long";

    public static Map<String, String> getParams(Context context, double latitude, double
            longitude, int dataRating, int connectivityRating, int valueRating, int customerCareRating) {
        Map<String, String> map = new LinkedHashMap<>();
        map.put(MOBILE_NUMBER_FULL, PreferenceUtils.getMobileNumber(context));
        map.put(REQUEST_DATE, getDate());
        map.put("data_speed", String.valueOf(dataRating));
        map.put("call_quality", String.valueOf(connectivityRating));
        map.put("network_quality", String.valueOf(valueRating));
        map.put("customer_care", String.valueOf(customerCareRating));
        map.put(LATITUDE, String.valueOf(latitude));
        map.put(LONGITUDE, String.valueOf(longitude));
        return getParamsWithCheckSum(context, map);
    }
}
