package com.app.Billbachao.apis;

import android.content.Context;

import com.app.Billbachao.utils.PreferenceUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by mihir on 17-08-2015.
 */
public class QuickRechargeApi extends ApiUtils {

    public static final String URL = BASE_URL + "GetQuickRechargePlansServlet";

    public static Map<String, String> getParams(Context context, String circleName, String
            operatorName, String amount) {
        Map<String, String> map = new LinkedHashMap<>();
        map.put(MOBILE_NUMBER, PreferenceUtils.getMobileNumber(context));
        map.put(REQUEST_DATE, getDate());
        map.put(AMOUNT, amount);
        map.put(REQUEST_FROM, APP);
        map.put(OPERATOR_NAME, operatorName);
        map.put(CIRCLE_NAME, circleName);
        map.put(TYPE, PREPAID);

        return getParamsWithCheckSum(context, map);
    }
}
