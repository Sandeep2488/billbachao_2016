package com.app.Billbachao.apis;

import android.content.Context;

import com.app.Billbachao.utils.PreferenceUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by mihir on 23-04-2016.
 */
public class TransactionHistoryApi extends ApiUtils {

    public static final String URL = BASE_URL + "GetTransactionHistory";

    // TODO Request types, need to be modified as per web/app and separate flag for type.
    public static final String MODE = "mode", GET_CASHBACK = "GET_CASHBACK", GET_HISTORY = "GET_HISTORY";

    public static Map<String, String> getCashbackParams(Context context) {
        return getParams(context, GET_CASHBACK);
    }

    public static Map<String, String> getHistoryParams(Context context) {
        return getParams(context, GET_HISTORY);
    }

    public static Map<String, String> getParams(Context context, String requestType) {
        Map<String, String> map = new LinkedHashMap<>();
        map.put(MOBILE, PreferenceUtils.getMobileNumber(context));
        map.put(CIRCLE_ID, PreferenceUtils.getCircleId(context));
        map.put(OPERATOR_ID, PreferenceUtils.getOperatorId(context));
        map.put(TYPE, PreferenceUtils.getCType(context));
        map.put(REQUEST_FROM, APP);
        map.put(MODE, requestType);
        map.put(REQUEST_DATE, getDate());
        return getParamsWithCheckSum(context, map);
    }
}
