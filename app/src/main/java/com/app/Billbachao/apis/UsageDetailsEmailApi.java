package com.app.Billbachao.apis;

import android.content.Context;

import com.app.Billbachao.usagelog.EmailHelper;
import com.app.Billbachao.utils.PreferenceUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by mihir on 18-11-2015.
 */
public class UsageDetailsEmailApi extends ApiUtils {

    public static final String URL = BASE_URL + "eMailUsageDetails";

    static final String USAGE_DETAILS = "usageDetails", USAGE_DAYS = "usageInDays", TOTAL_COST = "totalCost";

    public static Map<String, String> getParams(Context context, int daysCount, String email) {
        Map<String, String> map = new LinkedHashMap<>();

        EmailHelper helper = new EmailHelper(context);
        helper.getUsageDetails(daysCount);

        map.put(MOBILE, PreferenceUtils.getMobileNumber(context));
        map.put(USAGE_DETAILS, helper.getDataObject());
        map.put(TOTAL_COST, String.valueOf(helper.getTotalCost()));
        map.put(USAGE_DAYS, String.valueOf(daysCount));
        map.put(EMAIL, email);
        map.put(REQUEST_FROM, APP);
        map.put(REQUEST_DATE, getDate());
        return getParamsWithCheckSum(context, map);
    }
}
