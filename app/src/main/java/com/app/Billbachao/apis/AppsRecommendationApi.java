package com.app.Billbachao.apis;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.app.Billbachao.recommendations.app.model.RecommendedAppDetails;
import com.app.Billbachao.utils.DateUtils;
import com.app.Billbachao.utils.PreferenceUtils;
import com.google.myjson.Gson;
import com.google.myjson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mihir on 16-12-2015.
 */
public class AppsRecommendationApi extends ApiUtils {

    public static final String URL = BASE_URL + "GetAppOfDayForMe";

    static final String APPS_LIST = "appsList", APPS_DATE = "appsDate";

    public static Map<String, String> getParams(Context context) {
        Map<String, String> map = new LinkedHashMap<>();
        map.put(MOBILE, PreferenceUtils.getMobileNumber(context));
        map.put(REQUEST_FROM, APP);
        map.put(REQUEST_DATE, getDate());
        return getParamsWithCheckSum(context, map);
    }

    public static ArrayList<RecommendedAppDetails> parseAppsString(Context context, String result) {
        final String APPS_LIST = "appForDay";
        ArrayList<RecommendedAppDetails> appsList = new ArrayList<>();
        String appsListString = "";
        try {
            JSONObject dataObject = new JSONObject(result);
            String status = dataObject.getString(ApiUtils.STATUS);
            if (status.equalsIgnoreCase(ApiUtils.SUCCESS)) {
                String statusDesc = dataObject.getString(ApiUtils.STATUS_DESC);
                if (statusDesc.equalsIgnoreCase(ApiUtils.SUCCESS)) {
                    appsListString = dataObject.getString(APPS_LIST);
                    appsList = parseAppsJsonArray(appsListString);
                    if (appsList.size() > 0) {
                        saveAppsString(context, appsListString);
                    }
                } else {
                    // Error
                }
            } else {
                // Error
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        return appsList;
    }

    public static ArrayList<RecommendedAppDetails> parseAppsJsonArray(String appsJsonString) {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<RecommendedAppDetails>>() {
        }.getType();
        return gson.fromJson(appsJsonString, listType);
    }

    public static void saveAppsString(Context context, String appsList) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context)
                .edit();
        editor.putString(APPS_LIST, appsList);
        editor.putLong(APPS_DATE, DateUtils.getBeforeDays(0));
        editor.apply();
    }

    public static String getSavedAppString(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (!DateUtils.isToday(preferences.getLong(APPS_DATE, 0))) {
            return "";
        }
        return preferences.getString(APPS_LIST, "");
    }
}
