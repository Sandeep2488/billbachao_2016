package com.app.Billbachao.apis;

import android.content.Context;

import com.app.Billbachao.utils.PreferenceUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by mihir on 11-05-2016.
 */
public class PaymentAbortedApi extends ApiUtils {

    public static final String URL = BASE_URL + "AbortRechargeStatusServlet";

    static final String TRANSACTION_ID = "transactionId";

    public static Map<String, String> getParams(Context context, String transactionId) {
        Map<String, String> map = new LinkedHashMap<>();
        map.put(MOBILE, PreferenceUtils.getMobileNumber(context));
        map.put(TRANSACTION_ID, transactionId);
        map.put(REQUEST_FROM, APP);
        map.put(REQUEST_DATE, getDate());

        return getParamsWithCheckSum(context, map);
    }
}
