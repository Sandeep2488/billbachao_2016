package com.app.Billbachao.apis;

import android.content.Context;

import com.app.Billbachao.utils.PreferenceUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by mihir on 25-04-2016.
 */
public class BrowsePlansApi extends ApiUtils {

    public static final String URL = BASE_URL + "GetBrowsePlansServlet";

    static final String MODE = "mode";

    public static Map<String, String> getParams(Context context, String circleName, String
            operatorName, String mode, String amount) {
        Map<String, String> map = new LinkedHashMap<>();
        map.put(MOBILE_NUMBER, PreferenceUtils.getMobileNumber(context));
        map.put(REQUEST_DATE, getDate());
        map.put(OPERATOR_NAME, operatorName);
        map.put(CIRCLE_NAME, circleName);
        map.put(MODE, mode);
        map.put(AMOUNT, amount);
        map.put(REQUEST_FROM, APP);

        return getParamsWithCheckSum(context, map);
    }
}
