package com.app.Billbachao.apis;

import android.content.Context;

import com.app.Billbachao.recharge.plans.model.PlanValidationItem;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.utils.PreferenceUtils;
import com.google.myjson.GsonBuilder;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by mihir on 02-05-2016.
 */
public class ValidationRequestApi extends ApiUtils {

    public static final String URL = BASE_URL + "CyberplatValidation";

    public static final String TRANSACTION_AMOUNT = "txnAmt", PLAN_ARRAY = "jArr",
            RECHARGE_AMOUNT = "recharge_amt", OFFERED_DISCOUNT_AMT = "offered_discount_amt",
            PAID_COUPON_AMT = "paid_coupon_amt", COUPON_ARRAY = "coupon_array", MOBILE = "mobile",
            PG_MODE = "pgMode", LATITUDE = "lat", LONGITUDE = "long", CASHBACK_AMOUNT = 
            "cashback_amount", ZERO = "0";

    public static Map<String, String> getParams(Context context, ArrayList<PlanValidationItem>
            planItems, double transactionAmount, double cashbackAmount, double paymentAmount) {
        
        Map<String, String> map = new LinkedHashMap<>();
        map.put(MOBILE, PreferenceUtils.getMobileNumber(context));
        map.put(TRANSACTION_AMOUNT, String.valueOf(paymentAmount));
        map.put(OPERATOR_ID, PreferenceUtils.getOperatorId(context));
        map.put(CIRCLE_ID, PreferenceUtils.getCircleId(context));
        map.put(TYPE, PREPAID);
        map.put(REQUEST_FROM, APP);
        String planListString = new GsonBuilder().create().toJson(planItems);
        ILog.d(TAG, planListString);
        map.put(PLAN_ARRAY, planListString);
        map.put(COUPON_ARRAY, NA);
        map.put(UEMAIL, PreferenceUtils.getMailId(context));
        map.put(PG_MODE, PreferenceUtils.BILLDESK_PG_MODE);
        map.put(RECHARGE_AMOUNT, String.valueOf(transactionAmount));
        map.put(OFFERED_DISCOUNT_AMT, ZERO);
        map.put(PAID_COUPON_AMT, ZERO);
        map.put(LATITUDE, ZERO);
        map.put(LONGITUDE, ZERO);
        map.put(CASHBACK_AMOUNT, String.valueOf(cashbackAmount));
        map.put(REQUEST_DATE, getDate());
        return getParamsWithCheckSum(context, map);
    }
}
