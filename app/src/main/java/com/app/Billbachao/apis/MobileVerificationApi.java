package com.app.Billbachao.apis;

import android.content.Context;

import com.app.Billbachao.BuildConfig;
import com.app.Billbachao.common.permissions.PermissionWrapper;
import com.app.Billbachao.dualsim.telephony.DualSimManager;
import com.app.Billbachao.utils.PreferenceUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by sushil on 8/26/2015.
 */
public class MobileVerificationApi extends ApiUtils {

    public static final String URL = BASE_URL + "SendMobileVerificationCode";

    static final String MODE = "mode", SMS_MODE = "SENDSMS", OS_TYPE = "osType", ANDROID =
            "ANDROID", APK_VERSION = "apkVersion", USER_DETAILS = "userDetails";

    public static Map<String, String> getParams(Context context, String mobileNumber) {
        return getParams(context, mobileNumber, PreferenceUtils.getOperatorId(context),
                PreferenceUtils.getCircleId(context), PreferenceUtils.getCType(context), PreferenceUtils.getNetworkType(context), PreferenceUtils.getMailId(context));
    }

    public static Map<String, String> getParams(Context context, String mobileNumber, String opId, String cId, String billType, String networkType, String emailId) {
        Map<String, String> map = new LinkedHashMap<>();
        map.put(MOBILE, mobileNumber);
        map.put(USER_DETAILS, getUserDetails(context, mobileNumber, opId, cId, billType, networkType, emailId).toString());
        map.put(REQUEST_DATE, getDate());
        return getParamsWithCheckSum(context, map);
    }

    public static JSONObject getUserDetails(Context context, String mobileNumber, String opId, String cId, String billType, String networkType, String emailId) {
        JSONObject jsonObject_userDetails = new JSONObject();
        try {
            boolean hasPermissions = PermissionWrapper.hasTelephonyPermissions(context);
            DualSimManager simManager = null;
            if (hasPermissions)
                simManager = new DualSimManager(context);
            int SIM1 = 0, SIM2 = 1;
            jsonObject_userDetails.put(MOBILE, mobileNumber);
            jsonObject_userDetails.put(MODE, SMS_MODE);
            jsonObject_userDetails.put(OPERATOR_ID, opId);
            jsonObject_userDetails.put(CIRCLE_ID, cId);
            jsonObject_userDetails.put(TYPE, billType);
            jsonObject_userDetails.put(NETWORK_TYPE, networkType);
            jsonObject_userDetails.put(EMAIL, emailId);
            jsonObject_userDetails.put(OS_TYPE, ANDROID);
            jsonObject_userDetails.put(APK_VERSION, BuildConfig.VERSION_NAME);
            jsonObject_userDetails.put("imei1", hasPermissions ? simManager.getIMEI(SIM1) : NA);
            if (hasPermissions && simManager.isDualSIMSupported()) {
                jsonObject_userDetails.put("imei2", simManager.getIMEI(SIM2));
                jsonObject_userDetails.put("isDualSim", "Y");
            } else {
                jsonObject_userDetails.put("imei2", NA);
                jsonObject_userDetails.put("isDualSim", "N");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject_userDetails;

    }

}
