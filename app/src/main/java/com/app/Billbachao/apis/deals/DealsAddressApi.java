package com.app.Billbachao.apis.deals;

import android.content.Context;

import com.app.Billbachao.apis.ApiUtils;
import com.app.Billbachao.deals.models.DealsAddressModel;
import com.app.Billbachao.utils.PreferenceUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Api to get address/delete and update deals Address.
 * Created by nitesh on 24/02/2016.
 */
public class DealsAddressApi extends ApiUtils {

    public static final String URL_SHIPPING = BASE_URL + "getShippingDetails";
    private static final String GET_BY_MOBILE = "GETBYMOBILE";

    private static final String URL_VALIDATE_PINCODE = BASE_URL + "GetDetailsByPincode";
    private static final String GET_BY_PINCODE = "GET";
    private static final String OLD_PINCODE = "oldpincode";

    /**
     * Add & Update Apis
     * @param context
     * @param dealsAddressModel
     * @param mode
     * @param oldpincode
     * @return
     */
    public static Map<String, String> addInsertUpdateDealsAddress(Context context, DealsAddressModel dealsAddressModel, String mode, String oldpincode) {
        HashMap<String, String> params = new LinkedHashMap<>();

        params.put(MOBILE, PreferenceUtils.getMobileNumber(context));
        params.put(PIN_CODE, dealsAddressModel.pincode);
        params.put(OLD_PINCODE, oldpincode);
        params.put(ADDRESS, dealsAddressModel.address);
        params.put(CITY, dealsAddressModel.city);
        params.put(STATE, dealsAddressModel.state);
        params.put(FIRST_NAME, dealsAddressModel.firstname);
        params.put(LAST_NAME, dealsAddressModel.lastname);
        params.put(ADDRESS_TYPE, NA);

        params.put(EMAIL, PreferenceUtils.getMailId(context));
        params.put(LATITUDE, String.valueOf(0.0));
        params.put(LONGITUDE, String.valueOf(0.0));
        params.put(LANDMARK, dealsAddressModel.landmark);

        params.put(MODE, mode);
        params.put(REQUEST_FROM, APP);
        params.put(REQUEST_DATE, getDate());

        return getParamsWithCheckSum(context, params);
    }

    /**
     * Fetch Deals by Mobile
     *
     * @param context
     * @return
     */

    public static Map<String, String> getDealsAddressByMobile(Context context) {
        HashMap<String, String> params = new LinkedHashMap<>();

        params.put(MOBILE, PreferenceUtils.getMobileNumber(context));
        params.put(PIN_CODE, NA);
        params.put(OLD_PINCODE, NA);
        params.put(ADDRESS, NA);
        params.put(CITY, NA);
        params.put(STATE, NA);
        params.put(FIRST_NAME, NA);
        params.put(LAST_NAME, NA);
        params.put(ADDRESS_TYPE, NA);
        params.put(EMAIL, PreferenceUtils.getMailId(context));
        params.put(LATITUDE, String.valueOf(0.0));
        params.put(LONGITUDE, String.valueOf(0.0));
        params.put(LANDMARK, NA);
        params.put(MODE, GET_BY_MOBILE);

        params.put(REQUEST_FROM, APP);
        params.put(REQUEST_DATE, getDate());

        return getParamsWithCheckSum(context, params);

    }

    /**
     * Delete Address
     * @param mContext
     * @param dealsAddressModel
     * @param from
     * @return
     */
    public static Map<String, String> deleteDealsAddress(Context mContext, DealsAddressModel dealsAddressModel, String from) {

        HashMap<String, String> params = new LinkedHashMap<>();

        params.put(MOBILE, PreferenceUtils.getMobileNumber(mContext ));
        params.put(PIN_CODE, dealsAddressModel.pincode);
        params.put(OLD_PINCODE, dealsAddressModel.pincode);
        params.put(ADDRESS, NA);
        params.put(CITY, NA);
        params.put(STATE, NA);
        params.put(FIRST_NAME, NA);
        params.put(LAST_NAME, NA);
        params.put(ADDRESS_TYPE, NA);
        params.put(EMAIL, PreferenceUtils.getMailId(mContext));
        params.put(LATITUDE, String.valueOf(0.0));
        params.put(LONGITUDE, String.valueOf(0.0));
        params.put(LANDMARK, NA);
        params.put(MODE , from);

        params.put(REQUEST_FROM, APP);
        params.put(REQUEST_DATE, getDate());

        return getParamsWithCheckSum(mContext, params);
    }
}
