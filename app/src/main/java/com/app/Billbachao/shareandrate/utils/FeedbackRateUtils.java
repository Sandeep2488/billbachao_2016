package com.app.Billbachao.shareandrate.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by mihir on 04-12-2015.
 */
public class FeedbackRateUtils {

    public static final String RATED = "RATED_APP_TIME", SKIPPED = "SKIPPED_RATING_TIME";

    public static final long DAY_TO_MILLISECONDS = 86400 * 1000;

    // Rated span 60 days, skipped span 15 days
    public static final long RATED_TIME_SPAN = 60 * DAY_TO_MILLISECONDS, SKIPPED_TIME_SPAN = 15 * DAY_TO_MILLISECONDS;

    public static final int SHOW_AFTER_INSTALLED_DAYS = 7;

    public static void storeRateResponseTime(Context context, boolean isRated) {
        String prefKey = isRated ? RATED : SKIPPED;
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putLong(prefKey, System.currentTimeMillis()).apply();
    }

    private static boolean isRatingTimeSpanOver(Context context, boolean isRated) {
        final long ratingSpan = isRated ? RATED_TIME_SPAN : SKIPPED_TIME_SPAN;
        String prefKey = isRated ? RATED : SKIPPED;
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        long currentTime = System.currentTimeMillis();
        long previousTime = preferences.getLong(prefKey, 0);
        return (currentTime - previousTime > ratingSpan);
    }

    public static boolean canShowRateDialog(Context context) {
        return isRatingTimeSpanOver(context, false) && isRatingTimeSpanOver(context, true);
    }
}
