package com.app.Billbachao.shareandrate.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.ShareCompat;
import android.widget.Toast;

import com.app.Billbachao.R;
import com.app.Billbachao.utils.AppManagerUtils;
import com.app.Billbachao.utils.ILog;

/**
 * Created by mihir on 26-08-2015.
 */
public class ShareUtils {

    public static final String FACEBOOK_PACKAGE = "com.facebook.katana";

    public static void shareApp(Context context) {
        ILog.d("Mihir", "ShareApp context");
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.app_name));
        String shareText = context.getString(R.string.app_share_text, context.getString(R.string.app_share_url));
        sendIntent.putExtra(Intent.EXTRA_TEXT, shareText);
        sendIntent.setType("text/plain");
        context.startActivity(Intent.createChooser(sendIntent, null));
    }

    public static void shareApp(Activity activity) {
        ILog.d("Mihir", "ShareApp activity");
        Intent shareIntent = ShareCompat.IntentBuilder.from(activity).setType("text/plain").
                setSubject(activity.getString(R.string.app_name)).setText(activity.getString(R
                .string.app_share_text, activity.getString(R.string.app_share_url))).createChooserIntent();
        activity.startActivity(shareIntent);
    }

    public static void shareApp(Context context, String packageName) {

        if (AppManagerUtils.getPackageInfo(context, packageName) == null) {
            Toast.makeText(context, R.string.app_not_installed, Toast.LENGTH_SHORT).show();
            return;
        }

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT,
                context.getString(R.string.app_name));

        String shareText;
        if (isTextShareSupported(packageName)) {
            shareText = context.getString(R.string.app_share_text, context.getString(R.string.app_share_url));
        } else {
            shareText = context.getString(R.string.app_share_url);
        }
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                shareText);
        sendIntent.setPackage(packageName);
        sendIntent.setType("text/plain");
        context.startActivity(sendIntent);
    }

    /**
     * Checks if app supports text sharing or not
     * Designed for apps like facebook that support only URL sharing
     *
     * @param packageName
     * @return
     */
    public static boolean isTextShareSupported(String packageName) {

        if (packageName.equalsIgnoreCase(FACEBOOK_PACKAGE))
            return false;
        return true;
    }

    public static void launchStore(Context context) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(context.getString(R.string.app_url)));
        context.startActivity(intent);
    }

    public static void launchWebsite(Context context) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(context.getString(R.string.web_url)));
        context.startActivity(intent);
    }
}
