package com.app.Billbachao.shareandrate;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.R;
import com.app.Billbachao.apis.FeedbackApi;
import com.app.Billbachao.model.BaseGsonResponse;
import com.app.Billbachao.volley.request.NetworkGsonRequest;
import com.app.Billbachao.volley.utils.VolleySingleTon;

/**
 * Background task for sending feedback message
 * Created by mihir on 04-12-2015.
 */
public class SendFeedbackTask {

    Context mContext;

    String mFeedback;

    int mRating;

    boolean mShowResponse;

    /**
     * @param context      Context object
     * @param rating       Rating value
     * @param feedback     Feedback text
     * @param showResponse Show response in case of explicit request
     */
    public SendFeedbackTask(Context context, int rating, String feedback, boolean showResponse) {
        mContext = context;
        mFeedback = feedback;
        mRating = rating;
        mShowResponse = showResponse;
    }

    public void trigger() {
        NetworkGsonRequest<BaseGsonResponse> cashbackFetcher = new NetworkGsonRequest<>(Request
                .Method.POST, FeedbackApi.URL, BaseGsonResponse.class,
                FeedbackApi.getParams(mContext, mRating, mFeedback), new Response
                .Listener<BaseGsonResponse>() {
            @Override
            public void onResponse(BaseGsonResponse response) {
                if (mShowResponse)
                    if (response.isSuccess())
                        Toast.makeText(mContext, R.string.feedback_sent, Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(mContext, response.getStatusDesc(), Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        VolleySingleTon.getInstance(mContext).addToRequestQueue(cashbackFetcher, null);
    }
}
