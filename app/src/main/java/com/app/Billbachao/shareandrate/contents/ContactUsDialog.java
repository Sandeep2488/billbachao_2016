package com.app.Billbachao.shareandrate.contents;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;

import com.app.Billbachao.R;

/**
 * Created by mihir on 05-02-2016.
 */
public class ContactUsDialog extends DialogFragment {

    public static final String TAG = ContactUsDialog.class.getSimpleName();

    OnContactUsCallback mListener;

    public static ContactUsDialog getInstance() {
        return new ContactUsDialog();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnContactUsCallback)
            mListener = (OnContactUsCallback) activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AnimatedDialogTheme_EnterRightExitRight);
        builder.setTitle(R.string.contact_us);

        View view = View.inflate(getActivity(), R.layout.contact_us_dialog, null);

        Linkify.addLinks((TextView) view.findViewById(R.id.contact_mail), Linkify.ALL);
        Linkify.addLinks((TextView) view.findViewById(R.id.contact_whatsapp), Linkify.ALL);

        builder.setView(view);
        builder.setPositiveButton(R.string.ok, null);
        builder.setNeutralButton(R.string.send_feedback, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (mListener != null) {
                    mListener.onFeedbackRequested();
                }
            }
        });
        return builder.create();
    }

    public interface OnContactUsCallback {
        void onFeedbackRequested();
    }
}
