package com.app.Billbachao.common.menus;

import android.app.Activity;
import android.database.ContentObserver;
import android.net.Uri;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.recharge.cart.CartPlanHelper;
import com.app.Billbachao.recharge.cart.provider.CartPlansProvider;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.utils.UiUtils;

/**
 * Created by mihir on 03-05-2016.
 */
public class CommonMenuHelper {

    public static final String TAG = CommonMenuHelper.class.getSimpleName();

    Activity mActivity;

    boolean mIsCartSupported;

    public CommonMenuHelper(Activity activity, boolean isCartSupported) {
        mActivity = activity;
        mIsCartSupported = isCartSupported;
        if (mIsCartSupported) mActivity.getContentResolver().registerContentObserver
                (CartPlansProvider.PlanDetails.CONTENT_URI, true, mCartObserver);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        mActivity.getMenuInflater().inflate(R.menu.common_menu, menu);
        if (!mIsCartSupported) {
            menu.findItem(R.id.cart).setVisible(false);
        }
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        if (mIsCartSupported) {
            int cartCount = CartPlanHelper.getItemCount(mActivity);
            if (cartCount > 0) {
                View view = menu.findItem(R.id.cart).getActionView();
                TextView cartItems = (TextView) view.findViewById(R.id.cart_count);
                cartItems.setText(String.valueOf(cartCount));
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        UiUtils.launchCart(mActivity);
                    }
                });
            } else {
                menu.findItem(R.id.cart).setVisible(false);
            }
        }
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.cart:
                UiUtils.launchCart(mActivity);
                return true;
            case android.R.id.home:
                mActivity.finish();
                return true;
        }
        return false;
    }

    public void destroy() {
        if (mIsCartSupported)
            mActivity.getContentResolver().unregisterContentObserver(mCartObserver);
    }

    ContentObserver mCartObserver = new ContentObserver(null) {
        @Override
        public void onChange(boolean selfChange, Uri uri) {
            ILog.d(TAG, "onChange " + uri.getEncodedPath());
            mActivity.invalidateOptionsMenu();
        }

        @Override
        public void onChange(boolean selfChange) {
            ILog.d(TAG, "onChange " + selfChange);
            mActivity.invalidateOptionsMenu();
        }
    };
}
