package com.app.Billbachao.dualsim.telephony;

/**
 * Created by sushil on 8/25/2015.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.app.Billbachao.model.DualSIMBean;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class DualSimManager {

    private String SIM_VARINT = "";
    private String telephonyClassName = "";
    private SharedPreferences pref;
    private int slotNumber_1 = 0;
    private int slotNumber_2 = 1;
    private String slotName_1 = "null";
    private String slotName_2 = "null";
    private String[] listofClass;

    private String IMEI_1, IMSI_1, NETWORK_OPERATOR_NAME_1;
    private String IMEI_2, IMSI_2, NETWORK_OPERATOR_NAME_2;
    private String NETWORK_OPERATOR_CODE_1;
    private String NETWORK_OPERATOR_CODE_2;


    final static String m_IMEI = "getDeviceId";
    final static String m_IMSI = "getSubscriberId";
    final static String m_NETWORK_OPERATOR = "getNetworkOperatorName";
    final static String m_NETWORK_OPERATOR_CODE = "getNetworkOperator";
    protected static customTelephony customTelephony;

    public DualSimManager(Context mContext) {

        if (IMEI_1 == null) {
            customTelephony = new customTelephony(mContext);
        }
        if (customTelephony.getIMEIList().size() > 0)
            customTelephony.getDefaultSIMInfo();
    }

    public String getDeviceVersion() {
        DualSIMBean dualSIMBean = new DualSIMBean();

        dualSIMBean.setOSVersion("Android " + android.os.Build.VERSION.RELEASE);
        return "Android " + android.os.Build.VERSION.RELEASE;
    }

    public String getDeviceManufacturer() {
        return android.os.Build.MANUFACTURER;
    }

    public String getDeviceModel() {
        return android.os.Build.MODEL;
    }


    public boolean isDualSIMSupported() {
        if (!TextUtils.isEmpty(IMEI_1) && !TextUtils.isEmpty(IMEI_2)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isSecondSimActive() {
        if (IMSI_2 != null && (!IMSI_2.equalsIgnoreCase("null") && !IMSI_2.equals(""))) {
            return true;
        } else
            return false;

    }

    public String getIMEI(int slotnumber) {
        if (slotnumber == 0)
            return IMEI_1;
        else
            return IMEI_2;
    }

    public String getIMSI(int slotnumber) {
        if (slotnumber == 0)
            return IMSI_1;
        else
            return IMSI_2;
    }

    public String getNETWORK_OPERATOR_NAME(int slotnumber) {

        if (slotnumber == 0)
            return NETWORK_OPERATOR_NAME_1;
        else
            return NETWORK_OPERATOR_NAME_2;
    }

    public String getNETWORK_OPERATOR_CODE(int slotnumber) {
        String code = NETWORK_OPERATOR_CODE_1;
        if (slotnumber == 1) {
            code = NETWORK_OPERATOR_CODE_2;
        }

        return code;
    }

    class customTelephony {
        Context mContext;
        TelephonyManager telephony;

        public customTelephony(Context mContext) {
            try {
                this.mContext = mContext;
                telephony = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
                pref = PreferenceManager.getDefaultSharedPreferences(mContext);
                telephonyClassName = pref.getString("dualsim_telephonycls", "");
                SIM_VARINT = pref.getString("SIM_VARINT", "");
                slotName_1 = pref.getString("SIM_SLOT_NAME_1", "");
                slotName_2 = pref.getString("SIM_SLOT_NAME_2", "");
                slotNumber_1 = pref.getInt("SIM_SLOT_NUMBER_1", 0);
                slotNumber_2 = pref.getInt("SIM_SLOT_NUMBER_2", 1);
                telephonyClassName = pref.getString("dualsim_telephonycls", "");

                if (telephonyClassName.equalsIgnoreCase("")) {
                    fetchClassInfo();
                } else if (!isValidMethod(telephonyClassName)) {
                    fetchClassInfo();
                }
                gettingAllMethodValues();
            } catch (ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        }

        /**
         * This method returns the class name in which we fetch dual sim details
         */
        public void fetchClassInfo() throws ClassNotFoundException {

            telephonyClassName = "android.telephony.TelephonyManager";
            listofClass = new String[]{
                    "com.mediatek.telephony.TelephonyManagerEx",
                    "android.telephony.TelephonyManager",
                    "android.telephony.MSimTelephonyManager"};
            for (int index = 0; index < listofClass.length; index++) {
                if (isTelephonyClassExists(listofClass[index])) {
                    if (isMethodExists(listofClass[index], "getDeviceId")) {
                        System.out.println("getDeviceId method found");
                        if (!SIM_VARINT.equalsIgnoreCase("")) {
                            break;
                        }
                    }
                    if (isMethodExists(listofClass[index],
                            "getNetworkOperatorName")) {
                        System.out
                                .println("getNetworkOperatorName method found");
                        break;
                    } else if (isMethodExists(listofClass[index],
                            "getSimOperatorName")) {
                        System.out.println("getSimOperatorName method found");
                        break;
                    }
                }
            }
            for (int index = 0; index < listofClass.length; index++) {
                if (slotName_1 == null || slotName_1.equalsIgnoreCase("")) {
                    getValidSlotFields(listofClass[index]);

                    getSlotNumber(listofClass[index]);
                } else {
                    break;
                }
            }

            Editor edit = pref.edit();
            edit.putString("dualsim_telephonycls", telephonyClassName);
            edit.putString("SIM_VARINT", SIM_VARINT);
            edit.putString("SIM_SLOT_NAME_1", slotName_1);
            edit.putString("SIM_SLOT_NAME_2", slotName_2);
            edit.putInt("SIM_SLOT_NUMBER_1", slotNumber_1);
            edit.putInt("SIM_SLOT_NUMBER_2", slotNumber_2);
            edit.commit();
            System.out.println("Done");
        }

        /**
         * Check Method is found in class
         */
        public boolean isValidMethod(String className) {
            boolean isValidMail = false;

            if (isMethodExists(className, "getDeviceId")) {
                isValidMail = true;
            } else if (isMethodExists(className, "getNetworkOperatorName")) {
                isValidMail = true;
            } else if (isMethodExists(className, "getSimOperatorName")) {
                isValidMail = true;
            }
            return isValidMail;
        }

        /**
         * Check method with sim variant
         */
        public boolean isMethodExists(String className, String compairMethod) {
            boolean isExists = false;
            try {
                Class<?> telephonyClass = Class.forName(className);
                Class<?>[] parameter = new Class[1];
                parameter[0] = int.class;
                StringBuffer sbf = new StringBuffer();
                Method[] methodList = telephonyClass.getDeclaredMethods();
                for (int index = methodList.length - 1; index >= 0; index--) {
                    sbf.append("\n\n" + methodList[index].getName());
                    if (methodList[index].getReturnType().equals(String.class)) {
                        String methodName = methodList[index].getName();
                        if (methodName.contains(compairMethod)) {
                            Class<?>[] param = methodList[index]
                                    .getParameterTypes();
                            if (param.length > 0) {
                                if (param[0].equals(int.class)) {

                                    SIM_VARINT = methodName.substring(
                                            compairMethod.length(),
                                            methodName.length());
                                    telephonyClassName = className;
                                    isExists = true;
                                    break;

                                } else {
                                    telephonyClassName = className;
                                    isExists = true;
                                }
                            }
                        }
                    }
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            return isExists;
        }

        public String getMethodValue(String className, String compairMethod, int slotNumber_1) {
            String value = "";
            try {
                Class<?> telephonyClass = Class.forName(className);
                Class<?>[] parameter = new Class[1];
                parameter[0] = int.class;
                StringBuffer sbf = new StringBuffer();
                Method[] methodList = telephonyClass.getDeclaredMethods();
                for (int index = methodList.length - 1; index >= 0; index--) {
                    sbf.append("\n\n" + methodList[index].getName());
                    if (methodList[index].getReturnType().equals(String.class)) {
                        String methodName = methodList[index].getName();
                        if (methodName.contains(compairMethod)) {
                            Class<?>[] param = methodList[index]
                                    .getParameterTypes();
                            if (param.length > 0) {
                                if (param[0].equals(int.class)) {
                                    try {
                                        SIM_VARINT = methodName.substring(
                                                compairMethod.length(),
                                                methodName.length());
                                        if (!methodName.equalsIgnoreCase(compairMethod + "Name") && !methodName.equalsIgnoreCase(compairMethod + "ForSubscription")) {
                                            value = invokeMethod(telephonyClassName, slotNumber_1, compairMethod, SIM_VARINT);
                                            if (!TextUtils.isEmpty(value)) {
                                                break;
                                            }
                                        }
                                    } catch (NoSuchMethodException e) {
                                        e.printStackTrace();
                                    }
                                } else if (param[0].equals(long.class)) {
                                    SIM_VARINT = methodName.substring(
                                            compairMethod.length(),
                                            methodName.length());
                                    if (!methodName.equalsIgnoreCase(compairMethod + "Name") && !methodName.equalsIgnoreCase(compairMethod + "ForSubscription")) {
                                        value = invokeLongMethod(telephonyClassName, slotNumber_1, compairMethod, SIM_VARINT);
                                        if (!TextUtils.isEmpty(value)) {
                                            break;
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            return value;
        }

        private String invokeLongMethod(String className, long slotNumber,
                                        String methodName, String SIM_variant) {
            String value = "";

            try {
                Class<?> telephonyClass = Class.forName(className);
                Constructor[] cons = telephonyClass.getDeclaredConstructors();
                if (cons.length > 0) {
                    cons[0].getName();
                    cons[0].setAccessible(true);
                    Object obj = cons[0].newInstance();
                    Class<?>[] parameter = new Class[1];
                    parameter[0] = long.class;
                    Object ob_phone = null;
                    try {
                        Method getSimID = telephonyClass.getMethod(methodName
                                + SIM_variant, parameter);
                        Object[] obParameter = new Object[1];
                        obParameter[0] = slotNumber;
                        ob_phone = getSimID.invoke(obj, obParameter);
                    } catch (NoSuchMethodException e) {
                        if (slotNumber == 0) {
                            try {
                                Method getSimID = telephonyClass.getMethod(methodName
                                        + SIM_variant, parameter);
                                Object[] obParameter = new Object[1];
                                obParameter[0] = slotNumber;
                                ob_phone = getSimID.invoke(obj);
                            } catch (NoSuchMethodException ex) {
                                e.printStackTrace();
                            }
                        }
                    }

                    if (ob_phone != null) {
                        value = ob_phone.toString();
                    }
                }
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            return value;
        }

        public ArrayList<String> getIMEIList() {
            slotNumber_1 = 0;
            slotNumber_2 = 1;
            ArrayList<String> imeiList = new ArrayList<>();
            try {
                IMEI_1 = invokeMethod(telephonyClassName, slotNumber_1, m_IMEI, SIM_VARINT);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }

            if (TextUtils.isEmpty(IMEI_1)) {
                IMEI_1 = getMethodValue(telephonyClassName, m_IMEI, slotNumber_1);
            }
            if (IMEI_1 == null || IMEI_1.equalsIgnoreCase("")) {
                IMEI_1 = telephony.getDeviceId();
            }

            try {
                IMEI_2 = invokeMethod(telephonyClassName, slotNumber_2, m_IMEI, SIM_VARINT);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
            if (TextUtils.isEmpty(IMEI_2)) {
                IMEI_2 = getMethodValue(telephonyClassName, m_IMEI, slotNumber_2);
            }

            if (!TextUtils.isEmpty(IMEI_1)) {
                imeiList.add(IMEI_1);
            }
            if (!TextUtils.isEmpty(IMEI_2)) {
                imeiList.add(IMEI_2);
            }
            return imeiList;
        }

        public void getDefaultSIMInfo() {
            telephony = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            String IMSI = telephony.getSubscriberId();
            if (TextUtils.isEmpty(IMSI_1) || IMSI_1.equalsIgnoreCase(IMSI)) {
                IMEI_1 = telephony.getDeviceId();
                NETWORK_OPERATOR_CODE_1 = telephony.getSimOperator();
                NETWORK_OPERATOR_NAME_1 = telephony.getSimOperatorName();
                NETWORK_OPERATOR_CODE_1 = telephony.getNetworkOperator();
                NETWORK_OPERATOR_NAME_1 = telephony.getNetworkOperatorName();

            } else if (isSecondSimActive() && IMSI_2.equalsIgnoreCase(IMSI)) {
                IMEI_2 = telephony.getDeviceId();
                NETWORK_OPERATOR_CODE_2 = telephony.getSimOperator();
                NETWORK_OPERATOR_NAME_2 = telephony.getSimOperatorName();
                NETWORK_OPERATOR_CODE_2 = telephony.getNetworkOperator();
                NETWORK_OPERATOR_NAME_2 = telephony.getNetworkOperatorName();
            }
        }

        public void gettingAllMethodValues() {
            TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(mContext);
            //////////////////IMEI/////////////////////////
            try {

                IMEI_1 = invokeMethod(telephonyClassName, slotNumber_1, m_IMEI, SIM_VARINT);
            } catch (NoSuchMethodException ex) {
                ex.printStackTrace();
            }
            if (TextUtils.isEmpty(IMEI_1)) {
                IMEI_1 = getMethodValue(telephonyClassName, m_IMEI, slotNumber_1);
            }
            if (IMEI_1 == null || IMEI_1.equalsIgnoreCase("")) {
                IMEI_1 = telephony.getDeviceId();
            }

            try {
                IMEI_2 = invokeMethod(telephonyClassName, slotNumber_2, m_IMEI, SIM_VARINT);
            } catch (NoSuchMethodException ex) {
                ex.printStackTrace();
            }
            if (TextUtils.isEmpty(IMEI_2)) {
                IMEI_2 = getMethodValue(telephonyClassName, m_IMEI, slotNumber_2);
            }

            /////////////IMSI///////////////////////////////////////
            if (telephonyInfo.isSIM1Ready()) {
                try {
                    IMSI_1 = invokeMethod(telephonyClassName, slotNumber_1, m_IMSI, SIM_VARINT);
                } catch (NoSuchMethodException ex) {
                    ex.printStackTrace();
                }
                if (IMSI_1 == null || IMSI_1.equalsIgnoreCase("")) {
                    IMSI_1 = telephony.getSubscriberId();
                }
            }
            if (telephonyInfo.isSIM2Ready()) {
                try {
                    IMSI_2 = invokeMethod(telephonyClassName, slotNumber_2, m_IMSI, SIM_VARINT);
                } catch (NoSuchMethodException ex) {
                    ex.printStackTrace();
                }
                if (TextUtils.isEmpty(IMSI_2)) {

                    IMSI_2 = getMethodValue(telephonyClassName, m_IMSI, slotNumber_2);

                    if (TextUtils.isEmpty(IMSI_2)) {
                        IMSI_2 = getMethodValue(telephonyClassName, m_IMSI, slotNumber_2 + 1);
                    }
                }

            }


            if (!TextUtils.isEmpty(IMSI_2) && !TextUtils.isEmpty(IMSI_1)) {
                if (IMSI_1.equalsIgnoreCase(IMSI_2)) {
                    IMSI_1 = "";
                }
            }
            if (IMSI_1 != null && IMSI_2 != null && IMSI_1.equalsIgnoreCase("")) {
                String IMSI2 = getMethodValue(telephonyClassName, m_IMSI, slotNumber_2 + 1);
                if (!TextUtils.isEmpty(IMSI2)) {
                    IMSI_1 = IMSI_2;
                    IMSI_2 = IMSI2;
                    slotNumber_1 = slotNumber_2;
                    slotNumber_2 = slotNumber_2 + 1;
                }
            }

            if (telephonyInfo.isSIM1Ready())
                NETWORK_OPERATOR_NAME_1 = getMethodValue(telephonyClassName, m_NETWORK_OPERATOR, slotNumber_1);
            if (telephonyInfo.isSIM2Ready())
                NETWORK_OPERATOR_NAME_2 = getMethodValue(telephonyClassName, m_NETWORK_OPERATOR, slotNumber_2);
            if (TextUtils.isEmpty(NETWORK_OPERATOR_NAME_1) && telephonyInfo.isSIM1Ready()) {
                try {
                    NETWORK_OPERATOR_NAME_1 = invokeMethod(telephonyClassName, slotNumber_1, m_NETWORK_OPERATOR, SIM_VARINT);
                } catch (NoSuchMethodException ex) {
                    ex.printStackTrace();
                }
            }

            if (TextUtils.isEmpty(NETWORK_OPERATOR_NAME_1) && telephonyInfo.isSIM1Ready()) {
                NETWORK_OPERATOR_NAME_1 = getMethodValue(telephonyClassName, m_NETWORK_OPERATOR, slotNumber_1);
            } else {
                NETWORK_OPERATOR_NAME_1 = "";
            }

            if (TextUtils.isEmpty(NETWORK_OPERATOR_NAME_2) && telephonyInfo.isSIM2Ready()) {
                try {
                    NETWORK_OPERATOR_NAME_2 = invokeMethod(telephonyClassName, slotNumber_2, m_NETWORK_OPERATOR_CODE, SIM_VARINT);
                } catch (NoSuchMethodException ex) {
                    ex.printStackTrace();
                }
            }
            if (TextUtils.isEmpty(NETWORK_OPERATOR_NAME_2) && telephonyInfo.isSIM2Ready()) {
                NETWORK_OPERATOR_NAME_2 = getMethodValue(telephonyClassName, m_NETWORK_OPERATOR_CODE, slotNumber_2);
            } else {
                NETWORK_OPERATOR_NAME_2 = "";
            }

            if (NETWORK_OPERATOR_NAME_1.equalsIgnoreCase("") && telephonyInfo.isSIM1Ready()) {
                try {
                    NETWORK_OPERATOR_NAME_1 = invokeMethod(telephonyClassName, slotNumber_1, m_NETWORK_OPERATOR, SIM_VARINT);
                } catch (NoSuchMethodException ex) {
                    ex.printStackTrace();
                }
            }
            if (NETWORK_OPERATOR_NAME_2.equalsIgnoreCase("") && telephonyInfo.isSIM2Ready()) {
                try {
                    NETWORK_OPERATOR_NAME_2 = invokeMethod(telephonyClassName, slotNumber_2, m_NETWORK_OPERATOR, SIM_VARINT);
                } catch (NoSuchMethodException ex) {
                    ex.printStackTrace();
                }
            }
            if (TextUtils.isEmpty(NETWORK_OPERATOR_CODE_1) && telephonyInfo.isSIM1Ready()) {
                NETWORK_OPERATOR_NAME_1 = getMethodValue(telephonyClassName, m_NETWORK_OPERATOR, slotNumber_1);
            }

            if (TextUtils.isEmpty(NETWORK_OPERATOR_CODE_1) && telephonyInfo.isSIM2Ready()) {
                NETWORK_OPERATOR_NAME_2 = getMethodValue(telephonyClassName, m_NETWORK_OPERATOR, slotNumber_2);
            }

            if (telephonyInfo.isSIM1Ready())
                NETWORK_OPERATOR_CODE_1 = getMethodValue(telephonyClassName, m_NETWORK_OPERATOR_CODE, 0);
            if (telephonyInfo.isSIM2Ready())
                NETWORK_OPERATOR_CODE_2 = getMethodValue(telephonyClassName, m_NETWORK_OPERATOR_CODE, 1);
            if (TextUtils.isEmpty(NETWORK_OPERATOR_CODE_1) && telephonyInfo.isSIM1Ready()) {
                try {
                    NETWORK_OPERATOR_CODE_1 = invokeMethod(telephonyClassName, slotNumber_1, m_NETWORK_OPERATOR_CODE, SIM_VARINT);
                } catch (NoSuchMethodException ex) {
                    ex.printStackTrace();
                }
                if (TextUtils.isEmpty(NETWORK_OPERATOR_CODE_1) && telephonyInfo.isSIM1Ready()) {
                    NETWORK_OPERATOR_CODE_1 = getMethodValue(telephonyClassName, m_NETWORK_OPERATOR_CODE, slotNumber_1);
                }

                if (TextUtils.isEmpty(NETWORK_OPERATOR_CODE_2) && telephonyInfo.isSIM2Ready()) {
                    try {

                        NETWORK_OPERATOR_CODE_2 = invokeMethod(telephonyClassName, slotNumber_2, m_NETWORK_OPERATOR_CODE, SIM_VARINT);
                    } catch (NoSuchMethodException ex) {
                        ex.printStackTrace();
                    }
                    if (TextUtils.isEmpty(NETWORK_OPERATOR_CODE_2) && telephonyInfo.isSIM2Ready()) {
                        NETWORK_OPERATOR_CODE_2 = getMethodValue(telephonyClassName, m_NETWORK_OPERATOR_CODE, slotNumber_2);
                    }

                }
            }
        }


        public boolean isTelephonyClassExists(String className) {

            boolean isClassExists = false;
            try {
                Class<?> telephonyClass = Class.forName(className);
                isClassExists = true;
            } catch (ClassNotFoundException ex) {
                ex.printStackTrace();
            }
            return isClassExists;
        }

        /**
         * Here we are identify sim slot number
         */
        public void getValidSlotFields(String className) {

            String value = null;
            try {
                Class<?> telephonyClass = Class.forName(className);
                Class<?>[] parameter = new Class[1];
                parameter[0] = int.class;
                StringBuffer sbf = new StringBuffer();
                Field[] fieldList = telephonyClass.getDeclaredFields();
                for (int index = 0; index < fieldList.length; index++) {
                    sbf.append("\n\n" + fieldList[index].getName());
                    Class<?> type = fieldList[index].getType();
                    Class<?> type1 = int.class;
                    if (type.equals(type1)) {
                        String variableName = fieldList[index].getName();
                        if (variableName.contains("SLOT")
                                || variableName.contains("slot")) {
                            if (variableName.contains("1")) {
                                slotName_1 = variableName;
                            } else if (variableName.contains("2")) {
                                slotName_2 = variableName;
                            } else if (variableName.contains("" + slotNumber_1)) {
                                slotName_1 = variableName;
                            } else if (variableName.contains("" + slotNumber_2)) {
                                slotName_2 = variableName;
                            }
                        }
                    }
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        /**
         * Some device assign different slot number so here code execute
         * to get slot number
         */
        public void getSlotNumber(String className) {
            try {
                Class<?> c = Class.forName(className);
                Field fields1 = c.getField(slotName_1);
                fields1.setAccessible(true);
                slotNumber_1 = (Integer) fields1.get(null);
                Field fields2 = c.getField(slotName_2);
                fields2.setAccessible(true);
                slotNumber_2 = (Integer) fields2.get(null);
            } catch (NoSuchFieldException e) {
                slotNumber_1 = 0;
                slotNumber_2 = 1;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }


        private String invokeMethod(String className, int slotNumber,
                                    String methodName, String SIM_variant) throws NoSuchMethodException {
            String value = "";

            try {
                Class<?> telephonyClass = Class.forName(className);
                Constructor[] cons = telephonyClass.getDeclaredConstructors();
                if (cons.length > 0) {
                    cons[0].getName();
                    cons[0].setAccessible(true);
                    Object obj = cons[0].newInstance();
                    Class<?>[] parameter = new Class[1];
                    parameter[0] = int.class;
                    Object ob_phone = null;
                    try {
                        Method getSimID = telephonyClass.getMethod(methodName
                                + SIM_variant, parameter);
                        Object[] obParameter = new Object[1];
                        obParameter[0] = slotNumber;
                        ob_phone = getSimID.invoke(obj, obParameter);
                    } catch (NoSuchMethodException e) {
                        if (slotNumber == 0) {
                            Method getSimID = telephonyClass.getMethod(methodName
                                    + SIM_variant, parameter);
                            Object[] obParameter = new Object[1];
                            obParameter[0] = slotNumber;
                            ob_phone = getSimID.invoke(obj);
                        }
                    }

                    if (ob_phone != null) {
                        value = ob_phone.toString();
                    }
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                invokeOldMethod(className, slotNumber, methodName, SIM_variant);
            }

            return value;
        }

        public String invokeOldMethod(String className, int slotNumber,
                                      String methodName, String SIM_variant) {
            String val = "";
            try {
                Class<?> telephonyClass = Class
                        .forName("android.telephony.TelephonyManager");
                Constructor[] cons = telephonyClass.getDeclaredConstructors();
                if (cons.length > 0) {
                    cons[0].getName();
                    cons[0].setAccessible(true);
                    Object obj = cons[0].newInstance();
                    Class<?>[] parameter = new Class[1];
                    parameter[0] = int.class;
                    Object ob_phone = null;
                    try {
                        Method getSimID = telephonyClass.getMethod(methodName
                                + SIM_variant, parameter);
                        Object[] obParameter = new Object[1];
                        obParameter[0] = slotNumber;
                        ob_phone = getSimID.invoke(obj, obParameter);
                    } catch (NoSuchMethodException e) {
                        if (slotNumber == 0) {
                            Method getSimID = telephonyClass.getMethod(methodName
                                    + SIM_variant, parameter);
                            Object[] obParameter = new Object[1];
                            obParameter[0] = slotNumber;
                            ob_phone = getSimID.invoke(obj);
                        }
                    }

                    if (ob_phone != null) {
                        val = ob_phone.toString();
                    }
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            return val;
        }
    }
}



