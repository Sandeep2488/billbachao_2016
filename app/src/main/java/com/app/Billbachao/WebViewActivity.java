package com.app.Billbachao;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.app.Billbachao.utils.ILog;

/**
 * Generic WebView Activity
 * Created by nitesh 10/05/2016
 */
public class WebViewActivity extends BaseActivity {

    private static final String TAG = WebViewActivity.class.getName();
    private WebView contentWebView;
    boolean loadingFinished = true;
    boolean redirect = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        initData();

    }

    private void initData() {
        Intent intent = getIntent();

        if (intent != null && intent.getExtras() != null) {
            Bundle bundle = intent.getExtras();
            setTitle(bundle.getString(CONSTANTS.WEB_VIEW_TITLE));
            initWebView(bundle.getString(CONSTANTS.WEB_VIEW_URL));
        }
    }

    private void initWebView(String url) {
        contentWebView = (WebView) findViewById(R.id.generic_webView);
        contentWebView.getSettings().setJavaScriptEnabled(true);
        contentWebView.getSettings().setLoadWithOverviewMode(true);
        contentWebView.getSettings().setUseWideViewPort(true);
        contentWebView.setScrollBarStyle(contentWebView.SCROLLBARS_OUTSIDE_OVERLAY);
        contentWebView.getSettings().setBuiltInZoomControls(true);
        contentWebView.getSettings().setDomStorageEnabled(true);
        contentWebView.setWebChromeClient(new WebChromeClient());
        contentWebView.setWebViewClient(new GenericWebViewClient());
        contentWebView.loadUrl(url);
    }

    public class GenericWebViewClient extends WebViewClient {
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            hideProgress();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            ILog.d(TAG, "url |finished|" + url);

            if (!redirect)
                loadingFinished = true;

            if (loadingFinished && !redirect){
                hideProgress();
            }else {
                redirect = false;
            }
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            if (!loadingFinished)
                redirect = true;

            loadingFinished = false;
            view.loadUrl(url);

            ILog.d(TAG, "|shouldOverrideUrlLoading|" + url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            loadingFinished = false;
            showProgress(getString(R.string.loading_please_wait));
        }
    }

    private void showProgress(String message) {
        showProgressDialog(message);
    }

    private void hideProgress() {
        dismissProgressDialog();
    }


    public interface CONSTANTS{
        String WEB_VIEW_TITLE = "web_view_title";
        String WEB_VIEW_URL = "web_view_url";
    }
}


