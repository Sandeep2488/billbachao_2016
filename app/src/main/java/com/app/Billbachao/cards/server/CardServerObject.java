package com.app.Billbachao.cards.server;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.myjson.annotations.SerializedName;

/**
 * Created by mihir on 06-05-2016.
 */
public class CardServerObject implements Parcelable {

    @SerializedName("cardId")
    int cardId;

    @SerializedName("templateId")
    int templateId;

    @SerializedName("templateCode")
    String templateCode;

    @SerializedName("cardCode")
    String cardCode;

    @SerializedName("isDarkTheme")
    boolean isDarkTheme;

    @SerializedName("color")
    String color;

    @SerializedName("message")
    String message;

    @SerializedName("title")
    String title;

    @SerializedName("actionOneText")
    String actionOneText;

    @SerializedName("actionOneLink")
    String actionOneLink;

    protected CardServerObject(Parcel in) {
        cardId = in.readInt();
        templateId = in.readInt();
        templateCode = in.readString();
        cardCode = in.readString();
        isDarkTheme = in.readByte() != 0;
        color = in.readString();
        message = in.readString();
        title = in.readString();
        actionOneText = in.readString();
        actionOneLink = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(cardId);
        dest.writeInt(templateId);
        dest.writeString(templateCode);
        dest.writeString(cardCode);
        dest.writeByte((byte) (isDarkTheme ? 1 : 0));
        dest.writeString(color);
        dest.writeString(message);
        dest.writeString(title);
        dest.writeString(actionOneText);
        dest.writeString(actionOneLink);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CardServerObject> CREATOR = new Creator<CardServerObject>() {
        @Override
        public CardServerObject createFromParcel(Parcel in) {
            return new CardServerObject(in);
        }

        @Override
        public CardServerObject[] newArray(int size) {
            return new CardServerObject[size];
        }
    };
}
