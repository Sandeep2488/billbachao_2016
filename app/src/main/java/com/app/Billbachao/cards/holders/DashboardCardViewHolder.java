package com.app.Billbachao.cards.holders;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.app.Billbachao.cards.model.CardDataObject;

/**
 * CardHolder class for defining some common properties for dashboard card holders
 * Created by mihir on 06-01-2016.
 */
public abstract class DashboardCardViewHolder extends RecyclerView.ViewHolder {

    public DashboardCardViewHolder(View itemView) {
        super(itemView);
    }

    public void bindCommonView(CardDataObject cardDataObject) {
        setTheme(cardDataObject);
        int color = cardDataObject.getColor();
        if (color != -1) {
            if (itemView instanceof CardView) {
                ((CardView) itemView).setCardBackgroundColor(color);
            } else {
                itemView.setBackgroundColor(color);
            }
        }
    }

    public abstract void bindView(CardDataObject cardDataObject);

    public void setTheme(CardDataObject cardDataObject) {

    }
}
