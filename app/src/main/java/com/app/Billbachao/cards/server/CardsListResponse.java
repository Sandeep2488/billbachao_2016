package com.app.Billbachao.cards.server;

import com.app.Billbachao.model.BaseGsonResponse;
import com.google.myjson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by mihir on 06-05-2016.
 */
public class CardsListResponse extends BaseGsonResponse {

    @SerializedName("profileId")
    int profileId;

    @SerializedName("profileCode")
    String profileCode;

    @SerializedName("profile_code_mapping")
    ArrayList<CardServerObject> cardList;

    public ArrayList<CardServerObject> getCardList() {
        return cardList;
    }
}
