package com.app.Billbachao.cards.binders;

import android.content.Context;
import android.view.ViewGroup;

import com.app.Billbachao.cards.holders.DashboardCardViewHolder;
import com.app.Billbachao.cards.model.CardDataObject;


/**
 * DataBinder class encapsulating viewHolders for different cards
 * Created by mihir on 06-01-2016.DashboardCardViewHolder
 */
public abstract class CardDataBinder<T extends DashboardCardViewHolder> {

    private Context mContext;

    public CardDataBinder(Context context) {
        mContext = context;
    }

    /**
     * Called in sync with newView of adapter
     * @param parent
     * @return sub class of {@link DashboardCardViewHolder}
     */
    public abstract T newViewHolder(ViewGroup parent);

    /**
     * Called in sync with bindView of adapter
     * @param holder Subclass of {@link DashboardCardViewHolder}
     * @param position
     * @param cardDataObject
     */
    public void bindCommonViewHolder(T holder, int position, CardDataObject cardDataObject) {
        holder.bindCommonView(cardDataObject);
        bindViewHolder(holder, position, cardDataObject);
    }

    /**
     * Can be overridden by base classes
     * @param holder
     * @param position
     * @param cardDataObject
     */
    protected void bindViewHolder(T holder, int position, CardDataObject cardDataObject) {
        holder.bindView(cardDataObject);
    }

    protected Context getContext() {
        return mContext;
    }
}