package com.app.Billbachao.cards.binders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.R;
import com.app.Billbachao.apis.NetworkCardApi;
import com.app.Billbachao.cards.holders.NetworkCardViewHolder;
import com.app.Billbachao.recommendations.network.model.NetworkRecoCardGsonResponse;
import com.app.Billbachao.volley.request.NetworkGsonRequest;
import com.app.Billbachao.volley.utils.NetworkErrorHelper;
import com.app.Billbachao.volley.utils.VolleySingleTon;

/**
 * Created by mihir on 26-04-2016.
 */
public class NetworkCardBinder extends CardDataBinder<NetworkCardViewHolder> {

    NetworkCardViewHolder mViewHolder;

    public NetworkCardBinder(Context context) {
        super(context);
    }

    @Override
    public NetworkCardViewHolder newViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.network_reco_card,
                parent, false);
        fetchData();
        mViewHolder = new NetworkCardViewHolder(view);
        return mViewHolder;
    }

    void fetchData() {
        NetworkGsonRequest<NetworkRecoCardGsonResponse> networkRecoRequest = new
                NetworkGsonRequest<NetworkRecoCardGsonResponse>(Request.Method.POST,
                NetworkCardApi.URL, NetworkRecoCardGsonResponse.class, NetworkCardApi.getParams
                (getContext(), new int[]{NetworkCardApi.MODE_HOME, NetworkCardApi.MODE_WORK}),
                new Response.Listener<NetworkRecoCardGsonResponse>() {
                    @Override
                    public void onResponse(NetworkRecoCardGsonResponse response) {
                        mViewHolder.updateData(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mViewHolder.setNoDataView(NetworkErrorHelper.getErrorStatus(error)
                        .getErrorMessage());
            }
        }, true);
        VolleySingleTon.getInstance(getContext()).addToRequestQueue(networkRecoRequest);
    }
}
