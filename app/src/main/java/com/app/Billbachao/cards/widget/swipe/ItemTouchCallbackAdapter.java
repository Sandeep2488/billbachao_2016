package com.app.Billbachao.cards.widget.swipe;

/**
 * Created by mihir on 31-03-2016.
 */
public interface ItemTouchCallbackAdapter {

    void onItemSwiped(int position);

    void onItemMoved(int fromPosition, int toPosition);

}
