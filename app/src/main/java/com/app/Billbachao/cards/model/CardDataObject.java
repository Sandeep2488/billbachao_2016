package com.app.Billbachao.cards.model;

import com.app.Billbachao.cards.CardUtils;

import java.util.ArrayList;

/**
 * Created by mihir on 18-03-2016.
 */
public class CardDataObject {

    public static final int INVALID_COLOR = -1;

    // Card unique ID, for identifying functionality
    int mId;

    // Card design ID, for associating view template
    int designId;

    // Flag to identify feature/fixed cards.
    boolean mUniqueTemplate;

    // Card title
    String title;

    // Card message
    String message;

    // Action button texts
    String callToAction, callToActionAdditional;

    // Links associated with action buttons
    String actionLink, actionLinkAdditional;

    // Image url for loading from server
    String imageUrl;

    // Local icon resource id for setting image
    int iconResId;

    int color = INVALID_COLOR, iconBgColor = INVALID_COLOR;

    boolean disable;

    // Assign this for cards having non light background. This is for changing text color etc
    // dynamically. White on dark background and vice versa.
    boolean darkTheme;

    // Important : Only assign for horizontal scrollable lists. Array will further contain cards.
    ArrayList<CardDataObject> mHorizontalCards;

    // Only assign for horizontal scrollable lists. Height is different for different type of cards.
    int pagerHeight;

    CardDataObject(int mId) {
        designId = CardUtils.TEMPLATE_NONE;
        this.mId = mId;
    }

    public int getId() {
        return mId;
    }

    public int getDesignId() {
        return designId;
    }

    public boolean isUniqueTemplate() {
        return mUniqueTemplate;
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }

    public String getCallToAction() {
        return callToAction;
    }

    public String getCallToActionAdditional() {
        return callToActionAdditional;
    }

    public String getActionLink() {
        return actionLink;
    }

    public String getActionLinkAdditional() {
        return actionLinkAdditional;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public int getColor() {
        return color;
    }

    public int getIconBgColor() {
        return iconBgColor;
    }

    public int getIconResId() {
        return iconResId;
    }

    public boolean isDisable() {
        return disable;
    }

    public boolean isDarkTheme() {
        return darkTheme;
    }

    public ArrayList<CardDataObject> getHorizontalCards() {
        return mHorizontalCards;
    }

    public int getPagerHeight() {
        return pagerHeight;
    }
}
