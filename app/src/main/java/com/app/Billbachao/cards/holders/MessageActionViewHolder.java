package com.app.Billbachao.cards.holders;

import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.cards.model.CardDataObject;

/**
 * Created by mihir on 19-03-2016.
 */
public class MessageActionViewHolder extends ActionCardViewHolder {

    TextView message;

    int messageDarkColorId;

    public MessageActionViewHolder(View itemView) {
        super(itemView);
        message = (TextView) itemView.findViewById(R.id.message);
        messageDarkColorId = R.color.textPrimaryColorInverse;
    }

    @Override
    public void bindView(CardDataObject cardDataObject) {
        super.bindView(cardDataObject);
        if (cardDataObject.getMessage() != null) {
            message.setVisibility(View.VISIBLE);
            message.setText(getMessage(cardDataObject));
        } else {
            message.setVisibility(View.GONE);
        }
    }

    @Override
    public void setTheme(CardDataObject cardDataObject) {
        super.setTheme(cardDataObject);
        if (message != null) {
            if (cardDataObject.isDarkTheme())
                message.setTextColor(ContextCompat.getColor(message.getContext(), messageDarkColorId));
        }
    }

    String getMessage(CardDataObject cardDataObject) {
        return cardDataObject.getMessage();
    }
}
