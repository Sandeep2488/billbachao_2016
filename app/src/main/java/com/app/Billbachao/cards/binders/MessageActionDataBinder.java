package com.app.Billbachao.cards.binders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.Billbachao.R;
import com.app.Billbachao.cards.holders.MessageActionViewHolder;

/**
 * Created by mihir on 18-03-2016.
 */
public class MessageActionDataBinder extends CardDataBinder<MessageActionViewHolder> {

    public MessageActionDataBinder(Context context) {
        super(context);
    }

    @Override
    public MessageActionViewHolder newViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_message_action, parent, false);
        return new MessageActionViewHolder(view);
    }

}
