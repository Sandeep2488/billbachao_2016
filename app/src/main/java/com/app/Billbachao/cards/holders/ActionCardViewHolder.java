package com.app.Billbachao.cards.holders;

import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;

import com.app.Billbachao.R;
import com.app.Billbachao.cards.model.CardDataObject;
import com.app.Billbachao.cards.widget.ActionClickListener;

/**
 * Created by mihir on 19-03-2016.
 */
public abstract class ActionCardViewHolder extends DashboardCardViewHolder {

    Button callToAction, callToActionSecond;

    public ActionCardViewHolder(View itemView) {
        super(itemView);
        callToAction = (Button) itemView.findViewById(R.id.call_to_action);
        callToActionSecond = (Button) itemView.findViewById(R.id.call_to_action_second);
    }

    @Override
    public void bindView(CardDataObject cardDataObject) {
        bindActionView(cardDataObject);
    }

    protected void bindActionView(CardDataObject cardDataObject) {
        if (callToAction != null) {
            if (cardDataObject.getCallToAction() != null) {
                callToAction.setVisibility(View.VISIBLE);
                callToAction.setText(cardDataObject.getCallToAction());
                if (cardDataObject.getActionLink() != null)
                    callToAction.setOnClickListener(new ActionClickListener(cardDataObject
                            .getActionLink()));
            } else {
                callToAction.setVisibility(View.GONE);
            }
        }

        if (callToActionSecond != null) {
            if (cardDataObject.getCallToActionAdditional() != null) {
                callToActionSecond.setVisibility(View.VISIBLE);
                callToActionSecond.setText(cardDataObject.getCallToActionAdditional());
                if (cardDataObject.getActionLinkAdditional() != null)
                    callToActionSecond.setOnClickListener(new ActionClickListener(cardDataObject
                            .getActionLinkAdditional()));
            } else {
                callToActionSecond.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void setTheme(CardDataObject cardDataObject) {
        super.setTheme(cardDataObject);
        int colorId = cardDataObject.isDarkTheme() ? R.color.textPrimaryColorInverse : R.color.colorAccent;
        if (callToAction != null) {
            callToAction.setTextColor(ContextCompat.getColor(callToAction.getContext(), colorId));
        }

        if (callToActionSecond != null) {
            callToActionSecond.setTextColor(ContextCompat.getColor(callToActionSecond.getContext(), colorId));
        }
    }

}
