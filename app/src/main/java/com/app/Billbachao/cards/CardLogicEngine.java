package com.app.Billbachao.cards;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.app.Billbachao.cards.model.CardDataObject;
import com.app.Billbachao.utils.AppUtils;
import com.app.Billbachao.utils.DateUtils;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.utils.PreferenceUtils;

import java.util.ArrayList;

/**
 * Created by mihir on 11-01-2016.
 */
public class CardLogicEngine {

    public static int[] CARD_DEFAULT_SET = new int[]{CardUtils.TYPE_OFFERS,
            CardUtils.TYPE_USAGE, CardUtils.TYPE_BEST_PLAN, CardUtils
            .TYPE_BEST_NETWORK, CardUtils.TYPE_HAND_SET};

    // TODO incomplete replace by default set
    public static int[] CARD_SETUP_SET = new int[]{CardUtils.TYPE_USAGE, CardUtils
            .TYPE_DATA_CARD, CardUtils.TYPE_BEST_PLAN, CardUtils.TYPE_BEST_NETWORK,
            CardUtils.TYPE_OFFERS, CardUtils.TYPE_USSD, CardUtils.TYPE_SELF_HELP_LIST, CardUtils
            .TYPE_CASHBACK, CardUtils.TYPE_INVITE, CardUtils.TYPE_RATE};

    // TODO incomplete
    private static boolean isCardToBeShown(Context context, int cardType) {
        switch (cardType) {

            case CardUtils.TYPE_USSD:
                return PreferenceUtils.isPrepaid(context);
            case CardUtils.TYPE_DATA_CARD:
                return !isDataUsageToBeShown(context);
        }
        return true;
    }

    public static ArrayList<CardDataObject> generateCardSet(Context context) {
        ArrayList<CardDataObject> cardList = new ArrayList<>();

        for (int cardType : CARD_SETUP_SET) {
            if (isCardToBeShown(context, cardType)) {
                CardDataObject cardDataObject = CardFactory.generateCard(context, cardType);
                if (cardDataObject != null)
                    cardList.add(cardDataObject);
            }
        }
        return cardList;
    }

    public static ArrayList<CardDataObject> filterCardSet(Context context,
                                                          ArrayList<CardDataObject> cardDataObjects) {
        ILog.d("Mihir", "CardList filter" + cardDataObjects);
        if (cardDataObjects == null || cardDataObjects.isEmpty()) return cardDataObjects;
        ArrayList<CardDataObject> filteredCardDataObjects = new ArrayList<>();
        for (CardDataObject dataObject : cardDataObjects) {
            ILog.d("Mihir", "CardList filter id " + dataObject.getId());
            if (isCardToBeShown(context, dataObject.getId())) {
                filteredCardDataObjects.add(dataObject);
            }
        }
        return filteredCardDataObjects;
    }

    public static boolean isDataUsageToBeShown(Context context) {
        return AppUtils.getDaysSinceInstalled(context) >= 3;
    }

    static final String CARD_DAY = "cardDays", LAST_VISITED_DAY = "lastDay";


    /**
     * Checks if card data is already stored.
     *
     * @param context
     * @return
     */
    public static boolean isCardDataStored(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return DateUtils.isToday(preferences.getLong(LAST_VISITED_DAY, 0));
    }

    /**
     * Get and update card day if needed
     *
     * @param context
     * @return
     */
    public static int getAndUpdateCardDay(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        int cardDay = preferences.getInt(CARD_DAY, 0);
        if (DateUtils.isToday(preferences.getLong(LAST_VISITED_DAY, 0))) {
            return cardDay;
        }
        preferences.edit().putLong(LAST_VISITED_DAY, DateUtils.getBeforeDays(0)).putInt(CARD_DAY,
                ++cardDay).apply();
        return cardDay;
    }
}
