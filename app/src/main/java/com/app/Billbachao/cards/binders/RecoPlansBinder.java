package com.app.Billbachao.cards.binders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.R;
import com.app.Billbachao.apis.PlanRecoCardApi;
import com.app.Billbachao.cards.holders.PlanRecoViewHolder;
import com.app.Billbachao.model.RecoPlanCardGsonInfo;
import com.app.Billbachao.volley.request.NetworkGsonRequest;
import com.app.Billbachao.volley.utils.NetworkErrorHelper;
import com.app.Billbachao.volley.utils.VolleySingleTon;

/**
 * Created by mihir on 20-04-2016.
 */
public class RecoPlansBinder extends CardDataBinder<PlanRecoViewHolder> {

    public RecoPlansBinder(Context context) {
        super(context);
    }

    PlanRecoViewHolder mViewHolder;

    @Override
    public PlanRecoViewHolder newViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.plan_reco_card, parent, false);
        fetchData();
        mViewHolder = new PlanRecoViewHolder(view);
        return mViewHolder;
    }

    void fetchData() {
        NetworkGsonRequest<RecoPlanCardGsonInfo> planRecoRequest = new NetworkGsonRequest<>
                (Request.Method.POST, PlanRecoCardApi.URL, RecoPlanCardGsonInfo.class,
                        PlanRecoCardApi.getParams(getContext(), PlanRecoCardApi.MYBESTPLAN), new
                        Response.Listener<RecoPlanCardGsonInfo>() {
            @Override
            public void onResponse(RecoPlanCardGsonInfo response) {
                mViewHolder.updateData(response.getOperatorWisePlanInfos());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mViewHolder.updateError(NetworkErrorHelper.getErrorStatus(error).getErrorMessage());
            }
        }, true);
        planRecoRequest.setRetryPolicy(new DefaultRetryPolicy(25000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleTon.getInstance(getContext()).addToRequestQueue(planRecoRequest);
    }

}
