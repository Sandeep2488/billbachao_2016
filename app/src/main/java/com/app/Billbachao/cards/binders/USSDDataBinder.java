package com.app.Billbachao.cards.binders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.cards.holders.DashboardCardViewHolder;
import com.app.Billbachao.cards.model.CardDataObject;
import com.app.Billbachao.usagelog.AccessibilityHelper;
import com.app.Billbachao.usagelog.contents.utils.LogUtils;
import com.app.Billbachao.usagelog.widget.UsageHeaderItem;
import com.app.Billbachao.utils.UiUtils;

/**
 * Created by mihir on 06-01-2016.
 */
public class USSDDataBinder extends CardDataBinder<USSDDataBinder.ViewHolder> {

    public USSDDataBinder(Context context) {
        super(context);
    }

    @Override
    public ViewHolder newViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ussd_balance_card, parent, false);
        return new ViewHolder(view);
    }

    class ViewHolder extends DashboardCardViewHolder {

        View mOnStateView, mOffStateView;

        UsageHeaderItem mCallHeader, mDataHeader;

        AccessibilityHelper mAccessibilityHelper;

        public ViewHolder(View itemView) {
            super(itemView);
            mAccessibilityHelper = new AccessibilityHelper(getContext());
            initCommonView(itemView);
            initUssdOnView(itemView);
            initUssdOffView(itemView);
        }

        @Override
        public void bindView(CardDataObject cardDataObject) {
            if (mAccessibilityHelper.isEnabledAndRunning()) {
                mOnStateView.setVisibility(View.VISIBLE);
                mOffStateView.setVisibility(View.GONE);
                bindUSSDOnView();
            } else {
                mOnStateView.setVisibility(View.GONE);
                mOffStateView.setVisibility(View.VISIBLE);
                bindUSSDOffView();
            }
        }

        private void initCommonView(View itemView) {
            mOnStateView = itemView.findViewById(R.id.ussd_on_card);
            mOffStateView = itemView.findViewById(R.id.ussd_off_card);
        }

        private void initUssdOnView(View itemView) {
            mCallHeader = new UsageHeaderItem(itemView.findViewById(R.id.call_usage), LogUtils.TYPE_CALL);
            mDataHeader = new UsageHeaderItem(itemView.findViewById(R.id.data_usage), LogUtils.TYPE_DATA);
            Button actionButton = (Button) itemView.findViewById(R.id.call_to_action);
            actionButton.setOnClickListener(mClickListener);
            actionButton.setText(R.string.view_details);
            itemView.setOnClickListener(mClickListener);
        }

        private void initUssdOffView(View itemView) {
            ((TextView) itemView.findViewById(R.id.message_placeholder)).setText(R.string
                    .turn_on_ussd_card_message);
            Button callToAction = (Button) itemView.findViewById(R.id.call_to_action_placeholder);
            callToAction.setText(R.string.lets_do_it);
            callToAction.setOnClickListener(mClickListener);
        }

        private void bindUSSDOnView() {
            mCallHeader.update();
            mDataHeader.update();
        }

        private void bindUSSDOffView() {

        }

        View.OnClickListener mClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UiUtils.launchUsageLogOrSettings(getContext(), true);
            }
        };
    }

}
