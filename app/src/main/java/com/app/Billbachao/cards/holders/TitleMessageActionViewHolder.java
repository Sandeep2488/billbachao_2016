package com.app.Billbachao.cards.holders;

import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.cards.model.CardDataObject;

/**
 * Created by mihir on 19-03-2016.
 */
public class TitleMessageActionViewHolder extends MessageActionViewHolder {

    TextView title;

    public TitleMessageActionViewHolder(View itemView) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.title);
        messageDarkColorId = R.color.textSecondaryColorInverse;
    }

    @Override
    public void bindView(CardDataObject cardDataObject) {
        super.bindView(cardDataObject);
        if (cardDataObject.getTitle() != null) {
            title.setVisibility(View.VISIBLE);
            title.setText(cardDataObject.getTitle());
        } else {
            title.setVisibility(View.GONE);
        }
    }

    @Override
    public void setTheme(CardDataObject cardDataObject) {
        super.setTheme(cardDataObject);
        if (title != null) {
            if (cardDataObject.isDarkTheme())
                title.setTextColor(ContextCompat.getColor(title.getContext(), R.color.textPrimaryColorInverse));
        }
    }
}
