package com.app.Billbachao.cards.holders;

import android.view.View;
import android.widget.ImageView;

import com.app.Billbachao.R;
import com.app.Billbachao.cards.model.CardDataObject;
import com.squareup.picasso.Picasso;

/**
 * Created by mihir on 19-03-2016.
 */
public class IconMessageActionViewHolder extends MessageActionViewHolder {

    ImageView icon;

    View iconParent;

    public IconMessageActionViewHolder(View itemView) {
        super(itemView);
        icon = (ImageView) itemView.findViewById(R.id.image);
        iconParent = itemView.findViewById(R.id.image_parent);
    }

    @Override
    public void bindView(CardDataObject cardDataObject) {
        super.bindView(cardDataObject);
        bindIconView(cardDataObject);
    }

    protected void bindIconView(CardDataObject cardDataObject) {
        if (cardDataObject.getIconResId() != 0) {
            icon.setVisibility(View.VISIBLE);
            icon.setImageResource(cardDataObject.getIconResId());
            int iconBgColor = cardDataObject.getIconBgColor();
            if (iconBgColor != CardDataObject.INVALID_COLOR && iconParent != null) {
                iconParent.setBackgroundColor(iconBgColor);
            }
        } else if (cardDataObject.getImageUrl() != null) {
            icon.setVisibility(View.VISIBLE);
            Picasso.with(icon.getContext()).load(cardDataObject.getImageUrl()).into(icon);
        } else {
            icon.setVisibility(View.GONE);
        }
    }

}
