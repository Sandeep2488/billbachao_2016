package com.app.Billbachao.cards;

import android.content.Context;

import com.app.Billbachao.R;
import com.app.Billbachao.cards.binders.CardDataBinder;
import com.app.Billbachao.cards.binders.CashbackDataBinder;
import com.app.Billbachao.cards.binders.HorizontalScrollCardDataBinder;
import com.app.Billbachao.cards.binders.IconMessageActionDataBinder;
import com.app.Billbachao.cards.binders.ImageActionDataBinder;
import com.app.Billbachao.cards.binders.MessageActionDataBinder;
import com.app.Billbachao.cards.binders.NetworkCardBinder;
import com.app.Billbachao.cards.binders.RateAppDataBinder;
import com.app.Billbachao.cards.binders.RecoPlansBinder;
import com.app.Billbachao.cards.binders.TitleMessageActionDataBinder;
import com.app.Billbachao.cards.binders.USSDDataBinder;
import com.app.Billbachao.cards.binders.UsageDataBinder;
import com.app.Billbachao.cards.model.CardDataObject;
import com.app.Billbachao.cards.model.CardDataObjectBuilder;
import com.app.Billbachao.launch.Links;

import java.util.ArrayList;

/**
 * Created by mihir on 06-01-2016.
 */
public class CardFactory {

    public static CardDataBinder getDataBinder(Context context, int designType, int type) {
        switch (designType) {
            case CardUtils.TEMPLATE_TITLE_MSG_ACTION:
                return new TitleMessageActionDataBinder(context);
            case CardUtils.TEMPLATE_MSG_ACTION:
                return new MessageActionDataBinder(context);
            case CardUtils.TEMPLATE_IMAGE_ACTION:
                return new ImageActionDataBinder(context);
            case CardUtils.TEMPLATE_ICON_MSG_ACTION:
                return new IconMessageActionDataBinder(context);
            case CardUtils.TEMPLATE_HORIZONTAL_LIST:
                return new HorizontalScrollCardDataBinder(context);
            default:
                return getDataBinder(context, type);
        }
    }

    /**
     * Implement this including all pre-designed cards as a backup card
     *
     * @param context
     * @param type
     * @return
     */
    public static CardDataBinder getDataBinder(Context context, int type) {
        switch (type) {
            case CardUtils.TYPE_USAGE:
                return new UsageDataBinder(context);
            case CardUtils.TYPE_HAND_SET:
            case CardUtils.TYPE_APP_DAY:
                return new MessageActionDataBinder(context);
            case CardUtils.TYPE_BEST_NETWORK:
                return new NetworkCardBinder(context);
            case CardUtils.TYPE_CHECK_BALANCE:
            case CardUtils.TYPE_DND:
            case CardUtils.TYPE_INVITE:
            case CardUtils.TYPE_DATA_SETTINGS:
            case CardUtils.TYPE_PLAN_ALERTS:
                return new IconMessageActionDataBinder(context);
            case CardUtils.TYPE_RATE:
                return new RateAppDataBinder(context);
            case CardUtils.TYPE_USSD:
                return new USSDDataBinder(context);
            case CardUtils.TYPE_BEST_PLAN:
                return new RecoPlansBinder(context);
            case CardUtils.TYPE_CASHBACK:
                return new CashbackDataBinder(context);
        }
        return null;
    }

    public static CardDataObject generateCard(Context context, int id) {
        if (CardUtils.isSelfHelpCard(id)) return generateSelfHelpCard(context, id);

        switch (id) {
            case CardUtils.TYPE_USAGE:
                return new CardDataObjectBuilder(context, id).addAction(R.string.view_details,
                        Links.USAGE_BIFURCATION).buildFixedCard();
            case CardUtils.TYPE_RATE:
                return new CardDataObjectBuilder(context, id).addAction(R.string.rate_now)
                        .buildFixedCard();
            case CardUtils.TYPE_BEST_NETWORK:
                return new CardDataObjectBuilder(context, id).buildFixedCard();
            case CardUtils.TYPE_HAND_SET:
                return new CardDataObjectBuilder(context, id).addAction(R.string
                        .handset_card_action).setDarkTheme().setColorResource(R.color
                        .card_handset_bg).addMessage(R.string.handset_card_message)
                        .buildIconCardWithColorId(R.drawable.slow, R.color.card_handset_icon_bg);
            case CardUtils.TYPE_CHECK_BALANCE:
                return new CardDataObjectBuilder(context, id).addAction(R.string.check)
                        .setDarkTheme().setColorResource(R.color.card_2).buildMessageCard(R
                                .string.balance_card_message);
            case CardUtils.TYPE_APP_DAY:
                return new CardDataObjectBuilder(context, id).addAction(R.string.check_now)
                        .setDarkTheme().setColorResource(R.color.card_4).buildMessageCard(R
                                .string.app_day_message);
            case CardUtils.TYPE_INVITE:
                return new CardDataObjectBuilder(context, id).addAction(R.string
                        .invite_card_action, Links.SHARE_APP).addMessage(R.string
                        .invite_card_title).setDarkTheme().setColorResource(R.color
                        .card_app_invite_bg).buildIconCardWithColorId(R.drawable.invite, R.color
                        .card_app_invite_icon_bg);
            case CardUtils.TYPE_USSD:
                return new CardDataObjectBuilder(context, id).addAction(R.string.view_details,
                        Links.USAGE_LOG).buildFixedCard();
            case CardUtils.TYPE_BEST_PLAN:
                return new CardDataObjectBuilder(context, id).addAction(R.string.view_details,
                        Links.USAGE_BIFURCATION).buildFixedCard();
            case CardUtils.TYPE_CASHBACK:
                return new CardDataObjectBuilder(context, id).addAction(R.string.recharge_now,
                        Links.BROWSE_PLANS).buildFixedCard();
            case CardUtils.TYPE_OFFERS:
                ArrayList<CardDataObject> offerCards = new ArrayList<>();
                String url = "http://billbachao.pc.cdn.bitgravity" +
                        ".com/bbweb/bbdev/img/Cashback_creative4.jpg";
                offerCards.add(new CardDataObjectBuilder(context, id).addAction(R.string
                                .recharge_now,
                        Links.BROWSE_PLANS).buildImageCard(url));
                offerCards.add(new CardDataObjectBuilder(context, id).addAction(R.string
                                .recharge_now,
                        Links.BROWSE_PLANS).buildImageCard(url));
                int height = context.getResources().getDimensionPixelSize(R.dimen
                        .offer_card_height);
                return new CardDataObjectBuilder(context, id).buildHorizontalListCard(offerCards,
                        height);
            case CardUtils.TYPE_DATA_CARD:
                return new CardDataObjectBuilder(context, id).setDarkTheme().setColorResource(R
                        .color.card_1).buildMessageCard(R.string.data_usage_future_card);
        }
        return null;
    }

    public static CardDataObject generateSelfHelpCard(Context context, int id) {
        switch (id) {
            case CardUtils.TYPE_DND:
                return new CardDataObjectBuilder(context, id).addAction(R.string.dnd_card_action, Links.DND)
                        .addMessage(R.string.dnd_card_title).setDarkTheme().setColorResource(R
                                .color.card_app_invite_bg).buildIconCardWithColorId(R.drawable
                                .dnd, R.color.card_app_invite_icon_bg);
            case CardUtils.TYPE_PLAN_ALERTS:
                return new CardDataObjectBuilder(context, id).addAction(R.string.activate,
                        Links.PLAN_ALERTS).addMessage(R.string.plan_alerts_card).setDarkTheme().
                        setColorResource(R.color.card_app_invite_bg).buildIconCardWithColorId
                        (R.drawable.icn_card_1, R.color.card_app_invite_icon_bg);
            case CardUtils.TYPE_CHECK_BALANCE:
                return new CardDataObjectBuilder(context, id).addAction(R.string.check,
                        Links.CHECK_BALANCE).addMessage(R.string.check_balance_card).setDarkTheme().
                        setColorResource(R.color.card_app_invite_bg).buildIconCardWithColorId
                        (R.drawable.icn_card_2, R.color.card_app_invite_icon_bg);
            case CardUtils.TYPE_DATA_SETTINGS:
                return new CardDataObjectBuilder(context, id).addAction(R.string.get,
                        Links.DATA_SETTINGS).addMessage(R.string.data_settings_card).setDarkTheme().
                        setColorResource(R.color.card_app_invite_bg).buildIconCardWithColorId
                        (R.drawable.icn_card_3, R.color.card_app_invite_icon_bg);
            case CardUtils.TYPE_SELF_HELP_LIST:
                ArrayList<CardDataObject> horizontalCards = new ArrayList<>();
                horizontalCards.add(generateCard(context, CardUtils.TYPE_DND));
                horizontalCards.add(generateCard(context, CardUtils.TYPE_PLAN_ALERTS));
                horizontalCards.add(generateCard(context, CardUtils.TYPE_CHECK_BALANCE));
                horizontalCards.add(generateCard(context, CardUtils.TYPE_DATA_SETTINGS));
                return new CardDataObjectBuilder(context, id).buildHorizontalListCard
                        (horizontalCards);
        }
        return null;
    }
}
