package com.app.Billbachao.cards.binders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.Billbachao.R;
import com.app.Billbachao.cards.holders.TitleMessageActionViewHolder;

/**
 * Created by mihir on 18-03-2016.
 */
public class TitleMessageActionDataBinder extends CardDataBinder<TitleMessageActionViewHolder> {

    public TitleMessageActionDataBinder(Context context) {
        super(context);
    }

    @Override
    public TitleMessageActionViewHolder newViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_title_description_action, parent, false);
        return new TitleMessageActionViewHolder(view);
    }

}
