package com.app.Billbachao.deals.utils;

import com.app.Billbachao.deals.models.ProductPaymentModel;
import com.google.myjson.ExclusionStrategy;
import com.google.myjson.FieldAttributes;

/**
 * Created by nitesh on 29-03-2016.
 */
public class PaymentExcluisionStrategy implements ExclusionStrategy {

    public boolean shouldSkipClass(Class<?> arg0) {
        return false;
    }

    public boolean shouldSkipField(FieldAttributes f) {
        return (f.getDeclaringClass() == ProductPaymentModel.class && f.getName().equals(DealsConstant.KEY_IMAGE_URL));
    }

}
