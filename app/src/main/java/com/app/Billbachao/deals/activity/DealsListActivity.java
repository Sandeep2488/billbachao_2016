package com.app.Billbachao.deals.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.deals.adapter.DealsListAdapter;
import com.app.Billbachao.deals.utils.DealsConstant;
import com.app.Billbachao.deals.tasks.DealsListFetcherTask;
import com.app.Billbachao.deals.models.RelianceTvDetailsModel;
import com.app.Billbachao.apis.deals.DealsListApiUtils;
import com.app.Billbachao.utils.UiUtils;
import com.app.Billbachao.volley.utils.VolleySingleTon;
import com.app.Billbachao.widget.view.DividerItemDecoration;

import java.util.List;

/**
 * Deals Listing.
 * Created by nitesh on 24/02/2015.
 */
public class DealsListActivity extends BaseActivity implements DealsListFetcherTask.IDealsCallBack {

    private DealsListAdapter mAdapter;
    private RecyclerView mDealsRecyclerView;
    private TextView mTvHandsetNotFound;
    private Context mContext;
    private String TAG = DealsListActivity.class.getName();
    private String from;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deals_list);

        mContext = DealsListActivity.this;

        //init
        init();

        //initdata
        initData();

        //initProgressDialog
        initProgressDialog();

        //initApiCall
        initDealsApiCall();
    }

    private void initData() {
        if (getIntent() != null && getIntent().getExtras() != null){
            Bundle bundle = getIntent().getExtras();
            from = bundle.getString(DealsConstant.KEY_FROM);
        }
    }

    private void initDealsApiCall() {
        showProgressDialog(getString(R.string.loading_please_wait));

        DealsListFetcherTask fetcherTask = new DealsListFetcherTask(mContext, this);
        fetcherTask.fetchDeals(DealsListApiUtils.MODE_ALL, TAG);
    }

    private void init() {
        mTvHandsetNotFound = (TextView) findViewById(R.id.no_deals_found);
        mDealsRecyclerView = (RecyclerView) findViewById(R.id.deal_list_recyclerView);
        LinearLayoutManager llm = new LinearLayoutManager(mContext);
        mDealsRecyclerView.setLayoutManager(llm);
        mDealsRecyclerView.addItemDecoration(new DividerItemDecoration(getResources().getDimensionPixelSize(R.dimen.large_widget_spacing)));

    }

    @Override
    public void onDealsSuccess(final List<RelianceTvDetailsModel> result) {

        dismissProgressDialog();

        if (result != null && result.size() > 0){
            changeVisibility(true);

            mAdapter = new DealsListAdapter(mContext, result);

            mDealsRecyclerView.setAdapter(mAdapter);
            mAdapter.setDealsItemClickListener(new DealsListAdapter.IDealItemClick() {
                @Override
                public void itemClick(int position, View view, RelianceTvDetailsModel relianceTvDetailsModel) {

                    if (relianceTvDetailsModel != null) {
                        Bundle bundle = new Bundle();
                        bundle.putString(DealsConstant.KEY_FROM, from);
                        bundle.putParcelable(DealsConstant.KEY_RELIANCE_DETAILS, relianceTvDetailsModel);
                        UiUtils.launchDealsDetailsActivity(mContext, bundle);
                    }

                }
            });
        }else {
            onDealsError(getString(R.string.no_deals));
        }

    }


    @Override
    public void onDealsError(String error) {
        dismissProgressDialog();
        showSnack(error);
        changeVisibility(false);
    }

    public void changeVisibility(boolean isDealsAvailable) {

        if (isDealsAvailable) {
            mDealsRecyclerView.setVisibility(View.VISIBLE);
            mTvHandsetNotFound.setVisibility(View.GONE);
        } else {
            mDealsRecyclerView.setVisibility(View.GONE);
            mTvHandsetNotFound.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        getString(R.string.deals);
    }

    @Override
    protected boolean isCartItemSupported() {
        return false;
    }

    @Override
    protected void onDestroy() {

        VolleySingleTon.getInstance(mContext).cancelPendingRequests(TAG);
        super.onDestroy();
    }
}
