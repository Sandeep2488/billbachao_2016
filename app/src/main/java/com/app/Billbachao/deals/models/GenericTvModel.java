package com.app.Billbachao.deals.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sushil on 04/03/2016.
 */
public class GenericTvModel implements Parcelable {

    public String key;
    public String value;
    public String title;

    public GenericTvModel(String title, String key, String value) {
        this.key = key;
        this.value = value;
        this.title = title;
    }

    protected GenericTvModel(Parcel in) {
        key = in.readString();
        value = in.readString();
        title = in.readString();
    }

    public static final Creator<GenericTvModel> CREATOR = new Creator<GenericTvModel>() {
        @Override
        public GenericTvModel createFromParcel(Parcel in) {
            return new GenericTvModel(in);
        }

        @Override
        public GenericTvModel[] newArray(int size) {
            return new GenericTvModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(key);
        dest.writeString(value);
        dest.writeString(title);
    }
}
