package com.app.Billbachao.deals.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.app.Billbachao.R;
import com.app.Billbachao.apis.ApiUtils;
import com.app.Billbachao.common.ConnectionManager;
import com.app.Billbachao.deals.adapter.AddressListAdapter;
import com.app.Billbachao.deals.utils.DealsConstant;
import com.app.Billbachao.deals.i.IDealAddressCallback;
import com.app.Billbachao.deals.i.IDealsAddressItemClick;
import com.app.Billbachao.deals.models.DealsAddressModel;
import com.app.Billbachao.deals.models.ProductPaymentModel;
import com.app.Billbachao.deals.models.RelianceTvDetailsModel;
import com.app.Billbachao.deals.models.TvProductInformation;
import com.app.Billbachao.utils.UiUtils;

import java.util.ArrayList;

/**
 * Listing of address for deals
 * Created by nitesh on 24/02/2016.
 */
public class DealsAddressListingFragment extends Fragment implements View.OnClickListener, IDealAddressCallback {

    public static final String TAG = DealsAddressListingFragment.class.getName();
    private Context mContext;
    private View parentView;
    private RecyclerView dealsAddressListingRecylerView;
    private ArrayList<DealsAddressModel> dealsAddressModelArrayList = new ArrayList<>();
    private AddressListAdapter addressListAdapter;
    private TextView addAddressTv;
    private RelianceTvDetailsModel relianceTvProductDetails;
    private String from;
    private boolean isBooking;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        parentView = inflater.inflate(R.layout.fragment_deals_address_listing, null);
        return parentView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //init
        init();

        //initdata
        initData();
    }

    private void initData() {

        final Bundle bundle = getArguments();

        if (bundle != null) {

            from = bundle.getString(DealsConstant.KEY_FROM);
            isBooking = bundle.getBoolean(DealsConstant.KEY_IS_USE_BOOKING);

            relianceTvProductDetails = bundle.getParcelable(DealsConstant.KEY_RELIANCE_DETAILS);

            ArrayList<DealsAddressModel> parcelableArrayList = bundle.getParcelableArrayList(DealsConstant.KEY_ADDRESS_LIST);

            if (parcelableArrayList != null && parcelableArrayList.size() > 0)
                dealsAddressModelArrayList = parcelableArrayList;

            addressListAdapter = new AddressListAdapter(getActivity(), dealsAddressModelArrayList, from);
            dealsAddressListingRecylerView.setAdapter(addressListAdapter);

            addressListAdapter.setDealsItemSelectedClickListener(new IDealsAddressItemClick() {
                @Override
                public void itemClick(int adapterPosition, View view, DealsAddressModel dealsAddressModel) {

                    if (ConnectionManager.isConnected(mContext)) {

                        triggerAddressSelection();

                        Bundle dealsBundle = new Bundle();
                        dealsBundle.putParcelable(DealsConstant.KEY_ADDRESS_MODEL, dealsAddressModel);
                        dealsBundle.putString(DealsConstant.KEY_FROM, from);
                        dealsBundle.putBoolean(DealsConstant.KEY_IS_USE_BOOKING, isBooking);

                        prepareProductListing(dealsBundle);

                        UiUtils.launchDealsBookingActivity(getActivity(), dealsBundle);
                    } else {
                        showToast(getString(R.string.deals_check_network));
                    }

                }
            });

        }
    }

    private void triggerAddressSelection() {

        if (!TextUtils.isEmpty(from)) {

            if (from.equalsIgnoreCase(DealsConstant.KEY_FROM_TV_DEEP_LINKS)) {

                //TODO Add event
                //EventUtils.sendEvent(EventUtils.NOTIFICATION, Constants.NOTIFY_PH_SELECT_ADDR, Constants.PROPERTY_VALUE);
                //MoEngage
                //MoEngageAnalytics.trackEvent(mContext, EventUtils.NOTIFICATION, Constants.NOTIFY_PH_SELECT_ADDR, Constants.PROPERTY_VALUE);

            } else if (from.equalsIgnoreCase(DealsConstant.KEY_BELL_CLICKED)) {

                //TODO Add event

                //EventUtils.sendEvent(EventUtils.HANDSETS_FOR_ME, Constants.LYF_PH_SELECT_ADDR, Constants.PROPERTY_VALUE);
                //MoEngage Event
                //MoEngageAnalytics.trackEvent(mContext, EventUtils.HANDSETS_FOR_ME, Constants.LYF_PH_SELECT_ADDR, Constants.PROPERTY_VALUE);
            }
        }
    }

    private void prepareProductListing(Bundle bundle) {

        ArrayList<ProductPaymentModel> productPaymentModelList = new ArrayList<>();

        if (relianceTvProductDetails != null) {

            TvProductInformation tvProductInformation = relianceTvProductDetails.productInformation;

            if (tvProductInformation != null) {
                ProductPaymentModel productPaymentModel = new ProductPaymentModel(tvProductInformation.modelNo, tvProductInformation.productName, tvProductInformation.cost, "1", "0.0", tvProductInformation.offerCost, ApiUtils.NA, tvProductInformation.imageUrl);
                productPaymentModelList.add(productPaymentModel);
                bundle.putParcelableArrayList(DealsConstant.KEY_PRODUCT_PAYMENT, productPaymentModelList);
                bundle.putString(DealsConstant.KEY_BOOKING_WITH_PAYMENT_AMT, String.valueOf(tvProductInformation.bookWithPayment));
            }
        }
    }

    private void init() {
        dealsAddressListingRecylerView = (RecyclerView) parentView.findViewById(R.id.dealsAddressListingRecylerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        dealsAddressListingRecylerView.setLayoutManager(linearLayoutManager);
        addAddressTv = (TextView) parentView.findViewById(R.id.addAddressTv);
        addAddressTv.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.addAddressTv:
                addAddress(0);
                break;
        }
    }

    private void addAddress(int position) {

        if (!TextUtils.isEmpty(from)) {

            if (from.equalsIgnoreCase(DealsConstant.KEY_FROM_TV_DEEP_LINKS)) {
                //TODO Add event

                //EventUtils.sendEvent(EventUtils.NOTIFICATION, Constants.NOTIFY_PH_ADD_ADDR, Constants.PROPERTY_VALUE);
                //MoEngage
                //MoEngageAnalytics.trackEvent(mContext, EventUtils.NOTIFICATION, Constants.NOTIFY_PH_ADD_ADDR, Constants.PROPERTY_VALUE);

            } else if (from.equalsIgnoreCase(DealsConstant.KEY_BELL_CLICKED)) {
                //TODO Add event

                //EventUtils.sendEvent(EventUtils.HANDSETS_FOR_ME, Constants.LYF_PH_ADD_ADDR, Constants.PROPERTY_VALUE);
                //MoEngage Event
                //MoEngageAnalytics.trackEvent(mContext, EventUtils.HANDSETS_FOR_ME, Constants.LYF_PH_ADD_ADDR, Constants.PROPERTY_VALUE);
            }
        }
        UiUtils.launchDealsAddAddressActivity(getActivity(), ApiUtils.INSERT, position, null);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == DealsConstant.REQUEST_CODE_DEALS_ADDRESS) {

                if (data != null) {
                    Bundle bundle = data.getExtras();

                    if (bundle != null) {

                        if (dealsAddressModelArrayList == null) {
                            dealsAddressModelArrayList = new ArrayList<>();
                        }

                        if (addressListAdapter == null) {
                            addressListAdapter = new AddressListAdapter(getActivity(), dealsAddressModelArrayList, from);
                        }

                        String from = bundle.getString(DealsConstant.KEY_FROM);

                        if (from.equalsIgnoreCase(ApiUtils.UPDATE)) {

                            DealsAddressModel dealsAddressModel = bundle.getParcelable(DealsConstant.KEY_ADDRESS_MODEL);

                            if (dealsAddressModel != null) {
                                int position = bundle.getInt(DealsConstant.KEY_POSITION);

                                if (dealsAddressModelArrayList == null) {
                                    dealsAddressModelArrayList = new ArrayList<>();
                                }

                                dealsAddressModelArrayList.set(position, dealsAddressModel);
                            }


                        } else if (from.equalsIgnoreCase(ApiUtils.INSERT)) {

                            DealsAddressModel dealsAddressModel = bundle.getParcelable(DealsConstant.KEY_ADDRESS_MODEL);

                            if (dealsAddressModel != null) {

                                if (dealsAddressModelArrayList == null) {
                                    dealsAddressModelArrayList = new ArrayList<>();
                                }
                                dealsAddressModelArrayList.add(dealsAddressModel);
                            }
                        }

                        addressListAdapter.notifyDataSetChanged();
                    }
                }
            } else if (requestCode == DealsConstant.REQUEST_CODE_BOOKING) {
                getActivity().finish();
            }
        }
    }


    @Override
    public void onSuccessAddress(String from, String desc, int position) {

        if (from.equalsIgnoreCase(ApiUtils.DELETE)) {
            if (dealsAddressModelArrayList != null && dealsAddressModelArrayList.get(position) != null) {
                dealsAddressModelArrayList.remove(position);
                addressListAdapter.notifyDataSetChanged();

                if (!TextUtils.isEmpty(desc))
                    showToast(desc);
            }
        }
    }

    private void showToast(String desc) {
        Toast t = Toast.makeText(mContext, desc, Toast.LENGTH_SHORT);
        t.setGravity(Gravity.CENTER, 0, 0);
        t.show();
    }

    @Override
    public void onErrorAddress(String from, String error) {
        showToast(error);
    }
}