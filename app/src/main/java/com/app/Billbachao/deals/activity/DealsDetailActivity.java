package com.app.Billbachao.deals.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;

import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.deals.adapter.DealsDetailsPagerAdapter;
import com.app.Billbachao.deals.utils.DealsConstant;
import com.app.Billbachao.deals.tasks.DealsListFetcherTask;
import com.app.Billbachao.deals.utils.DealsValidationUtils;
import com.app.Billbachao.deals.models.DealsPaymentOptions;
import com.app.Billbachao.deals.models.RelianceTvDetailsModel;
import com.app.Billbachao.apis.deals.DealsListApiUtils;
import com.app.Billbachao.deals.view.DealsHeaderComponent;
import com.app.Billbachao.utils.UiUtils;
import com.app.Billbachao.volley.utils.VolleySingleTon;

import java.util.List;

/**
 * Details for Reliance Deals
 * Created by nitesh on 24/02/2016.
 */
public class DealsDetailActivity extends BaseActivity implements View.OnClickListener, DealsListFetcherTask.IDealsCallBack {

    private static final String ENQUIRY = "enquiry";
    private static final String BOOK_NOW = "book";
    private static final String BUY_NOW = "buy";
    private static final String TAG = DealsDetailActivity.class.getSimpleName();
    private Context mContext;
    private DealsPaymentOptions buyOptionsModel;
    private RelianceTvDetailsModel relianceTvDetailsModel;
    private String from;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = DealsDetailActivity.this;
        setContentView(R.layout.activity_reliance_details_television);

        initData();

    }

    @Override
    protected void onDestroy() {
        VolleySingleTon.getInstance(mContext).cancelPendingRequests(TAG);
        super.onDestroy();
    }

    private void setupViewPager(ViewPager viewPager, RelianceTvDetailsModel relianceTvDetailsModel) {
        DealsDetailsPagerAdapter adapter = new DealsDetailsPagerAdapter(mContext, getSupportFragmentManager(), relianceTvDetailsModel);
        adapter.addTitle(relianceTvDetailsModel.titleList);
        viewPager.setAdapter(adapter);
    }

    private void initData() {

        Intent intent = getIntent();

        if (intent != null && intent.getExtras() != null) {

            Bundle bundle = intent.getExtras();

            from = bundle.getString(DealsConstant.KEY_FROM);

            if (from != null) {
                if (from.equalsIgnoreCase(DealsConstant.KEY_BELL_CLICKED)) {
                    relianceTvDetailsModel = bundle.getParcelable(DealsConstant.KEY_RELIANCE_DETAILS);
                    updateWidgets(relianceTvDetailsModel);
                } else if (from.equalsIgnoreCase(DealsConstant.KEY_FROM_TV_DEEP_LINKS)) {
                    String productId = bundle.getString(DealsConstant.KEY_DEALS_PRODUCT_ID);

                    if (!TextUtils.isEmpty(productId))
                        initDealsApiCall(productId);
                    else {
                        relianceTvDetailsModel = bundle.getParcelable(DealsConstant.KEY_RELIANCE_DETAILS);

                        if (relianceTvDetailsModel != null)
                            updateWidgets(relianceTvDetailsModel);
                    }
                }
            }

        }
    }

    private void initDealsApiCall(String productId) {
        showProgressDialog(getString(R.string.loading_please_wait));
        DealsListFetcherTask fetcherTask = new DealsListFetcherTask(mContext, this);
        fetcherTask.fetchDeals(DealsListApiUtils.MODE_BY_PRODUCT, productId, TAG);
    }

    private void updateWidgets(RelianceTvDetailsModel relianceTvDetailsModel) {

        if (relianceTvDetailsModel != null) {

            //change the buying options.
            Button buyButton = (Button) findViewById(R.id.buyDealButton);
            buyButton.setOnClickListener(this);

            //change the title.
            changePaymentOptions(buyButton, relianceTvDetailsModel.dealPaymentOptions);

            //add the header component
            new DealsHeaderComponent(this, relianceTvDetailsModel, findViewById(R.id.include_tv_header));

            //add viewpager
            ViewPager mViewPager = (ViewPager) findViewById(R.id.htab_viewpager);
            mViewPager.setOffscreenPageLimit(1);
            setupViewPager(mViewPager, relianceTvDetailsModel);

            //add tab layout
            TabLayout tabLayout = (TabLayout) findViewById(R.id.htab_tabs);
            tabLayout.setupWithViewPager(mViewPager);

            tabLayout.setTabTextColors(ContextCompat.getColor(mContext, R.color.deals_tabs_unselected), ContextCompat.getColor(mContext, R.color.colorAccent));
        }

    }

    private void changePaymentOptions(Button dealPaymentOptionsBtn, DealsPaymentOptions dealPaymentOptions) {
        this.buyOptionsModel = dealPaymentOptions;
        if (buyOptionsModel != null) {
            dealPaymentOptionsBtn.setText(buyOptionsModel.buyOptions);
        }

    }

    private void buy() {

        if (buyOptionsModel != null) {

            String buyOptions = buyOptionsModel.buyOptions;

            if (!DealsValidationUtils.isEmpty(buyOptions)) {
                if (buyOptions.toLowerCase().contains(ENQUIRY.toLowerCase())) {

                    if (!TextUtils.isEmpty(from)) {
                        if (from.equalsIgnoreCase(DealsConstant.KEY_BELL_CLICKED)) {
                            if (relianceTvDetailsModel != null && relianceTvDetailsModel.productInformation != null && !TextUtils.isEmpty(relianceTvDetailsModel.productInformation.productName)) {

                                //TODO add event
                                //EventUtils.sendEvent(EventUtils.HANDSETS_FOR_ME, Constants.LYF_PH_ENQUIRY, Constants.LYF_PH_ENQUIRY_VALUE.replaceFirst(Constants.LYF_REPLACE_VAL, relianceTvDetailsModel.productInformation.productName));
                                //MoEngage Event
                                //MoEngageAnalytics.trackEvent(this, EventUtils.HANDSETS_FOR_ME, Constants.LYF_PH_ENQUIRY, Constants.LYF_PH_ENQUIRY_VALUE.replaceFirst(Constants.LYF_REPLACE_VAL, relianceTvDetailsModel.productInformation.productName));
                            }

                        } else if (from.equalsIgnoreCase(DealsConstant.KEY_FROM_TV_DEEP_LINKS)) {
                            if (relianceTvDetailsModel != null && relianceTvDetailsModel.productInformation != null && !TextUtils.isEmpty(relianceTvDetailsModel.productInformation.productName)) {

                                //TODO add event
                                //EventUtils.sendEvent(EventUtils.NOTIFICATION, Constants.NOTIFY_PH_ENQUIRY, Constants.NOTIFY_PH_ENQUIRY_VALUE.replaceFirst(Constants.LYF_REPLACE_VAL, relianceTvDetailsModel.productInformation.productName));
                                //MoEngage
                                //MoEngageAnalytics.trackEvent(this, EventUtils.NOTIFICATION, Constants.NOTIFY_PH_ENQUIRY, Constants.NOTIFY_PH_ENQUIRY_VALUE.replaceFirst(Constants.LYF_REPLACE_VAL, relianceTvDetailsModel.productInformation.productName));
                            }
                        }
                    }

                    startWebActivity(buyOptionsModel);
                } else if (buyOptions.toLowerCase().contains(BOOK_NOW.toLowerCase())) {

                    if (!TextUtils.isEmpty(from)) {

                        if (from.equalsIgnoreCase(DealsConstant.KEY_BELL_CLICKED)) {
                            if (relianceTvDetailsModel != null && relianceTvDetailsModel.productInformation != null && !TextUtils.isEmpty(relianceTvDetailsModel.productInformation.productName))
                                ;
                            //TODO add event
                            //EventUtils.sendEvent(EventUtils.HANDSETS_FOR_ME, Constants.LYF_PH_PRE_BOOK, Constants.LYF_PH_PRE_BOOK_VAlUE.replaceFirst(Constants.LYF_REPLACE_VAL, relianceTvDetailsModel.productInformation.productName));
                            //MoEngage Event
                            //MoEngageAnalytics.trackEvent(this, EventUtils.HANDSETS_FOR_ME, Constants.LYF_PH_PRE_BOOK, Constants.LYF_PH_PRE_BOOK_VAlUE.replaceFirst(Constants.LYF_REPLACE_VAL, relianceTvDetailsModel.productInformation.productName));


                        } else if (from.equalsIgnoreCase(DealsConstant.KEY_FROM_TV_DEEP_LINKS)) {
                            if (relianceTvDetailsModel != null && relianceTvDetailsModel.productInformation != null && !TextUtils.isEmpty(relianceTvDetailsModel.productInformation.productName)) {
                                //TODO add event
                                //EventUtils.sendEvent(EventUtils.NOTIFICATION, Constants.NOTIFY_PH_PRE_BOOK, Constants.NOTIFY_PH_PRE_BOOK_VAlUE.replaceFirst(Constants.LYF_REPLACE_VAL, relianceTvDetailsModel.productInformation.productName));
                                //MoEngage
                                //MoEngageAnalytics.trackEvent(this, EventUtils.NOTIFICATION, Constants.NOTIFY_PH_PRE_BOOK, Constants.NOTIFY_PH_PRE_BOOK_VAlUE.replaceFirst(Constants.LYF_REPLACE_VAL, relianceTvDetailsModel.productInformation.productName));
                            }
                        }
                    }

                    startBookNowFlow(relianceTvDetailsModel);

                } else if (buyOptions.toLowerCase().contains(BUY_NOW.toLowerCase())) {

                    if (from.equalsIgnoreCase(DealsConstant.KEY_BELL_CLICKED)) {
                        if (relianceTvDetailsModel != null && relianceTvDetailsModel.productInformation != null && !TextUtils.isEmpty(relianceTvDetailsModel.productInformation.productName)) {
                            //TODO add event
                            //EventUtils.sendEvent(EventUtils.HANDSETS_FOR_ME, Constants.LYF_PH_BUY_NOW, Constants.LYF_PH_BUY_NOW_VAlUE.replaceFirst(Constants.LYF_REPLACE_VAL, relianceTvDetailsModel.productInformation.productName));
                            //MoEngage Event
                            //MoEngageAnalytics.trackEvent(this, EventUtils.HANDSETS_FOR_ME, Constants.LYF_PH_BUY_NOW, Constants.LYF_PH_BUY_NOW_VAlUE.replaceFirst(Constants.LYF_REPLACE_VAL, relianceTvDetailsModel.productInformation.productName));

                        } else if (from.equalsIgnoreCase(DealsConstant.KEY_FROM_TV_DEEP_LINKS)) {
                            if (relianceTvDetailsModel != null && relianceTvDetailsModel.productInformation != null && !TextUtils.isEmpty(relianceTvDetailsModel.productInformation.productName)) {
                                //TODO add event
                                //EventUtils.sendEvent(EventUtils.NOTIFICATION, Constants.NOTIFY_PH_BUY_NOW, Constants.NOTIFY_PH_BUY_NOW_VAlUE.replaceFirst(Constants.LYF_REPLACE_VAL, relianceTvDetailsModel.productInformation.productName));
                                //MoEngage
                                //MoEngageAnalytics.trackEvent(this, EventUtils.NOTIFICATION, Constants.NOTIFY_PH_BUY_NOW, Constants.NOTIFY_PH_BUY_NOW_VAlUE.replaceFirst(Constants.LYF_REPLACE_VAL, relianceTvDetailsModel.productInformation.productName));
                            }
                        }

                    }

                    startBuyNowFlow(relianceTvDetailsModel);

                }

            }
        }

    }

    private void startBuyNowFlow(RelianceTvDetailsModel relianceTvDetailsModel) {
        UiUtils.launchDealsFlowActivity(mContext, relianceTvDetailsModel, from, false);
    }

    private void startBookNowFlow(RelianceTvDetailsModel relianceTvDetailsModel) {
        UiUtils.launchDealsFlowActivity(mContext, relianceTvDetailsModel, from, true);
    }

    private void startWebActivity(DealsPaymentOptions buyOptionsModel) {

        String url = buyOptionsModel.enquiry_url;
        //String url = "http://dev.billbachao.com/thanksdeal";
        if (URLUtil.isHttpUrl(url) || URLUtil.isHttpsUrl(url)) {
            UiUtils.launchWebViewActivity(mContext, url, buyOptionsModel.buyOptions);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.buyDealButton:
                buy();
                break;
        }
    }

    @Override
    public void onDealsSuccess(List<RelianceTvDetailsModel> result) {

        dismissProgressDialog();

        if (result != null && result.size() > 0) {
            relianceTvDetailsModel = result.get(0);
            updateWidgets(relianceTvDetailsModel);
        }
    }

    @Override
    public void onDealsError(String error) {
        dismissProgressDialog();
        showSnack(error);
    }

    @Override
    protected boolean isCartItemSupported() {
        return false;
    }

    @Override
    public void setTitle(CharSequence title) {
        getString(R.string.deals);
    }
}
