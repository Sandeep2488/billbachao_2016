package com.app.Billbachao.deals.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.myjson.annotations.SerializedName;

/**
 * Created by nitesh on 07/03/2016.
 */
public class DealsPaymentOptions implements Parcelable {

    @SerializedName(BUY_OPTION)
    public String buyOptions;

    @SerializedName(ENQUIRY_URL_OPTION)
    public String enquiry_url;

    @SerializedName(DELIVERY_OPTION)
    public String delivery_option;

    @SerializedName(PAYMENT_OPTION)
    public String payment_option;

    @SerializedName(SHIPPING_CITY)
    public String shipping_city;

    @SerializedName(KEY)
    public String key;

    protected DealsPaymentOptions(Parcel in) {
        buyOptions = in.readString();
        enquiry_url = in.readString();
        delivery_option = in.readString();
        payment_option = in.readString();
        shipping_city = in.readString();
        key = in.readString();
    }

    public static final Creator<DealsPaymentOptions> CREATOR = new Creator<DealsPaymentOptions>() {
        @Override
        public DealsPaymentOptions createFromParcel(Parcel in) {
            return new DealsPaymentOptions(in);
        }

        @Override
        public DealsPaymentOptions[] newArray(int size) {
            return new DealsPaymentOptions[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(buyOptions);
        dest.writeString(enquiry_url);
        dest.writeString(delivery_option);
        dest.writeString(payment_option);
        dest.writeString(shipping_city);
        dest.writeString(key);
    }


    private static final String BUY_OPTION = "buy_option";
    private static final String DELIVERY_OPTION = "delivery_option";
    private static final String PAYMENT_OPTION = "payment_option";
    private static final String SHIPPING_CITY = "shipping_city";
    private static final String ENQUIRY_URL_OPTION = "enquiry_url";
    private static final String KEY = "key";


}
