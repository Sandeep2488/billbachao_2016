package com.app.Billbachao.deals.tasks;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.R;
import com.app.Billbachao.apis.ApiUtils;
import com.app.Billbachao.deals.models.RelianceTvDetailsModel;
import com.app.Billbachao.apis.deals.DealsListApiUtils;
import com.app.Billbachao.volley.request.NetworkJSONRequest;
import com.app.Billbachao.volley.utils.NetworkErrorHelper;
import com.app.Billbachao.volley.utils.VolleySingleTon;

import org.json.JSONObject;

import java.util.List;

/**
 * Deals Fetcher from network using ALL/ BYPRODUCT as mode.
 * Created by nitesh on 09-05-2016.
 */
public class DealsListFetcherTask {

    private final Context mContext;
    private final IDealsCallBack iDealsCallBack;

    public DealsListFetcherTask(Context mContext, IDealsCallBack iDealsCallBack) {
        this.mContext = mContext;
        this.iDealsCallBack = iDealsCallBack;
    }

    public void fetchDeals(String mMode, String mProductId, String tag) {

        NetworkJSONRequest request = new NetworkJSONRequest(Request.Method.POST, DealsListApiUtils.PRODUCT_DEALS, DealsListApiUtils.getDealsForTvModeAll(mContext, mMode, mProductId), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                if (iDealsCallBack == null)
                    return;

                if (response == null)
                    iDealsCallBack.onDealsError(mContext.getString(R.string.no_deals));

                if (ApiUtils.isValidResponse(response.toString()))
                    iDealsCallBack.onDealsSuccess(DealsListApiUtils.getParseJsonTvDetails(response));
                else
                    iDealsCallBack.onDealsError(mContext.getString(R.string.no_deals));
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (iDealsCallBack != null)
                    iDealsCallBack.onDealsError(NetworkErrorHelper.getErrorStatus(error).getErrorMessage());
            }
        }, true);

        VolleySingleTon.getInstance(mContext).addToRequestQueue(request, tag);
    }


    /**
     * fetch deals without product Id
     *
     * @param mMode
     * @param tag
     */
    public void fetchDeals(String mMode, String tag) {
        fetchDeals(mMode, ApiUtils.NA, tag);
    }


    /**
     * Interface to return the result
     */
    public interface IDealsCallBack {
        void onDealsSuccess(List<RelianceTvDetailsModel> result);
        void onDealsError(String error);
    }
}
