package com.app.Billbachao.deals.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.app.Billbachao.R;
import com.app.Billbachao.deals.adapter.SpecificationAdapter;
import com.app.Billbachao.deals.utils.DealsConstant;
import com.app.Billbachao.deals.utils.InsertSpecificationData;
import com.app.Billbachao.deals.models.GenericTvModel;
import com.app.Billbachao.deals.models.SpecificationModel;
import com.app.Billbachao.utils.ILog;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nitesh on 24/02/2016.
 */
public class SpecificationFragment extends Fragment {

    private static final String TAG = SpecificationFragment.class.getName();
    private View parentView;
    private ListView sepecificationListView;
    private List<SpecificationModel> specificationModelList = null;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.fragment_deals_television_sepecification, null);

        initView();

        initData();

        return parentView;
    }

    private void initView() {
        sepecificationListView = (ListView) parentView.findViewById(R.id.specificationListView);
    }

    private void initData() {

        Bundle bundle = getArguments();

        if (bundle != null) {

            specificationModelList = new ArrayList<>();

            String from = bundle.getString(DealsConstant.KEY_FROM);

            ILog.d(TAG, "|from|" + from);

            InsertSpecificationData insertSpecificationData = new InsertSpecificationData(getActivity());

            ArrayList<GenericTvModel> genericTvModelList = bundle.getParcelableArrayList(DealsConstant.KEY_RELIANCE_GENERIC_DETAILS);

            if (genericTvModelList != null && genericTvModelList.size() > 0) {
                insertSpecificationData.fillGenericInfo(genericTvModelList, specificationModelList);
            }

            SpecificationAdapter adapter = new SpecificationAdapter(getActivity(), 0, specificationModelList);
            sepecificationListView.setAdapter(adapter);
        }
    }
}
