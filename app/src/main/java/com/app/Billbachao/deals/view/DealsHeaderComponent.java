package com.app.Billbachao.deals.view;

import android.content.Context;
import android.text.Html;
import android.text.Spannable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.StrikethroughSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.apis.ApiUtils;
import com.app.Billbachao.deals.models.RelianceTvDetailsModel;
import com.app.Billbachao.deals.models.TvProductInformation;
import com.squareup.picasso.Picasso;

/**
 * Deals Header specification
 * Created by nitesh on 25/02/2016.
 */
public class DealsHeaderComponent {

    private View view = null;
    private final Context context;
    private TextView tv_handset_title, tv_handset_mrp, tv_handset_price;
    private ImageView img_handset_icon;


    public DealsHeaderComponent(Context context, RelianceTvDetailsModel relianceTvDetailsModel, View viewById) {
        this.context = context;
        view = viewById;
        initData(relianceTvDetailsModel);
    }

    private void initData(RelianceTvDetailsModel relianceTvDetailsModel) {

        initView();

        showHandsetDetails(relianceTvDetailsModel);
    }


    private void initView() {

        tv_handset_title = (TextView) view.findViewById(R.id.tv_handset_title);
        tv_handset_mrp = (TextView) view.findViewById(R.id.tv_handset_mrp);
        tv_handset_price = (TextView) view.findViewById(R.id.tv_handset_price);
        img_handset_icon = (ImageView) view.findViewById(R.id.handset_large_icon);
    }


    private void showHandsetDetails(RelianceTvDetailsModel relianceTvDetailsModel) {
        TvProductInformation tvProductInformation = relianceTvDetailsModel.productInformation;

        if (tvProductInformation != null) {
            String handsetTitle = tvProductInformation.productName;
            String handsetIcon = tvProductInformation.imageUrl;
            String handsetMRP = tvProductInformation.cost;
            String handsetPrice = tvProductInformation.offerCost;

            tv_handset_title.setText(handsetTitle);

            tv_handset_mrp.setVisibility(View.VISIBLE);
            if (!handsetPrice.equals("0") && !handsetPrice.equalsIgnoreCase(ApiUtils.NA)) {
                tv_handset_mrp.setText("MRP:" + " " + context.getString(R.string.inr_n, handsetMRP), TextView.BufferType.SPANNABLE);
                Spannable spannable = (Spannable) tv_handset_mrp.getText();
                spannable.setSpan(new StrikethroughSpan(), 4, tv_handset_mrp.getText().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            } else {
                tv_handset_mrp.setText(Html.fromHtml("MRP:" + " " + context.getString(R.string.inr_n, handsetMRP)));
            }

            if (!handsetPrice.equals("0") && !handsetPrice.equalsIgnoreCase(ApiUtils.NA)) {
                tv_handset_price.setText("Price:" + " " + context.getString(R.string.inr_n, handsetPrice));
                tv_handset_price.setVisibility(View.VISIBLE);
            } else {
                tv_handset_price.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(handsetIcon)) {
                Picasso.with(context)
                        .load(handsetIcon.replace("\\", "").trim())
                        .noFade()
                        .fit()
                        .centerInside()
                        .into(img_handset_icon);
            }
        }


    }

    public View getView() {
        return view;
    }

}
