package com.app.Billbachao.deals.adapter;

import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListPopupWindow;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.apis.ApiUtils;
import com.app.Billbachao.deals.utils.DealsConstant;
import com.app.Billbachao.deals.i.IDealsAddressItemClick;
import com.app.Billbachao.deals.models.DealsAddressModel;
import com.app.Billbachao.deals.view.PopupUtils;
import com.app.Billbachao.utils.UiUtils;

import java.util.ArrayList;

/**
 * Deals Address Listing adapter
 * Created by nitesh on 24/02/2015.
 */
public class AddressListAdapter extends RecyclerView.Adapter<AddressListAdapter.AddressListViewHolder> {

    private FragmentActivity context;
    private ArrayList<DealsAddressModel> items;
    private IDealsAddressItemClick mDealsAddressItemClick;
    private String from;
    private IDealsDeleteAddress iDealsDeleteAddress;


    public AddressListAdapter(FragmentActivity context, ArrayList<DealsAddressModel> items, String from) {
        this.context = context;
        this.items = items;
        this.from = from;
        this.iDealsDeleteAddress = (IDealsDeleteAddress) context;
    }

    @Override
    public AddressListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.deals_address_listing_view, parent, false);
        return new AddressListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AddressListViewHolder viewHolder, int position) {

        final DealsAddressModel dealsAddressModel = items.get(position);

        viewHolder.deals_address_city_pincode_tv.setText(String.format(context.getString(R.string.deals_city_n_pincode), dealsAddressModel.city, dealsAddressModel.pincode));
        viewHolder.deals_address_user_name_tv.setText(String.format(context.getString(R.string.deals_first_n_last), dealsAddressModel.firstname, dealsAddressModel.lastname));
        viewHolder.deals_address_tv.setText(dealsAddressModel.address);
        viewHolder.deals_landmark_v.setText(dealsAddressModel.landmark);
        viewHolder.deals_address_state_tv.setText(dealsAddressModel.state);
        viewHolder.deals_address_mobile_tv.setText(String.format(context.getString(R.string.deals_address_mobile), dealsAddressModel.mobileNo));
        viewHolder.popupWindowImageView.setTag(position);
    }


    @Override
    public int getItemCount() {
        return (items != null && items.size() > 0 ? items.size() : 0);
    }

    public class AddressListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final TextView deals_address_city_pincode_tv;
        private final TextView deals_address_user_name_tv;
        private final TextView deals_address_tv;
        private final TextView deals_landmark_v;
        private final TextView deals_address_state_tv;
        private final TextView deals_address_mobile_tv;
        private final ImageView popupWindowImageView;
        private final ImageView dealsAddressItemSelectedIv;

        public AddressListViewHolder(View convertView) {
            super(convertView);

            deals_address_user_name_tv = (TextView) convertView.findViewById(R.id.deals_address_user_name_tv);
            deals_address_tv = (TextView) convertView.findViewById(R.id.deals_address_tv);
            deals_landmark_v = (TextView) convertView.findViewById(R.id.deals_landmark_v);
            deals_address_city_pincode_tv = (TextView) convertView.findViewById(R.id.deals_address_city_pincode_tv);
            deals_address_state_tv = (TextView) convertView.findViewById(R.id.deals_address_state_tv);
            deals_address_mobile_tv = (TextView) convertView.findViewById(R.id.deals_address_mobile_tv);
            popupWindowImageView = (ImageView) convertView.findViewById(R.id.popupWindowImageView);
            dealsAddressItemSelectedIv = (ImageView) convertView.findViewById(R.id.deals_selected_unselected_address);

            dealsAddressItemSelectedIv.setOnClickListener(this);
            popupWindowImageView.setOnClickListener(popUpWindow);
            //convertView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            if (view.getId() == dealsAddressItemSelectedIv.getId()){
                mDealsAddressItemClick.itemClick(getAdapterPosition(), view, items.get(getAdapterPosition()));
            }
        }
    }


    public void setDealsItemSelectedClickListener(IDealsAddressItemClick mDealsItemClickListener) {
        this.mDealsAddressItemClick = mDealsItemClickListener;
    }

    View.OnClickListener popUpWindow = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ArrayList<String> arrayList = new ArrayList<>();
            arrayList.add(context.getString(R.string.deals_edit));
            arrayList.add(context.getString(R.string.deals_delete));

            int parentPosition = (int) v.getTag();

            ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, arrayList);
            ListPopupWindow listPopupWindow = PopupUtils.showPopUp(context, v, adapter);
            listPopupWindow.setOnItemClickListener(new OnItemClickListWindow(parentPosition, listPopupWindow));
            listPopupWindow.show();
        }
    };


    public class OnItemClickListWindow implements AdapterView.OnItemClickListener {

        public OnItemClickListWindow(int parentPosition, ListPopupWindow listPopupWindow) {
            this.parentPosition = parentPosition;
            this.listPopupWindow = listPopupWindow;
        }

        private int parentPosition;
        private ListPopupWindow listPopupWindow;

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Object o = parent.getAdapter().getItem(position);

            if (o != null) {

                listPopupWindow.dismiss();
                String result = (String) o;

                if (result.equalsIgnoreCase(context.getString(R.string.deals_edit))) {
                    triggerEdit();
                    UiUtils.launchDealsAddAddressActivity(context, ApiUtils.UPDATE, parentPosition, items.get(parentPosition));
                } else if (result.equalsIgnoreCase(context.getString(R.string.deals_delete))) {
                    triggerDelete();
                    showAlertDialogToDelete(parentPosition);
                }
            }
        }
    }

    private void triggerDelete() {
        if (!TextUtils.isEmpty(from)){
            if (from.equalsIgnoreCase(DealsConstant.KEY_BELL_CLICKED)){

                //TODO add event

                //EventUtils.sendEvent(EventUtils.HANDSETS_FOR_ME, Constants.LYF_PH_DELETE_ADDR, Constants.PROPERTY_VALUE);
                //MoEngage Event
                //MoEngageAnalytics.trackEvent(context, EventUtils.HANDSETS_FOR_ME, Constants.LYF_PH_DELETE_ADDR, Constants.PROPERTY_VALUE);


            }else if (from.equalsIgnoreCase(DealsConstant.KEY_FROM_TV_DEEP_LINKS)){

                //TODO add event

                //EventUtils.sendEvent(EventUtils.NOTIFICATION, Constants.NOTIFY_PH_DELETE_ADDR, Constants.PROPERTY_VALUE);
                //MoEngage
                //MoEngageAnalytics.trackEvent(context, EventUtils.NOTIFICATION, Constants.NOTIFY_PH_DELETE_ADDR, Constants.PROPERTY_VALUE);

            }
        }
    }

    private void triggerEdit() {
        if (!TextUtils.isEmpty(from)){
            if (from.equalsIgnoreCase(DealsConstant.KEY_BELL_CLICKED)){

                //TODO add event
                //EventUtils.sendEvent(EventUtils.HANDSETS_FOR_ME, Constants.LYF_PH_EDIT_ADDR, Constants.PROPERTY_VALUE);
                //MoEngage Event
                //MoEngageAnalytics.trackEvent(context, EventUtils.HANDSETS_FOR_ME, Constants.LYF_PH_EDIT_ADDR, Constants.PROPERTY_VALUE);

            }else if (from.equalsIgnoreCase(DealsConstant.KEY_FROM_TV_DEEP_LINKS)){
                //TODO add event

                //EventUtils.sendEvent(EventUtils.NOTIFICATION, Constants.NOTIFY_PH_EDIT_ADDR, Constants.PROPERTY_VALUE);
                //MoEngage
                //MoEngageAnalytics.trackEvent(context, EventUtils.NOTIFICATION, Constants.NOTIFY_PH_EDIT_ADDR, Constants.PROPERTY_VALUE);
            }
        }
    }


    private void showAlertDialogToDelete(final int position) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setCancelable(false);
        alertDialog.setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.setMessage(context.getString(R.string.deals_delete_address));
        alertDialog.setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //new DealsAddAddressAsyntask(context, items.get(position), position).execute(ApiUtils.DELETE);

                if (iDealsDeleteAddress != null){
                    iDealsDeleteAddress.deleteDealsAddress(position, items.get(position));
                }

            }
        });

        AlertDialog createdDialog = alertDialog.create();

        if (createdDialog != null)
            alertDialog.show();
    }

    public void setOnAddressDeleted(IDealsDeleteAddress iDealsDeleteAddress){
        this.iDealsDeleteAddress = iDealsDeleteAddress;
    }


    public interface IDealsDeleteAddress {
        void deleteDealsAddress(int position, DealsAddressModel dealsAddressModel);
    }

}