package com.app.Billbachao.deals.i;

import com.app.Billbachao.deals.models.DealsBookingModel;

/**
 * Created by nitesh on 29/02/2016.
 */
public interface IDealsBooking {
    void dealsBooking(DealsBookingModel dealsBookingModel);
    void dealsBookingError(String error);
}
