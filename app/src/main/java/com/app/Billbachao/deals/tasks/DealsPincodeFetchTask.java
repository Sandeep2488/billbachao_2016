package com.app.Billbachao.deals.tasks;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.R;
import com.app.Billbachao.apis.ApiUtils;
import com.app.Billbachao.deals.i.IDealsPincodeValidation;
import com.app.Billbachao.deals.models.DealsBaseAddressPincodeModel;
import com.app.Billbachao.apis.deals.DealsPincodeValidationApi;
import com.app.Billbachao.volley.request.NetworkGsonRequest;
import com.app.Billbachao.volley.utils.NetworkErrorHelper;
import com.app.Billbachao.volley.utils.VolleySingleTon;

/**
 * Fetch the address for the deals
 * Created by nitesh on 10-05-2016.
 */
public class DealsPinCodeFetchTask {

    private Context mContext;
    private IDealsPincodeValidation iDealsPincodeValidation;

    public DealsPinCodeFetchTask(Context mContext, IDealsPincodeValidation iDealsPincodeValidation) {
        this.mContext = mContext;
        this.iDealsPincodeValidation = iDealsPincodeValidation;
    }

    public void fetchPinCodeDetails(String pinCode, String tag){
        NetworkGsonRequest<DealsBaseAddressPincodeModel> request = new NetworkGsonRequest<>(Request.Method.POST, DealsPincodeValidationApi.PINCODE_URL, DealsBaseAddressPincodeModel.class, DealsPincodeValidationApi.getDealsByPincode(mContext, pinCode), new Response.Listener<DealsBaseAddressPincodeModel>() {
            @Override
            public void onResponse(DealsBaseAddressPincodeModel model) {

                if (iDealsPincodeValidation == null)
                    return;

                if (model == null)
                    iDealsPincodeValidation.onErrorPincode(mContext.getString(R.string.deals_address_invalid_pincode));


                if (model.status.equalsIgnoreCase(ApiUtils.SUCCESS)){
                    iDealsPincodeValidation.onSuccessPincode(model.dealsAddressModel);
                }else {
                    iDealsPincodeValidation.onErrorPincode(mContext.getString(R.string.deals_address_invalid_pincode));
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (iDealsPincodeValidation != null){
                    iDealsPincodeValidation.onErrorPincode(NetworkErrorHelper.getErrorStatus(error).getErrorMessage());
                }
            }
        }, true);

        VolleySingleTon.getInstance(mContext).addToRequestQueue(request, tag);
    }
}
