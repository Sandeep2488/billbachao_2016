package com.app.Billbachao.deals.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.deals.adapter.DealsProductAdapter;
import com.app.Billbachao.deals.utils.DealsConstant;
import com.app.Billbachao.deals.models.ProductPaymentModel;

import java.util.List;

public class DealsPaymentReceiptActivity extends BaseActivity implements View.OnClickListener {

    private Context mContext;
    private CardView dealsSuccessTransactCV;
    private TextView dealsTransactionIdTv;
    private TextView dealsCostTv;
    private Button dealsSuccessButton;
    private RecyclerView receiptProductListingRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deals_success);

        mContext = DealsPaymentReceiptActivity.this;

        initView();

        initData();
    }

    private void initData() {

        Intent intent = getIntent();

        if (intent != null && intent.getExtras() != null) {
            Bundle bundle = intent.getExtras();

            if (bundle != null) {
                String orderAmount = bundle.getString(DealsConstant.KEY_RECEIPT_ORDER_AMOUNT);
                boolean isAmtPresent = !TextUtils.isEmpty(orderAmount);

                String orderId = bundle.getString(DealsConstant.KEY_RECEIPT_ORDER_ID);
                boolean isOrderIdPresent = !TextUtils.isEmpty(orderId);

                if (isAmtPresent || isOrderIdPresent){
                    dealsSuccessTransactCV.setVisibility(View.VISIBLE);

                    if (isOrderIdPresent){
                        dealsTransactionIdTv.setVisibility(View.VISIBLE);
                        dealsTransactionIdTv.setText(String.format(getString(R.string.deals_transaction_id), orderAmount));
                    }else {
                        dealsTransactionIdTv.setVisibility(View.GONE);
                    }

                    if (isAmtPresent){
                        dealsCostTv.setVisibility(View.VISIBLE);
                        dealsCostTv.setText(String.format(getString(R.string.deals_cost), orderAmount));
                    }else {
                        dealsCostTv.setVisibility(View.GONE);
                    }
                }else {
                    dealsSuccessTransactCV.setVisibility(View.GONE);
                }



                String title = bundle.getString(DealsConstant.KEY_PAYMENT_STATUS_TITLE);

                if (!TextUtils.isEmpty(title)){
                    setTitle(title);
                }else {
                    setTitle(getString(R.string.payment));
                }

                List<ProductPaymentModel> productPaymentModelList = bundle.getParcelableArrayList(DealsConstant.KEY_PRODUCT_PAYMENT);

                fillAdapter(productPaymentModelList);
            }
        }
    }

    private void initView() {

        dealsSuccessTransactCV = (CardView) findViewById(R.id.dealsSuccessTransactCV);
        dealsTransactionIdTv = (TextView) findViewById(R.id.dealsTransactionIdTv);
        dealsCostTv = (TextView) findViewById(R.id.dealsCostTv);

        dealsSuccessButton = (Button) findViewById(R.id.dealsSuccessButton);
        dealsSuccessButton.setOnClickListener(this);

        receiptProductListingRecyclerView = (RecyclerView) findViewById(R.id.receiptProductListingDeals);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        receiptProductListingRecyclerView.setLayoutManager(linearLayoutManager);

    }

    private void fillAdapter(List<ProductPaymentModel> productPaymentModelList) {

        DealsProductAdapter dealReceiptProductAdapter = new DealsProductAdapter(mContext, productPaymentModelList);
        receiptProductListingRecyclerView.setAdapter(dealReceiptProductAdapter);
    }

    @Override
    public void onBackPressed() {
        handleBackPressed();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.dealsSuccessButton:
                handleBackPressed();
                break;
        }
    }

    private void handleBackPressed() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                handleBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
