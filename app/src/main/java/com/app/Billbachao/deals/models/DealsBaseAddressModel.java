package com.app.Billbachao.deals.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.app.Billbachao.model.BaseGsonResponse;
import com.google.myjson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Base Address Model
 * Created by nitesh on 10-05-2016.
 */
public class DealsBaseAddressModel extends BaseGsonResponse implements Parcelable {

    @SerializedName("USERSHIPPINGDETAILS")
    public ArrayList<DealsAddressModel> dealsAddressModels;

    protected DealsBaseAddressModel(Parcel in) {
        dealsAddressModels = in.createTypedArrayList(DealsAddressModel.CREATOR);
    }

    public static final Creator<DealsBaseAddressModel> CREATOR = new Creator<DealsBaseAddressModel>() {
        @Override
        public DealsBaseAddressModel createFromParcel(Parcel in) {
            return new DealsBaseAddressModel(in);
        }

        @Override
        public DealsBaseAddressModel[] newArray(int size) {
            return new DealsBaseAddressModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(dealsAddressModels);
    }
}
