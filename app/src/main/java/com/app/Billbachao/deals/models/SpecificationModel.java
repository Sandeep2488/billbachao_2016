package com.app.Billbachao.deals.models;

/**
 * Created by nitesh on 25/02/2016.
 */
public class SpecificationModel {

    public String descriptionTitle;
    public String description;
    public String headerTitle;

    public boolean showHeaderTitle;

    public SpecificationModel(boolean showHeaderTitle, String headerTitle, String descriptionTitle, String description){
        this.descriptionTitle = descriptionTitle;
        this.description = description;
        this.showHeaderTitle = showHeaderTitle;
        this.headerTitle = headerTitle;
    }
}
