package com.app.Billbachao.deals.i;

import com.app.Billbachao.deals.models.DealsAddressModel;

import java.util.ArrayList;

/**
 * Created by sushil on 09/03/2016.
 */
public interface IDealAddressListingCallback {
    void onSuccessAddressListing(ArrayList<DealsAddressModel> dealsAddressModel);
    void onErrorAddressListing();
}
