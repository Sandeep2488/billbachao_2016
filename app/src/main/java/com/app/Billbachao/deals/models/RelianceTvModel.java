package com.app.Billbachao.deals.models;

import com.google.myjson.annotations.SerializedName;

import java.util.List;

/**
 * Created by nitesh on 24/02/2016.
 */
public class RelianceTvModel {

    @SerializedName(TV_DETAILS)
    public List<RelianceTvDetailsModel> relianceTvDetailsList;

    public static final String TV_DETAILS = "televisionDetails";

}
