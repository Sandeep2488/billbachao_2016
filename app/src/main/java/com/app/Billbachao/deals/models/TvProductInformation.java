package com.app.Billbachao.deals.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.myjson.annotations.SerializedName;

/**
 * Created by nitesh on 24/02/2016.
 */
public class TvProductInformation implements Parcelable {

    @SerializedName(PRODUCT_NAME)
    public String productName;

    @SerializedName(PRODUCT_COST)
    public String cost;

    @SerializedName(PROD_OFFER_COST)
    public String offerCost;

    @SerializedName(PROD_MODELNO)
    public String modelNo;

    @SerializedName(PROD_TV_TYPE)
    public String tv_type;

    @SerializedName(PROD_BRAND)
    public String brand;

    @SerializedName(PROD_COLOR)
    public String colour;

    @SerializedName(PROD_CURRENT_OFFER_TYPE)
    public String current_offer_type;

    @SerializedName(PROD_CURRENT_OFFER)
    public String current_offers;

    @SerializedName(PROD_PROMOTION)
    public String promotion;

    @SerializedName(PROD_URL)
    public String imageUrl;

    @SerializedName(PROD_KEY)
    public String key;

    @SerializedName(PROD_BOOK_WITH_PAYMENT)
    public int bookWithPayment;

    public static final String PROD_URL = "imageUrl";

    public static final String PRODUCT_NAME = "Product Name";

    public static final String PRODUCT_COST = "cost";

    public static final String PROD_OFFER_COST = "Offer Cost";

    public static final String PROD_MODELNO = "Model No";

    public static final String PROD_TV_TYPE = "TV Type";

    public static final String PROD_BRAND = "Brand";

    public static final String PROD_COLOR = "Colour";

    public static final String PROD_CURRENT_OFFER_TYPE = "Current Offer Type";

    public static final String PROD_CURRENT_OFFER = "Current Offers";

    public static final String PROD_PROMOTION = "Promotion";

    public static final String PROD_KEY = "key";

    public static final String PROD_BOOK_WITH_PAYMENT = "bookWithPayment";

    protected TvProductInformation(Parcel in) {
        productName = in.readString();
        cost = in.readString();
        offerCost = in.readString();
        modelNo = in.readString();
        tv_type = in.readString();
        brand = in.readString();
        colour = in.readString();
        current_offer_type = in.readString();
        current_offers = in.readString();
        promotion = in.readString();
        imageUrl = in.readString();
        key = in.readString();
        bookWithPayment = in.readInt();
    }

    public static final Creator<TvProductInformation> CREATOR = new Creator<TvProductInformation>() {
        @Override
        public TvProductInformation createFromParcel(Parcel in) {
            return new TvProductInformation(in);
        }

        @Override
        public TvProductInformation[] newArray(int size) {
            return new TvProductInformation[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(productName);
        dest.writeString(cost);
        dest.writeString(offerCost);
        dest.writeString(modelNo);
        dest.writeString(tv_type);
        dest.writeString(brand);
        dest.writeString(colour);
        dest.writeString(current_offer_type);
        dest.writeString(current_offers);
        dest.writeString(promotion);
        dest.writeString(imageUrl);
        dest.writeString(key);
        dest.writeInt(bookWithPayment);
    }
}
