package com.app.Billbachao.deals.i;

import android.view.View;

import com.app.Billbachao.deals.models.DealsAddressModel;

/**
 * Created by sushil on 09/03/2016.
 */
public interface IDealsAddressItemClick {
    void itemClick(int adapterPosition, View view, DealsAddressModel dealsAddressModel);
}
