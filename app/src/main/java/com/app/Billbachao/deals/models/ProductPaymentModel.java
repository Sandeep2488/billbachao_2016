package com.app.Billbachao.deals.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.app.Billbachao.deals.utils.DealsConstant;
import com.google.myjson.annotations.Expose;
import com.google.myjson.annotations.SerializedName;

/**
 * Created by nitesh on 11/03/2016.
 */
public class ProductPaymentModel implements Parcelable {

    @SerializedName("product_id")
    public String product_id;

    @SerializedName("prudct_name")
    public String prudct_name;

    @SerializedName("uniteprice") // cost price
    public String uniteprice;

    @SerializedName("quantity")
    public String quantity;

    @SerializedName("offer_price")
    public String offer_price;

    @SerializedName("processed_unit_price") // offer_price
    public String processed_unit_price;

    @SerializedName("promo_code")
    public String promo_code;

    @SerializedName(DealsConstant.KEY_IMAGE_URL)
    @Expose
    public String imageUrl;

    public ProductPaymentModel(String product_id, String prudct_name, String uniteprice, String quantity, String offer_price, String processed_unit_price, String promo_code, String imageUrl) {
        this.product_id = product_id;
        this.prudct_name = prudct_name;
        this.uniteprice = uniteprice;
        this.quantity = quantity;
        this.offer_price = offer_price;
        this.processed_unit_price = processed_unit_price;
        this.promo_code = promo_code;
        this.imageUrl = imageUrl;
    }

    protected ProductPaymentModel(Parcel in) {
        product_id = in.readString();
        prudct_name = in.readString();
        uniteprice = in.readString();
        quantity = in.readString();
        offer_price = in.readString();
        processed_unit_price = in.readString();
        promo_code = in.readString();
        imageUrl = in.readString();
    }

    public static final Creator<ProductPaymentModel> CREATOR = new Creator<ProductPaymentModel>() {
        @Override
        public ProductPaymentModel createFromParcel(Parcel in) {
            return new ProductPaymentModel(in);
        }

        @Override
        public ProductPaymentModel[] newArray(int size) {
            return new ProductPaymentModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(product_id);
        dest.writeString(prudct_name);
        dest.writeString(uniteprice);
        dest.writeString(quantity);
        dest.writeString(offer_price);
        dest.writeString(processed_unit_price);
        dest.writeString(promo_code);
        dest.writeString(imageUrl);
    }
}
