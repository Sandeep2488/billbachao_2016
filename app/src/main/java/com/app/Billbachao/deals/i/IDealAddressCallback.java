package com.app.Billbachao.deals.i;

/**
 * Created by nitesh on 02/03/2016.
 */
public interface IDealAddressCallback {
    void onSuccessAddress(String from, String desc, int position);
    void onErrorAddress(String from, String error);
}
