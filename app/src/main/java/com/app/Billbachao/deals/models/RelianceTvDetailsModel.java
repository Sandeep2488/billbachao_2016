package com.app.Billbachao.deals.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.myjson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by nitesh on 24/02/2016.
 */
public class RelianceTvDetailsModel implements Parcelable {
    @SerializedName(SPEC_PRODUCT_INFO)
    public TvProductInformation productInformation;

    @SerializedName(SPEC_PAYEMNT_OPTIONS)
    public DealsPaymentOptions dealPaymentOptions;

    public List<String> titleList;

    public HashMap<String, ArrayList<GenericTvModel>> genericTvModelHashMap;


    public RelianceTvDetailsModel(){}

    public static final String SPEC_PRODUCT_INFO = "Product Information";
    public static final String SPEC_PAYEMNT_OPTIONS = "Payment Option";

    protected RelianceTvDetailsModel(Parcel in) {
        productInformation = in.readParcelable(TvProductInformation.class.getClassLoader());
        dealPaymentOptions = in.readParcelable(DealsPaymentOptions.class.getClassLoader());
        titleList = in.createStringArrayList();
        genericTvModelHashMap = in.readHashMap(getClass().getClassLoader());
    }

    public static final Creator<RelianceTvDetailsModel> CREATOR = new Creator<RelianceTvDetailsModel>() {
        @Override
        public RelianceTvDetailsModel createFromParcel(Parcel in) {
            return new RelianceTvDetailsModel(in);
        }

        @Override
        public RelianceTvDetailsModel[] newArray(int size) {
            return new RelianceTvDetailsModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(productInformation, flags);
        dest.writeParcelable(dealPaymentOptions, flags);
        dest.writeStringList(titleList);
        dest.writeMap(genericTvModelHashMap);
    }

}




