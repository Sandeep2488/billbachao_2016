package com.app.Billbachao.deals.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.MenuItem;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.apis.ApiUtils;
import com.app.Billbachao.common.ConnectionManager;
import com.app.Billbachao.deals.utils.DealsConstant;
import com.app.Billbachao.deals.utils.PaymentExcluisionStrategy;
import com.app.Billbachao.deals.models.DealsAddressModel;
import com.app.Billbachao.deals.models.ProductPaymentModel;
import com.app.Billbachao.apis.deals.DealsBookingApi;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.utils.UiUtils;
import com.google.myjson.Gson;
import com.google.myjson.GsonBuilder;
import com.google.myjson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;

/**
 * Deal Booking Webview
 * Created 10/05/2016
 */
public class DealsBookingWebview extends BaseActivity {

    private static final String PAYEMNT_RESPONSE_SEGMENT = "productPaymentResponse";
    private static final String TAG = DealsBookingWebview.class.getSimpleName();
    private static final String BB_PG_ORDER_ID = "bbPgOrderId", AMOUNT = "amount", MSG = "msg", URL = "url";

    private WebView webView;

    private boolean firstLoad = false, isRedirected = false;

    private ProgressDialog pDialog;

    private AlertDialog mAlertDialog;

    private String keyFrom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_webview);

        initialiseView();
        getIntentValues();
    }

    void initialiseView() {
        webView = (WebView) findViewById(R.id.deals_webView);
        pDialog = new ProgressDialog(DealsBookingWebview.this);
        pDialog.setMessage(getString(R.string.loading_please_wait));
        pDialog.setCancelable(true);
        pDialog.show();
    }

    private void showAlert(String msg, String title, final String from) {

        dismissAlert();

        destroyWebView();

        if (TextUtils.isEmpty(msg) || TextUtils.isEmpty(from))
            return;

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DealsBookingWebview.this);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage(msg);

        if (!TextUtils.isEmpty(title))
            alertDialogBuilder.setTitle(title);

        alertDialogBuilder.setPositiveButton(getString(R.string.ok_got_it), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (from != null) {
                    dialog.cancel();
                    if (from.equalsIgnoreCase(ApiUtils.SUCCESS)) {
                        paymentSuccessfull();
                    } else if (from.equalsIgnoreCase(ApiUtils.ERROR) || from.equalsIgnoreCase(ApiUtils.FAILURE)) {
                        paymentfailed();
                    } else if (from.equalsIgnoreCase(DealsConstant.INTERNET_FAILED)) {
                        finish();
                    }
                }
            }
        });

        mAlertDialog = alertDialogBuilder.create();

        if (mAlertDialog != null) {
            mAlertDialog.show();
            TextView messageView = (TextView) mAlertDialog.findViewById(android.R.id.message);
            messageView.setGravity(Gravity.CENTER);

            TextView titleView = (TextView) mAlertDialog.findViewById(DealsBookingWebview.this.getResources().getIdentifier("alertTitle", "id", "android"));
            if (titleView != null) {
                titleView.setGravity(Gravity.CENTER);
            }
        }

    }

    private void paymentSuccessfull() {

        //trigger the events
        triggerSuccess();

        //set result to ok
        setResult(RESULT_OK);

        //finish the activity
        finish();
    }

    private void triggerFailure() {
        if (!TextUtils.isEmpty(keyFrom)) {
            if (keyFrom.equalsIgnoreCase(DealsConstant.KEY_BELL_CLICKED)) {

                //TODO add events
                //EventUtils.sendEvent(EventUtils.HANDSETS_FOR_ME, Constants.NOTIFY_PH_TRANSACT_FAILED, Constants.PROPERTY_VALUE);
                //MoEngage Event
                //MoEngageAnalytics.trackEvent(this, EventUtils.HANDSETS_FOR_ME, Constants.NOTIFY_PH_TRANSACT_FAILED, Constants.PROPERTY_VALUE);

            } else if (keyFrom.equalsIgnoreCase(DealsConstant.KEY_FROM_TV_DEEP_LINKS)) {

                //TODO add events

                //EventUtils.sendEvent(EventUtils.NOTIFICATION, Constants.LYF_PH_TRANSACT_FAILED, Constants.PROPERTY_VALUE);
                //MoEngage
                //MoEngageAnalytics.trackEvent(this, EventUtils.NOTIFICATION, Constants.LYF_PH_TRANSACT_FAILED, Constants.PROPERTY_VALUE);
            }
        }
    }

    private void triggerSuccess() {
        if (!TextUtils.isEmpty(keyFrom)) {
            if (keyFrom.equalsIgnoreCase(DealsConstant.KEY_BELL_CLICKED)) {
                //TODO add events

                //EventUtils.sendEvent(EventUtils.HANDSETS_FOR_ME, Constants.NOTIFY_PH_TRANSACT_SUCCESS, Constants.PROPERTY_VALUE);
                //MoEngage Event
                //MoEngageAnalytics.trackEvent(this, EventUtils.HANDSETS_FOR_ME, Constants.NOTIFY_PH_TRANSACT_SUCCESS, Constants.PROPERTY_VALUE);
            } else if (keyFrom.equalsIgnoreCase(DealsConstant.KEY_FROM_TV_DEEP_LINKS)) {
                //TODO add events

                //EventUtils.sendEvent(EventUtils.NOTIFICATION, Constants.LYF_PH_TRANSACT_SUCCESS, Constants.PROPERTY_VALUE);
                //MoEngage
                //MoEngageAnalytics.trackEvent(this, EventUtils.NOTIFICATION, Constants.LYF_PH_TRANSACT_SUCCESS, Constants.PROPERTY_VALUE);
            }
        }
    }

    private void dismissAlert() {

        if (mAlertDialog != null && mAlertDialog.isShowing()) {
            mAlertDialog.dismiss();
            mAlertDialog = null;
        }
    }

    void getIntentValues() {

        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {

            Bundle bundle = intent.getExtras();

            if (bundle != null) {
                DealsAddressModel dealsAddressModel = bundle.getParcelable(DealsConstant.KEY_ADDRESS_MODEL);
                keyFrom = bundle.getString(DealsConstant.KEY_FROM);

                //for booknow
                if (bundle.getBoolean(DealsConstant.KEY_IS_USE_BOOKING, false)) {
                    String bookingWithPayment = bundle.getString(DealsConstant.KEY_BOOKING_WITH_PAYMENT_AMT);

                    if (!TextUtils.isEmpty(bookingWithPayment)) {
                        int bookingAmt = Integer.valueOf(bookingWithPayment);

                        if (bookingAmt > 0) {
                            JSONArray array = getProductArray(bundle);

                            if (array != null && array.length() > 0) {
                                startPayment(bookingAmt, dealsAddressModel.pincode, array);
                            }
                        }
                    }
                    //for buy now
                } else {
                    ArrayList<ProductPaymentModel> productPaymentModelArrayList = bundle.getParcelableArrayList(DealsConstant.KEY_PRODUCT_PAYMENT);

                    if (productPaymentModelArrayList != null && productPaymentModelArrayList.size() > 0)
                        startPayment(calculateTxtAmt(productPaymentModelArrayList), dealsAddressModel.pincode, getProductArray(productPaymentModelArrayList));
                }
            }
        }

    }

    private int calculateTxtAmt(ArrayList<ProductPaymentModel> productPaymentModels) {

        if (productPaymentModels != null && productPaymentModels.size() > 0) {

            int txtAmount = 0;

            for (ProductPaymentModel productPaymentModel : productPaymentModels)
                txtAmount += Integer.valueOf(productPaymentModel.processed_unit_price);

            ILog.d(TAG, "|calculaeTxtAmt |" + txtAmount);
            return txtAmount;
        }

        return 0;
    }

    private void startPayment(int txtAmount, String pincode, JSONArray productListArray) {
        if (txtAmount > 0) {
            byte data[] = DealsBookingApi.getBookingDeals(DealsBookingWebview.this, pincode, productListArray, String.valueOf(txtAmount));
            startWebViewClient(data);
            ILog.d(TAG, "|apiRequest - Deals - Pay |" + data);
        }
    }

    private ArrayList<ProductPaymentModel> getProductList(Bundle bundle) {
        return bundle.getParcelableArrayList(DealsConstant.KEY_PRODUCT_PAYMENT);
    }

    private JSONArray getProductArray(Bundle bundle) {
        return getProductArray(getProductList(bundle));
    }

    private JSONArray getProductArray(ArrayList<ProductPaymentModel> productPaymentModelArrayList) {
        if (productPaymentModelArrayList != null && productPaymentModelArrayList.size() > 0) {

            //remove the imageUrl using gson exclusionStrategy
            Gson gson = new GsonBuilder().setExclusionStrategies(new PaymentExcluisionStrategy()).create();
            String listString = gson.toJson(productPaymentModelArrayList, new TypeToken<ArrayList<ProductPaymentModel>>() {
            }.getType());

            try {
                return new JSONArray(listString);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    void startWebViewClient(byte[] data) {
        webView.setWebViewClient(new WebPaymentClient());
        webView.getSettings().setJavaScriptEnabled(true);
        pDialog.show();
        webView.postUrl(DealsBookingApi.BOOKING_URL, data);
    }

    public class WebPaymentClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {

            ILog.d(TAG, "|onPageStarted|" + url);

            if (!isRedirected) {
                //Do something you want when starts loading
            }
            isRedirected = false;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            ILog.d(TAG, "|shouldOverrideUrlLoading|" + url);

            if (!ConnectionManager.isConnected(DealsBookingWebview.this)) {
                showAlert(getString(R.string.deals_check_network), "", DealsConstant.INTERNET_FAILED);
            } else {
                view.loadUrl(url);
                isRedirected = true;
                if (!firstLoad && pDialog != null && !pDialog.isShowing() && !isFinishing())
                    pDialog.show();
            }

            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {

            ILog.d(TAG, "|onPageFinished|" + url);
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.dismiss();
                pDialog = null;
                firstLoad = true;
            }
            if (!isRedirected) {
                //http://devfj.billbachao.com/CroptRecoEngine/productPaymentResponse?msg=
                try {
                    Uri uri = Uri.parse(URLDecoder.decode(url, "UTF-8"));

                    if (uri != null) {

                        String lastPathSegment = uri.getLastPathSegment();

                        if (!TextUtils.isEmpty(lastPathSegment) && lastPathSegment.equals(PAYEMNT_RESPONSE_SEGMENT)) {
                            destroyWebView();
                            String msg = uri.getQueryParameter(MSG);

                            if (!TextUtils.isEmpty(msg)) {
                                JSONObject jsonObject = new JSONObject(msg);
                                String status = jsonObject.optString(ApiUtils.STATUS);

                                if (status != null) {
                                    if (status.equalsIgnoreCase(ApiUtils.SUCCESS)) {

                                        String orderId = jsonObject.optString(DealsConstant.KEY_RECEIPT_ORDER_ID);
                                        String transactAmt = jsonObject.optString(DealsConstant.KEY_RECEIPT_ORDER_AMOUNT);

                                        //prepareBundle
                                        UiUtils.launchDealsPaymentReceiptActivity(DealsBookingWebview.this, prepareBundleForReceiptActivity(orderId, transactAmt, jsonObject.optString(ApiUtils.STATUS_DESC)), DealsConstant.REQUEST_PAYMENT_RECEIPT_SUCCESS);
                                        //showAlert(jsonObject.optString(ApiUtils.STATUS_DESC), "", status);
                                    } else if (status.equalsIgnoreCase(ApiUtils.ERROR) || status.equalsIgnoreCase(ApiUtils.FAILURE)) {
                                        //showAlert(getString(R.string.deals_payment_failed), "", status);
                                        UiUtils.launchDealsPaymentReceiptActivity(DealsBookingWebview.this, prepareBundleForReceiptActivity("", "", jsonObject.optString(ApiUtils.STATUS_DESC)), DealsConstant.REQUEST_PAYMENT_RECEIPT_FAILED);
                                    }
                                }
                            }
                        }

                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }


        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            showAlert(getString(R.string.deals_booking_error), "", DealsConstant.INTERNET_FAILED);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == DealsConstant.REQUEST_PAYMENT_RECEIPT_SUCCESS) {
                paymentSuccessfull();
            } else if (requestCode == DealsConstant.REQUEST_PAYMENT_RECEIPT_FAILED) {
                paymentfailed();
            }
            ;
        }
    }

    private void paymentfailed() {
        triggerFailure();
        finish();
    }

    private Bundle prepareBundleForReceiptActivity(String orderId, String transactAmt, String title) {

        Bundle bundle = new Bundle();
        bundle.putString(DealsConstant.KEY_FROM, keyFrom);
        bundle.putString(DealsConstant.KEY_RECEIPT_ORDER_AMOUNT, transactAmt);
        bundle.putString(DealsConstant.KEY_RECEIPT_ORDER_ID, orderId);
        bundle.putString(DealsConstant.KEY_PAYMENT_STATUS_TITLE, title);

        if (getIntent() != null && getIntent().getExtras() != null)
            bundle.putParcelableArrayList(DealsConstant.KEY_PRODUCT_PAYMENT, getProductList(getIntent().getExtras()));

        return bundle;
    }

    void cancelProgress() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    void destroyWebView() {
        if (webView != null)
            webView.destroy();
    }

    @Override
    protected void onDestroy() {
        destroyWebView();
        cancelProgress();
        dismissAlert();
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!ConnectionManager.isConnected(this)) {
            showAlert(getString(R.string.deals_check_network), "", DealsConstant.INTERNET_FAILED);
        }
    }


    void showCancelPaymentAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this).setMessage(R.string.
                payment_cancel_payment).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        }).setNegativeButton(R.string.no, null);
        builder.create().show();
    }

    @Override
    public void onBackPressed() {
        showCancelPaymentAlert();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                showCancelPaymentAlert();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
