package com.app.Billbachao.database;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.app.Billbachao.model.OperatorCircleInfo;

/**
 * Created by mihir on 14-08-2015.
 */
public class STDNumberSeriesDBHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "STDNumberSeries.db";

    public static final int DB_VERSION = 1;

    private static final String TABLE_NAME = "TblStdCodes";
    private static final String ROWID = "id";
    private static final String MOBILE_SERIES = "StdCode";
    private static final String OPERATOR_NAME = "operatorName";
    private static final String OPERATOR_CIRCLE = "CircleName";

    private SQLiteDatabase myDataBase;

    private static STDNumberSeriesDBHelper sInstance;

    public static STDNumberSeriesDBHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new STDNumberSeriesDBHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    /**
     * Constructor
     * Takes and keeps a reference of the passed context in order to access to the application assets and resources.
     *
     * @param context
     */
    STDNumberSeriesDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public void openDataBase() throws SQLException {

        //Open the database
        String myPath = DatabaseUtils.DB_PATH + DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);

    }

    @Override
    public synchronized void close() {

        if (myDataBase != null)
            myDataBase.close();

        super.close();

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public OperatorCircleInfo getOperatorCircleInfo(String number) {
        String series = number.substring(0, 3);
        if (series.charAt(0) == series.charAt(1)) {
            series = series.substring(0, 2);
        }

        Cursor cursor = myDataBase.query(TABLE_NAME, null, MOBILE_SERIES + " like '0" + series +
                "%'", null, null, null, null, null);
        int circleId = -1, operatorId = -1;
        if (cursor.moveToFirst()) {
            String circle = cursor.getString(cursor.getColumnIndex(OPERATOR_CIRCLE));
            String operator = cursor.getString(cursor.getColumnIndex(OPERATOR_NAME));
            if (!circle.equalsIgnoreCase("")) {
                circleId = Integer.parseInt(circle);
            }
            if (!operator.equalsIgnoreCase("")) {
                operatorId = Integer.parseInt(operator);
            }
        }
        cursor.close();
        return new OperatorCircleInfo(operatorId, circleId);
    }


}