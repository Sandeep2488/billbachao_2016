package com.app.Billbachao.recommendations.network.contents;

import android.content.Intent;
import android.content.IntentSender;
import android.location.Address;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.common.permissions.PermissionWrapper;
import com.app.Billbachao.externalsdk.google.AppIndexBehavior;
import com.app.Billbachao.launch.DeepLinkHelper;
import com.app.Billbachao.recommendations.network.tasks.AddressFetchTask;
import com.app.Billbachao.utils.LocationUtils;
import com.app.Billbachao.volley.utils.VolleySingleTon;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

/**
 * Created by mihir on 09-05-2016.
 */
@SuppressWarnings("MissingPermission")
public class NetworkActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, AppIndexBehavior {

    private static final int REQUEST_LOCATION_CHECK_SETTINGS = 100;

    GoogleApiClient mGoogleApiClient;

    LocationRequest mLocationRequestBalancedPowerAccuracy, mLocationRequestHighAccuracy;

    Location mLastLocation;

    LocationManager mLocationManager;

    boolean mGotLocation = false;

    TextView mAddressText;

    NetworkPagerFragment mFragment;

    String mCurrentAddress;

    public static final String TAG = NetworkActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network);
        if (!PermissionWrapper.hasLocationPermissions(this)) {
            showToast(R.string.no_location_permissions);
            finish();
            return;
        }
        initView();
        initLocation();
    }

    void initView() {
        mAddressText = (TextView) findViewById(R.id.address);
        mFragment = new NetworkPagerFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.network_fragment, mFragment)
                .commit();
        showProgressDialog(getString(R.string.fetching_location));
    }

    void initLocation() {
        mLocationRequestBalancedPowerAccuracy = LocationRequest.create().setPriority
                (LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequestHighAccuracy = LocationRequest.create().setPriority(LocationRequest
                .PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequestHighAccuracy)
                .addLocationRequest(mLocationRequestBalancedPowerAccuracy)
                .setAlwaysShow(true);

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder
                            .build());

            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            getLocation();
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                status.startResolutionForResult(NetworkActivity.this, REQUEST_LOCATION_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException e) {
                                e.printStackTrace();
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            noLocationFound();
                            break;
                    }
                }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        VolleySingleTon.getInstance(this).cancelPendingRequests(TAG);
        if (mLocationManager != null) {
            mLocationManager.removeUpdates(this);
        }
        super.onDestroy();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {
        mAddressText.setText("Connection suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        mAddressText.setText("Connection Failed");
    }

    public void getLocation() {
        getLocation(false);
    }

    public void getLocation(boolean byForce) {
        final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0; // 10 meters

        final long MIN_TIME_BW_UPDATES = 0;

        try {
            mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            Location location = null;
            // getting GPS status
            boolean isGPSEnabled = mLocationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            boolean isNetworkEnabled = mLocationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                if (byForce) {
                    noLocationFound();
                    return;
                }
            } else {
                if (isNetworkEnabled) {
                    mLocationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    if (mLocationManager != null) {
                        location = mLocationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            mLastLocation = location;
                        }
                    }
                }

                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location != null) {
                        mLocationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        if (mLocationManager != null) {
                            location = mLocationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                mLastLocation = location;
                            }
                        }
                    }
                }
                if (mLastLocation != null) {
                    mGotLocation = true;
                } else {
                    showProgressDialog();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (mLastLocation != null) {
            fetchAddress();
        }
    }

    void fetchAddress() {
        if (mLastLocation != null) {
            dismissProgressDialog();
            mAddressText.setText("Connected and loading...");
            new AddressFetchTask(this, mAddressCallBacks).execute(mLastLocation.getLatitude(),
                    mLastLocation.getLongitude());
            mFragment.setAddress(mLastLocation);
            LocationUtils.storeLocation(this, mLastLocation);
        }
    }

    AddressFetchTask.CallBacks mAddressCallBacks = new AddressFetchTask.CallBacks() {
        @Override
        public void onAddressReceived(Address address) {
            mCurrentAddress = LocationUtils.getAddressText(address, 1, 3);
            mAddressText.setText(mCurrentAddress);
        }

        @Override
        public void onFailed() {
            mAddressText.setText("Failed to fetch address due to network error");
        }
    };

    @Override
    public void onLocationChanged(Location location) {
        if (!mGotLocation) {
            if (location != null) {
                mLastLocation = location;
                fetchAddress();
                mGotLocation = true;
            }
        }
        if (location != null) {
            mLocationManager.removeUpdates(this);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
        getLocation();
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_LOCATION_CHECK_SETTINGS:
                switch (resultCode) {
                    case RESULT_OK:
                        getLocation();
                        break;
                    case RESULT_CANCELED:
                        getLocation(true);
                        break;
                    default:
                        break;
                }
                break;
        }
    }

    void noLocationFound() {
        showToast(R.string.cant_fetch_location);
        finish();
    }

    public String getCurrentAddress() {
        return mCurrentAddress;
    }

    @Override
    public String getAppIndexTitle() {
        return Keywords.NETWORK;
    }

    @Override
    public String getAppIndexAppendedUrl() {
        return DeepLinkHelper.NETWORK;
    }
}
