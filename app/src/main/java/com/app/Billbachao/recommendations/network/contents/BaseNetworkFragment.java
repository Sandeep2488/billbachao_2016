package com.app.Billbachao.recommendations.network.contents;

import android.app.ProgressDialog;
import android.location.Location;
import android.support.v4.app.Fragment;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.apis.NetworkCardApi;
import com.app.Billbachao.recommendations.network.model.NetworkLocationMap;
import com.app.Billbachao.recommendations.network.model.NetworkRecoCardGsonResponse;
import com.app.Billbachao.volley.request.NetworkGsonRequest;
import com.app.Billbachao.volley.utils.NetworkErrorHelper;
import com.app.Billbachao.volley.utils.VolleySingleTon;

/**
 * Created by mihir on 15-12-2015.
 */
public abstract class BaseNetworkFragment extends Fragment {

    ProgressDialog mProgressDialog;

    void initView(View container) {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage(getString(R.string.fetching_network_details));
    }

    public void setAddress(Location location) {
        showProgressDialog();
        fetchBestNetwork(location);
    }

    void fetchBestNetwork(Location location) {
        NetworkGsonRequest<NetworkRecoCardGsonResponse> networkGsonRequest = new NetworkGsonRequest<>(Request
                .Method.POST, NetworkCardApi.URL, NetworkRecoCardGsonResponse.class, NetworkCardApi
                .getParams(getActivity(), location.getLatitude(), location.getLongitude()), new Response
                .Listener<NetworkRecoCardGsonResponse>() {
            @Override
            public void onResponse(NetworkRecoCardGsonResponse response) {
                dismissProgressDialog();
                if (response.isSuccess()) {
                    NetworkLocationMap locationMap = new NetworkLocationMap(response);
                    networkLocationDataReceived(locationMap);
                } else {
                    finishWithError(response.getStatusDesc());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDialog();
                finishWithError(NetworkErrorHelper.getErrorStatus(error).getErrorMessage());
            }
        }, true);
        VolleySingleTon.getInstance(getActivity()).addToRequestQueue(networkGsonRequest,
                NetworkActivity.TAG);
    }

    abstract void networkLocationDataReceived(NetworkLocationMap locationMap);

    void finishWithError(String error) {
        ((BaseActivity)getActivity()).showToast(error);
        getActivity().finish();
    }

    void showProgressDialog() {
        if (mProgressDialog != null && !mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    void dismissProgressDialog() {
        if (mProgressDialog != null) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
            mProgressDialog = null;
        }
    }

    @Override
    public void onDestroyView() {
        dismissProgressDialog();
        super.onDestroyView();
    }
}
