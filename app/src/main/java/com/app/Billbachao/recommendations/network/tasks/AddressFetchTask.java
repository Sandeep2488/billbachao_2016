package com.app.Billbachao.recommendations.network.tasks;

import android.content.Context;
import android.location.Address;
import android.os.AsyncTask;

import com.app.Billbachao.utils.LocationUtils;

/**
 * Created by mihir on 09-12-2015.
 */
public class AddressFetchTask extends AsyncTask<Double, Integer, Address> {

    Context mContext;

    CallBacks mCallBacks;

    public AddressFetchTask(Context context, CallBacks callBacks) {
        mContext = context;
        mCallBacks = callBacks;
    }

    @Override
    protected Address doInBackground(Double... params) {
        if (params.length < 2)
            return null;

        return LocationUtils.getAddress(mContext, params[0], params[1]);
    }

    @Override
    protected void onPostExecute(Address result) {
        super.onPostExecute(result);
        if (mCallBacks != null) {
            if (result == null) {
                mCallBacks.onFailed();
            } else {
                mCallBacks.onAddressReceived(result);
            }
        }
    }

    public interface CallBacks {
        void onAddressReceived(Address address);
        void onFailed();
    }

}
