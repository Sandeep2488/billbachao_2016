package com.app.Billbachao.recommendations.network.contents.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.recommendations.network.model.NetworkOperatorInfo;

import java.util.ArrayList;

/**
 * Created by mihir on 10-12-2015.
 */
public class BestNetworkAdapter extends BaseExpandableListAdapter {

    Context mContext;

    ArrayList<NetworkOperatorInfo> mNetworkObjects;

    CallBack mCallback;

    static final int RATINGS_COUNT = NetworkOperatorInfo.RATING_TAGS.length;

    int mCurrentOperatorColor, mNormalOperatorColor;

    public BestNetworkAdapter(Context context, ArrayList<NetworkOperatorInfo> networkObjects, CallBack callBack) {
        mContext = context;
        mNetworkObjects = networkObjects;
        mCallback = callBack;
        initResources();
    }

    void initResources() {
        mCurrentOperatorColor = ContextCompat.getColor(mContext, R.color.colorAccent);
        mNormalOperatorColor = ContextCompat.getColor(mContext, R.color.dividerColor);
    }

    @Override
    public int getGroupCount() {
        return mNetworkObjects.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupPosition;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup
            parent) {
        if (convertView == null) {
            convertView = View.inflate(mContext, R.layout.network_group_view, null);
            GroupViewHolder holder = new GroupViewHolder(convertView);
            convertView.setTag(holder);
        }
        GroupViewHolder holder = (GroupViewHolder) convertView.getTag();
        NetworkOperatorInfo networkObject = mNetworkObjects.get(groupPosition);

        holder.signalStrength.setText(networkObject.getSignalStrength());
        holder.dataSpeed.setText(String.valueOf(networkObject.getDataSpeed()));
        holder.ratings.setText(String.valueOf(networkObject.getOverallRating()));
        holder.operator.setText(networkObject.getOperatorName());
        holder.operatorMarker.setBackgroundColor(networkObject.isSameNetwork() ? mCurrentOperatorColor :
                mNormalOperatorColor);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View
            convertView, ViewGroup parent) {
        return null;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    class GroupViewHolder {
        ImageView dropDown;
        TextView testSpeed, ratings, dataSpeed, signalStrength, operator, savingsPercent;
        Button requestMnp, rateOperator;
        View operatorMarker;

        public GroupViewHolder(View view) {
            operator = (TextView) view.findViewById(R.id.operator);
            dropDown = (ImageView) view.findViewById(R.id.drop_down);
            testSpeed = (TextView) view.findViewById(R.id.test_speed);
            dataSpeed = (TextView) view.findViewById(R.id.data_speed);
            signalStrength = (TextView) view.findViewById(R.id.signal_strength);
            ratings = (TextView) view.findViewById(R.id.rating);
            requestMnp = (Button) view.findViewById(R.id.request_mnp);
            rateOperator = (Button) view.findViewById(R.id.rate_operator);
            savingsPercent = (TextView) view.findViewById(R.id.savings_percent);
            operatorMarker = view.findViewById(R.id.operator_marker);
        }
    }

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.rate_operator:
                    if (mCallback != null) {
                        mCallback.onRateOperatorRequested();
                    }
                    break;
                case R.id.request_mnp:
                    if (mCallback != null) {
                        mCallback.onMnpRequested();
                    }
                    break;
                case R.id.test_speed:
                    //UiUtils.launchSpeedTest(mContext);
                    break;
            }
        }
    };

    public interface CallBack {
        void onMnpRequested();
        void onRateOperatorRequested();
    }
}
