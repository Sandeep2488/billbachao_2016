package com.app.Billbachao.recommendations.network.contents;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.recommendations.network.contents.adapters.BestNetworkAdapter;
import com.app.Billbachao.recommendations.network.contents.adapters.CurrentNetworkAdapter;
import com.app.Billbachao.recommendations.network.model.NetworkLocationFragmentInfo;
import com.app.Billbachao.recommendations.network.model.NetworkLocationMap;
import com.app.Billbachao.recommendations.network.tasks.AddressFetchTask;
import com.app.Billbachao.selfhelp.contents.dialogs.MNPPopupDialog;
import com.app.Billbachao.utils.LocationUtils;
import com.app.Billbachao.utils.USSDUtils;
import com.app.Billbachao.utils.UiUtils;

/**
 * Created by mihir on 15-12-2015.
 */
public class NetworkLocationFragment extends Fragment {

    public static final String NETWORK_OBJECT = "networkObject";

    NetworkLocationFragmentInfo mData;

    int mType;

    ExpandableListView mListView;

    TextView mAddressText;

    BestNetworkAdapter mAdapter;

    public static NetworkLocationFragment getInstance(NetworkLocationFragmentInfo locationObject) {
        NetworkLocationFragment locationFragment = new NetworkLocationFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(NETWORK_OBJECT, locationObject);
        locationFragment.setArguments(bundle);
        return locationFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View containerView = inflater.inflate(R.layout.fragment_network, container, false);
        initView(containerView);
        return containerView;
    }

    void initView(View container) {
        mListView = (ExpandableListView) container.findViewById(R.id.network_list);
        mAddressText = (TextView) container.findViewById(R.id.address);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mData = getArguments().getParcelable(NETWORK_OBJECT);
        mType = mData.getType();
        if (mType == NetworkLocationMap.CURRENT_TYPE) {
            mAdapter = new CurrentNetworkAdapter(getActivity(), mData.getNetworkList(), mAdapterCallback);
        } else {
            mAdapter = new BestNetworkAdapter(getActivity(), mData.getNetworkList(), mAdapterCallback);
        }
        mListView.setAdapter(mAdapter);

        String address = "";
        if (mType == NetworkLocationMap.CURRENT_TYPE) {
            if (getActivity() instanceof NetworkActivity) {
                address = ((NetworkActivity) getActivity()).getCurrentAddress();
            }
        } else {
            address = mData.getAddress();
            getAddressFromLocation();
        }
        if (!(address == null || address.isEmpty())) {
            mAddressText.setText(address);
            mAddressText.setVisibility(View.VISIBLE);
        }
    }

    BestNetworkAdapter.CallBack mAdapterCallback = new BestNetworkAdapter.CallBack() {
        @Override
        public void onMnpRequested() {
            MNPPopupDialog dialog = MNPPopupDialog.getInstance(NetworkLocationFragment.this);
            dialog.show(getFragmentManager(), MNPPopupDialog.TAG);
        }

        @Override
        public void onRateOperatorRequested() {
            // TODO events
            UiUtils.launchRateOperator(getActivity());
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case MNPPopupDialog.REQUEST_MNP:
                if (resultCode == Activity.RESULT_OK) {
                    if (getActivity() != null) {
                        // TODO events
                        USSDUtils.requestMnp(getActivity());
                    }
                }
                break;
            // TODO rate operator
            /*case RateOperator.REQUEST_RATE_OPERATOR:
                if (resultCode == Activity.RESULT_OK) {
                    if (getActivity() != null) {
                        ((AppBaseCompatActivity) getActivity()).showRateDialog();
                    }
                }
                break;*/
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    void getAddressFromLocation() {
        if (mData.getLatitude() != 0 && mData.getLongitude() != 0) {
            new AddressFetchTask(getActivity(), mCallBacks).execute(mData.getLatitude(), mData.getLongitude());
        }
    }

    AddressFetchTask.CallBacks mCallBacks = new AddressFetchTask.CallBacks() {
        @Override
        public void onAddressReceived(Address address) {
            mAddressText.setText(LocationUtils.getAddressText(address, 1, 3));
            mAddressText.setVisibility(View.VISIBLE);
        }

        @Override
        public void onFailed() {

        }
    };
}
