package com.app.Billbachao.recommendations.network.model;

import com.app.Billbachao.R;
import com.google.myjson.annotations.SerializedName;

/**
 * Created by mihir on 09-05-2016.
 */
public class NetworkOperatorInfo {

    @SerializedName("circle")
    String circle;

    @SerializedName("opname")
    String operatorName;

    @SerializedName("overall_rating")
    int overallRating;

    @SerializedName("ss_rating")
    int ssRating;

    @SerializedName("ds_rating")
    int dsRating;

    @SerializedName("cc_rating")
    int ccRating;

    @SerializedName("valuemoney_rating")
    int valueRating;

    @SerializedName("signStr")
    String signalStrength;

    @SerializedName("ssPercent")
    String ssPercent;

    @SerializedName("is3G")
    String is3g;

    @SerializedName("dataspeed")
    int dataSpeed;

    @SerializedName("current_op")
    String currentOp;

    public int getOverallRating() {
        return overallRating;
    }

    public String getSignalStrength() {
        return signalStrength;
    }

    public int getDataSpeed() {
        return dataSpeed;
    }

    public boolean isSameNetwork() {
        return currentOp != null && currentOp.equalsIgnoreCase("y");
    }

    public String getOperatorName() {
        return operatorName;
    }

    public int getSsRating() {
        return ssRating;
    }

    public int getDsRating() {
        return dsRating;
    }

    public int getCcRating() {
        return ccRating;
    }

    public int getValueRating() {
        return valueRating;
    }

    public static final int SS_RATING = 1, DS_RATING = 2, VALUE_RATING = 3, CC_RATING = 4;

    public static final int RATING_TAGS[] = new int[]{CC_RATING, DS_RATING, VALUE_RATING, SS_RATING};

    public static int getTitleResId(int ratingTag) {
        switch (ratingTag) {
            case VALUE_RATING:
                return R.string.rate_operator_value_for_money_lable;
            case DS_RATING:
                return R.string.rate_operator_data_lable;
            case SS_RATING:
                return R.string.rate_operator_customer_service_lable;
            case CC_RATING:
            default:
                return R.string.rate_operator_call_connectivity;
        }
    }

    public int getRatingValue (int ratingTag) {
        switch (ratingTag) {
            case VALUE_RATING:
                return valueRating;
            case DS_RATING:
                return dsRating;
            case SS_RATING:
                return ssRating;
            case CC_RATING:
            default:
                return ccRating;
        }
    }

}
