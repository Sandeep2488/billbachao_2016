package com.app.Billbachao.recommendations.network.contents;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.Billbachao.R;
import com.app.Billbachao.externalsdk.AppsFlyerUtils;
import com.app.Billbachao.recommendations.network.model.NetworkLocationFragmentInfo;
import com.app.Billbachao.recommendations.network.model.NetworkLocationMap;

/**
 * Created by mihir on 15-12-2015.
 */
public class NetworkPagerFragment extends BaseNetworkFragment {

    TabLayout mTabLayout;

    ViewPager mPager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View containerView = inflater.inflate(R.layout.network_location_fragment, container, false);
        initView(containerView);
        return containerView;
    }

    @Override
    void initView(View container) {
        super.initView(container);
        mPager = (ViewPager) container.findViewById(R.id.pager);
        mTabLayout = (TabLayout) container.findViewById(R.id.tabs);
    }

    @Override
    void networkLocationDataReceived(NetworkLocationMap locationMap) {
        CustomPagerAdapter adapter = new CustomPagerAdapter(getFragmentManager(), locationMap);
        mPager.setAdapter(adapter);
        mTabLayout.setupWithViewPager(mPager);
        AppsFlyerUtils.trackEvent(getActivity(), AppsFlyerUtils.MY_BEST_NETWORK);
    }


    class CustomPagerAdapter extends FragmentPagerAdapter {

        NetworkLocationMap mNetworkLocationMap = null;

        public CustomPagerAdapter(FragmentManager fm, NetworkLocationMap locationMap) {
            super(fm);
            mNetworkLocationMap = locationMap;
        }

        @Override
        public Fragment getItem(int position) {
            NetworkLocationFragmentInfo locationObject = mNetworkLocationMap.get(position);
            return NetworkLocationFragment.getInstance(locationObject);
        }

        @Override
        public int getCount() {
            return mNetworkLocationMap.getSize();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mNetworkLocationMap.get(position).getTitle();
        }
    }
}
