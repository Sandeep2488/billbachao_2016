package com.app.Billbachao.recommendations.network.utils;

import com.app.Billbachao.R;

/**
 * Created by mihir on 11-05-2016.
 */
public class OperatorRateUtils {

    public static final int DATA_RATING = 0, VALUE_RATING = 1, CALL_RATING = 2, CARE_RATING = 3;

    public static final int TAGS[] = new int[]{DATA_RATING, VALUE_RATING, CALL_RATING, CARE_RATING};

    public static int getParentId(int tag) {
        switch (tag) {
            case DATA_RATING:
                return R.id.data_rating;
            case CALL_RATING:
                return R.id.call_rating;
            case CARE_RATING:
                return R.id.customer_rating;
            case VALUE_RATING:
            default:
                return R.id.value_rating;
        }
    }


    public static int getIconId(int tag) {
        switch (tag) {
            case DATA_RATING:
                return R.drawable.icn_data_rate;
            case CALL_RATING:
                return R.drawable.icn_call;
            case CARE_RATING:
                return R.drawable.icn_customer_care;
            case VALUE_RATING:
            default:
                return R.drawable.icn_money;
        }
    }

    public static int getTextId(int tag) {
        switch (tag) {
            case DATA_RATING:
                return R.string.user_ratings_data;
            case CALL_RATING:
                return R.string.user_ratings_call_connectivity;
            case CARE_RATING:
                return R.string.user_ratings_customer_service;
            case VALUE_RATING:
            default:
                return R.string.user_ratings_value_of_money;
        }
    }


}
