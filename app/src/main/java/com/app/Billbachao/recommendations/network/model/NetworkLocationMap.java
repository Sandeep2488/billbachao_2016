package com.app.Billbachao.recommendations.network.model;

import java.util.ArrayList;

/**
 * Created by mihir on 09-05-2016.
 */
public class NetworkLocationMap {

    ArrayList<NetworkLocationFragmentInfo> locationObjectList;

    public NetworkLocationMap() {
        locationObjectList = new ArrayList<>();
    }

    public void addNetworkLocationObject(NetworkLocationFragmentInfo locationObject) {
        locationObjectList.add(locationObject);
    }

    public NetworkLocationFragmentInfo get(int index) {
        return locationObjectList.get(index);
    }

    public int getSize() {
        return locationObjectList == null ? 0 : locationObjectList.size();
    }

    public NetworkLocationMap(NetworkRecoCardGsonResponse response) {
        locationObjectList = new ArrayList<>();
        if (response.currentNetworkData != null && !response.currentNetworkData.isEmpty()) {
            addNetworkInfo(response.currentNetworkData, CURRENT_TYPE, 0, 0);
        }
        if (response.homeNetworkData != null && !response.homeNetworkData.isEmpty()) {
            addNetworkInfo(response.homeNetworkData, HOME_TYPE, response.homeLatitude, response.homeLongitude);
        }
        if (response.workNetworkData != null && !response.workNetworkData.isEmpty()) {
            addNetworkInfo(response.workNetworkData, WORK_TYPE, response.workLatitude,
                    response.workLongitude);
        }
    }

    void addNetworkInfo(ArrayList<NetworkOperatorInfo> networkOperatorInfos, int type, double
            latitude, double longitude) {
        int index = -1;
        for (int i = 0; i < LOCATION_TYPES.length; i++) {
            if (LOCATION_TYPES[i] == type) {
                index = i;
                break;
            }
        }
        String title = CURRENT;
        if (index != -1)
            title = LOCATION_TAGS[index];
        NetworkLocationFragmentInfo networkLocationFragmentInfo = new NetworkLocationFragmentInfo
                (networkOperatorInfos, type, title, latitude, longitude);
        locationObjectList.add(networkLocationFragmentInfo);
    }

    public static final int CURRENT_TYPE = 1, WORK_TYPE = 3, HOME_TYPE = 2;

    static final String HOME = "HOME", WORK = "WORK", CURRENT = "CURRENT";

    public static final int LOCATION_TYPES[] = {CURRENT_TYPE, HOME_TYPE, WORK_TYPE};

    public static final String LOCATION_TAGS[] = {CURRENT, HOME, WORK};

}
