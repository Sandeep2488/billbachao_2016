package com.app.Billbachao.recommendations.network.contents;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.apis.RateOperatorApi;
import com.app.Billbachao.recommendations.network.model.RateOperatorGsonResponse;
import com.app.Billbachao.recommendations.network.utils.OperatorRateUtils;
import com.app.Billbachao.utils.LocationUtils;
import com.app.Billbachao.utils.OperatorUtils;
import com.app.Billbachao.volley.request.NetworkGsonRequest;
import com.app.Billbachao.volley.utils.NetworkErrorHelper;
import com.app.Billbachao.volley.utils.VolleySingleTon;

/**
 * Created by mihir on 11-05-2016.
 */
public class RateOperatorActivity extends BaseActivity {

    RatingBar[] ratingBars = new RatingBar[OperatorRateUtils.TAGS.length];

    double longitude, latitude;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rate_operator_activity);
        initView();
    }

    private void initView() {
        ((TextView) findViewById(R.id.rate_operator_heading)).setText(getString(R.string
                .rate_operator_heading_lable, OperatorUtils.getOperatorName(this)));
        for (int tag : OperatorRateUtils.TAGS) {
            initRatingGroup(tag);
        }
        findViewById(R.id.rate_operator).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rateOperator();
            }
        });
    }

    private void initRatingGroup(int tag) {
        View view = findViewById(OperatorRateUtils.getParentId(tag));
        ratingBars[tag] = (RatingBar) view.findViewById(R.id.rate_operator_bar);
        ((ImageView) view.findViewById(R.id.rate_operator_icon)).setImageResource
                (OperatorRateUtils.getIconId(tag));
        ((TextView) view.findViewById(R.id.rate_operator_text)).setText(OperatorRateUtils.getTextId(tag));
    }

    private void getLocation() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        latitude = preferences.getFloat(LocationUtils.LAST_LATITUDE, 0);
        longitude = preferences.getFloat(LocationUtils.LAST_LONGITUDE, 0);
    }

    private boolean isFormFilled() {
        for (RatingBar ratingBar : ratingBars)
            if (ratingBar.getRating() == 0)
                return false;
        return true;
    }

    private void rateOperator() {
        if (!isFormFilled()) {
            showSnack("Please give ratings for all categories");
            return;
        }

        NetworkGsonRequest<RateOperatorGsonResponse> operatorRateRequest = new NetworkGsonRequest<>(Request
                .Method.POST, RateOperatorApi.URL, RateOperatorGsonResponse.class,
                RateOperatorApi.getParams(this, latitude, longitude, getRating(0), getRating(1),
                        getRating(2), getRating(3)),
                new Response.Listener<RateOperatorGsonResponse>() {
                    @Override
                    public void onResponse(RateOperatorGsonResponse response) {
                        if (response.isSuccess()) {
                            showToast(getString(R.string.rating_success_msg));
                            ratingDone();
                        } else {
                            showToast(getString(R.string.operator_rating_error));
                            ratingDone();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showToast(NetworkErrorHelper.getErrorStatus(error).getErrorMessage());
                ratingDone();
            }
        });
        VolleySingleTon.getInstance(this).addToRequestQueue(operatorRateRequest);
        showProgressDialog(getString(R.string.operator_rating_loading));
    }

    private int getRating(int tag) {
        return ratingBars[tag] == null ? 0 : (int) ratingBars[tag].getRating();
    }

    private void ratingDone() {
        dismissProgressDialog();
        finish();
    }
}
