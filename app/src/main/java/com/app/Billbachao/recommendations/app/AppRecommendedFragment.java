package com.app.Billbachao.recommendations.app;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.Billbachao.R;
import com.app.Billbachao.recommendations.app.model.RecommendedAppDetails;
import com.app.Billbachao.utils.ILog;
import com.squareup.picasso.Picasso;

/**
 * Created by mihir on 16-12-2015.
 */
public class AppRecommendedFragment extends Fragment {

    public static final String TAG = AppRecommendedFragment.class.getSimpleName();

    RecommendedAppDetails mAppDetails;

    public static final String APP_DETAILS = "app_details";

    private TextView mTitleText, mDetailsText, mRatingText;

    private ImageView mAppImage;

    private Button mGotoApp;

    public static AppRecommendedFragment getInstance(RecommendedAppDetails appDetails) {
        AppRecommendedFragment recommendedFragment = new AppRecommendedFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(APP_DETAILS, appDetails);
        recommendedFragment.setArguments(bundle);
        return recommendedFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_app_recommend, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null) {
            mAppDetails = getArguments().getParcelable(APP_DETAILS);
        }
        loadData();
    }

    void initView(View container) {
        mTitleText = (TextView) container.findViewById(R.id.title);
        mRatingText = (TextView) container.findViewById(R.id.rating);
        mDetailsText = (TextView) container.findViewById(R.id.details);
        mDetailsText.setMovementMethod(new ScrollingMovementMethod());
        mAppImage = (ImageView) container.findViewById(R.id.image);
        mGotoApp = (Button) container.findViewById(R.id.go_to_app);
        mGotoApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAppDetails == null || mAppDetails.getAppUrl() == null) {
                    Toast.makeText(getActivity(), R.string.app_url_not_found, Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(mAppDetails.getAppUrl()));
                startActivity(intent);
            }
        });
    }

    void loadData() {
        if (mAppDetails == null) {
            return;
        }
        mTitleText.setText(mAppDetails.getTitle());
        mRatingText.setText(String.valueOf(mAppDetails.getRating()));
        mDetailsText.setText(mAppDetails.getDetails());
        String imageUrl = mAppDetails.getImageUrl();
        ILog.d(TAG, "Image Url " + imageUrl);
        if (imageUrl != null) {
            Picasso.with(getActivity()).load(Uri.parse(imageUrl)).placeholder(R.drawable.ic_apps_large).into(mAppImage);
        }
        // TODO set date
    }

    public void updateAppDetails(RecommendedAppDetails appDetails) {
        mAppDetails = appDetails;
        loadData();
    }
}
