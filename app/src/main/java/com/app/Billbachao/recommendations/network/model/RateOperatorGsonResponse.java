package com.app.Billbachao.recommendations.network.model;

import com.app.Billbachao.model.BaseGsonResponse;

/**
 * Created by mihir on 11-05-2016.
 */
public class RateOperatorGsonResponse extends BaseGsonResponse {

    @Override
    public boolean isSuccess() {
        return status.equalsIgnoreCase("Y") || super.isSuccess();
    }
}
