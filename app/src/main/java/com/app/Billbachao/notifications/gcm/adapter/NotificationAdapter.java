package com.app.Billbachao.notifications.gcm.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListPopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.app.Billbachao.R;
import com.app.Billbachao.apis.ApiUtils;
import com.app.Billbachao.deals.fragment.DealsAddressListingFragment;
import com.app.Billbachao.deals.i.IDealAddressCallback;
import com.app.Billbachao.deals.tasks.DealsAddressCRUDTask;
import com.app.Billbachao.deals.view.PopupUtils;
import com.app.Billbachao.notifications.gcm.db.NotificationDataSource;
import com.app.Billbachao.notifications.gcm.interfaces.INotificationItemClick;
import com.app.Billbachao.notifications.gcm.model.NotificationModel;
import com.app.Billbachao.notifications.gcm.utils.NotificationConstant;
import com.app.Billbachao.utils.UiUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Notification Adapter for inbox activity
 * Created by nitesh on 27-04-2016.
 */
public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {

    private final List<NotificationModel> mList;
    private final LayoutInflater mInflater;
    private final Context mContext;
    private INotificationItemClick onItemClickListener;
    private INotificationDeleted iNotificationDeleted;

    public NotificationAdapter(Context context, List<NotificationModel> notificationModelList) {
        mContext = context;
        mList = notificationModelList;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NotificationViewHolder(mInflater.inflate(R.layout.view_row_inbox_notification, parent, false));
    }

    @Override
    public void onBindViewHolder(NotificationViewHolder holder, final int position) {
        final NotificationModel model = mList.get(position);

        holder.title.setText(model.gcm_title);
        holder.desc.setText(model.gcm_description);
        holder.createDateTv.setText(model.gcm_time_ms);

        String imageUrl = model.gcm_image_url;

        if (!TextUtils.isEmpty(imageUrl)) {
            holder.inboxIv.setVisibility(View.VISIBLE);
            Picasso.with(mContext).load(imageUrl).fit().into(holder.inboxIv);
        } else {
            holder.inboxIv.setVisibility(View.GONE);
        }

        if (model.gcm_is_read.equals(NotificationConstant.UNREAD)) {
            holder.updateWidgets(ContextCompat.getColor(mContext, android.R.color.secondary_text_light));
        } else {
            holder.updateWidgets(ContextCompat.getColor(mContext, android.R.color.darker_gray));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.notificationClick(position, mList.get(position));
            }
        });

        holder.inBoxPopupWindowImageView.setTag(position);

    }


    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public void setOnItemClickListener(INotificationItemClick onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setOnNotificationDeleted(INotificationDeleted iNotificationDeleted) {
        this.iNotificationDeleted = iNotificationDeleted;
    }


    public class NotificationViewHolder extends RecyclerView.ViewHolder {

        public final TextView title;
        public final TextView desc;
        public final TextView createDateTv;
        public final ImageView inboxIv;
        public final ImageView inBoxPopupWindowImageView;


        public NotificationViewHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.inboxTitleTv);
            desc = (TextView) itemView.findViewById(R.id.inboxDescTv);
            createDateTv = (TextView) itemView.findViewById(R.id.createDateTv);
            inboxIv = (ImageView) itemView.findViewById(R.id.inboxIv);
            inBoxPopupWindowImageView = (ImageView)itemView.findViewById(R.id.inBoxPopupWindowImageView);
            inBoxPopupWindowImageView.setOnClickListener(popUpWindowDeals);
        }

        public void updateWidgets(int color) {
            title.setTextColor(color);
            desc.setTextColor(color);
            createDateTv.setTextColor(color);
        }
    }


    View.OnClickListener popUpWindowDeals = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ArrayList<String> arrayList = new ArrayList<>();
            arrayList.add(mContext.getString(R.string.deals_delete));

            int parentPosition = (int) v.getTag();

            ArrayAdapter<String> adapter = new ArrayAdapter<>(mContext, android.R.layout.simple_list_item_1, arrayList);
            ListPopupWindow listPopupWindow = PopupUtils.showPopUp(mContext, v, adapter);
            listPopupWindow.setOnItemClickListener(new OnItemClickListWindowInBox(parentPosition, listPopupWindow));
            listPopupWindow.show();
        }
    };


    public class OnItemClickListWindowInBox implements AdapterView.OnItemClickListener {

        public OnItemClickListWindowInBox(int parentPosition, ListPopupWindow listPopupWindow) {
            this.parentPosition = parentPosition;
            this.listPopupWindow = listPopupWindow;
        }

        private int parentPosition;
        private ListPopupWindow listPopupWindow;

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Object o = parent.getAdapter().getItem(position);

            if (o != null) {

                listPopupWindow.dismiss();
                String result = (String) o;

                if (result.equalsIgnoreCase(mContext.getString(R.string.deals_delete))) {
                    showAlertDialogToDelete(parentPosition);
                }
            }
        }
    }


    private void showAlertDialogToDelete(final int position) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setCancelable(false);
        alertDialog.setNegativeButton(mContext.getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.setMessage(mContext.getString(R.string.inbox_delete_msg));
        alertDialog.setPositiveButton(mContext.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int deletedRow = new NotificationDataSource(mContext).delete(mList.get(position)._id);
                if (deletedRow > 0){
                    mList.remove(position);
                    notifyDataSetChanged();
                    showToast(mContext.getString(R.string.inbox_msg_deleted));

                    if (iNotificationDeleted != null){
                        iNotificationDeleted.notificationDeleted(mList.size());
                    }
                }
            }
        });

        AlertDialog createdDialog = alertDialog.create();

        if (createdDialog != null)
            alertDialog.show();
    }

    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    public interface INotificationDeleted {
        void notificationDeleted(int listSize);
    }
}
