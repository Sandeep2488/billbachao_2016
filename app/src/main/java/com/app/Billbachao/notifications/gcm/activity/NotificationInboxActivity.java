package com.app.Billbachao.notifications.gcm.activity;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.launch.DeepLinkHelper;
import com.app.Billbachao.notifications.gcm.adapter.NotificationAdapter;
import com.app.Billbachao.notifications.gcm.interfaces.INotificationItemClick;
import com.app.Billbachao.notifications.gcm.loaders.NotificationListLoaders;
import com.app.Billbachao.notifications.gcm.model.NotificationModel;
import com.app.Billbachao.notifications.gcm.utils.NotificationConstant;
import com.app.Billbachao.notifications.gcm.utils.NotificationHelper;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.widget.view.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * Notification Activity to show all the notifications send.
 */
public class NotificationInboxActivity extends BaseActivity implements LoaderManager.LoaderCallbacks<List<NotificationModel>>, INotificationItemClick, NotificationAdapter.INotificationDeleted {

    private static final int LOADER_ID = 999;

    private static final String TAG = NotificationInboxActivity.class.getSimpleName();

    private RecyclerView mInBoxRecyclerView;

    private TextView mNoNotificationTv;

    private NotificationAdapter mNotificationAdapter;

    private List<NotificationModel> mNotificationList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_inbox);

        setTitle(R.string.inbox_title);

        initView();

        initAdapter();

        initLoaders();

    }

    private void initLoaders() {
        getSupportLoaderManager().initLoader(LOADER_ID, null, this).forceLoad();
    }


    private void initAdapter() {
        mNotificationAdapter = new NotificationAdapter(this, mNotificationList);
        mInBoxRecyclerView.setAdapter(mNotificationAdapter);
        mNotificationAdapter.setOnNotificationDeleted(this);

        mNotificationAdapter.setOnItemClickListener(this);
    }

    private void initView() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        mInBoxRecyclerView = (RecyclerView) findViewById(R.id.inboxRecyclerView);
        mInBoxRecyclerView.setLayoutManager(linearLayoutManager);
        mInBoxRecyclerView.addItemDecoration(new DividerItemDecoration(getResources().getDimensionPixelSize(R.dimen.large_widget_spacing)));

        mNoNotificationTv = (TextView) findViewById(R.id.noNotificationTv);
    }

    @Override
    public Loader<List<NotificationModel>> onCreateLoader(int id, Bundle args) {
        return new NotificationListLoaders(this);
    }

    @Override
    public void onLoadFinished(Loader<List<NotificationModel>> loader, List<NotificationModel> data) {

        if (data != null && data.size() > 0) {
            mInBoxRecyclerView.setVisibility(View.VISIBLE);
            mNoNotificationTv.setVisibility(View.GONE);

            mNotificationList.clear();
            mNotificationList.addAll(data);
            mNotificationAdapter.notifyDataSetChanged();
        } else {
            noNotification();
        }
    }

    private void noNotification() {
        mInBoxRecyclerView.setVisibility(View.GONE);
        mNoNotificationTv.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLoaderReset(Loader<List<NotificationModel>> loader) {
    }

    @Override
    public void notificationClick(int position, NotificationModel model) {

        if (model == null)
            return;

        String deepLink = model.gcm_deep_link;

        if (!TextUtils.isEmpty(deepLink)) {
            Uri uri = Uri.parse(deepLink);
            if (uri != null && uri.getHost().equalsIgnoreCase(getString(R.string.app_host))) {
                String prefix = uri.getPathSegments().get(0);
                ILog.d(TAG, "Deep link Prefix | " + prefix);
                DeepLinkHelper.handleDeepLink(this, prefix, uri,  null);
            }
        }


        //update the notification as read
        if (model.gcm_is_read.equalsIgnoreCase(NotificationConstant.UNREAD)){
            model.gcm_is_read = NotificationConstant.READ;
            mNotificationAdapter.notifyDataSetChanged();
            new NotificationHelper(this).updateNotificationStatus(model);
        }

    }

    @Override
    protected boolean isCartItemSupported() {
        return false;
    }

    @Override
    public void notificationDeleted(int listSize) {
        if(listSize == 0){
            noNotification();
        }

    }
}
