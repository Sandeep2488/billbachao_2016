package com.app.Billbachao.notifications.gcm.task;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import com.app.Billbachao.apis.ApiUtils;
import com.app.Billbachao.notifications.gcm.interfaces.IClevertapConstant;
import com.app.Billbachao.notifications.gcm.model.NotificationModel;
import com.app.Billbachao.notifications.gcm.utils.NotificationConstant;
import com.app.Billbachao.notifications.gcm.utils.NotificationHelper;
import com.app.Billbachao.utils.ILog;

/**
 * Prepare Notification Model using payload.
 * Created by nitesh on 11-05-2016.
 */
public class PrepareNotificationModel {

    private static final String TAG = PrepareNotificationModel.class.getSimpleName();
    private Context context;
    public PrepareNotificationModel(Context context){
        this.context = context;
    }

    /**
     * Prepare Notification Model for Billbachao payload
     * @param extras
     * @return
     */
    public NotificationModel getBBNotificationModel(Bundle extras) {

        //title
        String title = extras.getString(NotificationConstant.BB_GCM_TITLE);

        if (TextUtils.isEmpty(title)) {
            title = context.getApplicationInfo().name;
            extras.putString(NotificationConstant.BB_GCM_TITLE, "");
        }

        //description
        String desc = extras.getString(NotificationConstant.BB_GCM_DESC);

        if (TextUtils.isEmpty(desc))
            return null;

        //DeepLink
        String deep_link = extras.getString(NotificationConstant.BB_GCM_DEEP_LINK);
        ILog.d(TAG, "|DeepLink|" + deep_link);

        //response callback
        String gcm_response_callback = extras.getString(NotificationConstant.BB_GCM_RESPONSE_CALLBACK);

        //expiry
        String gcm_expiry = extras.getString(NotificationConstant.BB_GCM_EXPIRY);

        if (TextUtils.isEmpty(gcm_expiry))
            gcm_expiry = NotificationConstant.BB_DEFAULT_EXPIRY;

        //imageUrl
        final String bigPictureUrl = extras.getString(NotificationConstant.BB_GCM_IMAGE_URL);

        //notification Id
        final String notificationId = extras.getString(NotificationConstant.BB_NOTICATION_ID);

        //gcm_category
        final String gcm_category = extras.getString(NotificationConstant.BB_GCM_CATEGORY);

        //gcm_api url
        String gcm_api_url = extras.getString(NotificationConstant.BB_GCM_API_URL);

        if (TextUtils.isEmpty(gcm_api_url))
            gcm_api_url = ApiUtils.NA;

        //gcm_is_silent.
        String gcm_is_silent = extras.getString(NotificationConstant.BB_GCM_IS_SILENT);
        if (TextUtils.isEmpty(gcm_is_silent) || gcm_is_silent.equalsIgnoreCase(ApiUtils.NA))
            gcm_is_silent = NotificationConstant.NOT_SILENT;

        NotificationModel model = new NotificationModel(notificationId, title, desc, deep_link, bigPictureUrl, NotificationConstant.BB_SOURCE, gcm_response_callback, gcm_expiry, NotificationConstant.UNREAD, NotificationHelper.getCurrentDate(), gcm_category, gcm_api_url, gcm_is_silent);
        return model;
    }


    /**
     * Prepare NotificationModel for clevertap
     * @param extras
     * @return
     */
    public NotificationModel getCleverTapNotificationModel(Bundle extras){
        String gcm_title = extras.getString(IClevertapConstant.TITLE);

        //title
        if (TextUtils.isEmpty(gcm_title)) {
            gcm_title = context.getApplicationInfo().name;
            extras.putString(NotificationConstant.BB_GCM_TITLE, "");
        }

        ILog.d(TAG, "|CT notifTitle| " + gcm_title);

        //description
        String gcm_description = extras.getString(IClevertapConstant.DESCRIPTION);
        if (TextUtils.isEmpty(gcm_description)) {
            return null;
        }

        //image Url
        String bigPictureUrl = extras.getString(IClevertapConstant.IMAGE_URL);

        ILog.d(TAG, "|CT bigPictureUrl|" + bigPictureUrl);

        //DeepLinking
        String gcm_deep_link = extras.getString(IClevertapConstant.DEEP_LINK);

        //source
        String gcm_source = "clevertap";


        //gcm expiry
        String gcm_expiry = extras.getString(IClevertapConstant.GCM_EXIPIRY);
        if (TextUtils.isEmpty(gcm_expiry)) {
            gcm_expiry = NotificationConstant.BB_DEFAULT_EXPIRY;
        }

        //create model and insert value
        NotificationModel model = new NotificationModel(ApiUtils.NA, gcm_title, gcm_description,
                gcm_deep_link, bigPictureUrl,
                gcm_source, "0",
                gcm_expiry, NotificationConstant.UNREAD,
                NotificationHelper.getCurrentDate(), ApiUtils.NA, ApiUtils.NA, NotificationConstant.NOT_SILENT);

        return model;
    }
}
