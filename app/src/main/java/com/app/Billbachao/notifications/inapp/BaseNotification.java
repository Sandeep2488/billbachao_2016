package com.app.Billbachao.notifications.inapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

/**
 * Created by mihir on 20-11-2015.
 */
public abstract class BaseNotification {

    protected Context mContext;

    protected int mIntentId = 0;

    BaseNotification(Context context) {
        mContext = context;
    }

    public void setIntentId(int intentId) {
        mIntentId = intentId;
    }

    protected abstract NotificationCompat.Builder getNotificationBuilder();

    public void show() {
        Notification notification = getNotificationBuilder().build();
        NotificationManager manager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(mIntentId, notification);
    }
}
