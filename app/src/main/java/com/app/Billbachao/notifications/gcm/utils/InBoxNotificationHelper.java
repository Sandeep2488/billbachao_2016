package com.app.Billbachao.notifications.gcm.utils;

import android.app.Activity;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;

import com.app.Billbachao.notifications.gcm.db.NotificationContentProvider;
import com.app.Billbachao.utils.ILog;

/**
 * InBox Helper create to register/unregister and fetchCount of inbox
 * Created by nitesh on 13-04-2016.
 */
public class InBoxNotificationHelper {

    private static final String TAG = InBoxNotificationHelper.class.getName();
    private final Activity mActivity;
    private NotificationObserver notificationObserver;

    public InBoxNotificationHelper(Activity mActivity) {
        this.mActivity = mActivity;
    }

    /**
     * Register InBox notification
     */
    public void registerInBoxObserver() {
        notificationObserver = new NotificationObserver(new Handler());
        register(notificationObserver);
    }

    public void unRegisterInBoxObserver() {
        unRegister(notificationObserver);
    }

    private void register(NotificationObserver contentObserver) {

        if (contentObserver == null || mActivity == null)
            return;

        ILog.d(TAG, "|register|");
        mActivity.getContentResolver().registerContentObserver(NotificationContentProvider.CONTENT_URI, false, contentObserver);
    }

    /**
     * Unregister the notificationObserver
     * @param mNotificationObserver
     */
    private void unRegister(NotificationObserver mNotificationObserver) {

        if (mNotificationObserver == null || mActivity == null)
            return;

        ILog.d(TAG, "|unRegister|");
        mActivity.getContentResolver().unregisterContentObserver(mNotificationObserver);
    }

    /**
     * Notification ContentObserver
     */
    public class NotificationObserver extends ContentObserver {

        private final String TAG = NotificationObserver.class.getName();

        public NotificationObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            ILog.d(TAG, "|onChange called without uri|");
            onChange(selfChange);
        }

        @Override
        public void onChange(boolean selfChange) {
            ILog.d(TAG, "|onChange called|");
            mActivity.invalidateOptionsMenu();
        }
    }
}
