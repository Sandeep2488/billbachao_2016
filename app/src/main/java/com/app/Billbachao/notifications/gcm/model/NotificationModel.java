package com.app.Billbachao.notifications.gcm.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Notification Model to be used in NotificationInBoxActivity/Insert/Delete/Update o notification.
 * Created by nitesh on 27-04-2016.
 */
public class NotificationModel implements Parcelable {

    public String gcm_not_id;

    public String gcm_title;

    public String gcm_description;

    public String gcm_deep_link;

    public String gcm_image_url;

    public String gcm_source;

    public String gcm_response_callback;

    public String gcm_expiry;

    public String gcm_is_read;

    public String gcm_time_ms;

    public String _id;

    public String gcm_category;

    public String gcm_api_url;

    public String gcm_is_silent;

    public NotificationModel(String notId, String gcm_title, String gcm_description, String gcm_deep_link, String gcm_image_url, String gcm_source, String gcm_response_callback, String gcm_expiry, String gcm_is_read, String gcm_time_ms, String gcm_category, String gcm_api_url, String gcm_is_silent) {
        this.gcm_not_id = notId;
        this.gcm_title = gcm_title;
        this.gcm_description = gcm_description;
        this.gcm_deep_link = gcm_deep_link;
        this.gcm_image_url = gcm_image_url;
        this.gcm_source = gcm_source;
        this.gcm_response_callback = gcm_response_callback;
        this.gcm_expiry = gcm_expiry;
        this.gcm_is_read = gcm_is_read;
        this.gcm_time_ms = gcm_time_ms;
        this.gcm_category = gcm_category;
        this.gcm_api_url = gcm_api_url;
        this.gcm_is_silent = gcm_is_silent;
    }

    protected NotificationModel(Parcel in) {
        gcm_not_id = in.readString();
        gcm_title = in.readString();
        gcm_description = in.readString();
        gcm_deep_link = in.readString();
        gcm_image_url = in.readString();
        gcm_source = in.readString();
        gcm_response_callback = in.readString();
        gcm_expiry = in.readString();
        gcm_is_read = in.readString();
        gcm_time_ms = in.readString();
        _id = in.readString();
        gcm_category = in.readString();
        gcm_api_url = in.readString();
        gcm_is_silent = in.readString();
    }

    public static final Creator<NotificationModel> CREATOR = new Creator<NotificationModel>() {
        @Override
        public NotificationModel createFromParcel(Parcel in) {
            return new NotificationModel(in);
        }

        @Override
        public NotificationModel[] newArray(int size) {
            return new NotificationModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(gcm_not_id);
        dest.writeString(gcm_title);
        dest.writeString(gcm_description);
        dest.writeString(gcm_deep_link);
        dest.writeString(gcm_image_url);
        dest.writeString(gcm_source);
        dest.writeString(gcm_response_callback);
        dest.writeString(gcm_expiry);
        dest.writeString(gcm_is_read);
        dest.writeString(gcm_time_ms);
        dest.writeString(_id);
        dest.writeString(gcm_category);
        dest.writeString(gcm_api_url);
        dest.writeString(gcm_is_silent);
    }

    public interface COLUMN_NAMES {

        String _id = "_id";

        String gcm_title = "gcm_title";

        String gcm_description = "gcm_description";

        String gcm_deep_link = "gcm_deep_link";

        String gcm_image_url = "gcm_image_url";

        String gcm_source = "gcm_source";

        String gcm_response_callback = "gcm_response_callback";

        String gcm_expiry = "gcm_expiry";

        //unead = 0 & read = 1;
        String gcm_is_read = "gcm_is_read";

        String gcm_time_ms = "gcm_time_ms";

        String gcm_not_id = "gcm_not_id";

        String gcm_category = "gcm_category";

        String gcm_api_url = "api_url";

        //silent 1 or else 0.
        String gcm_is_silent = "gcm_is_silent";
    }
}
