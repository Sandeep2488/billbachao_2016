package com.app.Billbachao.notifications.gcm.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.app.Billbachao.notifications.gcm.model.NotificationModel;

/**
 * Notification Database Helper
 * Created by nitesh on 27-04-2016.
 */
public class NotificationDbHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "notification.db";

    // Added isSilent and apiUrl columns in v2
    private static final int DATABASE_VERSION = 2;

    public static final String TABLE_NAME = "inbox";

    public NotificationDbHelper(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_COMMAND);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion == 1 && newVersion > 1) {
            db.execSQL(ADD_COLUMN_GCM_API_QUERY);
            db.execSQL(ADD_COLUMN_IS_SILENT_QUERY);
        }
    }

    //Create table
    public static final String CREATE_COMMAND = "create table " + TABLE_NAME
            + "(" + NotificationModel.COLUMN_NAMES._id + " integer primary key autoincrement  , "
            + NotificationModel.COLUMN_NAMES.gcm_deep_link + " text, "
            + NotificationModel.COLUMN_NAMES.gcm_description + " text, "
            + NotificationModel.COLUMN_NAMES.gcm_title + " text, "
            + NotificationModel.COLUMN_NAMES.gcm_expiry + " text, "
            + NotificationModel.COLUMN_NAMES.gcm_source + " text, "
            + NotificationModel.COLUMN_NAMES.gcm_image_url + " text, "
            + NotificationModel.COLUMN_NAMES.gcm_response_callback + " text, "
            + NotificationModel.COLUMN_NAMES.gcm_is_read + " text , "
            + NotificationModel.COLUMN_NAMES.gcm_not_id + " text , "
            + NotificationModel.COLUMN_NAMES.gcm_category + " text , "
            + NotificationModel.COLUMN_NAMES.gcm_api_url + " text , "
            + NotificationModel.COLUMN_NAMES.gcm_is_silent + " text , "
            + NotificationModel.COLUMN_NAMES.gcm_time_ms + " text );";

    public static final String ADD_COLUMN_GCM_API_QUERY = "alter table " + TABLE_NAME + " add " +
            "column " + NotificationModel.COLUMN_NAMES.gcm_api_url + " text";

    public static final String ADD_COLUMN_IS_SILENT_QUERY = "alter table " + TABLE_NAME + " add " +
            "column " + NotificationModel.COLUMN_NAMES.gcm_is_silent + " text";


    public static final String DELETE_CMD = "DROP TABLE IF EXISTS " + TABLE_NAME;

    public static ContentValues getContentValues(NotificationModel model) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(NotificationModel.COLUMN_NAMES.gcm_deep_link, model.gcm_deep_link);
        contentValues.put(NotificationModel.COLUMN_NAMES.gcm_description, model.gcm_description);
        contentValues.put(NotificationModel.COLUMN_NAMES.gcm_title, model.gcm_title);
        contentValues.put(NotificationModel.COLUMN_NAMES.gcm_expiry, model.gcm_expiry);
        contentValues.put(NotificationModel.COLUMN_NAMES.gcm_source, model.gcm_source);
        contentValues.put(NotificationModel.COLUMN_NAMES.gcm_response_callback, model.gcm_response_callback);
        contentValues.put(NotificationModel.COLUMN_NAMES.gcm_is_read, model.gcm_is_read);
        contentValues.put(NotificationModel.COLUMN_NAMES.gcm_time_ms, model.gcm_time_ms);
        contentValues.put(NotificationModel.COLUMN_NAMES.gcm_image_url, model.gcm_image_url);
        contentValues.put(NotificationModel.COLUMN_NAMES.gcm_not_id, model.gcm_not_id);
        contentValues.put(NotificationModel.COLUMN_NAMES.gcm_category, model.gcm_category);
        contentValues.put(NotificationModel.COLUMN_NAMES.gcm_api_url, model.gcm_api_url);
        contentValues.put(NotificationModel.COLUMN_NAMES.gcm_is_silent, model.gcm_is_silent);

        return contentValues;
    }
}
