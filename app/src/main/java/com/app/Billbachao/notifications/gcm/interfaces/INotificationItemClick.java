package com.app.Billbachao.notifications.gcm.interfaces;

import com.app.Billbachao.notifications.gcm.model.NotificationModel;

/**
 * Created by nitesh on 02-05-2016.
 */
public interface INotificationItemClick {
    void notificationClick(int position, NotificationModel model);
}
