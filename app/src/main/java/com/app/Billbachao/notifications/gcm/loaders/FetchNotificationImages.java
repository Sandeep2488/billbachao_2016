package com.app.Billbachao.notifications.gcm.loaders;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;

import com.app.Billbachao.notifications.gcm.model.NotificationModel;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Asyntask to fetch the image of the notification.
 * Created by nitesh on 02-05-2016.
 */

public class FetchNotificationImages extends AsyncTask<String, Void, Bitmap> {

    private final INotifyBitmap iCreateNotification;
    private final Bundle extras;
    private final NotificationModel model;


    public FetchNotificationImages(NotificationModel model, Bundle extras, INotifyBitmap iCreateNotification) {
        this.extras = extras;
        this.model = model;
        this.iCreateNotification = iCreateNotification;
    }

    @Override
    protected Bitmap doInBackground(String... params) {

        InputStream inputStream;
        try {

            String url = params[0];

            if (!TextUtils.isEmpty(url) && (url.startsWith("http") || url.startsWith("https"))) {

                URL fetchUrl = new URL(url);
                HttpURLConnection connection = (HttpURLConnection) fetchUrl.openConnection();
                connection.setDoInput(true);
                connection.connect();
                inputStream = connection.getInputStream();
                return BitmapFactory.decodeStream(inputStream);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

    }

    @Override
    protected void onPostExecute(Bitmap result) {
        super.onPostExecute(result);
        iCreateNotification.notifyBitmap(result, extras, model);
    }


    public interface INotifyBitmap {
        void notifyBitmap(Bitmap bitmap, Bundle extras, NotificationModel model);
    }
}
