package com.app.Billbachao.notifications.gcm.interfaces;

/**
 * Created by nitesh on 05-05-2016.
 */
public interface IClevertapConstant {
    String TITLE = "nt";
    String DESCRIPTION = "nm";
    String IMAGE_URL = "wzrk_bp";
    String DEEP_LINK = "wzrk_dl";
    String GCM_EXIPIRY = "gcm_expiry";
}
