package com.app.Billbachao.notifications.gcm.utils;

/**
 * Created by nitesh on 26-04-2016.
 */
public class NotificationConstant {

    //If present indicated that notification is from clevertap.
    public static final String IS_CLEVERTAP_SOURCE = "wzrk_pn";

    //If present indicates that notification is from BB.
    public static final String IS_BB_SOURCE = "gcm_source";

    //Indicates the source name of BB Notication
    public static final String BB_SOURCE = "bb";

    //Indicates the title of BB notification.
    public static final String BB_GCM_TITLE = "gcm_title";

    //Indicates the Description of BB notification.
    public static final String BB_GCM_DESC = "gcm_description";

    //Indicates the DeepLink of BB notification.
    public static final String BB_GCM_DEEP_LINK = "gcm_deep_link";

    //Indicates the ImageUrl of BB notification.
    public static final String BB_GCM_IMAGE_URL = "gcm_image_url";

    //Indicates if the notification is silent one.
    public static final String BB_GCM_IS_SILENT = "gcm_is_silent";

    //Indicate if the notifcation needs a callback on click.
    public static final String BB_GCM_RESPONSE_CALLBACK = "gcm_response_callback";

    //Notification id.
    public static final String BB_NOTICATION_ID = "notification_id";

    //expiry
    public static final String BB_GCM_EXPIRY = "gcm_expiry";

    //gcm category
    public static final String BB_GCM_CATEGORY = "gcm_category";

    //gcm api url.
    public static final String BB_GCM_API_URL = "api_url";

    //key for notification model used in dashboard to show deeplink.
    public static final String KEY_NOTIFICATION_MODEL = "key_notification_model";

    //Notification UnRead
    public static final String UNREAD = "0";

    //Notification Read
    public static final String READ = "1";

    //Send the callback for the data.
    public static final String SEND_RESPONSE_CALLBACK = "1";

    //Notification Read
    public static final String NOTIFICATION_DATE = "yyyy-MM-dd";
    public static final String NOTIFICATION_EXPIRY_DATE = "yyyy-MM-dd HH:mm:ss";

    public static final String TAG_GCM_STATUS = "gcm_notification_status";

    public static final String BB_DEFAULT_EXPIRY = "NA";

    public static final String SILENT = "1";
    public static final String NOT_SILENT = "0";

}
