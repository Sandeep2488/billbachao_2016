package com.app.Billbachao.notifications.gcm.loaders;

import android.content.Context;
import android.os.AsyncTask;

import com.app.Billbachao.notifications.gcm.db.NotificationDataSource;
import com.app.Billbachao.utils.ILog;

/**
 * NotificationLoader to update the read count.
 * Created by nitesh 27-04-2016.
 */
public class NotificationUpdateAsynctask extends AsyncTask<String, Void, Integer> {

    private static final String TAG = NotificationUpdateAsynctask.class.getSimpleName();
    private final Context context;

    public NotificationUpdateAsynctask(Context context) {
        this.context = context;
    }

    @Override
    protected Integer doInBackground(String... params) {
        return new NotificationDataSource(context).update(params[0]);
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
        ILog.d(TAG, "|Notification Update Asyntask |" + integer);
    }
}
